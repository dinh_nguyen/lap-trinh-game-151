import math
import copy
from Point import Point3
from Point import Point2

class Vector3(object):

    __MATH_FLOAT_SMALL = 10.0**(-37)

    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __iadd__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        self.x += other.x
        self.y += other.y
        self.z += other.z

        return self

    def __add__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        return Vector3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __isub__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z

        return self

    def __sub__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        return Vector3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __imul__(self, other):
        value = float(other)
        self.x *= value
        self.y *= value
        self.z *= value

        return self

    def __mul__(self, other):
        if isinstance(other, Vector3):
            return self.x*other.x + self.y*other.y + self.z*other.z
        else:
            value = float(other)
            return Vector3(self.x * value, self.y * value, self.z * value)

    # def __mod__(self, other):
    #     return self.vectorProduct(other)
    #
    # def __imod__(self, other):
    #     return self.vectorProduct(other)


    # Dinh's add static method
    @staticmethod
    def initFromPoints(p1, p2):
        assert (isinstance(p1, Point3) and isinstance(p2,Point3)), 'p1 and p2 must be Point3 instance'
        pTmp = p2-p1
        return Vector3(pTmp.x, pTmp.y, pTmp.z)

    # Return the angle (in radians) between  2 vectors
    @staticmethod
    def getAngle(v1, v2):
        assert (isinstance(v1, Vector3) and isinstance(v2,Vector3)), 'v1 and v2 must be Vector3 instance'
        dx = float(v1.x * v2.z - v1.z * v2.y);
        dy = float(v1.z * v2.x - v1.x * v2.z);
        dz = float(v1.x * v2.y - v1.y * v2.x);

        return math.atan2( math.sqrt(dx*dx + dy*dy + dz*dz) + Vector3.__MATH_FLOAT_SMALL, Vector3.scalarProduct(v1,v2) )

    @staticmethod
    def scalarProduct(v1,v2):
        assert (isinstance(v1, Vector3) and isinstance(v2, Vector3)), 'Vector3 instance required'
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z

    @staticmethod
    def componentProduct(v1, v2):
        assert (isinstance(v1, Vector3) and isinstance(v2, Vector3)), 'Vector3 instance required'
        return Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)

    #################################################################################################################

    def setWithValue(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def setWithVector(self,other):
        assert isinstance(other,Vector3), 'other must be Vector3 instance'
        self.setWithValue(other.x,other.y,other.z)

    def getAngle(self, v2):
        assert isinstance(v2,Vector3), 'v1 and v2 must be Vector3 instance'
        dx = float(self.x * v2.z - self.z * v2.y);
        dy = float(self.z * v2.x - self.x * v2.z);
        dz = float(self.x * v2.y - self.y * v2.x);

        return math.atan2( math.sqrt(dx*dx + dy*dy + dz*dz) + Vector3.__MATH_FLOAT_SMALL, Vector3.scalarProduct(self,v2) )

    def componentProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return Vector3(self.x * vector.x, self.y * vector.y, self.z * vector.z)

    def componentProductUpdate(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        self.x *= vector.x
        self.y *= vector.y
        self.z *= vector.z

    def crossProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return Vector3(self.y * vector.z - self.z * vector.y,
                       self.z * vector.x - self.x * vector.z,
                       self.x * vector.y - self.y * vector.x)

    def scalarProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return self.x * vector.x + self.y * vector.y + self.z * vector.z

    def addScaledVector(self, vector, scale):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        s = float(scale)
        self.x += vector.x * s
        self.y += vector.y * s
        self.z += vector.z * s

    def magnitude(self):
        return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)

    def squareMagnitude(self):
        return self.x*self.x + self.y*self.y + self.z*self.z

    def trim(self, size):
        if self.squareMagnitude() > size*size:
            self.normalize()
            self.x *= size
            self.y *= size

    def normalize(self):
        l = self.magnitude()
        if l>0:
            self.x *= (float(1)/l)
            self.y *= (float(1)/l)
            self.z *= (float(1)/l)

    def getNormalized(self):
        tmp = copy.deepcopy(self)
        tmp.normalize()
        return tmp

    def equal(self, other):
        if isinstance(other, Vector3):
            return self.x == other.x and self.y == other.y and self.z == other.z
        else:
            return False

    def clear(self):
        self.x = self.y = self.z = 0.0

    def flip(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z

    def toTuple(self):
        return (self.x, self.y, self.z)

    def toList(self):
        return [self.x, self.y, self.z]

    # Dinh's add
    def display(self):
        print(self.toTuple())

class Vector2(Vector3):

    __FLT_EPSILON      = 1.192092896 * ( 10 ** (-7) )

    def __init__(self, x = 0.0, y = 0.0):
        super(Vector2, self).__init__(x, y, 0.0)

    # Return the angle (in radians) between  2 vectors
    @staticmethod
    def getAngle(v1, v2):
        unitV1 = v1.getNormalized()
        unitV2 = v2.getNormalized()
        angle  = math.atan2(Vector2.crossProduct(unitV1,unitV2),Vector3.scalarProduct(unitV1,unitV2))
        if math.fabs(angle) < Vector2.__FLT_EPSILON : return 0.0
        else: return angle

    # # Rotate this vector by angle (radians) around the given point
    # def rotate(self,point, angle):
    #     assert isinstance(point, Point2), 'Point2 instance required'
    #     sinAngle = math.sin(angle)
    #     cosAngle = math.cos(angle)
    #     if ( point.equal(Point2(0,0))):
    #         tmpX = self.x * cosAngle - self.y * sinAngle
    #         self.y = self.y * cosAngle + self.x * sinAngle
    #         self.x = tmpX
    #     else:
    #         tmpX = self.x - point.x
    #         tmpY = self.y - point.y
    #
    #         x = tmpX * cosAngle - tmpY * sinAngle + point.x
    #         y = tmpY * cosAngle + tmpX * sinAngle + point.y

    # In Vector2, x and y after crossed = 0 => cross value = z
    def crossProduct(self, vector):
        vTmp = super(Vector2,self).crossProduct(vector)
        return float(vTmp.z)

    def getPerp(self):
        return Vector2( -self.y, self.x )

    def toTuple(self):
        return (self.x, self.y)

    def toList(self):
        return [self.x, self.y]




