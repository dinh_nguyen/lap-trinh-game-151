import math
import copy

class Point3(object):

    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __sub__(self, other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __isub__(self, other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3(self.x - other.x, self.y - other.y, self.z - other.z)

    def setWithValue(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def setWithPoint(self,other):
        assert isinstance(other,Point3), 'other must be Point3 instance'
        self.setWithValue(other.x,other.y,other.z)

    def equal(self, other):
        if isinstance(other, Point3):
            return self.x == other.x and self.y == other.y and self.z == other.z
        else:
            return False

    def clear(self):
        self.x = self.y = self.z = 0.0

    def flip(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z

    def toTuple(self):
        return (self.x, self.y, self.z)

    def toList(self):
        return [self.x, self.y, self.z]

    def display(self):
        print(self.toTuple())

    def getMidPoint(self,other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3( (self.x + other.x) / 2.0,(self.y + other.y) / 2.0,(self.z + other.z) / 2.0)

    @staticmethod
    def getMidPoint(p1,p2):
        assert (isinstance(p1, Point3) and isinstance(p2, Point3)), 'Point3 instance required'
        return Point3( (p1.x + p2.x) / 2.0,(p1.y + p2.y) / 2.0,(p1.z + p2.z) / 2.0)

class Point2(Point3):
    def __init__(self, x = 0.0, y = 0.0):
        super(Point2, self).__init__(x, y, 0.0)

    def toTuple(self):
        return (self.x, self.y)

    def toList(self):
        return [self.x, self.y]