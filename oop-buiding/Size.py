class Size(object):
    def __init__(self,w=0.0,h=0.0):
        self.width  = float(w)
        self.height = float(h)

    def __iadd__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        self.width  += other.width
        self.height += other.height

        return self

    def __add__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        return Size(self.width + other.width, self.height + other.height)

    def __isub__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        self.width  -= other.width
        self.height -= other.height

        return self

    def __sub__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        return Size(self.width - other.width, self.height - other.height)

    def __imul__(self, other):
        value = float(other)
        self.width *= value
        self.height *= value

        return self

    def __mul__(self, other):
        value = float(other)
        return Size(self.width * value, self.height * value)

    def setSizeValue(self,w=0.0,h=0.0):
        self.width  = float(w)
        self.height = float(h)

    def setSize(self,otherSize):
        assert isinstance(otherSize, Size), 'Size instance required'
        self.setSizeValue(otherSize.width,otherSize.height)

    def setWidth(self,w):
        self.width  = float(w)

    def setHeight(self,h):
        self.height = float(h)

    def equal(self, otherSize):
        if isinstance(otherSize, Size):
            return self.width == otherSize.width and self.height == otherSize.height
        else:
            return False

    def clear(self):
        self.width = self.height = 0.0

    def toTuple(self):
        return (self.width,self.height)

    def toList(self):
        return [self.width,self.height]

    def display(self):
        print(self.toTuple())
