#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      minhky
#
# Created:     31/08/2015
# Copyright:   (c) minhky 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pygame, sys
import pygame.font
from pygame.locals import *
from Point import Point2
from Point import Point3
from Vector import Vector2
from Vector import Vector3
from Size import *

class _Constant:
    FPS = 60

    # Application
    APP_NAME = 'Ass1'
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600

    # Anchor
    ANCHOR_TOPLEFT     = Point2(0, 0)
    ANCHOR_MIDTOP      = Point2(0.5, 0)
    ANCHOR_TOPRIGHT    = Point2(1, 0)
    ANCHOR_MIDLEFT     = Point2(0, 0.5)
    ANCHOR_MIDDLE      = Point2(0.5, 0.5)
    ANCHOR_MIDRIGHT    = Point2(1, 0.5)
    ANCHOR_BOTTOMLEFT  = Point2(0, 1)
    ANCHOR_MIDBOTTOM   = Point2(0.5, 1)
    ANCHOR_BOTTOMRIGHT = Point2(1, 1)
    ANCHOR = (ANCHOR_TOPLEFT, ANCHOR_MIDTOP, ANCHOR_TOPRIGHT, ANCHOR_MIDLEFT, ANCHOR_MIDDLE, ANCHOR_MIDRIGHT, ANCHOR_BOTTOMLEFT, ANCHOR_MIDBOTTOM, ANCHOR_BOTTOMRIGHT)

    # Label Font Style
    FONT_STYLE_NORMAL      = 'font_style_normal'
    FONT_STYLE_BOLD        = 'font_style_bold'
    FONT_STYLE_ITALIC      = 'font_style_italic'
    FONT_STYLE_BOLD_ITALIC = 'font_style_bold_italic'
    FONT_STYLE = (FONT_STYLE_BOLD, FONT_STYLE_BOLD_ITALIC, FONT_STYLE_ITALIC, FONT_STYLE_NORMAL)

    # Label Alignment
    ALIGN_X_LEFT   = 'align_x_left'
    ALIGN_X_MID    = 'align_x_mid'
    ALIGN_X_RIGHT  = 'align_x_right'
    ALIGN_Y_TOP    = 'align_y_top'
    ALIGN_Y_MID    = 'align_y_mid'
    ALIGN_Y_BOTTOM = 'align_y_bottom'
    ALIGN_X = (ALIGN_X_LEFT, ALIGN_X_MID, ALIGN_X_RIGHT)
    ALIGN_Y = (ALIGN_Y_BOTTOM, ALIGN_Y_MID, ALIGN_Y_TOP)

    #Button State
    BUTTON_NORMAL = 'button_normal'
    BUTTON_HIGHLIGHT = 'button_highlight'
    BUTTON_PRESSED = 'button_pressed'
    BUTTON_DISABLE = 'button_disable'
    BUTTON_STATE = (BUTTON_NORMAL, BUTTON_HIGHLIGHT, BUTTON_PRESSED, BUTTON_DISABLE)

    #Mouse State
    MOUSE_NORMAL = 'mouse_normal'
    MOUSE_DOWN = 'mouse_down'
    MOUSE_UP = 'mouse_up'

    # Resource
    BG_JPG_LINK = 'res/BG.jpg'
    CHECK_PNG_LINK = 'res/check.png'

class Time(object):

    @staticmethod
    def Init():
        Time.deltaTime = 0
        Time.unscaledDeltaTime = 0
        Time.FPSCLOCK = pygame.time.Clock()
        Time.scaleFactor = 1

    @staticmethod
    def Update():
        Time.unscaledDeltaTime += Time.FPSCLOCK.tick(_Constant.FPS)
        Time.deltaTime += Time.unscaledDeltaTime * Time.scaleFactor

class Screen(object):

    @staticmethod
    def Init():
        Screen.displaySurf = pygame.display.set_mode((_Constant.WINDOWWIDTH, _Constant.WINDOWHEIGHT))

class Scene:

    def __init__(self):
        self.layerManager = LayerManager()
        self.gameObject = GameObject()
        self.gameObject.transform.SetSizeValue(_Constant.WINDOWWIDTH, _Constant.WINDOWHEIGHT)
        self.gameObject.scene = self

    def addGameObject(self, gameObject):
        assert isinstance(gameObject, GameObject), 'GameObject Instance needed'
        self.gameObject.AddChild(gameObject)

    def update(self):
        self.gameObject.Update()

        self.layerManager.draw()

class LayerManager:
    layers = {'Default' : {0 : []}}

    def addLayer(self, obj, name, order):
        if name not in self.layers:
            raise KeyError("Layer {} is not defined.".format(key))
        listObj = self.layers[name]
        if order not in listObj:
            listObj[order] = [obj]
        else:
            listObj[order].extend([obj])

    def draw(self):
        for (key, listOrder) in self.layers.iteritems():
            for (key, listObj) in listOrder.iteritems():
                for obj in listObj:
                    obj.GetDrawableComponent().Draw()

class InputManager(object):
    mouses = (0, 0)
    mouseClick = _Constant.MOUSE_NORMAL
    key = None

    @staticmethod
    def update(event):
        InputManager.key = None
        InputManager.mouseClick = _Constant.MOUSE_NORMAL
        InputManager.mouses = pygame.mouse.get_pos()
        for ev in event:
            if ev.type == pygame.KEYDOWN:
                InputManager.key = ev.key
            elif ev.type == pygame.MOUSEBUTTONDOWN:
                InputManager.mouseClick = _Constant.MOUSE_DOWN
                InputManager.mouses = ev.pos
            elif ev.type == pygame.MOUSEBUTTONUP:
                InputManager.mouseClick = _Constant.MOUSE_UP

class GameObject(object):

    def __init__(self, scene = None):
        self.transform = Transform(self)
        self.components = [self.transform]
        self.enable = True
        self.scene = None
        self.parent = None
        self.childs = []
        if scene != None:
            scene.addGameObject(self)

    def Update(self):
        for component in self.components:
            component.Update()

        for child in self.childs:
            child.Update()

    # Can change from this parent to parent ?? Shold I implement this
    def SetParent(self, gameObject):
        assert isinstance(gameObject, GameObject), 'GameObject Instance Needed'
        assert self.parent == None, 'GameObject Aldready Has Parent'

        self.parent = gameObject
        self.parent.childs.append(self)
        self.AddScene(self.parent.scene)

        for component in self.components:
            component.OnSetParent()

        for child in self.childs:
            child.OnParentAddParent()

    def AddChild(self, gameObject):
        gameObject.SetParent(self)

    def OnParentAddParent(self):
        self.AddScene(self.parent.scene)

    # Can change from this scene to another ?? Shold I implement this
    def AddScene(self, scene):
        assert self.scene == None, 'This GameObject adlready attack to a Scene'
        if scene != None:
            self.scene = scene

        for component in self.components:
                component.OnAddScene()

    def AddComponent(self, componentType):
        assert issubclass(componentType, Component), 'Component Type Needed'
        if componentType.isDrawable == True:
            assert self.GetDrawableComponent() == None, 'Only one instance of Drawable Component per GameObject'
        newComponent = componentType(self)
        self.components.append(newComponent)
        return newComponent

    def GetComponent(self, componentType):
        assert issubclass(componentType, Component), 'Component Type Needed'
        for component in self.components:
            if isinstance(component, componentType):
                return component
        return None

    def GetComponents(self, componentType):
        assert issubclass(componentType, Component), 'Component Type Needed'
        results = []
        for component in self.components:
            if isinstance(component, componentType):
                results.append(component)
        return results

    def GetDrawableComponent(self):
        for component in self.components:
            if component.isDrawable == True:
                return component
        return None

class Component(object):

    def __init__(self, gameObject):
        self.gameObject = gameObject
        self.isDrawable = False
        self.enable = True

    def Update(self):
        pass

    def OnAddScene(self):
        pass

    def OnSetParent(self):
        pass

class Transform(Component):
    # anchorPosiion, offset from anchor in Pixel
    # size, real Size
    # deltaSize, calculate Size with Scale
    # anchor, anchor Point
    # pivot
    # scale
    # tmpRect, just use for calculate when gameObject dont't have Parent
    # position, World Position, never touch this property
    # localPosition, Relative Position of gameObject ot it's Parent, None if don't have Parent
    # Remember to call UpdateLocalPosition after gameObject set Parent
    def __init__(self, gameObject):
        super(Transform, self).__init__(gameObject)
        self.anchor = _Constant.ANCHOR_MIDDLE
        self.anchorPosition = Point2(0, 0)
        self.size = Size(100, 100)
        self.deltaSize = Size(100, 100)
        self.pivot = Point2(0.5, 0.5)
        self.scale = Point2(1, 1)
        self.rotate = 0
        self.position = None
        self.parentRect = pygame.Rect(0, 0, _Constant.WINDOWWIDTH, _Constant.WINDOWHEIGHT)
        self.localPosition = None
        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    # Calculate Local Position, this.rect.topleft - parent.position
    def UpdateLocalPosition(self):
        offsetAnchorPoint = Point2(self.parentRect.width * self.anchor.x, self.parentRect.height * self.anchor.y)
        offsetPivot = Point2 (self.deltaSize.width*self.pivot.x, self.deltaSize.height*self.pivot.y)

        self.localPosition = Point2 (offsetAnchorPoint.x - offsetPivot.x + self.anchorPosition.x, offsetAnchorPoint.y - offsetPivot.y + self.anchorPosition.y)

    def UpdateWorldPosition(self):
        self.position = Point2(self.parentRect.left + self.localPosition.x, self.parentRect.top + self.localPosition.y)
        self.rect = pygame.Rect(self.position.toTuple(), self.deltaSize.toTuple())

    def UpdateDeltaSize(self):
        self.deltaSize.width = self.size.width * self.scale.x
        self.deltaSize.height = self.size.height * self.scale.y

        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    def GetRect(self):
        return self.rect

    def SetAnchorPosition(self, x, y):
        if self.anchorPosition.x == x and self.anchorPosition.y == y:
            return
        if x != None:
            self.anchorPosition.x = x
        if y != None:
            self.anchorPosition.y = y

        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    def SetAnchorPoint(self, anchorPoint):
        if self.anchor.equal(anchorPoint):
            return
        self.anchorPosition.x = (self.anchor.x - anchorPoint.x)*self.parentRect.width
        self.anchorPosition.y = (self.anchor.y - anchorPoint.y)*self.parentRect.height
        self.anchor.setWithPoint(anchorPoint)

        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    def SetSizeValue(self, sizeWidth, sizeHeight):
        if self.size.width == sizeWidth and self.size.height == sizeHeight:
            return
        if sizeWidth != None:
            self.size.width = sizeWidth
        if sizeHeight != None:
            self.size.height = sizeHeight

        self.UpdateDeltaSize()

    def SetSize(self, size):
        self.setSizeValue(size.width,size.heigth)

    def SetPivot(self, pivotX, pivotY):
        if self.pivot.x == pivotX and self.pivot.y == pivotY :
            return
        if pivotX != None:
            self.pivot.x = pivotX
        if pivotY != None:
            self.pivot.y = pivotY

        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    def SetPivotPoint(self, pivotPoint):
        if self.pivot.equal(pivotPoint):
            return
        self.SetPivot(pivotPoint.x,pivotPoint.y)


    def SetScale(self, scaleX, scaleY):
        if self.scale.x == scaleX and self.scale.y == scaleY:
            return
        if scaleX != None:
            self.scale.x = scaleX
        if scaleY != None:
            self.scale.y = scaleY

        self.UpdateDeltaSize()

    def SetScaleX(self, scaleX):
        self.setScale(scaleX,self.scale.y)

    def SetScaleY(self, scaleY):
        self.setScale(self.scale.x,scaleY)

    def SetScalePoint(self, scalePoint):
        self.SetScale(scalePoint.x,scalePoint.y)

    def SetRotate(self, rotate):
        if self.rotate == rotate:
            return;
        self.rotate = rotate
        # Do I need some more task

    def SetLocalPosition(self, localX, localY):
        if self.localPosition.x == localX and self.localPosition.y == localY:
            return
        if localX != None:
            self.anchorPosition.x += localX - self.localPosition.x
            self.localPosition.x = localX
        if localY != None:
            self.anchorPosition.y += localY - self.localPosition.y
            self.localPosition.y = localY

        self.UpdateWorldPosition()

    def SetLocalPositionPoint(self, localPos):
        self.SetLocalPosition(localPos.x,localPos.y)

    def SetPosition(self, posX, posY):
        if self.position.x == posX and self.position.y == posY:
            return
        self.anchorPosition.x += posX - self.position.x
        self.anchorPosition.y += posY - self.position.y

        self.UpdateLocalPosition()
        self.UpdateWorldPosition()

    def SetPositionPoint(self, point):
        self.SetPosition(point.x,point.y)

#prama mark - Override Base Method

    def Update(self):
        if self.gameObject.parent != None:
            self.parentRect = self.gameObject.parent.transform.rect

        self.UpdateWorldPosition()

class Image(Component):
    isDrawable = True

    # Sprite, Surface with image blit
    def __init__(self, gameObject, sprite = None):
        super(Image, self).__init__(gameObject)
        self.isDrawable = True
        self.addToLayer = False
        self.SetSprite(sprite)

    def Update(self):
        if not self.addToLayer:
            self.addToLayer = True
            self.gameObject.scene.layerManager.addLayer(self.gameObject, 'Default', 0)

    def SetLayer(self, layerName, sortOrder):
        if not self.addToLayer:
            self.addToLayer = True
            self.gameObject.scene.layerManager.addLayer(self.gameObject, layerName, sortOrder)

    def Draw(self):
        if self.sprite == None:
            raise TypeError('Sprite Property in Image Component is missing')

        if self.sprite.get_size() != self.gameObject.transform.rect.size:
            self.sprite = pygame.transform.scale(self.sprite, self.gameObject.transform.rect.size)
        Screen.displaySurf.blit(self.sprite, self.gameObject.transform.rect)

    def SetSprite(self, sprite):
        if sprite != None:
            assert isinstance(sprite, pygame.Surface), 'Surface Needed'
            self.sprite = sprite
            self.gameObject.transform.SetSizeValue(sprite.get_width(), sprite.get_height())

class Label(Component):
    # When Set Property, please don't use 3rd parameter

    isDrawable = True

    def __init__(self, gameObject):
        super(Label, self).__init__(gameObject)
        self.isDrawable = True

        self.text = 'Text'

        self.font = None
        self.fontName = None
        self.fontSize = 14
        self.isBold = False
        self.isItalic = False
        self.textColor = (255, 255, 255, 255)
        self.antialiasing = True

        self.alignHoriz = _Constant.ALIGN_X_LEFT
        self.alignVert  = _Constant.ALIGN_Y_TOP

        self.addToLayer = False
        self.listOfSingleLineText = []
        self.multipleLine = False

        self.sprite = None

        self.SetFont(self.fontName, self.fontSize, self.isBold, self.isItalic, self.textColor, self.antialiasing)

    def Update(self):
        if self.addToLayer == False:
            self.addToLayer = True
            self.gameObject.scene.layerManager.addLayer(self.gameObject, 'Default', 0)

    def Draw(self):
        if self.sprite == None:
            raise TypeError('Sprite Property in Label Component is missing')

        Screen.displaySurf.blit(self.sprite, self.gameObject.transform.rect)

    def SetLayer(self, layerName, sortOrder):
        if self.addToLayer == False:
            self.addToLayer = True
            self.gameObject.scene.layerManager.addLayer(self.gameObject, layerName, sortOrder)

    def SetFont(self, fontName, fontSize, isBold, isItalic, textColor, antialiasing, shouldRender = True):
        self.fontName = fontName
        self.fontSize = fontSize
        self.isBold = isBold
        self.isItalic = isItalic
        self.textColor = textColor
        self.antialiasing = antialiasing
        self.font = pygame.font.Font(self.fontName, self.fontSize)
        self.font.set_bold(isBold)
        self.font.set_italic(isItalic)
        if shouldRender:
            self.Render()

    def SetAlignX(self, alignX, shouldRender = True):
        self.SetAlign(self, alignX, self.alignVert, shouldRender)

    def SetAlignY(self, alignY, shouldRender = True):
        self.SetAlign(self, self.alignHoriz, alignY, shouldRender)

    def SetAlign(self, alignX, alignY, shouldRender = True):
        assert alignX in _Constant.ALIGN_X, 'AlignX Wrong Format'
        assert alignY in _Constant.ALIGN_Y, 'AlignY Wrong Format'
        if self.alignHoriz != alignX or self.alignVert != alignY:
            self.alignHoriz = alignX
            self.alignVert  = alignY
            if self.font != None and shouldRender:
                self.Render()

    def SetText(self, text, shouldRender = True):
        assert isinstance(text, basestring), 'String Needed'
        if self.text != text:
            self.text = text

            if "\n" in self.text:
                self.multipleLine = True
            else:
                self.multipleLine = False

            if self.font != None and shouldRender:
                self.Render()

    def Render(self):
        rect = pygame.Rect((0, 0), self.gameObject.transform.rect.size)
        self.sprite = pygame.Surface(rect.size, pygame.SRCALPHA, 32)

        if self.multipleLine == True:
            self.CalculateForMultiple()

            if self.alignVert != _Constant.ALIGN_Y_TOP:
                sumsize = 0
                for line in self.listOfSingleLineText:
                    sumsize += self.font.size(line)[1]

                rect.height -= sumsize
                if self.alignVert == _Constant.ALIGN_Y_MID:
                    rect.top += rect.height/2
                elif self.alignVert == _Constant.ALIGN_Y_BOTTOM:
                    rect.top += rect.height

            for line in self.listOfSingleLineText:
                size = self.font.size(line)

                if size[1] > rect.height:
                    return;
                else:
                    render = self.font.render(line, self.antialiasing, self.textColor).convert_alpha()
                    if size[0] > rect.width:
                        self.sprite.blit(render, rect.topleft, pygame.Rect((0, 0), rect.size))
                    else:
                        render_rect = render.get_rect()
                        # align x
                        if self.alignHoriz == _Constant.ALIGN_X_LEFT:
                            render_rect.left = rect.left
                        elif self.alignHoriz == _Constant.ALIGN_X_MID:
                            render_rect.centerx = rect.centerx
                        else:
                            render_rect.right = rect.right

                        # align y
                        render_rect.top = rect.top

                        self.sprite.blit(render, render_rect)
                    rect.height -= render.get_height()
                    rect.top += render.get_height()
            pass
        else:
            size = self.font.size(self.text)

            if size[1] > rect.height:
                return;
            else:
                render = self.font.render(self.text, self.antialiasing, self.textColor).convert_alpha()
                if size[0] > rect.width:
                    self.sprite.blit(render, (0, 0), pygame.Rect((0, 0), rect.size))
                else:
                    render_rect = render.get_rect()
                    # align x
                    if self.alignHoriz == _Constant.ALIGN_X_LEFT:
                        render_rect.left = rect.left
                    elif self.alignHoriz == _Constant.ALIGN_X_MID:
                        render_rect.centerx = rect.centerx
                    else:
                        render_rect.right = rect.right

                    # align y
                    if self.alignVert == _Constant.ALIGN_Y_TOP:
                        render_rect.top = rect.top
                    elif self.alignVert == _Constant.ALIGN_Y_MID:
                        render_rect.centery = rect.centery
                    else:
                        render_rect.bottom = rect.bottom

                    self.sprite.blit(render, render_rect)

    def CalculateForMultiple(self):
        if self.multipleLine == False:
            return;

        self.listOfSingleLineText = []
        listAfterSplitNewLine = self.text.splitlines()
        desiredWidth = self.gameObject.transform.rect.width
        desiredHeight = self.gameObject.transform.rect.height

        for line in listAfterSplitNewLine:
            if self.font.size(line)[0] > desiredWidth:
                words = line.split(' ')

                # if each word too big for a row, then dont' draw anythin
                for word in words:
                    if self.font.size(word)[0] > desiredWidth:
                        print('Label, Word Too Big for a Row, PLease Increase Width')
                        return;
                accumulated_line = ""
                for word in words:
                    test_line = accumulated_line + word + ' '
                    if self.font.size(test_line)[0] < desiredWidth:
                        accumulated_line = test_line
                    else:
                        self.listOfSingleLineText.append(accumulated_line)
                        accumulated_line = word + ' '
                accumulated_line.strip()
                if accumulated_line != '':
                    self.listOfSingleLineText.append(accumulated_line)
            else:
                self.listOfSingleLineText.append(line)

class Button(Component):
    # Button use Image Component to draw so make sure set Image Sprite for Button Normal State

    isDrawable = False

    def __init__(self, gameObject):
        super(Button, self).__init__(gameObject)
        self.addToLayer = False

        self.spriteForNormal = None
        self.spriteForHightLight = None
        self.spriteForPressed = None
        self.spriteForDisable = None

        self.state = _Constant.BUTTON_NORMAL
        self.label = None

        if self.gameObject.GetComponent(Image) == None:
            self.gameObject.AddComponent(Image)
        self.image = self.gameObject.GetComponent(Image)

    def Update(self):
        if self.addToLayer == False:
            self.addToLayer = True
            self.gameObject.scene.layerManager.addLayer(self.gameObject, 'Default', 0)

        if self.spriteForNormal == None:
            self.spriteForNormal = self.image.sprite

        if self.label != None:
            self.label.gameObject.transform.SetSize(self.gameObject.transform.deltaSize)

        if self.state != _Constant.BUTTON_DISABLE:
            if self.gameObject.transform.rect.collidepoint(InputManager.mouses):
                if InputManager.mouseClick != _Constant.MOUSE_DOWN:
                    self.SetState(_Constant.BUTTON_HIGHLIGHT)
                else:
                    self.SetState(_Constant.BUTTON_PRESSED)
            else:
                self.SetState(_Constant.BUTTON_NORMAL)

    def SetLayer(self, layerName, sortOrder):
        self.image.SetLayer(layerName, sortOrder)

	# Never Use This Method
    def SetState(self, state):
        assert state in _Constant.BUTTON_STATE, 'Button State Wrong Format'
        if self.state != state:
            self.state = state

            if self.state == _Constant.BUTTON_NORMAL and self.spriteForNormal != None:
                self.image.SetSprite(self.spriteForNormal)
            elif self.state == _Constant.BUTTON_HIGHLIGHT and self.spriteForHightLight != None:
                self.image.SetSprite(self.spriteForHightLight)
                for component in self.gameObject.GetComponents(Bahavior):
					component.onButtonHighLight()
            elif self.state == _Constant.BUTTON_PRESSED and self.spriteForPressed != None:
                self.image.SetSprite(self.spriteForPressed)
                for component in self.gameObject.GetComponents(Bahavior):
					component.onButtonPressed()
            elif self.state == _Constant.BUTTON_DISABLE and self.spriteForDisable != None:
                self.image.SetSprite(self.spriteForDisable)
            	for component in self.gameObject.GetComponents(Bahavior):
					component.onButtonDisable()

	def SetInteractable(self, interactable):
		if interactable == True:
			self.SetState(_Constant.BUTTON_NORMAL)
		else:
			self.SetState(_Constant.BUTTON_DISABLE)

    def SetSpriteForHighLight(self, sprite):
        assert isinstance(sprite, pygame.Surface), 'Sprite Needed To Be Read in Pygame Surface'
        self.spriteForHightLight = sprite

    def SetSpriteForPressed(self, sprite):
        assert isinstance(sprite, pygame.Surface), 'Sprite Needed To Be Read in Pygame Surface'
        self.spriteForPressed = sprite

    def SetSpriteForDisable(self, sprite):
        assert isinstance(sprite, pygame.Surface), 'Sprite Needed To Be Read in Pygame Surface'
        self.spriteForDisable = sprite

    def SetText(self, text):
        if self.label == None:
            for child in self.gameObject.childs:
                self.label = child.GetComponent(Label)
                if self.label != None:
                    break

            if self.label == None:
                lbl = GameObject()
                lbl.SetParent(self.gameObject)
                lbl.transform.SetAnchorPoint(_Constant.ANCHOR_TOPLEFT)
                lbl.transform.SetSize(self.gameObject.transform.deltaSize)
                self.label = lbl.AddComponent(Label)

        self.label.SetText(text)

class Bahavior(Component):

    def __init__(self, gameObject):
        super(Bahavior, self).__init__(gameObject)

    def onButtonHighLight(self):
        pass

    def onButtonPressed(self):
        pass

    def onButtonDisable(self):
        pass

def main():
    pygame.init()
    Time.Init()
    Screen.Init()
    pygame.display.set_caption(_Constant.APP_NAME)

    startSceen = Scene()

    lbl = GameObject(startSceen)
    lbl.transform.SetLocalPosition(0,0)
    lbl.AddComponent(Label)
    lblcomponent = lbl.GetComponent(Label)
    lblcomponent.SetText("Mach Chi Da\nNguyen Nhu Dinh\nNguyen Le Duy")
    lblcomponent.SetAlign(_Constant.ALIGN_X_RIGHT, _Constant.ALIGN_Y_BOTTOM)

    while True:
        checkForQuit()
        InputManager.update(pygame.event.get())
        Screen.displaySurf.fill((0, 0, 0))
        startSceen.update()

        # pygame Call
        pygame.display.update()
        Time.Update()

# pragma mark - Global Method
def checkForQuit():
    for event in pygame.event.get(QUIT): # get all the QUIT events
        terminate() # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP): # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate() # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event) # put the other KEYUP event objects back

def terminate():
    pygame.quit()
    sys.exit()

if __name__ == '__main__':
    main()