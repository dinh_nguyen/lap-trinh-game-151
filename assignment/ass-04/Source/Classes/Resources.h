#ifndef	__RESOURCES_H__
#define	__RESOURCES_H__

#define   INDEX_OF_SNOW_PNG                                                     0  
#define   INDEX_OF_ERASBDBM_PNG                                                 1  
#define   INDEX_OF_WILD_WOOD_PNG                                                2  
#define   INDEX_OF_CLOSENORMAL_PNG                                              3  
#define   INDEX_OF_CLOSESELECTED_PNG                                            4  
#define   INDEX_OF_HELLOWORLD_PNG                                               5  
#define   INDEX_OF_INTRO_SPRITE_SHEET_PNG                                       6  
#define   INDEX_OF_MAIN_MENU_SPRITE_SHEET_PNG                                   7  
#define   INDEX_OF_GAME_CONTROLLER_SPRITE_SHEET_PNG                             8  
#define   INDEX_OF_CREDITS_SPRITE_SHEET_PNG                                     9  
#define   INDEX_OF_PLAY_SCENE_SPRITE_SHEET_PNG                                  10 

#define   INDEX_OF_INTRO_SPRITE_SHEET_PLIST                                     0  
#define   INDEX_OF_MAIN_MENU_SPRITE_SHEET_PLIST                                 1  
#define   INDEX_OF_GAME_CONTROLLER_SPRITE_SHEET_PLIST                           2  
#define   INDEX_OF_CREDITS_SPRITE_SHEET_PLIST                                   3  
#define   INDEX_OF_SNOW_PARTICLE_PLIST                                          4  
#define   INDEX_OF_PLAY_SCENE_SPRITE_SHEET_PLIST                                5  

#define   INDEX_OF_BUTTON_CLICK_MP3                                             0  
#define   INDEX_OF_MAIN_MENU_BACKGROUND_MUSIC_MP3                               1  
#define   INDEX_OF_MOVE_CHESS_MP3                                               2  
#define   INDEX_OF_INTRO_BACKGROUND_MUSIC_MP3                                   3  
#define   INDEX_OF_GAMEPLAY_BACKGROUND_MUSIC_MP3                                4  
#define   INDEX_OF_WRONG_MOVE_MP3                                               5  
#define   INDEX_OF_WIN_GAME_MP3                                                 6  
#define   INDEX_OF_LOSE_GAME_MP3                                                7  
#define   INDEX_OF_CHECK_MP3                                                    8  

#define   INDEX_OF_SETTING_ON_WAV                                               0  
#define   INDEX_OF_SETTING_OFF_WAV                                              1  
#define   INDEX_OF_CLICK_WAV                                                    2  
#define   INDEX_OF_SELECT_CHESS_WAV                                             3  
#define   INDEX_OF_KILL_CHESS_WAV                                               4  


#define   INDEX_OF_VPSPHDIH_0_TTF                                               0  
#define   INDEX_OF_VPSPHDIT_0_TTF                                               1  
#define   INDEX_OF_ERASBD_TTF                                                   2  
#define   INDEX_OF_ARIAL_TTF                                                    3  
#define   INDEX_OF_MARKER_FELT_TTF                                              4  


#define   INDEX_OF_ERASBDBM_FNT                                                 0  
#define   INDEX_OF_WILD_WOOD_FNT                                                1  


#define   INDEX_OF_ANIMATION_FLARE_FRAME_NAME                                   0  
#define   INDEX_OF_IMAGE_INTRO_BACKGROUND_FRAME_NAME                            1  
#define   INDEX_OF_IMAGE_CONTINUE_DISABLED_FRAME_NAME                           2  
#define   INDEX_OF_IMAGE_NORMAL_FRAME_NAME                                      3  
#define   INDEX_OF_IMAGE_CONTINUE_FRAME_NAME                                    4  
#define   INDEX_OF_IMAGE_BACK_CLICKED_FRAME_NAME                                5  
#define   INDEX_OF_IMAGE_COMPUTER_CLICKED_FRAME_NAME                            6  
#define   INDEX_OF_IMAGE_BACK_FRAME_NAME                                        7  
#define   INDEX_OF_IMAGE_CREDITS_CLICKED_FRAME_NAME                             8  
#define   INDEX_OF_IMAGE_EXIT_CLICKED_FRAME_NAME                                9  
#define   INDEX_OF_IMAGE_EASY_CLICKED_FRAME_NAME                                10 
#define   INDEX_OF_IMAGE_RED_CLICKED_FRAME_NAME                                 11 
#define   INDEX_OF_IMAGE_COMPUTER_FRAME_NAME                                    12 
#define   INDEX_OF_IMAGE_HARD_CLICKED_FRAME_NAME                                13 
#define   INDEX_OF_IMAGE_BLACK_FRAME_NAME                                       14 
#define   INDEX_OF_IMAGE_TWO_PLAYER_FRAME_NAME                                  15 
#define   INDEX_OF_IMAGE_TWO_PLAYER_CLICKED_FRAME_NAME                          16 
#define   INDEX_OF_IMAGE_NEW_FRAME_NAME                                         17 
#define   INDEX_OF_IMAGE_EASY_FRAME_NAME                                        18 
#define   INDEX_OF_IMAGE_BLACK_CLICKED_FRAME_NAME                               19 
#define   INDEX_OF_IMAGE_HARD_FRAME_NAME                                        20 
#define   INDEX_OF_IMAGE_NORMAL_CLICKED_FRAME_NAME                              21 
#define   INDEX_OF_IMAGE_MAIN_MENU_BACKGROUND_FRAME_NAME                        22 
#define   INDEX_OF_IMAGE_NEW_CLICKED_FRAME_NAME                                 23 
#define   INDEX_OF_IMAGE_CREDITS_FRAME_NAME                                     24 
#define   INDEX_OF_IMAGE_CONTINUE_CLICKED_FRAME_NAME                            25 
#define   INDEX_OF_IMAGE_RED_FRAME_NAME                                         26 
#define   INDEX_OF_IMAGE_EXIT_FRAME_NAME                                        27 
#define   INDEX_OF_IMAGE_MUSIC_OFF_FRAME_NAME                                   28 
#define   INDEX_OF_IMAGE_HOME_FRAME_NAME                                        29 
#define   INDEX_OF_IMAGE_HOME_CLICKED_FRAME_NAME                                30 
#define   INDEX_OF_IMAGE_MUSIC_ON_FRAME_NAME                                    31 
#define   INDEX_OF_IMAGE_SOUND_OFF_FRAME_NAME                                   32 
#define   INDEX_OF_IMAGE_SOUND_ON_FRAME_NAME                                    33 
#define   INDEX_OF_IMAGE_CREDITS_SCREEN_FRAME_NAME                              34 
#define   INDEX_OF_IMAGE_RED_ADVISOR_FRAME_NAME                                 35 
#define   INDEX_OF_IMAGE_RED_HORSE_FRAME_NAME                                   36 
#define   INDEX_OF_IMAGE_RED_KING_FRAME_NAME                                    37 
#define   INDEX_OF_IMAGE_BLACK_ADVISOR_FRAME_NAME                               38 
#define   INDEX_OF_IMAGE_GAMEPLAY_BACKGROUND_FRAME_NAME                         39 
#define   INDEX_OF_IMAGE_BLACK_KING_FRAME_NAME                                  40 
#define   INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME                               41 
#define   INDEX_OF_IMAGE_RED_CAR_FRAME_NAME                                     42 
#define   INDEX_OF_IMAGE_GAMEPLAY_BOARD_FRAME_NAME                              43 
#define   INDEX_OF_IMAGE_BLACK_CAR_FRAME_NAME                                   44 
#define   INDEX_OF_IMAGE_BLACK_ELEPHANT_FRAME_NAME                              45 
#define   INDEX_OF_IMAGE_BLACK_HORSE_FRAME_NAME                                 46 
#define   INDEX_OF_IMAGE_RED_ELEPHANT_FRAME_NAME                                47 
#define   INDEX_OF_IMAGE_CIRCLE_CAN_MOVE_FRAME_NAME                             48 
#define   INDEX_OF_IMAGE_BORDER_PIECE_FRAME_NAME                                49 
#define   INDEX_OF_IMAGE_BLACK_CANON_FRAME_NAME                                 50 
#define   INDEX_OF_IMAGE_PARTICLE_FIRE_FRAME_NAME                               51 
#define   INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME                                 52 
#define   INDEX_OF_IMAGE_RED_CANON_FRAME_NAME                                   53 

const int NUMBER_OF_PNG_FILE                                                    =  11 ;
const int NUMBER_OF_PLIST_FILE                                                  =  6  ;
const int NUMBER_OF_MP3_FILE                                                    =  9  ;
const int NUMBER_OF_WAV_FILE                                                    =  5  ;
const int NUMBER_OF_OGG_FILE                                                    =  0  ;
const int NUMBER_OF_TTF_FILE                                                    =  5  ;
const int NUMBER_OF_OTF_FILE                                                    =  0  ;
const int NUMBER_OF_FNT_FILE                                                    =  2  ;
const int NUMBER_OF_JSON_FILE                                                   =  0  ;

const int NUMBER_OF_OBJECT_IN_PLIST_FILE                                        =  54 ;
const int NUMBER_OF_ANIMATION                                                   =  1  ;

extern const char	*pngResourcesList[]                                               ;
extern const char	*plistResourcesList[]                                             ;
extern const char	*mp3ResourcesList[]                                               ;
extern const char	*wavResourcesList[]                                               ;
extern const char	*oggResourcesList[]                                               ;
extern const char	*ttfResourcesList[]                                               ;
extern const char	*otfResourcesList[]                                               ;
extern const char	*fntResourcesList[]                                               ;
extern const char	*jsonResourcesList[]                                              ;
extern const char	*objectNameInPlistFileList[]                                      ;
extern const int	numberOfFrameAnimationList[]                                      ;
extern const char	*objectInJSONList[]                                               ;
#endif