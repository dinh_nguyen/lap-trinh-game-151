#include "Constant.h"

// This file is definition

// State

// Tags
const int HIGHLIGHTED_NODE                   = 101;
const int HIGHLIGHTED_NODE_CAN_MOVING_TAG    = 151;
const int HIGHLIGHTED_CHESS_CAN_DEFEATED_TAG = 201;

// UI Tags
const int MAIN_MENU_LAYOUT             = 251;
const int SELECT_PLAYING_MODE_LAYOUT   = 252;
const int SELECT_LEVEL_COMPUTER_LAYOUT = 253;

// Transistion scene time
const float TIME_FADE_TRANSITION                      = 1.5f;
const float TIME_JUMP_ZOOM_TRANSITION                 = 0.75f;
const float TIME_DELAY_BEFORE_DOING_TRANSITION_SCENES = 1.0f;

// Key in UserDefault
const char* KEY_SETUP_SOUND          = "key_setup_sound";
const char* KEY_SETUP_MUSIC          = "key_setup_music";
const char* KEY_SETUP_MODE_PLAYING   = "key_setup_mode_playing";
const char* KEY_SETUP_YOUR_PLAYER    = "key_setup_your_player";
const char* KEY_SETUP_LEVEL_COMPUTER = "key_setup_level_computer";

const char* KEY_DATA_CONTINUE_GAME   = "key_load_continue_game";
const char* KEY_CAN_CONTINUE         = "key_can_continue";
const char* KEY_TIMER_RED            = "key_timer_red";
const char* KEY_TIMER_BLACK          = "key_timer_black";
const char* KEY_MODE_GAME            = "key_mode_game";
const char* KEY_SIDE                 = "key_side";;


// GRID
const float GRID_X = 75.0f;
const float GRID_Y = 75.0f;

// BOARD properties
extern const int BOARD_WIDTH = 9;
extern const int BOARD_HEIGHT = 10;

// BOARD
const int startIndexSide[] = { 0, 16 };
const int endIndexSide[] = { 16, 32 };
