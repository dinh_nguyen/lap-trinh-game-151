#include "AudioManager.h"

AudioManager* AudioManager::_pAudioManager = nullptr;
CocosDenshion::SimpleAudioEngine* AudioManager::_pSimpleAudioEngine = nullptr;

AudioManager* AudioManager::getInstance()
{
	AudioManager::init();
	return _pAudioManager;
}
void AudioManager::init()
{
	if (_pAudioManager == nullptr) {
		_pAudioManager = new AudioManager();
	}

	if (_pSimpleAudioEngine == nullptr) {
		_pSimpleAudioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	}
}
void AudioManager::release()
{
	if (_pSimpleAudioEngine != nullptr)
	{
		_pSimpleAudioEngine = nullptr;
	}

	if (_pAudioManager != nullptr)
	{
		CC_SAFE_DELETE(_pAudioManager);
	}
}

void AudioManager::playBackgroundMusic(int id,const char* aType, bool bLoop)
{
	// If don't set to user default, default value is second parameter
	if (UserDefault::getInstance()->getBoolForKey(KEY_SETUP_MUSIC, true) == true)
	{
		if (StringUtils::toString(aType).compare("mp3") == 0) {
			if (id >= 0 && id < NUMBER_OF_MP3_FILE) _pSimpleAudioEngine->playBackgroundMusic(mp3ResourcesList[id], bLoop);
		}

		else if (StringUtils::toString(aType).compare("wav") == 0) {
			if (id >= 0 && id < NUMBER_OF_WAV_FILE) _pSimpleAudioEngine->playBackgroundMusic(wavResourcesList[id], bLoop);
		}
		else return;
	}
}

void AudioManager::playEffect(int id,const char* aType, bool loop,
	float pitch, float pan, float gain)
{
	// If don't set to user default, default value is second parameter
	if (UserDefault::getInstance()->getBoolForKey(KEY_SETUP_SOUND, true) == true)
	{
		std::string aTypeString = StringUtils::toString(aType);
		if (aTypeString.compare("mp3") == 0) {
			if (id >= 0 && id < NUMBER_OF_MP3_FILE) _pSimpleAudioEngine->playEffect(mp3ResourcesList[id], loop, pitch, pan, gain);
		}

		else if (aTypeString.compare("wav") == 0) {
			if (id >= 0 && id < NUMBER_OF_WAV_FILE) _pSimpleAudioEngine->playEffect(wavResourcesList[id], loop, pitch, pan, gain);
		}
	}
}

void AudioManager::stopBackgroundMusic(bool bReleaseData)
{
	_pSimpleAudioEngine->stopBackgroundMusic(bReleaseData);
}

void AudioManager::pauseBackgroundMusic()
{
		_pSimpleAudioEngine->pauseBackgroundMusic();
}
void AudioManager::resumeBackgroundMusic()
{
	if (UserDefault::getInstance()->getBoolForKey(KEY_SETUP_MUSIC, true) == true)
	{
		_pSimpleAudioEngine->resumeBackgroundMusic();
	}
}

bool AudioManager::isBackgroundMusicPlaying()
{
	return _pSimpleAudioEngine->isBackgroundMusicPlaying();
}