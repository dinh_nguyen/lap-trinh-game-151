#include "GameplayManager.h"

GameplayManager* GameplayManager::_pGameplayManager;

GameplayManager* GameplayManager::getInstance()
{
	GameplayManager::init();
	return _pGameplayManager;
}

void GameplayManager::init()
{
	if (_pGameplayManager == nullptr) {
		_pGameplayManager= new GameplayManager();
	}
}

void GameplayManager::release()
{
	if (_pGameplayManager != nullptr) {
		CC_SAFE_DELETE(_pGameplayManager);
	}
}

AI_Mode GameplayManager::getAIMode()
{
	return _AIMode;
}
void GameplayManager::setAIMode(AI_Mode aiMode)
{
	_AIMode = aiMode;
}

Mode GameplayManager::getGameMode()
{
	return _gameMode;
}
void GameplayManager::setGameMode(Mode gameMode)
{
	_gameMode = gameMode;
}

PLAYER GameplayManager::getFirstPlayer()
{
	return _firstPlayer;
}
void GameplayManager::setFirstPlayer(PLAYER player)
{
	_firstPlayer = player;
}

Board* GameplayManager::getBoard()
{
	return _board;
}
void GameplayManager::setBoard(Board* b)
{
	_board = b;
}

void GameplayManager::updateEachTouchBoard(Point pos)
{
	_board->updateTouchEachTurn(pos);
}

void GameplayManager::initNewGame()
{
	if (_board != nullptr)
	{
		_board->setMode(_gameMode);
		_board->setCurrentTurn(_firstPlayer);
		_timerBlack = 1800.0f;
		_timerRed = 1800.0f;
	}
}

bool GameplayManager::isStart()
{
	return _isStart;
}
void GameplayManager::setIsStart(bool isStart)
{
	_isStart = isStart;
}

void GameplayManager::saveOldGame()
{
	if (_board != nullptr)
	{
		if (_board->_gameOver)
		{
			UserDefault::getInstance()->setBoolForKey(KEY_CAN_CONTINUE, false);
			UserDefault::getInstance()->flush();
		}
		else
		{
			std::string tmp = "";
			for (int i = 0; i < 32; i++)
			{
				tmp.append(StringUtils::toString(_board->_chessPos[i].x));
				tmp.append(" ");
				tmp.append(StringUtils::toString(_board->_chessPos[i].y));
				tmp.append(" ");
			}
			UserDefault::getInstance()->setStringForKey(KEY_DATA_CONTINUE_GAME, tmp);
			UserDefault::getInstance()->setBoolForKey(KEY_CAN_CONTINUE, true);
			UserDefault::getInstance()->setFloatForKey(KEY_TIMER_BLACK, _timerBlack);
			UserDefault::getInstance()->setFloatForKey(KEY_TIMER_RED, _timerRed);
			UserDefault::getInstance()->setIntegerForKey(KEY_MODE_GAME, _board->getMode());
			UserDefault::getInstance()->setIntegerForKey(KEY_SIDE, _board->getCurrentTurn());
			UserDefault::getInstance()->flush();
		}
	}
	
	
}