#ifndef  __SCENE_MANAGER_H__
#define __SCENE_MANAGER_H__

#include "cocos2d.h"
#include "base/ResourcesManager.h"
#include "base/AudioManager.h"
#include "Constant.h"

USING_NS_CC;

class SceneManager {
private:
	static GameSceneState _currentState;
protected:
	SceneManager() {}
	~SceneManager() {}
	SceneManager(const SceneManager& aResourcesManager) {}
	SceneManager(const SceneManager*& aResroucesManager) {}
	SceneManager operator &= (SceneManager &aResourcesManager) {}

public:
	static SceneManager* _pSceneManager;

	GameSceneState getCurrentGameState();

	// Initialize and Release
	static SceneManager* getInstance();
	static void init();
	static void release();
	static void changeToScene(GameSceneState aState);
	static void loadTexture();
	// If you don't preload sound, Window can play but in Android it's not
	// SimpleAudioEngine currently no support OGG file
	static void loadSound();
	static void playBackgroundMusic();
};

#endif