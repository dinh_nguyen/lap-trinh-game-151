#ifndef __AUDIO_MANAGER_H__
#define __AUDIO_MANAGER_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "Resources.h"
#include "Constant.h"

USING_NS_CC;


class AudioManager {
protected:
	AudioManager() {}
	~AudioManager() {}
	AudioManager(const AudioManager& aResourcesManager) {}
	AudioManager(const AudioManager*& aResroucesManager) {}
	AudioManager operator &=(AudioManager &aResourcesManager) {}
public:
	static AudioManager* _pAudioManager;
	static CocosDenshion::SimpleAudioEngine* _pSimpleAudioEngine;

	static AudioManager* getInstance();
	static void init();
	static void release();

	static void playBackgroundMusic(int id,const char* aType, bool bLoop = true);
	static void playEffect(int id,const char* aType, bool loop = false, float pitch = 1.0f,
		float pan = 0.0f, float gain = 1.0f);

	static void pauseBackgroundMusic();
	static void resumeBackgroundMusic();

	static void stopBackgroundMusic(bool bReleaseData = false);
	
	static bool isBackgroundMusicPlaying();

};

#endif