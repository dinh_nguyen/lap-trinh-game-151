#ifndef __MY_LAYER_SCENE_H__
#define __MY_LAYER_SCENE_H__

#include "cocos2d.h"
#include "base/ResourcesManager.h"
#include "base/SceneManager.h"
#include "base/AudioManager.h"
#include "base/GameplayManager.h"
#include "Constant.h"

USING_NS_CC;

class MyLayer : public cocos2d::Layer
{
public:
	static ResourcesManager* _resourcesManager;
	static AudioManager* _audioManager;
};

#endif