#include "base/SceneManager.h"

SceneManager* SceneManager::_pSceneManager;
GameSceneState SceneManager::_currentState;

SceneManager* SceneManager::getInstance()
{
	SceneManager::init();
	return _pSceneManager;
}

void SceneManager::init()
{
	if (_pSceneManager == nullptr) {
		_pSceneManager = new SceneManager();
		_currentState = GameSceneState::Intro;
	}
}

void SceneManager::release()
{
	if (_pSceneManager != nullptr) {
		delete _pSceneManager;
		_pSceneManager = nullptr;
	}
}

void SceneManager::changeToScene(GameSceneState aState)
{
	_currentState = aState;
	ResourcesManager::getInstance()->clear();
	loadTexture();
	loadSound();
	// Don't need to load background music, because cocos2d-x not implemented it
	playBackgroundMusic();
}

void SceneManager::loadTexture()
{
	ResourcesManager* rm = ResourcesManager::getInstance();
	if (_currentState == GameSceneState::Intro)
	{
		rm->addSpriteFrameWithId(INDEX_OF_INTRO_SPRITE_SHEET_PLIST);
	}
	else if (_currentState == GameSceneState::MainMenu)
	{
		rm->addSpriteFrameWithId(INDEX_OF_GAME_CONTROLLER_SPRITE_SHEET_PLIST);
		rm->addSpriteFrameWithId(INDEX_OF_MAIN_MENU_SPRITE_SHEET_PLIST);
	}
	else if (_currentState == GameSceneState::GamePlay)
	{
		//rm->addSpriteFrameWithId(INDEX_OF_MAIN_MENU_SPRITE_SHEET_PLIST);
		rm->addSpriteFrameWithId(INDEX_OF_GAME_CONTROLLER_SPRITE_SHEET_PLIST);
		rm->addSpriteFrameWithId(INDEX_OF_PLAY_SCENE_SPRITE_SHEET_PLIST);
	}
	else if (_currentState == GameSceneState::Credits)
	{
		rm->addSpriteFrameWithId(INDEX_OF_CREDITS_SPRITE_SHEET_PLIST);
	}
	// v...v...
}

// If you don't preload sound, Window can play but in Android it's not
void SceneManager::loadSound()
{
	ResourcesManager* rm = ResourcesManager::getInstance();

	if (_currentState == GameSceneState::Intro)
	{
		rm->preLoadSoundEffect(INDEX_OF_CLICK_WAV, "wav");
	}
	else if (_currentState == GameSceneState::MainMenu)
	{
		rm->preLoadSoundEffect(INDEX_OF_CLICK_WAV, "wav");
		rm->preLoadSoundEffect(INDEX_OF_BUTTON_CLICK_MP3, "mp3");
	}
	else if (_currentState == GameSceneState::GamePlay)
	{
		rm->preLoadSoundEffect(INDEX_OF_KILL_CHESS_WAV, "wav");
		rm->preLoadSoundEffect(INDEX_OF_MOVE_CHESS_MP3, "mp3");
		rm->preLoadSoundEffect(INDEX_OF_SELECT_CHESS_WAV, "wav");
		rm->preLoadSoundEffect(INDEX_OF_WIN_GAME_MP3, "mp3");
		rm->preLoadSoundEffect(INDEX_OF_LOSE_GAME_MP3, "mp3");
		rm->preLoadSoundEffect(INDEX_OF_CHECK_MP3, "mp3");
		rm->preLoadSoundEffect(INDEX_OF_WRONG_MOVE_MP3, "mp3");
	}
	else if (_currentState == GameSceneState::Credits)
	{
		rm->preLoadSoundEffect(INDEX_OF_CLICK_WAV, "wav");
	}
}

void SceneManager::playBackgroundMusic()
{
	AudioManager* audioManager = AudioManager::getInstance();

	if (_currentState == GameSceneState::Intro)
	{
		audioManager->playBackgroundMusic(INDEX_OF_INTRO_BACKGROUND_MUSIC_MP3,"mp3");
	}
	else if (_currentState == GameSceneState::MainMenu)
	{
		if (!audioManager->isBackgroundMusicPlaying())
			audioManager->playBackgroundMusic(INDEX_OF_INTRO_BACKGROUND_MUSIC_MP3, "mp3");
	}
	else if (_currentState == GameSceneState::GamePlay)
	{
		audioManager->stopBackgroundMusic(true);
		audioManager->playBackgroundMusic(INDEX_OF_GAMEPLAY_BACKGROUND_MUSIC_MP3, "mp3");
	}
	else if (_currentState == GameSceneState::Credits)
	{

	}
}

GameSceneState SceneManager::getCurrentGameState()
{
	return _currentState;
}