#ifndef __GAMEPLAY_MANAGER_H__
#define __GAMEPLAY_MANAGER_H__

#include "cocos2d.h"
#include "components/Board.h"
#include "Constant.h"

USING_NS_CC;

class GameplayManager {
private:
	PLAYER _firstPlayer = PLAYER::PLAYER_1_RED;
	Mode _gameMode = Mode::PLAYER_VS_PLAYER;
	AI_Mode _AIMode = AI_Mode::NO_AI;
	Board* _board;

	bool _isStart = true;
	// Round information

protected:
	GameplayManager() {}
	~GameplayManager() {}
	GameplayManager(const GameplayManager& aGameplayManager) {}
	GameplayManager(const GameplayManager*& aGameplayManager) {}
	GameplayManager operator &= (GameplayManager &aGameplayManager) {}
	

public:

	const std::string defaultChessPos = "4 0 3 0 5 0 2 0 6 0 1 0 7 0 0 0 8 0 1 2 7 2 0 3 2 3 4 3 6 3 8 3 4 9 5 9 3 9 6 9 2 9 7 9 1 9 8 9 0 9 7 7 1 7 8 6 6 6 4 6 2 6 0 6";

	float _timerRed   = 0.0f;
	float _timerBlack = 0.0f;
	
	static GameplayManager* _pGameplayManager;

	static GameplayManager* getInstance();
	static void init();
	static void release();

	Board* getBoard(); 
	void setBoard(Board* b);

	Mode getGameMode();
	void setGameMode(Mode gameMode);

	PLAYER getFirstPlayer();
	void setFirstPlayer(PLAYER player);

	AI_Mode getAIMode();
	void setAIMode(AI_Mode aiMode);

	bool isStart();
	void setIsStart(bool isStart);

	void updateEachTouchBoard(Point pos);

	void initNewGame();
	void saveOldGame();
	
};

#endif