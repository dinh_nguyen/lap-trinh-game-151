#include "ResourcesManager.h"

ResourcesManager* ResourcesManager::_pResourcesManager                  = nullptr;
CocosDenshion::SimpleAudioEngine* ResourcesManager::_pSimpleAudioEngine = nullptr;
SpriteFrameCache* ResourcesManager::_pSpriteFrameCache                  = nullptr;
std::vector<const char*> ResourcesManager::_currentLoadingSoundEffect;

ResourcesManager* ResourcesManager::getInstance()
{
	ResourcesManager::init();
	return _pResourcesManager;
}

void ResourcesManager::init()
{
	if (_pResourcesManager == nullptr) 
	{
		_pResourcesManager = new ResourcesManager();
	}

	if (_pSimpleAudioEngine == nullptr)
	{
		_pSimpleAudioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	}

	if (_pSpriteFrameCache == nullptr)
	{
		_pSpriteFrameCache = SpriteFrameCache::getInstance();
	}

	_currentLoadingSoundEffect = std::vector<const char*>();
}

void ResourcesManager::clear()
{
	if (_pSpriteFrameCache != nullptr)
	{
		_pSpriteFrameCache->removeUnusedSpriteFrames();
	}

	if (_pSimpleAudioEngine != nullptr)
	{
		_pSimpleAudioEngine->stopAllEffects();
		if (!_currentLoadingSoundEffect.empty())
		{
			for (const char*& curItem : _currentLoadingSoundEffect)
			{
				_pSimpleAudioEngine->unloadEffect(curItem);
			}
			_currentLoadingSoundEffect.clear();
		}
	}
}

void ResourcesManager::release()
{
	clear();

	if (_pSpriteFrameCache != nullptr)
	{
		_pSpriteFrameCache->destroyInstance();
		_pSpriteFrameCache = nullptr;
	}

	if (_pSimpleAudioEngine)
	{
		_pSimpleAudioEngine->stopBackgroundMusic(true);
		_pSimpleAudioEngine = nullptr;
	}
}

void ResourcesManager::addSpriteFrameWithId(int anId)
{
	if (anId >= NUMBER_OF_PLIST_FILE) return;
	if (_pSpriteFrameCache != nullptr)
	{
		auto test = _pSpriteFrameCache->isSpriteFramesWithFileLoaded(plistResourcesList[anId]);
		if (!_pSpriteFrameCache->isSpriteFramesWithFileLoaded(plistResourcesList[anId])) 
			_pSpriteFrameCache->addSpriteFramesWithFile(plistResourcesList[anId]);
	}
	
}

Sprite* ResourcesManager::createSpriteWithId(int anId)
{
	if (anId >= NUMBER_OF_OBJECT_IN_PLIST_FILE) return nullptr;
	SpriteFrame *frame = _pSpriteFrameCache->getSpriteFrameByName(objectNameInPlistFileList[anId]);

#if COCOS2D_DEBUG > 0
	char msg[256] = { 0 };
	sprintf(msg, "Invalid spriteFrameName: %s", objectNameInPlistFileList[anId]);
	CCASSERT(frame != nullptr, msg);
#endif

	return Sprite::createWithSpriteFrame(frame);
}

ui::Button* ResourcesManager::createButtonWithId(int normalId, int selectedId, int disableId)
{
	if (normalId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || selectedId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || disableId >= NUMBER_OF_OBJECT_IN_PLIST_FILE) return nullptr;

	if (disableId == -1 && selectedId == -1)
		return ui::Button::create(objectNameInPlistFileList[normalId],
		"",
		"",
		ui::TextureResType::PLIST);

	if (disableId == -1)
		return ui::Button::create(objectNameInPlistFileList[normalId],
		objectNameInPlistFileList[selectedId],
		"",
		ui::TextureResType::PLIST);

	return ui::Button::create(objectNameInPlistFileList[normalId],
		objectNameInPlistFileList[selectedId],
		objectNameInPlistFileList[disableId],
		ui::TextureResType::PLIST);
}

Label* ResourcesManager::createLabelTTFWithId(const std::string& text, int id, float fontSize, const Size& dimensions /*= Size::ZERO*/, TextHAlignment hAlignment /*= TextHAlignment::LEFT*/, TextVAlignment vAlignment /*= TextVAlignment::TOP*/)
{
	if (id >= NUMBER_OF_TTF_FILE) return nullptr;
	return Label::createWithTTF(text, ttfResourcesList[id], fontSize, dimensions, hAlignment, vAlignment);
}

Label* ResourcesManager::createLabelBMFontWithId(int id, const std::string& text, const TextHAlignment& hAlignment /* = TextHAlignment::LEFT */, int maxLineWidth /* = 0 */, const Vec2& imageOffset /* = Vec2::ZERO */)
{
	if (id >= NUMBER_OF_FNT_FILE) return nullptr;
	return Label::createWithBMFont(fntResourcesList[id], text, hAlignment, maxLineWidth, imageOffset);
}

CCLabelBMFontAnimated* ResourcesManager::createAnimatedLabelTTFWithId(const std::string& text, int id, float fontSize, const Size& dimensions /* = Size::ZERO */, TextHAlignment hAlignment /* = TextHAlignment::LEFT */, TextVAlignment vAlignment /* = TextVAlignment::TOP */)
{
	if (id >= NUMBER_OF_TTF_FILE) return nullptr;
	return CCLabelBMFontAnimated::createWithTTF(text, ttfResourcesList[id], fontSize, dimensions, hAlignment, vAlignment);
}

CCLabelBMFontAnimated* ResourcesManager::createAnimatedLabelBMFontWithId(int id, const std::string& text, const cocos2d::TextHAlignment& hAlignment /* = cocos2d::TextHAlignment::LEFT  */, int maxLineWidth /* = 0  */, const cocos2d::Vec2& imageOffset /* = cocos2d::Vec2::ZERO */)
{
	if (id >= NUMBER_OF_FNT_FILE) return nullptr;
	return CCLabelBMFontAnimated::createWithBMFont(fntResourcesList[id], text, hAlignment, maxLineWidth, imageOffset);
}

MenuItemSprite * ResourcesManager::createMenuItemWithId(const int normalId, const int selectedId, const int disabledId)
{
	if (normalId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || selectedId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || disabledId >= NUMBER_OF_OBJECT_IN_PLIST_FILE) return nullptr;
	auto rm = ResourcesManager::getInstance();
	if (disabledId == -1)
		return MenuItemSprite::create(
		rm->createSpriteWithId(normalId),
		rm->createSpriteWithId(selectedId),
		nullptr);

	return MenuItemSprite::create(
		rm->createSpriteWithId(normalId),
		rm->createSpriteWithId(selectedId),
		rm->createSpriteWithId(disabledId));

}
//
MenuItemSprite * ResourcesManager::createMenuItemWithId(const int normalId, const int selectedId, const ccMenuCallback& callback)
{
	if (normalId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || selectedId >= NUMBER_OF_OBJECT_IN_PLIST_FILE) return nullptr;
	auto rm = ResourcesManager::getInstance();

	return MenuItemSprite::create(
		rm->createSpriteWithId(normalId),
		rm->createSpriteWithId(selectedId),
		callback);
}

MenuItemSprite * ResourcesManager::createMenuItemWithId(const int normalId, const int selectedId, const int disabledId, const ccMenuCallback& callback)
{
	if (normalId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || selectedId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || disabledId >= NUMBER_OF_OBJECT_IN_PLIST_FILE) return nullptr;
	auto rm = ResourcesManager::getInstance();
	return MenuItemSprite::create(
		rm->createSpriteWithId(normalId),
		rm->createSpriteWithId(selectedId),
		rm->createSpriteWithId(disabledId),
		callback);
}

ui::Slider* ResourcesManager::createSliderWithId(int barId, int progressBarId)
{
	if (barId >= NUMBER_OF_OBJECT_IN_PLIST_FILE || progressBarId >= NUMBER_OF_OBJECT_IN_PLIST_FILE)
		return nullptr;

	auto slider = ui::Slider::create();
	slider->loadBarTexture(objectNameInPlistFileList[barId], ui::TextureResType::PLIST);
	slider->loadProgressBarTexture(objectNameInPlistFileList[progressBarId], ui::TextureResType::PLIST);
	return slider;
}

Vector<SpriteFrame*> ResourcesManager::createVectorSpriteFramesWithId(int anId)
{
	auto numFrames = numberOfFrameAnimationList[anId];
	Vector<SpriteFrame*> animFrames(numFrames);
	if (anId >= NUMBER_OF_ANIMATION || numFrames < 0 ) return animFrames;

	Value tmpValue(objectNameInPlistFileList[anId]);
	std::string tmp = tmpValue.asString();
	tmp = tmp.substr(0, tmp.length() - 1 - 7);

	for (int i = 0; i < numFrames; i++)
	{
		std::string str = tmp + (StringUtils::format("%04d.png", i));
		auto frame = _pSpriteFrameCache->getSpriteFrameByName(str);
		animFrames.pushBack(frame);
	}

	return animFrames;
}

void ResourcesManager::preLoadSoundEffect(const char* aFilePath)
{
	_currentLoadingSoundEffect.push_back(aFilePath);
	_pSimpleAudioEngine->preloadEffect(aFilePath);
}

void ResourcesManager::preLoadSoundEffect(int id, char* aType)
{
	std::string aTypeString = StringUtils::toString(aType);
	if (aTypeString.compare("mp3") == 0) {
		if (id >= NUMBER_OF_MP3_FILE) return;
		preLoadSoundEffect(mp3ResourcesList[id]);
	}

	else if (aTypeString.compare("wav") == 0) {
		if (id >= NUMBER_OF_WAV_FILE) return;
		preLoadSoundEffect(wavResourcesList[id]);
	}
}