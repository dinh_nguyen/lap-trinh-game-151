#ifndef __RESOURCES_MANAGER_H__ 
#define __RESOURCES_MANAGER_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "Resources.h"
#include "ui/CocosGUI.h"
#include "components/CCLabelBMFontAnimated.h"
#include <string>

USING_NS_CC;


class ResourcesManager {
private:
	static std::vector<const char*> _currentLoadingSoundEffect;

protected:
	ResourcesManager() {}
	~ResourcesManager() {}
	ResourcesManager(const ResourcesManager& aResourcesManager) {}
	ResourcesManager(const ResourcesManager*& aResroucesManager) {}
	ResourcesManager operator &= (ResourcesManager &aResourcesManager) {}
public:
	static ResourcesManager* _pResourcesManager;
	static CocosDenshion::SimpleAudioEngine* _pSimpleAudioEngine;
	static SpriteFrameCache* _pSpriteFrameCache;

	// Initialize and Release
	static ResourcesManager* getInstance();
	static void init();
	static void clear();
	static void release();

	// Image manager
	// Create sprite, menu, label, animate label, button ... with sprite

	static void addSpriteFrameWithId(int id);
	static Sprite* createSpriteWithId(int id);

	static ui::Button* createButtonWithId(int normalId, int selectedId = -1, int disableId = -1);

	static MenuItemSprite * createMenuItemWithId(int normalId, int selectedId, int disabledId = -1);
	static MenuItemSprite * createMenuItemWithId(int normalId, int selectedId, const ccMenuCallback& callback);
	static MenuItemSprite * createMenuItemWithId(int normalId, int selectedId, int disabledId, const ccMenuCallback& callback);

	static ui::Slider* createSliderWithId(int barId, int progressBarId);

	static Label* createLabelTTFWithId(const std::string& text, int id, float fontSize, const Size& dimensions = Size::ZERO, TextHAlignment hAlignment = TextHAlignment::LEFT, TextVAlignment = TextVAlignment::TOP);
	static Label* createLabelBMFontWithId(int id, const std::string& text, const TextHAlignment& hAlignment = TextHAlignment::LEFT, int maxLineWidth = 0, const Vec2& imageOffset = Vec2::ZERO);

	static CCLabelBMFontAnimated* createAnimatedLabelTTFWithId(const std::string& text, int id, float fontSize, const Size& dimensions = Size::ZERO, TextHAlignment hAlignment = TextHAlignment::LEFT, TextVAlignment vAlignment = TextVAlignment::TOP);
	static CCLabelBMFontAnimated* createAnimatedLabelBMFontWithId(int id, const std::string& text, const cocos2d::TextHAlignment& hAlignment = cocos2d::TextHAlignment::LEFT, int maxLineWidth = 0, const cocos2d::Vec2& imageOffset = cocos2d::Vec2::ZERO);

	static Vector<SpriteFrame*> createVectorSpriteFramesWithId(int id);

	// Audio Manager

	static void preLoadSoundEffect(const char* aSoundPath);
	static void preLoadSoundEffect(int id, char* aType);

	// Handler when change scene
	static void loadPlistFile();

};

#endif