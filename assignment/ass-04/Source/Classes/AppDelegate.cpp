#include "AppDelegate.h"
#include "scenes/IntroScene.h"
#include "scenes/PlayingScene.h"


USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
	//set OpenGL context attributions,now can only set six attributions:
	//red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

	GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
	return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if(!glview) {
		glview = GLViewImpl::create("My Chinese Chess");
		director->setOpenGLView(glview);
	}

	// turn on display FPS
	director->setDisplayStats(false);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

	register_all_packages();

	// change resolution

	// Original size 
	auto designSize = Size(720, 1280);
	

	// Use this when you want test difference resolution
	glview->setFrameSize(720, 1280);
	glview->setFrameZoomFactor(0.575f);

	glview->setDesignResolutionSize(designSize.width, designSize.height, ResolutionPolicy::EXACT_FIT);
	
	// create a scene. it's an autorelease object
	//auto scene = Intro::createScene();
	auto scene = Intro::createScene();
	// run
	director->runWithScene(scene);

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();

	bool isPaused = UserDefault::getInstance()->getBoolForKey(KEY_SETUP_MUSIC, true);

	if (isPaused == true) {
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	}

	// if you use SimpleAudioEngine, it must be pause
	// SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();
	bool isPaused = UserDefault::getInstance()->getBoolForKey(KEY_SETUP_MUSIC, true);
	if (isPaused == true) {
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	}
	// if you use SimpleAudioEngine, it must resume here
	// SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
