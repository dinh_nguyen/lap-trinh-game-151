#ifndef __AIBOT_H__
#define __AIBOT_H__

#include "Constant.h"
#include "cocos2d.h"

USING_NS_CC;

class AIBot {
protected:
	int _deep, _width;
	bool first;
protected:
	int search(int[9][10], Point[32], int, int, Point, int, int);
	virtual int heuristic(int[9][10], Point[32], int);
	void quickSort(int[], int[], Point[], int, int);
	void swapInt(int&, int&);
	void swapPoint(Point&, Point&);
	static int  getSide(int);
	static bool isRed(int[9][10], int, int);
	static bool isBlack(int[9][10], int, int);
	static bool isEmpty(int[9][10], int, int);
public:
	virtual bool generateMove(int[9][10], Point[32], int, int&, Point&);
	static bool getMoveList(int[9][10], Point[32], int, int*, Point*, int&);
	AIBot();
};
#endif