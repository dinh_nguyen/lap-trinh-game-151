#include "components\AIBot_Level2.h"
#include "Board.h"

const int AIBot_Level2::positionValueAdvisor[2][10][9] = {
	{
		{ 0, 0, 0, 1, 0, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 3, 0, 0, 0, 0 },
		{ 0, 0, 0,-1, 0,-1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	},
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0,-1, 0,-1, 0, 0, 0 },
		{ 0, 0, 0, 0, 3, 0, 0, 0, 0 },
		{ 0, 0, 0, 1, 0, 1, 0, 0, 0 }
	}
};

const int AIBot_Level2::positionValueElephant[2][10][9] = {
	{
		{ 0, 0, 1, 0, 0, 0, 1, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{-2, 0, 0, 0, 3, 0, 0, 0,-2 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0,-1, 0, 0, 0,-1, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	},
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0,-1, 0, 0, 0,-1, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{-2, 0, 0, 0, 3, 0, 0, 0,-2 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0, 0, 1, 0, 0 }
	}
};

const int AIBot_Level2::positionValueHorse[2][10][9] = {
	{
		{ 1, 0, 1, 1, 1, 1, 1, 0, 1 },
		{ 1, 1, 1, 1, 0, 1, 1, 1, 1 },
		{ 1, 1, 2, 2, 1, 2, 2, 1, 1 },
		{ 1, 2, 2, 2, 2, 2, 2, 2, 1 },
		{ 1, 2, 3, 3, 2, 3, 3, 2, 1 },
		{ 2, 3, 3, 3, 3, 3, 3, 3, 2 },
		{ 2, 4, 3, 4, 4, 4, 3, 4, 2 },
		{ 2, 3, 3, 4, 3, 4, 3, 3, 2 },
		{ 1, 2, 4, 3, 2, 3, 4, 2, 1 },
		{ 1, 2, 3, 2, 1, 2, 3, 2, 1 }
	},
	{
		{ 1, 2, 3, 2, 1, 2, 3, 2, 1 },
		{ 1, 2, 4, 3, 2, 3, 4, 2, 1 },
		{ 2, 3, 3, 4, 3, 4, 3, 3, 2 },
		{ 2, 4, 3, 4, 4, 4, 3, 4, 2 },
		{ 2, 3, 3, 3, 3, 3, 3, 3, 2 },
		{ 1, 2, 3, 3, 2, 3, 3, 2, 1 },
		{ 1, 2, 2, 2, 2, 2, 2, 2, 1 },
		{ 1, 1, 2, 2, 1, 2, 2, 1, 1 },
		{ 1, 1, 1, 1, 0, 1, 1, 1, 1 },
		{ 1, 0, 1, 1, 1, 1, 1, 0, 1 }
	}
};
const int AIBot_Level2::positionValueCar[2][10][9] = {
	{
		{ 0, 2, 2, 3, 2, 3, 2, 2, 0 },
		{ 2, 1, 2, 3, 2, 3, 2, 1, 2 },
		{ 1, 2, 2, 3, 2, 3, 2, 2, 1 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 2, 3, 2, 3, 3, 3, 2, 3, 2 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 3, 4, 3, 4, 4, 4, 3, 4, 3 },
		{ 3, 3, 2, 3, 3, 3, 2, 3, 3 }
	},
	{
		{ 3, 3, 2, 3, 3, 3, 2, 3, 3 },
		{ 3, 4, 3, 4, 4, 4, 3, 4, 3 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 2, 3, 2, 3, 3, 3, 2, 3, 2 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 1, 2, 2, 3, 2, 3, 2, 2, 1 },
		{ 2, 1, 2, 3, 2, 3, 2, 1, 2 },
		{ 0, 2, 2, 3, 2, 3, 2, 2, 0 }
	}
};
const int AIBot_Level2::positionValueCanon[2][10][9] = {
	{
		{ 2, 2, 3, 4, 4, 4, 3, 2, 2 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 3, 2, 4, 4, 4, 4, 4, 2, 3 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 2, 2, 3, 3, 4, 3, 3, 2, 2 },
		{ 2, 2, 2, 3, 4, 3, 2, 2, 2 },
		{ 2, 2, 2, 3, 4, 3, 2, 2, 2 },
		{ 3, 3, 2, 1, 1, 1, 2, 3, 3 },
		{ 3, 3, 2, 2, 1, 2, 2, 3, 3 },
		{ 4, 3, 2, 1, 1, 1, 2, 3, 4 }
	},
	{
		{ 4, 3, 2, 1, 1, 1, 2, 3, 4 },
		{ 3, 3, 2, 2, 1, 2, 2, 3, 3 },
		{ 3, 3, 2, 1, 1, 1, 2, 3, 3 },
		{ 2, 2, 2, 3, 4, 3, 2, 2, 2 },
		{ 2, 2, 2, 3, 4, 3, 2, 2, 2 },
		{ 2, 2, 3, 3, 4, 3, 3, 2, 2 },
		{ 2, 2, 2, 3, 3, 3, 2, 2, 2 },
		{ 3, 2, 4, 4, 4, 4, 4, 2, 3 },
		{ 2, 3, 3, 4, 4, 4, 3, 3, 2 },
		{ 2, 2, 3, 4, 4, 4, 3, 2, 2 }
	}
};

const int AIBot_Level2::positionValueSolder[2][10][9] = {
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0,-1, 0, 0, 0,-1, 0, 0 },
		{ 0, 0, 2, 0, 2, 0, 2, 0, 0 },
		{ 2, 2, 3, 3, 4, 3, 3, 2, 2 },
		{ 2, 4, 4, 4, 4, 4, 4, 4, 2 },
		{ 3, 4, 4, 4, 4, 4, 4, 4, 3 },
		{ 3, 4, 4, 4, 4, 4, 4, 4, 3 },
		{ 0, 0, 2, 2, 2, 2, 2, 0, 0 }
	},
	{
		{ 0, 0, 2, 2, 2, 2, 2, 0, 0 },
		{ 3, 4, 4, 4, 4, 4, 4, 4, 3 },
		{ 3, 4, 4, 4, 4, 4, 4, 4, 3 },
		{ 2, 4, 4, 4, 4, 4, 4, 4, 2 },
		{ 2, 2, 3, 3, 4, 3, 3, 2, 2 },
		{ 0, 0, 2, 0, 2, 0, 2, 0, 0 },
		{ 0, 0,-1, 0, 0, 0,-1, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	}
};

AIBot_Level2::AIBot_Level2() : AIBot() {
	_deep = 6;
	_width = 8;
}

int AIBot_Level2::heuristic(int map[9][10], Point chessPos[32], int side) {
	int result = 0;
	for (int i = startIndexSide[side]; i < endIndexSide[side]; i++) {
		if (int(chessPos[i].x) >= 0) {
			result += calculatePoint(chessPos, i);
		}
	}

	for (int i = startIndexSide[1 - side]; i < endIndexSide[1 - side]; i++) {
		if (int(chessPos[i].x) >= 0) {
			result -= calculatePoint(chessPos, i);
		}
	}
	return result;
}

int AIBot_Level2::calculatePoint(Point chessPos[32], int index)
{
	switch (Board::type[index]) {
	case RED_KING:
	case BLACK_KING:
		return 300;
	case RED_ADVISOR:
		return 133 * positionValueAdvisor[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_ADVISOR:
		return 133 * positionValueAdvisor[0][int(chessPos[index].y)][int(chessPos[index].x)];
	case RED_ELEPHANT:
		return 166 * positionValueElephant[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_ELEPHANT:
		return 166 * positionValueElephant[0][int(chessPos[index].y)][int(chessPos[index].x)];
	case RED_HORSE:
		return 266 * positionValueHorse[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_HORSE:
		return 266 * positionValueHorse[0][int(chessPos[index].y)][int(chessPos[index].x)];
	case RED_CAR:
		return 600 * positionValueCar[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_CAR:
		return 600 * positionValueCar[0][int(chessPos[index].y)][int(chessPos[index].x)];
	case RED_CANON:
		return 300 * positionValueCanon[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_CANON:
		return 300 * positionValueCanon[0][int(chessPos[index].y)][int(chessPos[index].x)];
	case RED_SOLDER:
		return 66 * positionValueSolder[1][int(chessPos[index].y)][int(chessPos[index].x)];
	case BLACK_SOLDER:
		return 66 * positionValueSolder[0][int(chessPos[index].y)][int(chessPos[index].x)];
	}
	return 0;
}