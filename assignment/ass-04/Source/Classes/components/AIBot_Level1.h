#ifndef __AIBOT_LEVEL1_H__
#define __AIBOT_LEVEL1_H__

#include "AIBot.h"

class AIBot_Level1 : public AIBot{
protected:
	virtual int heuristic(int[9][10], Point[32], int);
	int calculatePoint(Point[32], int);
public:
	const static int bonusForSolderMap[2][10][9];
	const static int bonusForSolder[5];
public:
	AIBot_Level1();
};
#endif