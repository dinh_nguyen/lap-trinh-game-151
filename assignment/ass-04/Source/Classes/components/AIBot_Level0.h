#ifndef __AIBOT_LEVEL0_H__
#define __AIBOT_LEVEL0_H__

#include "AIBot.h"

class AIBot_Level0 : public AIBot{
public:
	virtual bool generateMove(int[9][10], Point[32], int, int&, Point&);
};
#endif