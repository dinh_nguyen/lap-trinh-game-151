#include "components\AIBot_Level0.h"
#include "Board.h"


bool AIBot_Level0::generateMove(int map[9][10], Point chessPos[32], int side, int &resultChess, Point &resultPoint)
{
	Board* b = new Board();
	for (int i = 0; i < 32; i++) {
		b->_chessPos[i].x = chessPos[i].x;
		b->_chessPos[i].y = chessPos[i].y;
	}
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 10; j++) {
			b->_map[i][j] = map[i][j];
		}
	}

	do
	{
		resultPoint = Point((float)RandomHelper::random_int(0, 8), (float)RandomHelper::random_int(0, 9));
		resultChess = RandomHelper::random_int(16 * side, 15 + 16 * side);
	} while (!b->canMove(resultChess, b->_chessPos[resultChess], resultPoint) || b->isCheck(resultChess, resultPoint));

	free(b);
	return true;
}