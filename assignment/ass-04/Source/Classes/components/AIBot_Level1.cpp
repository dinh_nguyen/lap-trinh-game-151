#include "components\AIBot_Level1.h"
#include "Board.h"

const int AIBot_Level1::bonusForSolderMap[][10][9] = {
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 1, 2, 3, 4, 4, 4, 3, 2, 1 },
		{ 1, 2, 3, 4, 4, 4, 3, 2, 1 },
		{ 1, 2, 3, 3, 3, 3, 3, 2, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 0, 0, 1, 0, 0, 0, 1, 0, 0 },
		{ 0, 0, 0, 0, 2, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	},
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 2, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0, 0, 1, 0, 0 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 2, 3, 3, 3, 3, 3, 2, 1 },
		{ 1, 2, 3, 4, 4, 4, 3, 2, 1 },
		{ 1, 2, 3, 4, 4, 4, 3, 2, 1 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	}
};

const int AIBot_Level1::bonusForSolder[] = { 0, 75, 150, 225, 300 };

AIBot_Level1::AIBot_Level1() : AIBot() {
	_deep = 6;
	_width = 8;
}

int AIBot_Level1::heuristic(int map[9][10], Point chessPos[32], int side) {
	int result = 0;
	for (int i = startIndexSide[side]; i < endIndexSide[side]; i++) {
		if (int(chessPos[i].x) >= 0) {
			result += calculatePoint(chessPos, i);
		}
	}

	for (int i = startIndexSide[1 - side]; i < endIndexSide[1 - side]; i++) {
		if (int(chessPos[i].x) >= 0) {
			result -= calculatePoint(chessPos, i);
		}
	}
	return result;
}

int AIBot_Level1::calculatePoint(Point chessPos[32], int index)
{
	switch (Board::type[index]) {
	case RED_KING:
	case BLACK_KING:
		return 300;
	case RED_ADVISOR:
	case BLACK_ADVISOR:
		return 400;
	case RED_ELEPHANT:
	case BLACK_ELEPHANT:
		return 300;
	case RED_HORSE:
	case BLACK_HORSE:
		return 480;
	case RED_CAR:
	case BLACK_CAR:
		return 900;
	case RED_CANON:
	case BLACK_CANON:
		return 600;
	case RED_SOLDER:
		return 150 + bonusForSolder[bonusForSolderMap[1][int(chessPos[index].y)][int(chessPos[index].x)]];
	case BLACK_SOLDER:
		return 150 + bonusForSolder[bonusForSolderMap[0][int(chessPos[index].y)][int(chessPos[index].x)]];
	}
	return 0;
}