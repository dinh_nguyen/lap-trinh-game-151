#include "components/Board.h"
#include "components/AIBot_Level0.h"
#include "components/AIBot_Level1.h"
#include "components/AIBot_Level2.h"
#include "base/GameplayManager.h"


const ChessType Board::type[] = {
	ChessType::RED_KING,
	ChessType::RED_ADVISOR,
	ChessType::RED_ADVISOR,
	ChessType::RED_ELEPHANT,
	ChessType::RED_ELEPHANT,
	ChessType::RED_HORSE,
	ChessType::RED_HORSE,
	ChessType::RED_CAR,
	ChessType::RED_CAR,
	ChessType::RED_CANON,
	ChessType::RED_CANON,
	ChessType::RED_SOLDER,
	ChessType::RED_SOLDER,
	ChessType::RED_SOLDER,
	ChessType::RED_SOLDER,
	ChessType::RED_SOLDER,
	ChessType::BLACK_KING,
	ChessType::BLACK_ADVISOR,
	ChessType::BLACK_ADVISOR,
	ChessType::BLACK_ELEPHANT,
	ChessType::BLACK_ELEPHANT,
	ChessType::BLACK_HORSE,
	ChessType::BLACK_HORSE,
	ChessType::BLACK_CAR,
	ChessType::BLACK_CAR,
	ChessType::BLACK_CANON,
	ChessType::BLACK_CANON,
	ChessType::BLACK_SOLDER,
	ChessType::BLACK_SOLDER,
	ChessType::BLACK_SOLDER,
	ChessType::BLACK_SOLDER,
	ChessType::BLACK_SOLDER
};

// CHESS SPRITE
const int Board::listChessSpriteId[32] = {
	INDEX_OF_IMAGE_RED_KING_FRAME_NAME, //ChessType::RED_KING
	INDEX_OF_IMAGE_RED_ADVISOR_FRAME_NAME, //ChessType::RED_ADVISOR
	INDEX_OF_IMAGE_RED_ADVISOR_FRAME_NAME, //ChessType::RED_ADVISOR
	INDEX_OF_IMAGE_RED_ELEPHANT_FRAME_NAME, //ChessType::RED_ELEPHANT
	INDEX_OF_IMAGE_RED_ELEPHANT_FRAME_NAME, //ChessType::RED_ELEPHANT
	INDEX_OF_IMAGE_RED_HORSE_FRAME_NAME, //ChessType::RED_HORSE
	INDEX_OF_IMAGE_RED_HORSE_FRAME_NAME, //ChessType::RED_HORSE
	INDEX_OF_IMAGE_RED_CAR_FRAME_NAME, //ChessType::RED_CAR
	INDEX_OF_IMAGE_RED_CAR_FRAME_NAME, //ChessType::RED_CAR
	INDEX_OF_IMAGE_RED_CANON_FRAME_NAME, //ChessType::RED_CANON
	INDEX_OF_IMAGE_RED_CANON_FRAME_NAME, //ChessType::RED_CANON
	INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME, //ChessType::RED_SOLDIER
	INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME, //ChessType::RED_SOLDIER
	INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME, //ChessType::RED_SOLDIER
	INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME, //ChessType::RED_SOLDIER
	INDEX_OF_IMAGE_RED_SOLDIER_FRAME_NAME, //ChessType::RED_SOLDIER
	INDEX_OF_IMAGE_BLACK_KING_FRAME_NAME,//ChessType::BLACK_KING
	INDEX_OF_IMAGE_BLACK_ADVISOR_FRAME_NAME, //ChessType::BLACK_ADVISOR
	INDEX_OF_IMAGE_BLACK_ADVISOR_FRAME_NAME, //ChessType::BLACK_ADVISOR
	INDEX_OF_IMAGE_BLACK_ELEPHANT_FRAME_NAME, //ChessType::BLACK_ELEPHANT
	INDEX_OF_IMAGE_BLACK_ELEPHANT_FRAME_NAME, //ChessType::BLACK_ELEPHANT
	INDEX_OF_IMAGE_BLACK_HORSE_FRAME_NAME, //ChessType::BLACK_HORSE
	INDEX_OF_IMAGE_BLACK_HORSE_FRAME_NAME, //ChessType::BLACK_HORSE
	INDEX_OF_IMAGE_BLACK_CAR_FRAME_NAME, //ChessType::BLACK_CAR
	INDEX_OF_IMAGE_BLACK_CAR_FRAME_NAME, //ChessType::BLACK_CAR
	INDEX_OF_IMAGE_BLACK_CANON_FRAME_NAME, //ChessType::BLACK_CANON
	INDEX_OF_IMAGE_BLACK_CANON_FRAME_NAME, //ChessType::BLACK_CANON
	INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME, //ChessType::BLACK_SOLDIER
	INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME, //ChessType::BLACK_SOLDIER
	INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME, //ChessType::BLACK_SOLDIER
	INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME, //ChessType::BLACK_SOLDIER
	INDEX_OF_IMAGE_BLACK_SOLDIER_FRAME_NAME//ChessType::BLACK_SOLDIER
};

Board::Board()
{
	setup();
}

void Board::setup()
{
	side = PLAYER::PLAYER_1_RED; // RED first

	switch (GameplayManager::getInstance()->getAIMode())
	{
	case AI_Mode::EASY:
		_bot = new AIBot_Level0();
		break;
	case AI_Mode::NORMAL:
		_bot = new AIBot_Level1();
		break;
	case AI_Mode::HARD:
		_bot = new AIBot_Level2();
		break;
	default:
		break;
	}

	if (GameplayManager::getInstance()->isStart())
		reset();
	else
		loadOldGame();
}

void Board::resetBoard()
{
	side = 1 - side; // SWITCH SIDE TO GO FIRST
	reset();
}

void Board::loadOldGame()
{
	std::string oldData = UserDefault::getInstance()->getStringForKey(KEY_DATA_CONTINUE_GAME, GameplayManager::getInstance()->defaultChessPos);
	std::stringstream stream(oldData);
	std::string segment;
	for (int i = 0; i < 32; i++)
	{
		std::getline(stream, segment, ' ');
		Value val(segment);
		_chessPos[i].x = val.asFloat();
		std::getline(stream, segment, ' ');
		Value val2(segment);
		_chessPos[i].y = val2.asFloat();
	}

	GameplayManager::getInstance()->_timerBlack = UserDefault::getInstance()->getFloatForKey(KEY_TIMER_BLACK, 1800.0f);
	GameplayManager::getInstance()->_timerRed = UserDefault::getInstance()->getFloatForKey(KEY_TIMER_RED, 1800.0f);
	_mode = UserDefault::getInstance()->getIntegerForKey(KEY_MODE_GAME, 0);
	side = UserDefault::getInstance()->getIntegerForKey(KEY_SIDE, 0);

	resetMap();
}

void Board::reset()
{
	_gameOver = false;

	// RED SIDE
	_chessPos[0].x = 4; _chessPos[0].y = 0; // KING 
	_chessPos[1].x = 3; _chessPos[1].y = 0; // ADVISOR
	_chessPos[2].x = 5; _chessPos[2].y = 0; // ADVISOR
	_chessPos[3].x = 2; _chessPos[3].y = 0; // ELEPHANT
	_chessPos[4].x = 6; _chessPos[4].y = 0; // ELEPHANT
	_chessPos[5].x = 1; _chessPos[5].y = 0; // HORSE
	_chessPos[6].x = 7; _chessPos[6].y = 0; // HORSE 
	_chessPos[7].x = 0; _chessPos[7].y = 0; // CAR
	_chessPos[8].x = 8; _chessPos[8].y = 0; // CAR
	_chessPos[9].x = 1; _chessPos[9].y = 2; // CANON
	_chessPos[10].x = 7; _chessPos[10].y = 2; // CANON
	_chessPos[11].x = 0; _chessPos[11].y = 3; // SOLDER
	_chessPos[12].x = 2; _chessPos[12].y = 3; // SOLDER
	_chessPos[13].x = 4; _chessPos[13].y = 3; // SOLDER
	_chessPos[14].x = 6; _chessPos[14].y = 3; // SOLDER
	_chessPos[15].x = 8; _chessPos[15].y = 3; // SOLDER

	// BLACK SIDE
	_chessPos[16].x = 4; _chessPos[16].y = 9; // KING 
	_chessPos[17].x = 5; _chessPos[17].y = 9; // ADVISOR
	_chessPos[18].x = 3; _chessPos[18].y = 9; // ADVISOR
	_chessPos[19].x = 6; _chessPos[19].y = 9; // ELEPHANT
	_chessPos[20].x = 2; _chessPos[20].y = 9; // ELEPHANT
	_chessPos[21].x = 7; _chessPos[21].y = 9; // HORSE
	_chessPos[22].x = 1; _chessPos[22].y = 9; // HORSE 
	_chessPos[23].x = 8; _chessPos[23].y = 9; // CAR
	_chessPos[24].x = 0; _chessPos[24].y = 9; // CAR
	_chessPos[25].x = 7; _chessPos[25].y = 7; // CANON
	_chessPos[26].x = 1; _chessPos[26].y = 7; // CANON
	_chessPos[27].x = 8; _chessPos[27].y = 6; // SOLDER
	_chessPos[28].x = 6; _chessPos[28].y = 6; // SOLDER
	_chessPos[29].x = 4; _chessPos[29].y = 6; // SOLDER
	_chessPos[30].x = 2; _chessPos[30].y = 6; // SOLDER
	_chessPos[31].x = 0; _chessPos[31].y = 6; // SOLDER

	//testMap();

	resetMap();
}

void Board::resetMap()
{
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 10; j++)
			_map[i][j] = PiecesState::BLANK;

	for (int i = 0; i < 32; i++)
		if (_chessPos[i].x != PiecesState::WAS_BEATING)
			_map[int(_chessPos[i].x)][int(_chessPos[i].y)] = i;
}

Board* Board::create()
{
	Board *board = new (std::nothrow) Board();
	if (board && board->init())
	{
		board->autorelease();
		return board;
	}
	CC_SAFE_DELETE(board);
	return nullptr;
}

Point Board::changeCoordinate(float x, float y)
{
	return Point(getPositionX() - x, y - getPositionY());
}

Point Board::changeCoordinate(Point p)
{
	return Point(getPositionX() - p.x, p.y - getPositionY());
}

Point Board::inverseChangeCoordinate(float x, float y)
{
	return Point(getPositionX() - x, getPositionY() + y);
}
Point Board::inverseChangeCoordinate(Point p)
{
	return Point(getPositionX() - p.x, getPositionY() + p.y);
}

Point Board::changeLocalCoordinateParent(float x, float y)
{
	return Point(-x, y);
}

Point Board::changeLocalCoordinateParent(Point p)
{
	return Point(-p.x, p.y);
}

void Board::addGrid()
{
	// Player 1
	int i = 0;
	for (i = 0; i < 4; i++) {
		for (int j = 0; j < 8; j++) {
			Rect* rect = new Rect(j*GRID_X, i*GRID_Y, GRID_X, GRID_Y);
			_grid.push_back(rect);
		}
	}

	i = i + 1;

	// Player 2
	for (; i < 9; i++) {
		for (int j = 0; j < 8; j++) {
			Rect* rect = new Rect(j*GRID_X, i*GRID_Y, GRID_X, GRID_Y);
			_grid.push_back(rect);
		}
	}
}

void Board::addCenterPoints()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 9; j++) {
			_centerPoint.push_back(new Point(j*GRID_X, i*GRID_Y));
		}
	}
}

std::vector<Point*> Board::getListCenterPoint()
{
	return _centerPoint;
}

int Board::getChessInPosition(Point p)
{
	int rowCheck = p.x;
	int colCheck = p.y;

	if (rowCheck > (BOARD_WIDTH - 1) || rowCheck < 0 || (colCheck > BOARD_HEIGHT - 1) || colCheck < 0) return PiecesState::BLANK;

	return _map[rowCheck][colCheck];

}

Point Board::getClosestCenterPointFrom(Point p)
{
	Point res = changeCoordinate(p);
	Point pCheck = Point(-999, -999);

	int xBottomRightIndex = res.x / GRID_X;
	int yBottomRightIndex = res.y / GRID_Y;

	Point listCheck[4] =
	{
		Point(xBottomRightIndex * GRID_X, yBottomRightIndex * GRID_Y),
		Point((xBottomRightIndex + 1) * GRID_X, yBottomRightIndex * GRID_Y),
		Point(xBottomRightIndex*GRID_X, (yBottomRightIndex + 1) * GRID_Y),
		Point((xBottomRightIndex + 1)*GRID_X, (yBottomRightIndex + 1) * GRID_Y)
	};

	int i = 0;
	for (; i < 4; i++)
	{
		if (res.distance(listCheck[i]) <= (GRID_X / 2))
		{
			pCheck = listCheck[i];
			break;
		}
	}
	//return pCheck;
	return Point(pCheck.x / GRID_X, pCheck.y / GRID_Y);
}

bool Board::move(int chess, Point targetPoint)
{
	if (_gameOver)
		return false;

	if (!canMove(chess, _chessPos[chess], targetPoint))
		return false;



	if (_map[int(targetPoint.x)][int(targetPoint.y)] != PiecesState::BLANK)
		_chessPos[_map[int(targetPoint.x)][int(targetPoint.y)]].x = PiecesState::WAS_BEATING;


	_chessPos[chess].x = int(targetPoint.x);
	_chessPos[chess].y = int(targetPoint.y);

	resetMap();

	return true;
}

bool Board::move(int chess, Point targetPoint, int condition)
{
	bool result = false;
	if (condition == MoveCondition::NO)
	{
		result = move(chess, targetPoint);
	}
	else if (condition == MoveCondition::CHESSMATE)
	{
		Point backupChessPos[32];
		std::copy(std::begin(_chessPos), std::end(_chessPos), std::begin(backupChessPos));

		int backupMap[9][10];
		std::copy(reinterpret_cast<int*>(_map), reinterpret_cast<int*>(_map)+9 * 10, reinterpret_cast<int*>(backupMap));

		result = move(chess, targetPoint);
		bool isChessMateAfterMoved = isCheck(getSide(chess));

		//If you were checked after moved, return false and rollback data
		if (result && isChessMateAfterMoved)
		{
			std::copy(std::begin(backupChessPos), std::end(backupChessPos), std::begin(_chessPos));
			std::copy(reinterpret_cast<int*>(backupMap), reinterpret_cast<int*>(backupMap)+9 * 10, reinterpret_cast<int*>(_map));
			result = false;
		}
	}
	return result;
}

bool Board::canMove(int chess, Point fromPos, Point toPos)
{
	// Dinh's add
	if (fromPos.equals(toPos) || _chessPos[chess].x == PiecesState::WAS_BEATING) return false;

	if (!isValidPosition(type[chess], toPos))
		return false;

	else if (getSide(chess) == PLAYER::PLAYER_1_RED) {
		// RED MOVE
		if (_map[int(toPos.x)][int(toPos.y)] != PiecesState::BLANK && getSide(_map[int(toPos.x)][int(toPos.y)]) == PLAYER::PLAYER_1_RED)
			return false;
	}
	else if (getSide(chess) == PLAYER::PLAYER_2_BLACK) {
		// BLACK MOVE
		if (_map[int(toPos.x)][int(toPos.y)] != PiecesState::BLANK && getSide(_map[int(toPos.x)][int(toPos.y)]) == PLAYER::PLAYER_2_BLACK)
			return false;
	}

	switch (type[chess])
	{
	case RED_SOLDER:
		if (toPos.y < fromPos.y) return false;
		if (toPos.y - fromPos.y + fabs(toPos.x - fromPos.x) > 1) return false;
		break;

	case BLACK_SOLDER:
		if (toPos.y > fromPos.y) return false;
		if (fromPos.y - toPos.y + fabs(toPos.x - fromPos.x) > 1) return false;
		break;

	case RED_ADVISOR:
	case BLACK_ADVISOR:
		if (int(fabs(toPos.x - fromPos.x)) != 1 ||
			int(fabs(toPos.y - fromPos.y)) != 1)
			return false;
		break;

	case RED_ELEPHANT:
	case BLACK_ELEPHANT:
		if (int(fabs(toPos.x - fromPos.x)) != 2 || int(fabs(toPos.y - fromPos.y)) != 2)  return false;
		if (_map[int((fromPos.x + toPos.x) / 2)][int((fromPos.y + toPos.y) / 2)] != PiecesState::BLANK) return false;
		break;

	case RED_KING:
		if (toPos.x == _chessPos[16].x && toPos.y == _chessPos[16].y)
		{
			bool flag = true;
			for (int i = fromPos.y + 1; i <= toPos.y - 1; i++){
				if (_map[int(fromPos.x)][i] != 32)
					flag = false;
			}
			return flag;
		}
		else if (int(fabs(toPos.x - fromPos.x)) + int(fabs(toPos.y - fromPos.y)) > 1) return false;
		break;
	case BLACK_KING:
		if (toPos.x == _chessPos[0].x && toPos.y == _chessPos[0].y) {
			bool flag = true;
			for (int i = fromPos.y - 1; i >= toPos.y + 1; i--){
				if (_map[int(fromPos.x)][i] != 32)
					flag = false;
			}
			return flag;
		}
		else if (int(fabs(toPos.x - fromPos.x)) + int(fabs(toPos.y - fromPos.y)) > 1) return false;
		break;

	case RED_CAR:
	case BLACK_CAR:
		if (fromPos.y != toPos.y && fromPos.x != toPos.x) return false;

		if (fromPos.y == toPos.y)
		{
			if (fromPos.x < toPos.x) {
				for (int i = int(fromPos.x) + 1; i < int(toPos.x); i++) {
					if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
						return false;
				}
			}
			else {
				for (int i = int(toPos.x) + 1; i < int(fromPos.x); i++) {
					if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
						return false;
				}
			}
		}
		else
		{
			if (fromPos.y < toPos.y) {
				for (int i = int(fromPos.y) + 1; i < int(toPos.y); i++) {
					if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
						return false;
				}
			}
			else {
				for (int i = int(toPos.y) + 1; i < int(fromPos.y); i++) {
					if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
						return false;
				}
			}
		}
		break;

	case RED_CANON:
	case BLACK_CANON:
		if (fromPos.y != toPos.y && fromPos.x != toPos.x) return false;

		if (_map[int(toPos.x)][int(toPos.y)] != PiecesState::BLANK)
		{
			int count = 0;
			if (fromPos.y == toPos.y)
			{
				if (fromPos.x < toPos.x) {
					for (int i = int(fromPos.x) + 1; i < int(toPos.x); i++) {
						if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
							count++;
					}
					if (count != 1) return false;
				}
				else {
					for (int i = int(toPos.x) + 1; i < int(fromPos.x); i++) {
						if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
							count++;
					}
					if (count != 1) return false;
				}
			}
			else
			{
				if (fromPos.y < toPos.y) {
					for (int i = int(fromPos.y) + 1; i < int(toPos.y); i++) {
						if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
							count++;
					}
					if (count != 1) return false;
				}
				else {
					for (int i = int(toPos.y) + 1; i < int(fromPos.y); i++) {
						if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
							count++;
					}
					if (count != 1) return false;
				}
			}
		}
		else
		{
			if (fromPos.y == toPos.y)
			{
				if (fromPos.x < toPos.x) {
					for (int i = int(fromPos.x) + 1; i < int(toPos.x); i++) {
						if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
							return false;
					}
				}
				else {
					for (int i = int(toPos.x) + 1; i < int(fromPos.x); i++) {
						if (_map[i][int(fromPos.y)] != PiecesState::BLANK)
							return false;
					}
				}
			}
			else
			{
				if (fromPos.y < toPos.y) {
					for (int i = int(fromPos.y) + 1; i < int(toPos.y); i++) {
						if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
							return false;
					}
				}
				else {
					for (int i = int(toPos.y) + 1; i < int(fromPos.y); i++) {
						if (_map[int(fromPos.x)][i] != PiecesState::BLANK)
							return false;
					}
				}
			}
		}

		break;

	case RED_HORSE:
	case BLACK_HORSE:
		if (!(
			(int(fabs(toPos.x - fromPos.x)) == 2 && int(fabs(toPos.y - fromPos.y)) == 1) ||
			(int(fabs(toPos.x - fromPos.x)) == 1 && int(fabs(toPos.y - fromPos.y)) == 2)))
			return false;

		int x, y;
		if (int(toPos.x - fromPos.x) == 2) {
			x = fromPos.x + 1;
			y = fromPos.y;
		}
		else if (int(toPos.x - fromPos.x) == -2) {
			x = fromPos.x - 1;
			y = fromPos.y;
		}
		else if (int(toPos.y - fromPos.y) == 2) {
			x = fromPos.x;
			y = fromPos.y + 1;
		}
		else if (int(toPos.y - fromPos.y) == -2) {
			x = fromPos.x;
			y = fromPos.y - 1;
		}

		if (_map[x][y] != PiecesState::BLANK)
			return false;
		break;
	}
	return true;
}

int Board::getSide(int chess)
{
	if (chess <= 15) return PLAYER::PLAYER_1_RED;
	else if (chess <= 31) return PLAYER::PLAYER_2_BLACK;
	else return -1;
}

bool Board::isValidPosition(ChessType type, Point targetPoint)
{
	if (targetPoint.x < 0 || targetPoint.x >(BOARD_WIDTH - 1) || targetPoint.y < 0 || targetPoint.y >(BOARD_HEIGHT - 1))
		return false;

	switch (type)
	{
	case RED_KING:
		if (targetPoint.x == _chessPos[16].x && targetPoint.y == _chessPos[16].y && _chessPos[0].x == _chessPos[16].x)
			return true;
		if (targetPoint.x < 3 || targetPoint.x > 5 || targetPoint.y > 2 || targetPoint.y < 0)
			return false;
		break;

	case BLACK_KING:
		if (targetPoint.x == _chessPos[0].x && targetPoint.y == _chessPos[0].y && _chessPos[0].x == _chessPos[16].x)
			return true;
		if (targetPoint.x < 3 || targetPoint.x > 5 || targetPoint.y > 9 || targetPoint.y < 7)
		{
			return false;
		}
		break;
	case RED_ADVISOR:
		if (!(
			(targetPoint.x == 3 && targetPoint.y == 0) ||
			(targetPoint.x == 5 && targetPoint.y == 0) ||
			(targetPoint.x == 4 && targetPoint.y == 1) ||
			(targetPoint.x == 3 && targetPoint.y == 2) ||
			(targetPoint.x == 5 && targetPoint.y == 2)))
			return false;
		break;

	case BLACK_ADVISOR:
		if (!(
			(targetPoint.x == 3 && targetPoint.y == 9) ||
			(targetPoint.x == 5 && targetPoint.y == 9) ||
			(targetPoint.x == 4 && targetPoint.y == 8) ||
			(targetPoint.x == 3 && targetPoint.y == 7) ||
			(targetPoint.x == 5 && targetPoint.y == 7)))
			return false;
		break;

	case RED_ELEPHANT:
		if (!(
			(targetPoint.x == 2 && targetPoint.y == 0) ||
			(targetPoint.x == 6 && targetPoint.y == 0) ||
			(targetPoint.x == 0 && targetPoint.y == 2) ||
			(targetPoint.x == 4 && targetPoint.y == 2) ||
			(targetPoint.x == 8 && targetPoint.y == 2) ||
			(targetPoint.x == 2 && targetPoint.y == 4) ||
			(targetPoint.x == 6 && targetPoint.y == 4)))
			return false;
		break;

	case BLACK_ELEPHANT:
		if (!(
			(targetPoint.x == 2 && targetPoint.y == 9) ||
			(targetPoint.x == 6 && targetPoint.y == 9) ||
			(targetPoint.x == 0 && targetPoint.y == 7) ||
			(targetPoint.x == 4 && targetPoint.y == 7) ||
			(targetPoint.x == 8 && targetPoint.y == 7) ||
			(targetPoint.x == 2 && targetPoint.y == 5) ||
			(targetPoint.x == 6 && targetPoint.y == 5)))
			return false;
		break;

	case RED_SOLDER:
		if (targetPoint.y < 3) return false;
		if (targetPoint.y < (BOARD_HEIGHT / 2) && int(targetPoint.x) % 2 == 1) return false;
		break;

	case BLACK_SOLDER:
		if (targetPoint.y > 6) return false;
		if (targetPoint.y >= (BOARD_HEIGHT / 2) && int(targetPoint.x) % 2 == 1) return false;
		break;
	}
	return true;
}

int Board::getIndexList(int row, int col)
{
	return row + col * BOARD_WIDTH;
}

void Board::initChineseChessSprite()
{
	int numberOfSprite = sizeof(listChessSpriteId) / sizeof(int);

	for (int i = 0; i < numberOfSprite; i++) {
		if (_chessPos[i].x != PiecesState::WAS_BEATING)
		{
			Point localPos = _chessPos[i];
			Point originPos = changeLocalCoordinateParent(*_centerPoint.at(getIndexList(localPos.x, localPos.y)));

			Sprite* sprite = (Sprite*)this->getChildByTag(i);

			if (sprite == nullptr)
			{
				sprite = ResourcesManager::getInstance()->createSpriteWithId(listChessSpriteId[i]);
				sprite->setAnchorPoint(Point(0.5f, 0.5f));
				sprite->setTag(i);
				this->addChild(sprite, zValue::zForeground);
			}
			sprite->setPosition(originPos);
		}
	}
}

void Board::initBorderPieces()
{
	Sprite* borderPieces = (Sprite*)this->getChildByTag(HIGHLIGHTED_NODE);
	if (borderPieces == nullptr)
	{
		borderPieces = ResourcesManager::getInstance()->createSpriteWithId(INDEX_OF_IMAGE_BORDER_PIECE_FRAME_NAME);
		borderPieces->setTag(HIGHLIGHTED_NODE);
		borderPieces->setVisible(false);
		this->addChild(borderPieces, zValue::zMiddleground);
	}
}

bool Board::isRed(int x, int y)
{
	return getSide(_map[x][y]) == PLAYER::PLAYER_1_RED;
}

bool Board::isBlack(int x, int y)
{
	return getSide(_map[x][y]) == PLAYER::PLAYER_2_BLACK;
}

bool Board::isEmpty(int x, int y)
{
	return _map[x][y] == PiecesState::BLANK;
}

bool Board::init()
{
	Sprite::init();
	addGrid();
	addCenterPoints();
	initChineseChessSprite();
	this->setAnchorPoint(Point(1, 0));
	return true;
}

void Board::updateBoard()
{
	for (int i = 0; i < 32; i++) {
		if (_chessPos[i].x == PiecesState::WAS_BEATING) {
			Node* chessNode = this->getChildByTag(i);
			if (chessNode != nullptr) {
				chessNode->removeFromParentAndCleanup(true);
			}
		}
	}
}

void Board::updateHighlight()
{
	int currentTurn = getCurrentTurn();

	// Update player highlight

	if (_mode == Mode::PLAYER_VS_PLAYER)
	{
		if (currentTurn == PLAYER::PLAYER_1_RED)
		{
			onHighlightPlayer(_selectedChessPlayer1);
			onHightlightCanMoveFromChess(_selectedChessPlayer1);
		}
		else if (currentTurn == PLAYER::PLAYER_2_BLACK)
		{
			onHighlightPlayer(_selectedChessPlayer2);
			onHightlightCanMoveFromChess(_selectedChessPlayer2);
		}
	}

	else if (_mode == Mode::PLAYER_VS_COMPUTER)
	{
		if (currentTurn == PLAYER_1_RED)
		{
			onHighlightPlayer(_selectedChessPlayer1);
			onHightlightCanMoveFromChess(_selectedChessPlayer1);
		}
	}
}

void Board::updateChessSelection(Point pos)
{
	int typeChess = getChessInPosition(pos);

	if (_mode == Mode::PLAYER_VS_PLAYER)
	{
		if (typeChess != PiecesState::BLANK)
		{
			if (this->getCurrentTurn() == PLAYER::PLAYER_1_RED && this->getSide(typeChess) == PLAYER::PLAYER_1_RED)
			{
				onDeHightlightCanMoveFromChess();
				onDeselectPlayer(_selectedChessPlayer1);
				_selectedChessPlayer1 = this->getChildByTag(typeChess);
			}

			else if (this->getCurrentTurn() == PLAYER::PLAYER_2_BLACK && this->getSide(typeChess) == PLAYER::PLAYER_2_BLACK)
			{
				onDeHightlightCanMoveFromChess();
				onDeselectPlayer(_selectedChessPlayer2);
				_selectedChessPlayer2 = this->getChildByTag(typeChess);
			}
		}
	}

	else if (_mode == Mode::PLAYER_VS_COMPUTER)
	{
		int currentTurn = getCurrentTurn();
		int firstPlayer = GameplayManager::getInstance()->getFirstPlayer();

		if (currentTurn == PLAYER_1_RED  && currentTurn == this->getSide(typeChess))
		{
			onDeHightlightCanMoveFromChess();
			onDeselectPlayer(_selectedChessPlayer1);
			_selectedChessPlayer1 = this->getChildByTag(typeChess);
		}
	}
}

void Board::endTurn(Point pos)
{
	int currentTurn = getCurrentTurn();

	CallFunc* callComputerOnExit = nullptr;

	if (_mode == Mode::PLAYER_VS_COMPUTER)
	{
		int currentTurn = getCurrentTurn();
		int firstPlayer = GameplayManager::getInstance()->getFirstPlayer();

		if (currentTurn == PLAYER_1_RED)
		{
			callComputerOnExit = CallFunc::create(CC_CALLBACK_0(Board::onStartNewComputer, this));
			movingChessCallback((Sprite*&)_selectedChessPlayer1, (Sprite*&)_selectedChessPlayer2, pos, callComputerOnExit);
		}
		else
		{
			movingChessCallback((Sprite*&)_selectedChessPlayer2, (Sprite*&)_selectedChessPlayer1, pos, callComputerOnExit);
		}
	}

	else if (_mode == Mode::PLAYER_VS_PLAYER)
	{
		if (currentTurn == PLAYER_1_RED && _selectedChessPlayer1 != nullptr)
		{
			movingChessCallback((Sprite*&)_selectedChessPlayer1, (Sprite*&)_selectedChessPlayer2, pos, callComputerOnExit);
		}
		else if (currentTurn == PLAYER_2_BLACK && _selectedChessPlayer2 != nullptr)
		{
			movingChessCallback((Sprite*&)_selectedChessPlayer2, (Sprite*&)_selectedChessPlayer1, pos, callComputerOnExit);
		}
	}
}

void Board::updateTouchEachTurn(Point pos)
{
	if (!_isChessPieceMoving && !_gameOver)
	{
		Point pClosest = getClosestCenterPointFrom(pos);

		this->updateChessSelection(pClosest);
		this->updateHighlight();
		this->endTurn(pClosest);
	}
}

void Board::updateComputerTurn(Point pos)
{
	if (_isChessPieceMoving && !_gameOver)
	{
		this->updateChessSelection(pos);
		this->updateHighlight();
		this->endTurn(pos);
	}
}

int Board::getCurrentTurn()
{
	return side;
}

void Board::setCurrentTurn(int turn)
{
	side = turn;
}

int Board::getMode()
{
	return _mode;
}
void Board::setMode(Mode mode)
{
	_mode = mode;
}

void Board::movingChessCallback(Sprite* &playerMoving, Sprite* &playerUnhighlight, Point pos, CallFunc* callFunc)
{
	if (playerMoving != nullptr)
	{
		int chessDestinationType = _map[(int)pos.x][(int)pos.y];
		Sprite* chessFadeOut = (Sprite*)this->getChildByTag(chessDestinationType);
		bool isMoved = move(playerMoving->getTag(), pos, MoveCondition::CHESSMATE);
		
		// On moving player
		if (isMoved)
		{
			setIsChessPieceMoving(true);

			Point worldPoint = Point(pos.x * GRID_X, pos.y * GRID_Y);
			Point normalizedLocalPoint = changeLocalCoordinateParent(worldPoint);
			MoveTo* moveAction = MoveTo::create(0.4f, normalizedLocalPoint);
			EaseOut* moveWithEaseAction = EaseOut::create(moveAction, 4.0f);

			onDeHightlightCanMoveFromChess();
			onMovingPlayer(playerMoving);

			int currentSide = getSide(playerMoving->getTag());
			int opponentSide = 1 - currentSide;


			// ANIMATION WHEN CHECK OR CHECKMATE
			// Check
			if (isChessMate(opponentSide) == 1)
			{
				AudioManager::getInstance()->playEffect(INDEX_OF_CHECK_MP3, "mp3");
				onCheckAnimation(currentSide);
			}
			// Chessmate
			else if (isChessMate(opponentSide) == 2)
			{
				_gameOver = true;
				onChessmateAnimation(currentSide);
				if (_mode == Mode::PLAYER_VS_PLAYER || (_mode == Mode::PLAYER_VS_COMPUTER && side == PLAYER::PLAYER_1_RED))
				{
					AudioManager::getInstance()->playEffect(INDEX_OF_WIN_GAME_MP3, "mp3");
				}
				else if (_mode == Mode::PLAYER_VS_COMPUTER && side == PLAYER::PLAYER_2_BLACK)
				{
					AudioManager::getInstance()->playEffect(INDEX_OF_LOSE_GAME_MP3, "mp3");
				}
			}

			// Remove highlight from player don't move 
			if (playerUnhighlight != nullptr)
			{
				onDeselectPlayer(playerUnhighlight);
				playerUnhighlight = nullptr;
			}

			switchTurn();
			GameplayManager::getInstance()->saveOldGame();

			CallFunc* endMoving = CallFunc::create([&]{
				setIsChessPieceMoving(false);
			});

			// If kill opponent, add additional fade and flare animation
			if (chessFadeOut != nullptr && (chessDestinationType != PiecesState::BLANK || chessDestinationType != PiecesState::WAS_BEATING))
			{
				FadeTo* fadeAction = FadeTo::create(0.3f, 255);
				chessFadeOut->runAction(fadeAction);
				Spawn* onKillEnemyChessAction =
					Spawn::create(
					CallFunc::create(CC_CALLBACK_0(Board::onExitEndTurn, this)),
					CallFunc::create(CC_CALLBACK_0(Board::animationFlareCallback, this, normalizedLocalPoint)),
					nullptr);

				playerMoving->runAction(Sequence::create(
					Spawn::create(moveWithEaseAction, Sequence::create(DelayTime::create(0.15f), onKillEnemyChessAction, DelayTime::create(1.0f), endMoving, nullptr), nullptr),
					callFunc,
					nullptr));
			}

			// Else just move
			else
			{
				playerMoving->runAction(Sequence::create(moveWithEaseAction, CallFunc::create(CC_CALLBACK_0(Board::onExitEndTurn, this)), endMoving, callFunc, nullptr));
			}
		}
		else
		{
			Point movingPlayerPosition = _chessPos[playerMoving->getTag()];
			if (!pos.equals(movingPlayerPosition))
			{
				std::vector<Point*> moveList = getMoveList(playerMoving->getTag());
				bool canMove = false;
				for (Point* p : moveList)
				{
					if (p->equals(pos))
					{
						canMove = true;
						break;
					}
				}
				if (!canMove) AudioManager::getInstance()->playEffect(INDEX_OF_WRONG_MOVE_MP3, "mp3");
			}
		}
	}
}

void Board::onExitEndTurn()
{
	this->updateBoard();
}

void Board::onStartNewComputer()
{
	if (!_gameOver)
	{
		setIsChessPieceMoving(true);
		//std::thread t1(CC_CALLBACK_0(Board::callBackOnStartNewComputer, this));
		//t1.detach();
		callBackOnStartNewComputer();
	}
}

void Board::callBackOnStartNewComputer()
{
	int chessComputer;
	Point computerMove;
	bool result = _bot->generateMove(_map, _chessPos, side, chessComputer, computerMove);
	_selectedChessPlayer2 = this->getChildByTag(chessComputer);
	if (result && _selectedChessPlayer2 != nullptr)
	{
		updateComputerTurn(computerMove);
	}
}

int Board::generateComputerChess(int side)
{
	return RandomHelper::random_int(16 * side, 15 + 16 * side);
}
Point Board::generateComputerChessPosition(int side, int &chess)
{
	Point tmp = Point(0.0f, 0.0f);
	do
	{
		tmp = Point((float)RandomHelper::random_int(0, 8), (float)RandomHelper::random_int(0, 9));
		chess = RandomHelper::random_int(16 * side, 15 + 16 * side);
	} while (!canMove(chess, _chessPos[chess], tmp) || isCheck(chess, tmp) || this->getChildByTag(chess) == nullptr);
	return tmp;
}

void Board::switchTurn()
{
	side = 1 - side;
}

void Board::onHighlightPlayer(Node* chessSprite)
{
	if (chessSprite != nullptr)
	{
		Color3B colorChess = chessSprite->getColor();
		if (!colorChess.equals(Color3B(200, 255, 80)))
		{
			AudioManager::getInstance()->playEffect(INDEX_OF_SELECT_CHESS_WAV, "wav");
			chessSprite->setColor(Color3B(200, 255, 80));
		}
	}
	else
	{
		AudioManager::getInstance()->playEffect(INDEX_OF_WRONG_MOVE_MP3, "mp3");
	}
}
void Board::onHighlightOpponent(Node* chessSprite)
{
	onAnimationHighlight(chessSprite, Color4F::ORANGE);
}
void Board::onDeselectPlayer(Node* chessSprite)
{
	if (chessSprite != nullptr)
	{
		chessSprite->setOpacity(255);
		chessSprite->setColor(Color3B(255, 255, 255));
	}
}

void Board::onMovingPlayer(Node* chessSprite)
{
	AudioManager::getInstance()->playEffect(INDEX_OF_MOVE_CHESS_MP3, "mp3");
	onAnimationHighlight(chessSprite, Color4F::ORANGE);
}

void Board::animationFlareCallback(Point pos)
{
	Sprite* flareSprite = ResourcesManager::getInstance()->createSpriteWithId(INDEX_OF_ANIMATION_FLARE_FRAME_NAME);

	this->addChild(flareSprite, zValue::zForeground);

	Vector<SpriteFrame*> flareFrames = ResourcesManager::getInstance()->createVectorSpriteFramesWithId(INDEX_OF_ANIMATION_FLARE_FRAME_NAME);
	Animation* animationFlare = Animation::createWithSpriteFrames(flareFrames, 0.02f);
	Animate* animateFlare = Animate::create(animationFlare);
	Repeat* flareAction = Repeat::create(animateFlare, 1);

	flareSprite->setAnchorPoint(Point(0.5f, 0.5f));
	flareSprite->setPosition(pos);
	flareSprite->runAction(Sequence::create(flareAction, RemoveSelf::create(), nullptr));

	flareFrames.clear();

	AudioManager::getInstance()->playEffect(INDEX_OF_KILL_CHESS_WAV, "wav");
}

void Board::onAnimationHighlight(Node* node, Color4F color)
{
	if (node != nullptr)
	{
		node->setOpacity(255);
		node->setColor(Color3B(255, 255, 255));

		Sprite* borderPieces = nullptr;

		for (int i = 0; i < 32; i++)
		{
			Node* pNode = this->getChildByTag(i);
			// If boreder pieces != nullptr, return it and end loop
			if (pNode != nullptr && borderPieces == nullptr) borderPieces = (Sprite*)pNode->getChildByTag(HIGHLIGHTED_NODE);
		}

		if (borderPieces != nullptr)
		{
			borderPieces->removeFromParentAndCleanup(true);
			borderPieces = nullptr;
		}

		borderPieces = ResourcesManager::getInstance()->createSpriteWithId(INDEX_OF_IMAGE_BORDER_PIECE_FRAME_NAME);
		borderPieces->setAnchorPoint(Point(0.5f, 0.5f));
		borderPieces->setVisible(true);
		borderPieces->setTag(HIGHLIGHTED_NODE);
		borderPieces->setPosition(GRID_X / 2, GRID_Y / 2);

		ScaleTo* scaleSmall = ScaleTo::create(0.8f, 0.85f);
		ScaleTo* scaleBig = ScaleTo::create(0.8f, 0.9f);
		Sequence* scaleRepeat = Sequence::create(scaleSmall, scaleBig, nullptr);

		RepeatForever* actionBorderPieces = RepeatForever::create(scaleRepeat);
		borderPieces->runAction(actionBorderPieces);
		node->addChild(borderPieces, zValue::zMiddleground);
	}
}

void Board::setIsChessPieceMoving(bool isMove)
{
	_isChessPieceMoving = isMove;
}
bool Board::isChessPieceMoving()
{
	return _isChessPieceMoving;
}

std::vector<Point*> Board::getMoveList(int index) {
	std::vector<Point*> result = std::vector<Point*>();

	if (index < 0 || index > 31)
		return result;

	int x = int(_chessPos[index].x);
	int y = int(_chessPos[index].y);
	ChessType t = type[index];

	// Check all can move conditions
	switch (t)
	{
	case RED_CAR:
	case BLACK_CAR:
	case RED_CANON:
	case BLACK_CANON:
		for (int i = 0; i <= 8; i++) {
			Point* p = new Point();
			p->x = i;
			p->y = y;
			if (canMove(index, _chessPos[index], *p))
				result.push_back(p);
		}

		for (int i = 0; i <= 9; i++) {
			Point* p = new Point();
			p->x = x;
			p->y = i;
			if (canMove(index, _chessPos[index], *p))
				result.push_back(p);
		}
		break;
	default:
		for (int i = (x - 2); i <= (x + 2); i++) {
			for (int j = (y - 2); j <= (y + 2); j++) {
				if (i < 0 || i > 8 || j < 0 || j > 9)
					continue;
				if (i == x && j == y)
					continue;
				Point* p = new Point();
				p->x = i;
				p->y = j;
				if (canMove(index, _chessPos[index], *p))
					result.push_back(p);
			}
		}
	}

	// If that move make you was checked, remove it
	std::vector<Point*>::iterator it = result.begin();
	while (it != result.end())
	{
		Point pCheck = Point((*it)->x, (*it)->y);
		if (isCheck(index, pCheck))
		{
			it = result.erase(it);
		}
		else
		{
			++it;
		}
	}

	return result;
}

Sprite* Board::createGlowEffectSprite(const Color3B& colour, const Size& size)
{

	Sprite* glowSprite = ResourcesManager::getInstance()->createSpriteWithId(INDEX_OF_IMAGE_PARTICLE_FIRE_FRAME_NAME);
	glowSprite->setColor(colour);
	glowSprite->setAnchorPoint(Point(0.5f, 0.5f));

	BlendFunc f = { GL_ONE, GL_ONE };
	glowSprite->setBlendFunc(f);

	// Run some animation which scales a bit the glow
	Sequence* s1 = Sequence::createWithTwoActions(ScaleTo::create(0.9f, size.width, size.height),
		ScaleTo::create(0.9f, size.width*0.9f, size.height*0.9f));

	RepeatForever* r = RepeatForever::create(s1);
	glowSprite->runAction(r);
	return glowSprite;
}

void Board::onHightlightCanMoveFromChess(Node* chessSprite)
{
	if (chessSprite != nullptr)
	{
		int typeChess = chessSprite->getTag();
		int j = 0;// number of node can defeat
		std::vector<Point*> listMove = getMoveList(typeChess);
		for (unsigned int i = 0; i < listMove.size(); i++)
		{
			Point p = *listMove.at(i);
			Point pNormalized = changeLocalCoordinateParent(p);

			//Highlight can move node
			Sprite* highlightCanMoveNode = (Sprite*)this->getChildByTag(HIGHLIGHTED_NODE_CAN_MOVING_TAG + i);
			if (highlightCanMoveNode == nullptr)
			{
				highlightCanMoveNode = ResourcesManager::getInstance()->createSpriteWithId(INDEX_OF_IMAGE_CIRCLE_CAN_MOVE_FRAME_NAME);
				highlightCanMoveNode->setTag(HIGHLIGHTED_NODE_CAN_MOVING_TAG + i);
				this->addChild(highlightCanMoveNode, zValue::zMiddleground);
			}
			highlightCanMoveNode->setPosition(pNormalized.x * GRID_X, pNormalized.y * GRID_Y);

			// Highlight chess can be defeated
			int index = _map[(int)p.x][(int)p.y];
			if (index != PiecesState::BLANK && index != PiecesState::WAS_BEATING)
			{
				Sprite* chessCanDefeated = (Sprite*)this->getChildByTag(index);
				if (chessCanDefeated != nullptr)
				{
					Sprite* glowEffectSprite = (Sprite*)this->getChildByTag(HIGHLIGHTED_CHESS_CAN_DEFEATED_TAG + j);
					if (glowEffectSprite == nullptr)
					{
						glowEffectSprite = createGlowEffectSprite(Color3B::GREEN, Size(4, 4));
						glowEffectSprite->setTag(HIGHLIGHTED_CHESS_CAN_DEFEATED_TAG + j);
						this->addChild(glowEffectSprite, zValue::zForeground + 2);
					}
					glowEffectSprite->setPosition(pNormalized.x * GRID_X, pNormalized.y * GRID_Y);
					j++;
				}
			}
		}
		listMove.clear();
	}
}


void Board::onDeHightlightCanMoveFromChess()
{
	int i = 0;
	Sprite* highlightNode = nullptr;
	do
	{
		highlightNode = (Sprite*)this->getChildByTag(HIGHLIGHTED_NODE_CAN_MOVING_TAG + i);
		if (highlightNode != nullptr)
		{
			highlightNode->setVisible(false);
			highlightNode->removeFromParentAndCleanup(true);
		}
		i++;
	} while (highlightNode != nullptr);

	i = 0;
	Sprite* highlightDefeatNode = nullptr;
	do
	{
		highlightDefeatNode = (Sprite*)this->getChildByTag(HIGHLIGHTED_CHESS_CAN_DEFEATED_TAG + i);
		if (highlightDefeatNode != nullptr)
		{
			highlightDefeatNode->setVisible(false);
			highlightDefeatNode->removeFromParentAndCleanup(true);
		}
		i++;
	} while (highlightDefeatNode != nullptr);
}

void Board::onChessmateAnimation(int side)
{
	Size visibileSize = Director::getInstance()->getVisibleSize();
	Label* chessmateLabel = ResourcesManager::getInstance()->createLabelTTFWithId("", INDEX_OF_MARKER_FELT_TTF, 50);
	chessmateLabel->setPosition(visibileSize.width / 2, visibileSize.height / 2);

	if (side == PLAYER::PLAYER_1_RED)
	{
		chessmateLabel->setString("RED WIN");
		chessmateLabel->setPosition(visibileSize.width / 2, visibileSize.height / 2 - GRID_Y * 5.5);
		chessmateLabel->setColor(Color3B::RED);
	}
	else if (side == PLAYER::PLAYER_2_BLACK)
	{
		chessmateLabel->setString("BLACK WIN");
		chessmateLabel->setPosition(visibileSize.width / 2, visibileSize.height / 2 + GRID_Y * 6);
		chessmateLabel->setColor(Color3B::BLACK);
	}

	Director::getInstance()->getRunningScene()->addChild(chessmateLabel, zValue::zForeground + 5);
}
void Board::onCheckAnimation(int side)
{
	Size visibileSize = Director::getInstance()->getVisibleSize();

	Label* checkLabel = ResourcesManager::getInstance()->createLabelTTFWithId("CHECK", INDEX_OF_MARKER_FELT_TTF, 50);
	if (side == PLAYER::PLAYER_1_RED)
	{
		checkLabel->setPosition(visibileSize.width / 2, visibileSize.height / 2 - GRID_Y * 5.5);
		checkLabel->setColor(Color3B::RED);
	}
	else if (side == PLAYER::PLAYER_2_BLACK)
	{
		checkLabel->setPosition(visibileSize.width / 2, visibileSize.height / 2 + GRID_Y * 6);
		checkLabel->setColor(Color3B::BLACK);
	}
	Director::getInstance()->getRunningScene()->addChild(checkLabel, zValue::zForeground + 5);
	Sequence* action = Sequence::create(DelayTime::create(2.0f), RemoveSelf::create(), nullptr);
	checkLabel->runAction(action);
}

bool Board::isCheck(int side) {
	// Side Which being checkmate
	for (int i = startIndexSide[1 - side] + 1; i < endIndexSide[1 - side]; i++) {
		if (canMove(i, _chessPos[i], _chessPos[16 * side]))
			return true;
	}

	if (isTwoKingHeadToHead()) return true;
	return false;
}

bool Board::isCheck(int chess, Point pos)
{
	Point backupChessPos[32];
	std::copy(std::begin(_chessPos), std::end(_chessPos), std::begin(backupChessPos));

	int backupMap[9][10];
	std::copy(reinterpret_cast<int*>(_map), reinterpret_cast<int*>(_map)+9 * 10, reinterpret_cast<int*>(backupMap));

	bool isMoved = move(chess, pos);
	bool isChessmate = isCheck(getSide(chess));

	// If you moved successfully, rollback
	if (isMoved)
	{
		std::copy(std::begin(backupChessPos), std::end(backupChessPos), std::begin(_chessPos));
		std::copy(reinterpret_cast<int*>(backupMap), reinterpret_cast<int*>(backupMap)+9 * 10, reinterpret_cast<int*>(_map));
	}

	return isChessmate;
}

bool Board::isTwoKingHeadToHead()
{
	Point pRedKing = _chessPos[0];
	bool flagKing = false;
	bool flagObstacle = false;
	for (int i = (int)pRedKing.y + 1; i < BOARD_HEIGHT; i++)
	{
		if (_map[(int)pRedKing.x][(int)i] == 16)
		{
			flagKing = true;
			break;
		}
		if (_map[(int)pRedKing.x][(int)i] != PiecesState::WAS_BEATING && _map[(int)pRedKing.x][(int)i] != PiecesState::BLANK)
		{
			flagObstacle = true;
		}
	}
	if (flagKing && !flagObstacle) return true;
	return false;
}

int Board::isChessMate(int side) {
	// Side Which being checkmate

	// Clone Board
	Board* b = new Board();

	for (int i = 0; i < 32; i++) {
		b->_chessPos[i].x = _chessPos[i].x;
		b->_chessPos[i].y = _chessPos[i].y;
	}
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 10; j++) {
			b->_map[i][j] = _map[i][j];
		}
	}

	// If Chessmate Side can Eat other King, return false
	int chessArray[125], count = 0;
	Point targetPoint[125];
	if (!AIBot::getMoveList(b->_map, b->_chessPos, side, chessArray, targetPoint, count)) {
		if (count > 0)
			return 0;
		if (count == 0)
			return 2;
	}

	// Test chess
	bool chess = 0;
	for (int i = startIndexSide[1 - side]; i < endIndexSide[1 - side]; i++) {
		if (b->canMove(i, b->_chessPos[i], b->_chessPos[16 * side])) {
			chess = 1;
			break;
		}
	}

	int xs, ys, ate;

	for (int i = 0; i < count; i++) {
		xs = int(b->_chessPos[chessArray[i]].x);
		ys = int(b->_chessPos[chessArray[i]].y);
		ate = b->_map[int(targetPoint[i].x)][int(targetPoint[i].y)];
		if (ate != 32)
			b->_chessPos[ate].x = -1;

		b->_map[int(targetPoint[i].x)][int(targetPoint[i].y)] = chessArray[i];
		b->_map[int(b->_chessPos[chessArray[i]].x)][int(b->_chessPos[chessArray[i]].y)] = 32;
		b->_chessPos[chessArray[i]] = targetPoint[i];

		bool chess2 = false;
		for (int k = startIndexSide[1 - side]; k < endIndexSide[1 - side]; k++) {
			if (b->canMove(k, b->_chessPos[k], b->_chessPos[16 * side])) {
				chess2 = true;
				break;
			}
		}

		if (!chess2)
			return chess;

		b->_chessPos[chessArray[i]].x = xs;
		b->_chessPos[chessArray[i]].y = ys;
		b->_map[xs][ys] = chessArray[i];

		if (ate != 32)
			b->_chessPos[ate] = targetPoint[i];
		b->_map[int(targetPoint[i].x)][int(targetPoint[i].y)] = ate;
	}
	return 2;
}