#include "components\AIBot.h"
#include "Constant.h"

AIBot::AIBot() {
	first = true;
}

bool AIBot::generateMove(int map[9][10], Point chessPos[32], int side, int &resultChess, Point &resultPoint)
{
	// Clone Board
	int _map[9][10];
	Point _chessPos[32];
	for (int i = 0; i < 32; i++) {
		_chessPos[i].x = chessPos[i].x;
		_chessPos[i].y = chessPos[i].y;
	}
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 10; j++) {
			_map[i][j] = map[i][j];
		}
	}

	int chessArray[125];
	Point targetPoint[125];
	int count = 0;

	if (getMoveList(_map, _chessPos, side, chessArray, targetPoint, count))
	{
		int cur, maxValue, curValue;
		if (_deep >= 2 && count > _width + 2)
		{
			int value[125];
			cur = 0;
			maxValue = -10000;
			while (cur < count)
			{
				curValue = search(_map, _chessPos, side, chessArray[cur], targetPoint[cur], -10000, _deep - 2);
				value[cur] = curValue;
				maxValue = curValue > maxValue ? curValue : maxValue;
				cur++;
			}
			quickSort(value, chessArray, targetPoint, 0, count - 1);
			count = _width;
			if (count > 1 && value[0] != -9999) {
				for (int i = 0; i < _width; i++) {
					if (value[i] == -9999) {
						count = i;
						break;
					}
				}
			}
		}


		if (first) {
			int index = RandomHelper::random_int(0, count - 1);
			resultChess = chessArray[index];
			resultPoint = targetPoint[index];
			first = false;
		}
		else {
			if (count == 1) {
				resultChess = chessArray[0];
				resultPoint = targetPoint[0];
			}
			else {
				cur = 0;
				maxValue = -10000;
				while (cur < count)
				{
					curValue = search(_map, _chessPos, side, chessArray[cur], targetPoint[cur], -10000, _deep);
					if (curValue > maxValue)
					{
						maxValue = curValue;
						resultChess = chessArray[cur];
						resultPoint = targetPoint[cur];
					}
					cur++;
				}
			}
		}
		return true;
	}
	else {
		if (count > 0)
		{
			resultChess = chessArray[count - 1];
			resultPoint = targetPoint[count - 1];
			return true;
		}
		else {
			resultChess = 32;
			resultPoint.x = -1;
			return false;
		}
	}
}

bool AIBot::getMoveList(int map[9][10], Point chessPos[32], int side, int *chessArray, Point *targetPoint, int &count)
{
	int x, y, i, j;
	bool flag1, flag2;
	for (int n = startIndexSide[side]; n < endIndexSide[side]; n++)
	{
		x = int(chessPos[n].x);
		if (x == -1) continue;
		y = int(chessPos[n].y);

		switch (n) {
		case 0: // RED_KING
			// if facing other king
			flag2 = false;
			if (x == int(chessPos[16].x))
			{
				// check no obstacle
				flag1 = false;
				for (int i = y + 1; i < int(chessPos[16].y); i++) {
					if (map[x][i] != 32) {
						flag1 = true;
						break;
					}
				}

				if (!flag1)
				{
					chessArray[count] = n;
					targetPoint[count].x = chessPos[16].x;
					targetPoint[count].y = chessPos[16].y;
					count++;

					return false;
				}
			}

			j = y + 1;
			if (j <= 2 && !AIBot::isRed(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			j = y - 1;
			if (j >= 0 && !AIBot::isRed(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 1;
			if (i <= 5 && !AIBot::isRed(map, i, y)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = y;
				count++;
			}
			i = x - 1;
			if (i >= 3 && !AIBot::isRed(map, i, y)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = y;
				count++;
			}
			break;
		case 1: // RED_ADVISOR
		case 2: // RED_ADVISOR
			i = x + 1; j = y + 1;
			if (i <= 5 && j <= 2 && !AIBot::isRed(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 1; j = y - 1;
			if (i <= 5 && j >= 0 && !AIBot::isRed(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 1; j = y + 1;
			if (i >= 3 && j <= 2 && !AIBot::isRed(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 1; j = y - 1;
			if (i >= 3 && j >= 0 && !AIBot::isRed(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			break;
		case 3: // RED_ELEPHANT 
		case 4: // RED_ELEPHANT
			i = x + 2; j = y + 2;
			if (i <= 8 && j <= 4 && !AIBot::isRed(map, i, j) && AIBot::isEmpty(map, x+1, y+1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 2; j = y - 2;
			if (i <= 8 && j >= 0 && !AIBot::isRed(map, i, j) && AIBot::isEmpty(map, x + 1, y - 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 2; j = y + 2;
			if (i >= 0 && j <= 4 && !AIBot::isRed(map, i, j) && AIBot::isEmpty(map, x - 1, y + 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 2; j = y - 2;
			if (i >= 0 && j >= 0 && !AIBot::isRed(map, i, j) && AIBot::isEmpty(map, x - 1, y - 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			break;
		case 5: // RED_HORSE
		case 6: // RED_HORSE
			i = x + 2;
			if (i <= 8 && AIBot::isEmpty(map, i - 1, y))
			{
				j = y + 1;
				if (j <= 9 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				j = y - 1;
				if (j >= 0 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			i = x - 2;
			if (i >= 0 && AIBot::isEmpty(map, i + 1, y))
			{
				j = y + 1;
				if (j <= 9 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				j = y - 1;
				if (j >= 0 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			j = y + 2;
			if (j <= 9 && AIBot::isEmpty(map, x, j - 1))
			{
				i = x + 1;
				if (i <= 8 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			j = y - 2;
			if (j >= 0 && AIBot::isEmpty(map, x, j + 1))
			{
				i = x + 1;
				if (i <= 8 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isRed(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			break;
		case 7: // RED_CAR
		case 8: // RED_CAR
			i = x + 1;
			while (i <= 8)
			{
				if (AIBot::isEmpty(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				else {
					if (AIBot::isBlack(map, i, y)) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
					break;
				}
				i++;
			}
			i = x - 1;
			while (x >= 0)
			{
				if (AIBot::isEmpty(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				else {
					if (AIBot::isBlack(map, i, y)) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
					break;
				}
				i--;
			}
			j = y + 1;
			while (j <= 9)
			{
				if (AIBot::isEmpty(map, x, j)) {
					chessArray[count] = n;
					targetPoint[count].x = x;
					targetPoint[count].y = j;
					count++;
				}
				else {
					if (AIBot::isBlack(map, x, j)) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
					break;
				}
				j++;
			}
			j = y - 1;
			while (j >= 0)
			{
				if (AIBot::isEmpty(map, x, j)) {
					chessArray[count] = n;
					targetPoint[count].x = x;
					targetPoint[count].y = j;
					count++;
				}
				else {
					if (AIBot::isBlack(map, x, j)) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
					break;
				}
				j--;
			}
			break;
		case 9: // RED_CANON
		case 10: // RED_CANON
			i = x + 1;
			flag1 = false;
			while (i <= 8)
			{
				if (AIBot::isEmpty(map, i, y)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isBlack(map, i, y)) {
							chessArray[count] = n;
							targetPoint[count].x = i;
							targetPoint[count].y = y;
							count++;
						}
						break;
					}
				}
				i++;
			}
			i = x - 1;
			flag1 = false;
			while (i >= 0)
			{
				if (AIBot::isEmpty(map, i, y)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isBlack(map, i, y)) {
							chessArray[count] = n;
							targetPoint[count].x = i;
							targetPoint[count].y = y;
							count++;
						}
						break;
					}
				}
				i--;
			}
			j = y + 1;
			flag1 = false;
			while (j <= 9)
			{
				if (AIBot::isEmpty(map, x, j)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isBlack(map, x, j)) {
							chessArray[count] = n;
							targetPoint[count].x = x;
							targetPoint[count].y = j;
							count++;
						}
						break;
					}
				}
				j++;
			}
			j = y - 1;
			flag1 = false;
			while (j >= 0)
			{
				if (AIBot::isEmpty(map, x, j)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isBlack(map, x, j)) {
							chessArray[count] = n;
							targetPoint[count].x = x;
							targetPoint[count].y = j;
							count++;
						}
						break;
					}
				}
				j--;
			}
			break;
		case 11: // RED_SOLDER
		case 12: // RED_SOLDER
		case 13: // RED_SOLDER
		case 14: // RED_SOLDER
		case 15: // RED_SOLDER
			j = y + 1;
			if (j <= 9 && !AIBot::isRed(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			if (y > 4) {
				i = x + 1;
				if (i <= 8 && !AIBot::isRed(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isRed(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
			}

			break;

		case 16: //BLACK_KING
			// if facing other king
			flag2 = false;
			if (x == int(chessPos[0].x))
			{
				// check no obstacle
				flag1 = false;
				for (int i = y - 1; i > int(chessPos[0].y); i--) {
					if (map[x][i] != 32) {
						flag1 = true;
						break;
					}
				}

				if (!flag1)
				{
					chessArray[count] = n;
					targetPoint[count].x = chessPos[0].x;
					targetPoint[count].y = chessPos[0].y;
					count++;

					return false;
				}
			}

			j = y + 1;
			if (j <= 9 && !AIBot::isBlack(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			j = y - 1;
			if (j >= 7 && !AIBot::isBlack(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 1;
			if (i <= 5 && !AIBot::isBlack(map, i, y)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = y;
				count++;
			}
			i = x - 1;
			if (i >= 3 && !AIBot::isBlack(map, i, y)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = y;
				count++;
			}
			break;
		case 17: // BLACK_ ADVISOR
		case 18: // BLACK_ ADVISOR
			i = x + 1; j = y + 1;
			if (i <= 5 && j <= 9 && !AIBot::isBlack(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 1; j = y - 1;
			if (i <= 5 && j >= 7 && !AIBot::isBlack(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 1; j = y + 1;
			if (i >= 3 && j <= 9 && !AIBot::isBlack(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 1; j = y - 1;
			if (i >= 3 && j >= 7 && !AIBot::isBlack(map, i, j)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			break;
		case 19: // BLACK_ELEPHANT 
		case 20: // BLACK_ELEPHANT
			i = x + 2; j = y + 2;
			if (i <= 8 && j <= 9 && !AIBot::isBlack(map, i, j) && AIBot::isEmpty(map, x + 1, y + 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x + 2; j = y - 2;
			if (i <= 8 && j >= 5 && !AIBot::isBlack(map, i, j) && AIBot::isEmpty(map, x + 1, y - 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 2; j = y + 2;
			if (i >= 0 && j <= 9 && !AIBot::isBlack(map, i, j) && AIBot::isEmpty(map, x - 1, y + 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			i = x - 2; j = y - 2;
			if (i >= 0 && j >= 5 && !AIBot::isBlack(map, i, j) && AIBot::isEmpty(map, x - 1, y - 1)) {
				chessArray[count] = n;
				targetPoint[count].x = i;
				targetPoint[count].y = j;
				count++;
			}
			break;
		case 21: // BLACK_HORSE
		case 22: // BLACK_HORSE
			i = x + 2;
			if (i <= 8 && AIBot::isEmpty(map, i - 1, y))
			{
				j = y + 1;
				if (j <= 9 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				j = y - 1;
				if (j >= 0 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			i = x - 2;
			if (i >= 0 && AIBot::isEmpty(map, i + 1, y))
			{
				j = y + 1;
				if (j <= 9 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				j = y - 1;
				if (j >= 0 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			j = y + 2;
			if (j <= 9 && AIBot::isEmpty(map, x, j - 1))
			{
				i = x + 1;
				if (i <= 8 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			j = y - 2;
			if (j >= 0 && AIBot::isEmpty(map, x, j + 1))
			{
				i = x + 1;
				if (i <= 8 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isBlack(map, i, j)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = j;
					count++;
				}
			}
			break;
		case 23: // BLACK_CAR
		case 24: // BLACK_CAR
			i = x + 1;
			while (i <= 8)
			{
				if (AIBot::isEmpty(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				else {
					if (AIBot::isRed(map, i, y)) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
					break;
				}
				i++;
			}
			i = x - 1;
			while (i >= 0)
			{
				if (AIBot::isEmpty(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				else
				{
					if (AIBot::isRed(map, i, y)) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
					break;
				}
				i--;
			}
			j = y + 1;
			while (j <= 9)
			{
				if (AIBot::isEmpty(map, x, j)) {
					chessArray[count] = n;
					targetPoint[count].x = x;
					targetPoint[count].y = j;
					count++;
				}
				else {
					if (AIBot::isRed(map, x, j)) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
					break;
				}
				j++;
			}
			j = y - 1;
			while (j >= 0)
			{
				if (AIBot::isEmpty(map, x, j)) {
					chessArray[count] = n;
					targetPoint[count].x = x;
					targetPoint[count].y = j;
					count++;
				}
				else {
					if (AIBot::isRed(map, x, j)) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
					break;
				}
				j--;
			}
			break;
		case 25: // BLACK_CANON
		case 26: // BLACK_CANON
			i = x + 1;
			flag1 = false;
			while (i <= 8)
			{
				if (AIBot::isEmpty(map, i, y)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isRed(map, i, y)) {
							chessArray[count] = n;
							targetPoint[count].x = i;
							targetPoint[count].y = y;
							count++;
						}
						break;
					}
				}
				i++;
			}
			i = x - 1;
			flag1 = false;
			while (i >= 0)
			{
				if (AIBot::isEmpty(map, i, y)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = i;
						targetPoint[count].y = y;
						count++;
					}
				}
				else  {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isRed(map, i, y)) {
							chessArray[count] = n;
							targetPoint[count].x = i;
							targetPoint[count].y = y;
							count++;
						}
						break;
					}
				}
				i--;
			}
			j = y + 1;
			flag1 = false;
			while (j <= 9)
			{
				if (AIBot::isEmpty(map, x, j)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isRed(map, x, j)) {
							chessArray[count] = n;
							targetPoint[count].x = x;
							targetPoint[count].y = j;
							count++;
						}
						break;
					}
				}
				j++;
			}
			j = y - 1;
			flag1 = false;
			while (j >= 0)
			{
				if (AIBot::isEmpty(map, x, j)) {
					if (!flag1) {
						chessArray[count] = n;
						targetPoint[count].x = x;
						targetPoint[count].y = j;
						count++;
					}
				}
				else {
					if (!flag1) flag1 = true;
					else {
						if (AIBot::isRed(map, x, j)) {
							chessArray[count] = n;
							targetPoint[count].x = x;
							targetPoint[count].y = j;
							count++;
						}
						break;
					}
				}
				j--;
			}
			break;
		case 27: // BLACK_SOLDER
		case 28: // BLACK_SOLDER
		case 29: // BLACK_SOLDER
		case 30: // BLACK_SOLDER
		case 31: // BLACK_SOLDER
			j = y - 1;
			if (j >= 0 && !AIBot::isBlack(map, x, j)) {
				chessArray[count] = n;
				targetPoint[count].x = x;
				targetPoint[count].y = j;
				count++;
			}
			if (y < 5) {
				i = x + 1;
				if (i <= 8 && !AIBot::isBlack(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
				i = x - 1;
				if (i >= 0 && !AIBot::isBlack(map, i, y)) {
					chessArray[count] = n;
					targetPoint[count].x = i;
					targetPoint[count].y = y;
					count++;
				}
			}

			break;
		}
	}
	return true;
}

int  AIBot::search(int map[9][10], Point chessPos[32], int side, int chess, Point point, int upmax, int depth) {
	int xs = int(chessPos[chess].x);
	int ys = int(chessPos[chess].y);

	int ate = 32;

	if (AIBot::getSide(map[int(point.x)][int(point.y)]) == 1-side)
	{
		ate = map[int(point.x)][int(point.y)];
		if (ate == 0 || ate == 16)
		{
			// Eat King
			return 9999;
		}
		chessPos[ate].x = -1;
	}

	map[int(point.x)][int(point.y)] = chess;
	map[xs][ys] = 32;
	chessPos[chess] = point;
	side = 1 - side;

	int maxValue;

	if (depth > 1)
	{
		int chessArray[125];
		Point targetPoint[125];
		int count = 0, cur, curValue;
		if (getMoveList(map, chessPos, side, chessArray, targetPoint, count)) {
			if (depth >= 2 && count > _width + 2) {
				cur = 0;
				maxValue = -10000;
				int value[125];
				while (cur < count) {
					curValue = search(map, chessPos, side, chessArray[cur], targetPoint[cur], -10000, depth - 3);
					value[cur] = curValue;
					cur++;
				}
				quickSort(value, chessArray, targetPoint, 0, count - 1);
				count = _width;
				if (count > 1 && value[0] != -9999) {
					for (int i = 0; i < _width; i++) {
						if (value[i] == -9999) {
							count = i;
							break;
						}
					}
				}
			}

			cur = 0;
			maxValue = -10000;
			while (cur < count) {
				curValue = search(map, chessPos, side, chessArray[cur], targetPoint[cur], -10000, depth - 1);
				maxValue = curValue > maxValue ? curValue : maxValue;
				if (curValue > -upmax)
					break;
				cur++;
			}
		}
		else {
			maxValue = 9800;
		}
	}
	else {
		maxValue = heuristic(map, chessPos, side);
	}

	chessPos[chess].x = xs;
	chessPos[chess].y = ys;
	map[xs][ys] = chess;
	if (ate != 32)
	{
		chessPos[ate] = point;
		map[int(point.x)][int(point.y)] = ate;
	}
	else {
		map[int(point.x)][int(point.y)] = 32;
	}
	side = 1 - side;

	return -maxValue;
}

void AIBot::quickSort(int value[], int chess[], Point targetPoint[], int low, int high)
{
	if (high - low <= 0)
	{
		return;
	}
	else {
		if (high - low == 1)
		{
			if (value[high] > value[low]) {
				swapInt(value[high], value[low]);
				swapInt(chess[high], chess[low]);
				swapPoint(targetPoint[high], targetPoint[low]);
				return;
			}
		}
	}

	int mid = (low + high) / 2;
	int pivot = value[mid];
	int pivotChess = chess[mid];
	Point pivotPoint = targetPoint[mid];

	swapInt(value[mid], value[low]);
	swapInt(chess[mid], chess[low]);
	swapPoint(targetPoint[mid], targetPoint[low]);

	int left = low + 1;
	int right = high;

	do {
		while (left <= right && value[left] >= pivot)
			left++;
		while (pivot > value[right] && right > 0)
			right--;
		if (left < right)
		{
			swapInt(value[left], value[right]);
			swapInt(chess[left], chess[right]);
			swapPoint(targetPoint[left], targetPoint[right]);
		}
	} while (left < right);

	value[low] = value[right];
	value[right] = pivot;
	chess[low] = chess[right];
	chess[right] = pivotChess;
	targetPoint[low] = targetPoint[right];
	targetPoint[right] = pivotPoint;

	if (low < right - 1)
		quickSort(value, chess, targetPoint, low, right - 1);
	if (right + 1 < high)
		quickSort(value, chess, targetPoint, right + 1, high);
}

void AIBot::swapInt(int &a, int &b)
{
	int c = a;
	a = b;
	b = c;
}

void AIBot::swapPoint(Point &a, Point &b)
{
	Point c = a;
	a = b;
	b = c;
}

int  AIBot::heuristic(int map[9][10], Point chessPos[32], int)
{
	return 0;
}

int  AIBot::getSide(int chess) {
	if (chess <= 15) return PLAYER::PLAYER_1_RED;
	else if (chess <= 31) return PLAYER::PLAYER_2_BLACK;
	else return -1;
}

bool AIBot::isRed(int map[9][10], int x, int y) {
	return getSide(map[x][y]) == PLAYER::PLAYER_1_RED;
}

bool AIBot::isBlack(int map[9][10], int x, int y) {
	return getSide(map[x][y]) == PLAYER::PLAYER_2_BLACK;
}

bool AIBot::isEmpty(int map[9][10], int x, int y) {
	return map[x][y] == 32;
}