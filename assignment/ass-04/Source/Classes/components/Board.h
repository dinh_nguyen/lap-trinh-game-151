#ifndef __BOARD_H__
#define __BOARD_H__

#include "cocos2d.h"
#include "Constant.h"
#include "base/ResourcesManager.h"
#include "components/AIBot.h"
#include "base/AudioManager.h"

USING_NS_CC;

class Board: public cocos2d::Sprite {

private:

	std::vector<Rect*> _grid = std::vector<Rect*>();
	std::vector<Point*> _centerPoint = std::vector<Point*>();

	int _mode = Mode::PLAYER_VS_PLAYER;
	
	int side;
	AIBot *_bot;

	//std::thread threadComputerCalculate;

public:
	Node* _selectedChessPlayer1 = nullptr;
	Node* _selectedChessPlayer2 = nullptr;

	bool _isChessPieceMoving = false;

	Board();
	static Board* create();

	bool _gameOver = false;
	int _map[9][10];
	Point _chessPos[32];
	// CHESS TYPE
	const static ChessType type[32];
	// CHESS SPRITES
	const static int listChessSpriteId[32];

	void setup();
	void reset();
	void resetMap();
	void resetBoard();
	void loadOldGame();

	int getSide(int chess);

	// init
	bool init();
	// this medthod add sprite as child as board, TAG 0 -> 31
	void initChineseChessSprite();
	void initBorderPieces();

	// get and set method
	int getCurrentTurn();
	void setCurrentTurn(int turn);

	int getMode();
	void setMode(Mode mode);

	void setIsChessPieceMoving(bool isMove);
	bool isChessPieceMoving();

	// switch turn between two players
	void switchTurn();

	// this method use point have width and height range 0 -> 31 
	bool move(int chess, Point targetPoint);
	// In check condition, move only if you don't chess after moved 
	bool move(int chess, Point targetPoint, int condition);

	bool canMove(int chess, Point fromPos, Point toPos);
	bool isValidPosition(ChessType type, Point targetPoint);
	bool isTwoKingHeadToHead();

	bool isRed(int x, int y);
	bool isBlack(int x, int y);
	bool isEmpty(int x, int y);
	std::vector<Point*> getMoveList(int index);

	// Check use side index
	bool isCheck(int side);
	// Check if moved to this position, range x 0 -> 8, range y 0 -> 9, don't move
	bool isCheck(int chess,Point pos);

	int isChessMate(int side);
	
	// Convert to board coordinate
	Point changeCoordinate(float x, float y);
	Point changeCoordinate(Point p);

	// Convert from board to original cocos2d-x coordinate
	Point inverseChangeCoordinate(float x, float y);
	Point inverseChangeCoordinate(Point p);

	// Change from cocos2d-x coordinate to local
	Point changeLocalCoordinateParent(float x, float y);
	Point changeLocalCoordinateParent(Point p);

	// Use cocos2d-x pos to determine piece type
	int getChessInPosition(Point p);

	Point getClosestCenterPointFrom(Point p);

	// Return index of 1D array
	int getIndexList(int row, int col);

	std::vector<Point*> getListCenterPoint();

	void addGrid();
	void addCenterPoints();

	// update board each turn
	void updateBoard();

	void updateTouchEachTurn(Point pos);
	void updateComputerTurn(Point pos);

	void updateHighlight();
	void updateChessSelection(Point pos);
	void endTurn(Point pos);

	void movingChessCallback(Sprite* &playerMoving, Sprite* &playerUnhighlight, Point pos, CallFunc* callFunc);

	void onExitEndTurn();
	void onStartNewComputer();

	int generateComputerChess(int side);
	Point generateComputerChessPosition(int side, int &chess);

	// animation, effect in chess sprite
	void onHighlightPlayer(Node* chessSprite);
	void onHighlightOpponent(Node* chessSprite);
	void onDeselectPlayer(Node* chessSprite);
	void onMovingPlayer(Node* chessSprite);

	void animationFlareCallback(Point pos);
	void onAnimationHighlight(Node* node, Color4F color);
	void onHightlightCanMoveFromChess(Node* chessSprite);
	void onDeHightlightCanMoveFromChess();

	void onChessmateAnimation(int side);
	void onCheckAnimation(int side);

	Sprite* createGlowEffectSprite(const Color3B& colour, const Size& size);

	void callBackOnStartNewComputer();
};

#endif