#ifndef __AIBOT_LEVEL2_H__
#define __AIBOT_LEVEL2_H__

#include "AIBot.h"

class AIBot_Level2 : public AIBot{
protected:
	virtual int heuristic(int[9][10], Point[32], int);
	int calculatePoint(Point[32], int);
public:
	const static int positionValueKing[2][10][9];
	const static int positionValueAdvisor[2][10][9];
	const static int positionValueElephant[2][10][9];
	const static int positionValueHorse[2][10][9];
	const static int positionValueCar[2][10][9];
	const static int positionValueCanon[2][10][9];
	const static int positionValueSolder[2][10][9];
public:
	AIBot_Level2();
};
#endif