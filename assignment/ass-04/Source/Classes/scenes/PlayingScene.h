#ifndef __PLAYING_SCENE_H__
#define __PLAYING_SCENE_H__

#include "base/MyLayer.h"
#include "components/Board.h"

class Playing: public MyLayer
{
private:
	Sprite* _backgroundSprite;
	Board* _boardGame;

	float _timerBlack = 0.0f;// = GameplayManager::getInstance()->_timerBlack;
	float _timerRed = 0.0f;// = GameplayManager::getInstance()->_timerRed;

	Label* _labelTimerRedPLayer = nullptr;
	Label* _labelTimerBlackPLayer = nullptr;

	Label* _waitingComputerLabel = nullptr;

public:
	Playing();
	virtual ~Playing();

	static Scene* createScene();

	virtual bool init();

	bool onTouchBegan(Touch *touch, Event *unused_event);

	void update(float dt);

	void onEnterTransitionDidFinish();
	
	CREATE_FUNC(Playing);
};

#endif