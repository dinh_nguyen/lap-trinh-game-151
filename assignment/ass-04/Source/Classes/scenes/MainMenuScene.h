#ifndef __MAIN_MENU_SCENE_H__
#define __MAIN_MENU_SCENE_H__

#include "base/MyLayer.h"
#include "scenes/PlayingScene.h"
#include "scenes/CreditsScene.h"
#include "PlayingScene.h"
#include "../layers/GameControllerLayer.h"

class MainMenu: public MyLayer
{
private:
	Sprite* _backgroundSprite;
	Label* _timerLabel;
	Label* _mainMenuLabel;

	Menu* _newgameButton;
	Menu* _continueButton;

	ui::Button* _settingButton;
	ui::Button* _creditsButton;

	Action* _actionButton;

	float _timer = 0.0f;
public:
	MainMenu();
	virtual ~MainMenu();

	static Scene* createScene();

	virtual bool init();

	//////////////////////////////

	// callback

	void changeToGameSceneCallBack(Ref* pSender, GameSceneState aGameState);
	
	void onPlaySoundEffect();
	void onResumeBackgroundmusic();

	// create layout
	void createMainMenuLayout();

	void onButtonLayoutCallback(Ref* pSender, ui::Widget::TouchEventType type, ui::Layout* fromLayer, ui::Layout* toLayer, int selectingState);
	void onTransitionGameplay();

	CREATE_FUNC(MainMenu);
};

#endif