#include "scenes/IntroScene.h"

Intro::Intro()
{
	_backgroundSprite = nullptr;
}

Intro::~Intro()
{}

Scene* Intro::createScene()
{
	SceneManager::getInstance()->changeToScene(GameSceneState::Intro);

	Scene* scene = Scene::create();

	Intro* layer = Intro::create();

	scene->addChild(layer);

	return scene;
}

bool Intro::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibileSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_backgroundSprite = _resourcesManager->createSpriteWithId(INDEX_OF_IMAGE_INTRO_BACKGROUND_FRAME_NAME);
	_backgroundSprite->setAnchorPoint(Point(0, 0));
	_backgroundSprite->setPosition(Point(0, 0));
	this->addChild(_backgroundSprite, zValue::zBackground);
	
	ParticleSystemQuad* p = ParticleSystemQuad::create(plistResourcesList[INDEX_OF_SNOW_PARTICLE_PLIST]);
	p->setVisible(true);
	p->setPosition(0, visibileSize.height);
	this->addChild(p,zValue::zBackground);

	Label* pressAnyKeyLabel = _resourcesManager->createLabelTTFWithId("TAP SCREEN TO CONTINUE", INDEX_OF_ERASBD_TTF,40);
	pressAnyKeyLabel->setColor(Color3B(165, 42, 42));
	pressAnyKeyLabel->setPosition(visibileSize.width / 2, visibileSize.height * 0.71f);

	Sequence* blinkMenu = Sequence::create(FadeOut::create(1.5f), FadeIn::create(1.5f), nullptr);
	pressAnyKeyLabel->runAction(RepeatForever::create(blinkMenu));

	this->addChild(pressAnyKeyLabel, zValue::zForeground);

	// add listener for touch screen event
	EventListenerTouchOneByOne* listenerTouchOneByOne = EventListenerTouchOneByOne::create();
	listenerTouchOneByOne->onTouchBegan = CC_CALLBACK_2(Intro::onTouchBegan, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listenerTouchOneByOne, this);

	return true;
}

bool Intro::onTouchBegan(Touch *touch, Event *unused_event)
{
	Scene* scene = MainMenu::createScene();
	TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
	Director::getInstance()->replaceScene(transitionScene);
	return false;
}