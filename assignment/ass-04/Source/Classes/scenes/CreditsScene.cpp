#include "scenes/CreditsScene.h"
#include "scenes/MainMenuScene.h"

Credits::Credits()
{
}

Credits::~Credits()
{
}

Scene* Credits::createScene()
{
	// update resources
	SceneManager::getInstance()->changeToScene(GameSceneState::Credits);

	// play sound when this scene is created (changed by another screen)
	_audioManager->playEffect(INDEX_OF_BUTTON_CLICK_MP3, "mp3");

	Scene* scene = Scene::create();

	Credits* layer = Credits::create();

	GameControl* controlLayer = GameControl::create();

	scene->addChild(controlLayer, zValue::zMiddleground);
	scene->addChild(layer);

	return scene;
}

bool Credits::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	Sprite* backgroundSprite = _resourcesManager->createSpriteWithId(INDEX_OF_IMAGE_CREDITS_SCREEN_FRAME_NAME);
	backgroundSprite->setAnchorPoint(Point(0, 0));
	backgroundSprite->setPosition(0, 0);
	this->addChild(backgroundSprite, zValue::zBackground);
	
	Label* nhudinhLabel = _resourcesManager->createLabelTTFWithId("NGUYEN NHU DINH", INDEX_OF_ARIAL_TTF, 40);
	nhudinhLabel->setColor(Color3B::RED);
	nhudinhLabel->setAnchorPoint(Point(0, 0.5f));
	nhudinhLabel->setPosition(visibleSize.width*0.05f, visibleSize.height*0.65f);
	this->addChild(nhudinhLabel,zValue::zForeground);

	Label* idNhuDinhLabel = _resourcesManager->createLabelTTFWithId("51200780", INDEX_OF_ARIAL_TTF, 40);
	idNhuDinhLabel->setColor(Color3B::RED);
	idNhuDinhLabel->setAnchorPoint(Point(0, 0.5f));
	idNhuDinhLabel->setPosition(visibleSize.width * 0.7f, nhudinhLabel->getPositionY());
	this->addChild(idNhuDinhLabel, zValue::zForeground);

	Label* chidaLabel = _resourcesManager->createLabelTTFWithId("MACH CHI DA", INDEX_OF_ARIAL_TTF, 40);
	chidaLabel->setColor(Color3B::RED);
	chidaLabel->setAnchorPoint(Point(0, 0.5f));
	chidaLabel->setPosition(nhudinhLabel->getPositionX(), nhudinhLabel->getPositionY() - nhudinhLabel->getContentSize().height * 2.0f);
	this->addChild(chidaLabel, zValue::zForeground);

	Label* idChiDaLabel = _resourcesManager->createLabelTTFWithId("51200659", INDEX_OF_ARIAL_TTF, 40);
	idChiDaLabel->setColor(Color3B::RED);
	idChiDaLabel->setAnchorPoint(Point(0, 0.5f));
	idChiDaLabel->setPosition(visibleSize.width * 0.7f, chidaLabel->getPositionY());
	this->addChild(idChiDaLabel, zValue::zForeground);

	Label* leduyLabel = _resourcesManager->createLabelTTFWithId("NGUYEN LE DUY", INDEX_OF_ARIAL_TTF, 40);
	leduyLabel->setColor(Color3B::RED);
	leduyLabel->setAnchorPoint(Point(0, 0.5f));
	leduyLabel->setPosition(chidaLabel->getPositionX(), chidaLabel->getPositionY() - chidaLabel->getContentSize().height * 2.0f);
	this->addChild(leduyLabel, zValue::zForeground);

	Label* idLeDuyLabel = _resourcesManager->createLabelTTFWithId("51200553", INDEX_OF_ARIAL_TTF, 40);
	idLeDuyLabel->setColor(Color3B::RED);
	idLeDuyLabel->setAnchorPoint(Point(0, 0.5f));
	idLeDuyLabel->setPosition(visibleSize.width * 0.7f, leduyLabel->getPositionY());
	this->addChild(idLeDuyLabel, zValue::zForeground);

	ui::Button* backButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_BACK_FRAME_NAME, INDEX_OF_IMAGE_BACK_CLICKED_FRAME_NAME);
	backButton->setAnchorPoint(Point(0.5f, 0.5f));
	backButton->setPosition(Point(visibleSize.width * 0.5f, visibleSize.height * 0.25f));
	backButton->addTouchEventListener([&](Ref* pSender, ui::Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			break;
		case cocos2d::ui::Widget::TouchEventType::MOVED:
			break;
		case cocos2d::ui::Widget::TouchEventType::ENDED:
		{
			Scene* scene = MainMenu::createScene();
			TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
			Director::getInstance()->replaceScene(transitionScene);
			break;
		}
		case cocos2d::ui::Widget::TouchEventType::CANCELED:
			break;
		default:
			break;
		}
	});
	this->addChild(backButton, zValue::zForeground);

	return true;
}