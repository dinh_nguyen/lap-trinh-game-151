#ifndef __INTRO_SCENE_H__
#define __INTRO_SCENE_H__

#include "base/MyLayer.h"
#include "scenes/MainMenuScene.h"

class Intro : public MyLayer
{
private:
	Sprite* _backgroundSprite;

public:
	Intro();
	virtual ~Intro();

	static Scene* createScene();
	
	virtual bool init();

	// callback
	bool onTouchBegan(Touch *touch, Event *unused_event);

	CREATE_FUNC(Intro);
};

#endif