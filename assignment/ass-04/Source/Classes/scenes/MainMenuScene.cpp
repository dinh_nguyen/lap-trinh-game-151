#include "scenes/MainMenuScene.h"

MainMenu::MainMenu()
{
	_backgroundSprite = nullptr;
	_timerLabel       = nullptr;
	_mainMenuLabel    = nullptr;

	_newgameButton    = nullptr;
	_continueButton   = nullptr;

	_actionButton     = nullptr;

	_timer = 0.0f;
}

MainMenu::~MainMenu()
{
	CC_SAFE_RELEASE_NULL(_actionButton);
	_timer = 0.0f;
}

Scene* MainMenu::createScene()
{
	// update resources
	SceneManager::getInstance()->changeToScene(GameSceneState::MainMenu);

	Scene* scene = Scene::create();

	MainMenu* layer = MainMenu::create();

	GameControl* controlLayer = GameControl::create();

	scene->addChild(controlLayer,zValue::zMiddleground);
	scene->addChild(layer);

	return scene;
}

bool MainMenu::init()
{

	if (!Layer::init())
	{
		return false;
	}

	// play sound when this scene is created (changed by another screen)
	this->runAction(Sequence::createWithTwoActions(CallFunc::create(CC_CALLBACK_0(MainMenu::onPlaySoundEffect, this)),
		CallFunc::create(CC_CALLBACK_0(MainMenu::onResumeBackgroundmusic, this))));

	Size visibileSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_backgroundSprite = _resourcesManager->createSpriteWithId(INDEX_OF_IMAGE_MAIN_MENU_BACKGROUND_FRAME_NAME);
	_backgroundSprite->setAnchorPoint(Point(0, 0));
	_backgroundSprite->setPosition(Point(0, 0));
	this->addChild(_backgroundSprite, zValue::zBackground);

	ParticleSystemQuad* p = ParticleSystemQuad::create(plistResourcesList[INDEX_OF_SNOW_PARTICLE_PLIST]);
	p->setVisible(true);
	p->setPosition(0, visibileSize.height);
	this->addChild(p, zValue::zBackground);

	createMainMenuLayout();

	return true;
}

void MainMenu::createMainMenuLayout()
{
	Size visibileSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Main menu layout
	ui::Layout* layout = ui::Layout::create();
	layout->setLayoutType(ui::LayoutType::VERTICAL);
	layout->setPosition(Point(visibileSize.width / 2, visibileSize.height * 0.72f));

	ui::LinearLayoutParameter* lp1 = ui::LinearLayoutParameter::create();
	lp1->setGravity(ui::LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);
	lp1->setMargin(ui::Margin(0, visibileSize.height * 0.03f, 0, 0));

	ui::Button* newGameButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_NEW_FRAME_NAME, INDEX_OF_IMAGE_NEW_CLICKED_FRAME_NAME);
	newGameButton->setLayoutParameter(lp1);
	layout->addChild(newGameButton);

	ui::Button* continueButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_CONTINUE_FRAME_NAME, INDEX_OF_IMAGE_CONTINUE_CLICKED_FRAME_NAME, INDEX_OF_IMAGE_CONTINUE_DISABLED_FRAME_NAME);
	continueButton->setLayoutParameter(lp1);
	continueButton->setEnabled(UserDefault::getInstance()->getBoolForKey(KEY_CAN_CONTINUE,false));
	continueButton->setBright(UserDefault::getInstance()->getBoolForKey(KEY_CAN_CONTINUE, false));
	layout->addChild(continueButton);

	ui::Button* creditsButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_CREDITS_FRAME_NAME, INDEX_OF_IMAGE_CREDITS_CLICKED_FRAME_NAME);
	creditsButton->setLayoutParameter(lp1);
	layout->addChild(creditsButton);

	ui::Button* exitButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_EXIT_FRAME_NAME, INDEX_OF_IMAGE_EXIT_CLICKED_FRAME_NAME);
	exitButton->setLayoutParameter(lp1);
	
	layout->addChild(exitButton);
	this->addChild(layout, zValue::zForeground);

	// Mode select layout
	ui::Layout* modeSelectingLayout = ui::Layout::create();
	modeSelectingLayout->setLayoutType(ui::LayoutType::VERTICAL);
	modeSelectingLayout->setPosition(Point(visibileSize.width / 2, visibileSize.height * 0.7f));

	ui::LinearLayoutParameter* lp2 = ui::LinearLayoutParameter::create();
	lp2->setGravity(ui::LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);
	lp2->setMargin(ui::Margin(0, visibileSize.height * 0.04f, 0, 0));

	ui::Button* computerButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_COMPUTER_FRAME_NAME, INDEX_OF_IMAGE_COMPUTER_CLICKED_FRAME_NAME);
	computerButton->setLayoutParameter(lp2);
	modeSelectingLayout->addChild(computerButton);

	ui::Button* twoPlayerButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_TWO_PLAYER_FRAME_NAME, INDEX_OF_IMAGE_TWO_PLAYER_CLICKED_FRAME_NAME);
	twoPlayerButton->setLayoutParameter(lp2);
	modeSelectingLayout->addChild(twoPlayerButton);

	ui::Button* backButtonModeSelecting = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_BACK_FRAME_NAME,INDEX_OF_IMAGE_BACK_CLICKED_FRAME_NAME);
	backButtonModeSelecting->setLayoutParameter(lp2);
	modeSelectingLayout->addChild(backButtonModeSelecting);

	modeSelectingLayout->setVisible(false);
	this->addChild(modeSelectingLayout, zValue::zForeground);

	// Level computer select 
	ui::Layout* modeComputerLayout = ui::Layout::create();
	modeComputerLayout->setLayoutType(ui::LayoutType::VERTICAL);
	modeComputerLayout->setPosition(Point(visibileSize.width / 2, visibileSize.height * 0.7f));

	ui::LinearLayoutParameter* lp3 = ui::LinearLayoutParameter::create();
	lp3->setGravity(ui::LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);
	lp3->setMargin(ui::Margin(0, visibileSize.height * 0.03f, 0, 0));

	ui::Button* easyButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_EASY_FRAME_NAME, INDEX_OF_IMAGE_EASY_CLICKED_FRAME_NAME);
	easyButton->setLayoutParameter(lp3);
	modeComputerLayout->addChild(easyButton);

	ui::Button* normalButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_NORMAL_FRAME_NAME, INDEX_OF_IMAGE_NORMAL_CLICKED_FRAME_NAME);
	normalButton->setLayoutParameter(lp3);
	modeComputerLayout->addChild(normalButton);

	ui::Button* hardButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_HARD_FRAME_NAME, INDEX_OF_IMAGE_HARD_CLICKED_FRAME_NAME);
	hardButton->setLayoutParameter(lp3);
	modeComputerLayout->addChild(hardButton);

	ui::Button* backButtonModeComputer = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_BACK_FRAME_NAME, INDEX_OF_IMAGE_BACK_CLICKED_FRAME_NAME);
	backButtonModeComputer->setLayoutParameter(lp3);
	modeComputerLayout->addChild(backButtonModeComputer);

	modeComputerLayout->setVisible(false);
	this->addChild(modeComputerLayout, zValue::zForeground);

	// Player select layout
	ui::Layout* modeSelectPlayerLayout = ui::Layout::create();
	modeSelectPlayerLayout->setLayoutType(ui::LayoutType::VERTICAL);
	modeSelectPlayerLayout->setPosition(Point(visibileSize.width / 2, visibileSize.height * 0.7f));

	ui::LinearLayoutParameter* lp4 = ui::LinearLayoutParameter::create();
	lp4->setGravity(ui::LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);
	lp4->setMargin(ui::Margin(0, visibileSize.height * 0.04f, 0, 0));

	ui::Button* player1RedButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_RED_FRAME_NAME, INDEX_OF_IMAGE_RED_CLICKED_FRAME_NAME);
	player1RedButton->setLayoutParameter(lp4);
	modeSelectPlayerLayout->addChild(player1RedButton);

	ui::Button* player2BlackButton = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_BLACK_FRAME_NAME, INDEX_OF_IMAGE_BLACK_CLICKED_FRAME_NAME);
	player2BlackButton->setLayoutParameter(lp4);
	modeSelectPlayerLayout->addChild(player2BlackButton);

	ui::Button* backButtonSelectPlayer = _resourcesManager->createButtonWithId(INDEX_OF_IMAGE_BACK_FRAME_NAME, INDEX_OF_IMAGE_BACK_CLICKED_FRAME_NAME);
	backButtonSelectPlayer->setLayoutParameter(lp4);
	modeSelectPlayerLayout->addChild(backButtonSelectPlayer);

	modeSelectPlayerLayout->setVisible(false);
	this->addChild(modeSelectPlayerLayout, zValue::zForeground);

	// Callback button
	newGameButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, layout, modeSelectingLayout, MainMenuSelection::NEXT));
	exitButton->addTouchEventListener([&](Ref* pSender, ui::Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			break;
		case cocos2d::ui::Widget::TouchEventType::MOVED:
			break;
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			_audioManager->playEffect(INDEX_OF_BUTTON_CLICK_MP3, "mp3");
			ResourcesManager::getInstance()->release();
			AudioManager::getInstance()->release();
			SceneManager::getInstance()->release();
			GameplayManager::getInstance()->release();
			Director::getInstance()->end();
			break;
		case cocos2d::ui::Widget::TouchEventType::CANCELED:
			break;
		default:
			break;
		}
	});

	continueButton->addTouchEventListener([&](Ref* pSender, ui::Widget::TouchEventType type){
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			break;
		case cocos2d::ui::Widget::TouchEventType::MOVED:
			break;
		case cocos2d::ui::Widget::TouchEventType::ENDED:
		{
			GameplayManager::getInstance()->setIsStart(false);
			Scene* scene = Playing::createScene();
			TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
			Director::getInstance()->replaceScene(transitionScene);
			break;
		}
		case cocos2d::ui::Widget::TouchEventType::CANCELED:
			break;
		default:
			break;
		}
	});

	creditsButton->addTouchEventListener([&](Ref* pSender, ui::Widget::TouchEventType type) 
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			break;
		case cocos2d::ui::Widget::TouchEventType::MOVED:
			break;
		case cocos2d::ui::Widget::TouchEventType::ENDED:
		{
			Scene* scene = Credits::createScene();
			TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
			Director::getInstance()->replaceScene(transitionScene);
			break;
		}
		case cocos2d::ui::Widget::TouchEventType::CANCELED:
			break;
		default:
			break;
		}
	});

	computerButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectingLayout, modeComputerLayout, MainMenuSelection::PLAYER_VS_COMPUTER_MODE));
	twoPlayerButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectingLayout, modeSelectPlayerLayout, MainMenuSelection::PLAYER_VS_PLAYER_MODE));
	backButtonModeSelecting->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectingLayout, layout, MainMenuSelection::BACK));
	
	easyButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeComputerLayout, modeSelectPlayerLayout, MainMenuSelection::EASY_LEVEL));
	normalButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeComputerLayout, modeSelectPlayerLayout, MainMenuSelection::NORMAL_LEVEL));
	hardButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeComputerLayout, modeSelectPlayerLayout, MainMenuSelection::HARD_LEVEL));
	backButtonModeComputer->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeComputerLayout, modeSelectingLayout, MainMenuSelection::BACK));

	player1RedButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectPlayerLayout, nullptr, MainMenuSelection::RED));
	player2BlackButton->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectPlayerLayout, nullptr, MainMenuSelection::BLACK));
	backButtonSelectPlayer->addTouchEventListener(CC_CALLBACK_2(MainMenu::onButtonLayoutCallback, this, modeSelectPlayerLayout, modeSelectingLayout, MainMenuSelection::BACK));
}


void MainMenu::changeToGameSceneCallBack(Ref* pSender, GameSceneState aGameState)
{
	// Create new transition scene
	Scene* scene                     = nullptr;
	TransitionScene* transitionScene = nullptr;

	if (aGameState == GameSceneState::GamePlay)
	{
		scene = Playing::createScene();
	}
	else if (aGameState == GameSceneState::Credits)
	{
		scene = Credits::createScene();
	}

	if (scene == nullptr) return;
	
	// if you want different transition scene, just change in if condition
	transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
	Director::getInstance()->replaceScene(transitionScene);
}

void MainMenu::onPlaySoundEffect()
{
	_audioManager->pauseBackgroundMusic();
	_audioManager->playEffect(INDEX_OF_CLICK_WAV, "wav");
}
void MainMenu::onResumeBackgroundmusic()
{
	_audioManager->resumeBackgroundMusic();
}

void MainMenu::onButtonLayoutCallback(Ref* pSender, ui::Widget::TouchEventType type, ui::Layout* fromLayer, ui::Layout* toLayer, int selectingState)
{
	// update selection
	switch (selectingState)
	{
	case MainMenuSelection::PLAYER_VS_PLAYER_MODE:
		GameplayManager::getInstance()->setGameMode(Mode::PLAYER_VS_PLAYER);
		break;
	case MainMenuSelection::RED:
		GameplayManager::getInstance()->setFirstPlayer(PLAYER::PLAYER_1_RED);
		break;
	case MainMenuSelection::BLACK:
		GameplayManager::getInstance()->setFirstPlayer(PLAYER::PLAYER_2_BLACK);
		break;
	case MainMenuSelection::PLAYER_VS_COMPUTER_MODE:
		GameplayManager::getInstance()->setGameMode(Mode::PLAYER_VS_COMPUTER);
		break;
	case MainMenuSelection::EASY_LEVEL:
		GameplayManager::getInstance()->setAIMode(AI_Mode::EASY);
		break;
	case MainMenuSelection::NORMAL_LEVEL:
		GameplayManager::getInstance()->setAIMode(AI_Mode::NORMAL);
		break;
	case MainMenuSelection::HARD_LEVEL:
		GameplayManager::getInstance()->setAIMode(AI_Mode::HARD);
		break;
	default:
		break;
	}

	// update layout visible and transition to gameplay scene
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		AudioManager::getInstance()->playEffect(INDEX_OF_BUTTON_CLICK_MP3, "mp3");
		if (fromLayer != nullptr) 
			fromLayer->setVisible(false);
		if (toLayer != nullptr)
			toLayer->setVisible(true);

		// change to gameplay scene
		if (selectingState == MainMenuSelection::RED || selectingState == MainMenuSelection::BLACK)
		{
			GameplayManager::getInstance()->setIsStart(true);
			onTransitionGameplay();
		}
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		break;
	default:
		break;
	}
}

void MainMenu::onTransitionGameplay()
{
	Scene* scene = Playing::createScene();
	TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION, scene);
	Director::getInstance()->replaceScene(transitionScene);
}