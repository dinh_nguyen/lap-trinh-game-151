#include "scenes/PlayingScene.h"
#include "layers/GameControllerLayer.h"

Playing::Playing()
{
	_backgroundSprite = nullptr;
}

Playing::~Playing()
{
}

Scene* Playing::createScene()
{
	// update resources
	SceneManager::getInstance()->changeToScene(GameSceneState::GamePlay);

	// play sound when this scene is created (changed by another screen)
	_audioManager->playEffect(INDEX_OF_BUTTON_CLICK_MP3, "mp3");

	Scene* scene = Scene::create();

	Playing* layer = Playing::create();
	GameControl* controlLayer = GameControl::create();
	controlLayer->visibleHomeButton();
	controlLayer->setName("controllerLayer");

	scene->addChild(controlLayer,zValue::zMiddleground);
	scene->addChild(layer);

	return scene;
}

bool Playing::init()
{
	if (!Layer::init())
	{
		return false;
	}
	

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_backgroundSprite = _resourcesManager->createSpriteWithId(INDEX_OF_IMAGE_GAMEPLAY_BACKGROUND_FRAME_NAME);
	_backgroundSprite->setAnchorPoint(Point(0, 0));
	_backgroundSprite->setPosition(Point(0, 0));
	this->addChild(_backgroundSprite, zValue::zBackground);

	Sprite* chessBoardSprite = _resourcesManager->createSpriteWithId(INDEX_OF_IMAGE_GAMEPLAY_BOARD_FRAME_NAME);
	chessBoardSprite->setAnchorPoint(Point(0.5f, 0.5f));
	chessBoardSprite->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(chessBoardSprite, zValue::zBackground);

	_boardGame = Board::create();
	_boardGame->setPosition(visibleSize.width - (visibleSize.width - GRID_X * 8) / 2, (visibleSize.height - GRID_Y * 9) / 2);
	this->addChild(_boardGame);

	//_boardGame->drawGrid();
	GameplayManager::getInstance()->setBoard(_boardGame);
	if (GameplayManager::getInstance()->isStart()) GameplayManager::getInstance()->initNewGame();
	
	_timerBlack = GameplayManager::getInstance()->_timerBlack;
	_timerRed = GameplayManager::getInstance()->_timerRed;

	_labelTimerRedPLayer = _resourcesManager->createLabelTTFWithId("",INDEX_OF_MARKER_FELT_TTF,50);
	_labelTimerRedPLayer->setPosition(visibleSize.width - GRID_X, visibleSize.height/2 - GRID_Y * 5.5f);
	_labelTimerRedPLayer->setColor(Color3B::RED);
	this->addChild(_labelTimerRedPLayer, zValue::zForeground);

	_labelTimerBlackPLayer = _resourcesManager->createLabelTTFWithId("", INDEX_OF_MARKER_FELT_TTF, 50);
	_labelTimerBlackPLayer->setPosition(GRID_X, visibleSize.height / 2 + GRID_Y * 6.0f);
	_labelTimerBlackPLayer->setColor(Color3B::BLACK);
	this->addChild(_labelTimerBlackPLayer, zValue::zForeground);

	_waitingComputerLabel = _resourcesManager->createLabelTTFWithId("WAITING", INDEX_OF_MARKER_FELT_TTF, 50);
	_waitingComputerLabel->setPosition(visibleSize.width / 2, visibleSize.height / 2 + GRID_Y * 6.0f);
	_waitingComputerLabel->setColor(Color3B::BLACK);
	_waitingComputerLabel->setVisible(false);
	this->addChild(_waitingComputerLabel, zValue::zForeground);

	EventListenerTouchOneByOne* listenerTouchOneByOne = EventListenerTouchOneByOne::create();
	listenerTouchOneByOne->onTouchBegan = CC_CALLBACK_2(Playing::onTouchBegan, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listenerTouchOneByOne, this);

	this->scheduleUpdate();

	return true;
}

void Playing::update(float dt)
{
	GameControl* controllerLayer = (GameControl*)this->getParent()->getChildByName("controllerLayer");
	Board* curBoard = GameplayManager::getInstance()->getBoard();
	if (controllerLayer != nullptr && curBoard != nullptr)
	{
		bool isMoving = curBoard->_isChessPieceMoving;
		if (isMoving)
		{
			controllerLayer->setVisible(false);
		}
		else
		{
			controllerLayer->setVisible(true);
		}
	}

	if (_waitingComputerLabel != nullptr && curBoard != nullptr)
	{
		bool isMoving = GameplayManager::getInstance()->getBoard()->_isChessPieceMoving;
		int side = GameplayManager::getInstance()->getBoard()->getCurrentTurn();
		if (isMoving && side == PLAYER::PLAYER_2_BLACK)
		{
			_waitingComputerLabel->setVisible(true);
		}
		else
		{
			_waitingComputerLabel->setVisible(false);
		}
	}

	char tmp[20];
	sprintf(tmp, "%02d:%02d", (int)_timerRed / 60, (int)((int)_timerRed % 60));
	_labelTimerRedPLayer->setString(tmp);
	sprintf(tmp, "%02d:%02d", (int)_timerBlack / 60, (int)((int)_timerBlack % 60));
	_labelTimerBlackPLayer->setString(tmp);
	
	Board* b = GameplayManager::getInstance()->getBoard();
	if (b != nullptr)
	{
		if (!b->_gameOver)
		{
			if (b->getCurrentTurn() == PLAYER::PLAYER_1_RED)
			{
				_timerRed -= dt;
				_labelTimerBlackPLayer->setOpacity(100);
				_labelTimerRedPLayer->setOpacity(255);
				GameplayManager::getInstance()->_timerRed = _timerRed;
			}
			else if (b->getCurrentTurn() == PLAYER::PLAYER_2_BLACK)
			{
				_timerBlack -= dt;
				_labelTimerRedPLayer->setOpacity(100);
				_labelTimerBlackPLayer->setOpacity(255);
				GameplayManager::getInstance()->_timerBlack = _timerBlack;
			}
		}
	}
}

bool Playing::onTouchBegan(Touch *touch, Event *unused_event)
{
	Point tap = touch->getLocation();
	GameplayManager::getInstance()->updateEachTouchBoard(tap);

	return false;
}

void Playing::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();
	if (GameplayManager::getInstance()->isStart() == true)
	{
		GameplayManager::getInstance()->setIsStart(false);
		if (GameplayManager::getInstance()->getFirstPlayer() == PLAYER::PLAYER_2_BLACK && GameplayManager::getInstance()->getGameMode() == Mode::PLAYER_VS_COMPUTER)
		{
			_boardGame->onStartNewComputer();
		}
	}
}