#ifndef __CREDITS_SCENE_H__
#define __CREDITS_SCENE_H__

#include "base/MyLayer.h"

class Credits: public MyLayer {
private:
	int a;
public:
	Credits();
	virtual ~Credits();

	static Scene* createScene();

	virtual bool init();

	CREATE_FUNC(Credits);
};

#endif

