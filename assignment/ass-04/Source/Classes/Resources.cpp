#include "Resources.h"

char const *pngResourcesList[] =
{
	"res/particle/snow.png",
	"fonts/bitmap/ERASBDBM.png",
	"fonts/bitmap/wild-wood.png",
	"CloseNormal.png",
	"CloseSelected.png",
	"HelloWorld.png",
	"res/IntroScene/texture/intro-sprite-sheet.png",
	"res/MainMenuScene/texture/main-menu-sprite-sheet.png",
	"res/GameControllerLayer/game-controller-sprite-sheet.png",
	"res/CreditsScene/credits-sprite-sheet.png",
	"res/GameplayScene/texture/play-scene-sprite-sheet.png",
	nullptr
};

char const *plistResourcesList[] =
{
	"res/IntroScene/texture/intro-sprite-sheet.plist",
	"res/MainMenuScene/texture/main-menu-sprite-sheet.plist",
	"res/GameControllerLayer/game-controller-sprite-sheet.plist",
	"res/CreditsScene/credits-sprite-sheet.plist",
	"res/particle/snow-particle.plist",
	"res/GameplayScene/texture/play-scene-sprite-sheet.plist",
	nullptr
};

char const *mp3ResourcesList[] =
{
	"res/sound/button-click.mp3",
	"res/MainMenuScene/music/main-menu-background-music.mp3",
	"res/GameplayScene/sound/move-chess.mp3",
	"res/IntroScene/music/intro-background-music.mp3",
	"res/GameplayScene/music/gameplay-background-music.mp3",
	"res/GameplayScene/sound/wrong-move.mp3",
	"res/GameplayScene/sound/win-game.mp3",
	"res/GameplayScene/sound/lose-game.mp3",
	"res/GameplayScene/sound/check.mp3",
	nullptr
};

char const *wavResourcesList[] =
{
	"res/OptionScene/sound/setting-on.wav",
	"res/OptionScene/sound/setting-off.wav",
	"res/sound/click.wav",
	"res/GameplayScene/sound/select-chess.wav",
	"res/GameplayScene/sound/kill-chess.wav",
	nullptr
};

char const *oggResourcesList[] =
{
	nullptr
};

char const *ttfResourcesList[] =
{
	"fonts/VPSPHDIH_0.TTF",
	"fonts/VPSPHDIT_0.TTF",
	"fonts/ERASBD.TTF",
	"fonts/arial.ttf",
	"fonts/Marker Felt.ttf",
	nullptr
};

char const *otfResourcesList[] =
{
	nullptr
};

char const *fntResourcesList[] =
{
	"fonts/bitmap/ERASBDBM.fnt",
	"fonts/bitmap/wild-wood.fnt",
	nullptr
};

char const *jsonResourcesList[] =
{
	nullptr
};

char const *objectNameInPlistFileList[] =
{
	"flare0000.png",
	"intro-background.png",
	"continue-disabled.png",
	"normal.png",
	"continue.png",
	"back-clicked.png",
	"computer-clicked.png",
	"back.png",
	"credits-clicked.png",
	"exit-clicked.png",
	"easy-clicked.png",
	"red-clicked.png",
	"computer.png",
	"hard-clicked.png",
	"black.png",
	"two-player.png",
	"two-player-clicked.png",
	"new.png",
	"easy.png",
	"black-clicked.png",
	"hard.png",
	"normal-clicked.png",
	"main-menu-background.png",
	"new-clicked.png",
	"credits.png",
	"continue-clicked.png",
	"red.png",
	"exit.png",
	"music-off.png",
	"home.png",
	"home-clicked.png",
	"music-on.png",
	"sound-off.png",
	"sound-on.png",
	"credits-screen.png",
	"RED_ADVISOR.png",
	"RED_HORSE.png",
	"RED_KING.png",
	"BLACK_ADVISOR.png",
	"gameplay-background.png",
	"BLACK_KING.png",
	"BLACK_SOLDIER.png",
	"RED_CAR.png",
	"gameplay-board.png",
	"BLACK_CAR.png",
	"BLACK_ELEPHANT.png",
	"BLACK_HORSE.png",
	"RED_ELEPHANT.png",
	"circle-can-move.png",
	"border-piece.png",
	"BLACK_CANON.png",
	"particle-fire.png",
	"RED_SOLDIER.png",
	"RED_CANON.png",
	nullptr
};

const int numberOfFrameAnimationList[] =
{
	40
};
char const *objectInJSONList[] =
{
	nullptr
};
