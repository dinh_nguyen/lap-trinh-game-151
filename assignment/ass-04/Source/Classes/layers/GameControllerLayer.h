#ifndef __GAME_CONTROLLER_LAYER_H__
#define __GAME_CONTROLLER_LAYER_H__

#include "cocos2d.h"
#include "base/MyLayer.h"
#include "components/Board.h"

USING_NS_CC;

class GameControl : public MyLayer
{
public:
	virtual bool init();

	CREATE_FUNC(GameControl);

	// callback
	void setupAudioStateCallBack(Ref* pSender, const char* keySetup);

	void homeButtonCallBack(Ref* pSender);

	void visibleHomeButton();

};

#endif