#include "layers/GameControllerLayer.h"
#include "scenes/MainMenuScene.h"

bool GameControl::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	MenuItemSprite* itemMusicOn = _resourcesManager->createMenuItemWithId(INDEX_OF_IMAGE_MUSIC_ON_FRAME_NAME, INDEX_OF_IMAGE_MUSIC_ON_FRAME_NAME);
	MenuItemSprite* itemMusicOff = _resourcesManager->createMenuItemWithId(INDEX_OF_IMAGE_MUSIC_OFF_FRAME_NAME, INDEX_OF_IMAGE_MUSIC_OFF_FRAME_NAME);

	MenuItemToggle* musicToogle =
		MenuItemToggle::createWithCallback(CC_CALLBACK_1
		(GameControl::setupAudioStateCallBack, this, KEY_SETUP_MUSIC),
		itemMusicOff, itemMusicOn, nullptr);

	musicToogle->setSelectedIndex(UserDefault::getInstance()->getBoolForKey(KEY_SETUP_MUSIC, true));
	musicToogle->setAnchorPoint(Point(1.0f, 1.0f));

	MenuItemSprite* itemSoundOn = _resourcesManager->createMenuItemWithId(INDEX_OF_IMAGE_SOUND_ON_FRAME_NAME, INDEX_OF_IMAGE_SOUND_ON_FRAME_NAME);
	MenuItemSprite* itemSoundOff = _resourcesManager->createMenuItemWithId(INDEX_OF_IMAGE_SOUND_OFF_FRAME_NAME, INDEX_OF_IMAGE_SOUND_OFF_FRAME_NAME);

	MenuItemToggle* soundToogle =
		MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameControl::setupAudioStateCallBack, this, KEY_SETUP_SOUND),
		itemSoundOff, itemSoundOn, nullptr);


	soundToogle->setSelectedIndex(UserDefault::getInstance()->getBoolForKey(KEY_SETUP_SOUND, true));
	soundToogle->setAnchorPoint(Point(1.0f, 1.0f));

	MenuItemSprite* homeButton = _resourcesManager->createMenuItemWithId(INDEX_OF_IMAGE_HOME_FRAME_NAME, INDEX_OF_IMAGE_HOME_CLICKED_FRAME_NAME,CC_CALLBACK_1(GameControl::homeButtonCallBack,this));
	homeButton->setAnchorPoint(Point(1.0f, 1.0f));
	homeButton->setVisible(false);
	homeButton->setName("homeButton");


	Menu* menuController = Menu::create(musicToogle, soundToogle, homeButton, nullptr);
	menuController->alignItemsHorizontally();
	menuController->setPosition(visibleSize.width - homeButton->getContentSize().width/2 - soundToogle->getContentSize().width / 2.0f, visibleSize.height);
	menuController->setName("menuController");
	this->addChild(menuController, zValue::zMiddleground);

	return true;
}

void GameControl::setupAudioStateCallBack(Ref* pSender, const char* keySetup)
{
	int selectedIndex = ((MenuItemToggle*)pSender)->getSelectedIndex();

	if (selectedIndex == AudioState::ON)
	{
		// Setup key audio depend on parameter
		UserDefault::getInstance()->setBoolForKey(keySetup, true);
		// If this is background music callback, do another thing
		if (StringUtils::toString(keySetup).compare(StringUtils::toString(KEY_SETUP_MUSIC)) == 0)
		{
			if (_audioManager->isBackgroundMusicPlaying())
				_audioManager->resumeBackgroundMusic();
			else
			{
				GameSceneState curState = SceneManager::getInstance()->getCurrentGameState();
				if (curState == GameSceneState::Intro || curState == GameSceneState::Credits || curState == GameSceneState::MainMenu)
				{
					_audioManager->playBackgroundMusic(INDEX_OF_INTRO_BACKGROUND_MUSIC_MP3, "mp3");
				}
				else if (curState == GameSceneState::GamePlay)
				{
					_audioManager->playBackgroundMusic(INDEX_OF_GAMEPLAY_BACKGROUND_MUSIC_MP3, "mp3");
				}
			}
		}
		_audioManager->playEffect(INDEX_OF_SETTING_ON_WAV, "wav");
	}
	else if (selectedIndex == AudioState::OFF)
	{
		UserDefault::getInstance()->setBoolForKey(keySetup, false);
		if (StringUtils::toString(keySetup).compare(StringUtils::toString(KEY_SETUP_MUSIC)) == 0) _audioManager->pauseBackgroundMusic();
		_audioManager->playEffect(INDEX_OF_SETTING_OFF_WAV, "wav");
	}
	UserDefault::getInstance()->flush();
}

void GameControl::homeButtonCallBack(Ref* pSender)
{
	AudioManager::getInstance()->stopBackgroundMusic(true);
	//GameplayManager::getInstance()->saveOldGame();
	Scene* scene = MainMenu::createScene();
	TransitionScene* transitionScene = TransitionFade::create(TIME_FADE_TRANSITION,scene);
	Director::getInstance()->replaceScene(transitionScene);
}

void GameControl::visibleHomeButton()
{
	this->getChildByName("menuController")->getChildByName("homeButton")->setVisible(true);
}