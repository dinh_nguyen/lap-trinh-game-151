#ifndef __CONSTANT_H__ 
#define __CONSTANT_H__

#include <string>
#include "Resources.h"

// This file is declaration

enum class GameSceneState {
	Intro    ,
	MainMenu ,
	Option   ,
	Credits  ,
	GamePlay ,
	GameOver ,
	Pause    ,
	Exit     
};

enum ChessType {
	RED_KING      = 0,
	RED_ADVISOR   = 1,
	RED_ELEPHANT  = 2,
	RED_HORSE     = 3,
	RED_CAR       = 4,
	RED_CANON     = 5,
	RED_SOLDER    = 6,

	BLACK_KING    = 7,
	BLACK_ADVISOR = 8,
	BLACK_ELEPHANT= 9,
	BLACK_HORSE   = 10,
	BLACK_CAR     = 11,
	BLACK_CANON   = 12,
	BLACK_SOLDER  = 13
};


enum zValue {
	zBackground   = 0,
	zMiddleground = 1,
	zForeground   = 2
};

// State
typedef enum {
	OFF = 0,
	ON  = 1
} AudioState;

// PIECES

// MODE
enum Mode {
	PLAYER_VS_PLAYER = 0,
	PLAYER_VS_COMPUTER = 1
};

// PLAYER
enum PLAYER {
	PLAYER_1_RED = 0,
	PLAYER_2_BLACK = 1
};

// AI Mode
enum AI_Mode
{
	NO_AI  = 0,
	EASY   = 1,
	NORMAL = 2,
	HARD   = 3
};

// PIECES STATE IN BOARD
enum PiecesState {
	WAS_BEATING = -1,
	BLANK = 32
};

// CONDITION IN MOVING
enum MoveCondition {
	NO = 0,
	CHESSMATE = 1
};

// Selection in Main Menu
enum MainMenuSelection {
	PLAYER_VS_PLAYER_MODE   = 0,
	RED                     = 1,
	BLACK                   = 2,
	PLAYER_VS_COMPUTER_MODE = 3,
	EASY_LEVEL              = 4,
	NORMAL_LEVEL            = 5,
	HARD_LEVEL              = 6,
	BACK                    = 7,
	NEXT                    = 8
};



// BOARD properties
extern const int BOARD_WIDTH;
extern const int BOARD_HEIGHT;

// Tags

extern const int HIGHLIGHTED_NODE;
extern const int HIGHLIGHTED_NODE_CAN_MOVING_TAG;
extern const int HIGHLIGHTED_CHESS_CAN_DEFEATED_TAG;

// UI Tags
extern const int MAIN_MENU_LAYOUT;
extern const int SELECT_PLAYING_MODE_LAYOUT;
extern const int SELECT_LEVEL_COMPUTER_LAYOUT;



// Transition scene time
extern const float TIME_FADE_TRANSITION ;
extern const float TIME_JUMP_ZOOM_TRANSITION;
extern const float TIME_DELAY_BEFORE_DOING_TRANSITION_SCENES ;

// Note: If don't use extern to declare 2 new variables and split to cpp file
// 2 line below will raise error

// User set up information
extern const char* KEY_SETUP_SOUND ;
extern const char* KEY_SETUP_MUSIC ;
extern const char* KEY_SETUP_MODE_PLAYING;
extern const char* KEY_SETUP_YOUR_PLAYER;
extern const char* KEY_SETUP_LEVEL_COMPUTER;

// Data stored to continue game
extern const char* KEY_CAN_CONTINUE;
extern const char* KEY_DATA_CONTINUE_GAME;
extern const char* KEY_TIMER_RED;
extern const char* KEY_TIMER_BLACK;
extern const char* KEY_MODE_GAME;
extern const char* KEY_SIDE;


// GRID
extern const float GRID_X;
extern const float GRID_Y;

// BOARD
extern const int startIndexSide[2];
extern const int endIndexSide[2];



#endif