import glob
import plistlib
import os
import re
from fnmatch import fnmatch
import json
from pprint import pprint

#def SubDirPath(d):
	#return  [name for name in os.listdir(d)
					#if os.path.isdir(os.path.join(d,name))]
					
def scan_dir(dir):
	lstDir = []
	for name in os.listdir(dir):
		path = os.path.join(dir,name)
		if (os.path.isdir(path) and not (path in lstDir)) :
			lstDir.append(path)
			lstDir.extend(scan_dir(path))
	return lstDir

def writeContentToFile(file,listContent):
	file.write("{\n")
	for content in listContent :
		file.write("\t"+"\""+str(content)+"\",")
		file.write("\n")
	file.write("\t"+"nullptr")
	file.write("\n")
	file.write("};")
	
def initResourceConstArray(file,listContent,varName):
	file.write("char const *"+ varName + "[]" + " =\n")
	writeContentToFile(file,listContent);
	file.write("\n")
	
def writeDefContent(file,prefix,var,postfix,value):
	data = str(prefix).ljust(10,str(unichr(32)))
	data = data +var
	file.write(data.ljust(80,str(unichr(32))))
	file.write(postfix)
	file.write(str(value).ljust(3,str(unichr(32))))
	
def writeIndexOfContentToFile(file,listFile) :
	for i in range(0,len(listFile)):
		element = listFile[i].split(".",1)[0]
		extension = listFile[i].split(".",1)[1]
		numberOfSlashAppear = element.count('/')
		if "/" in listFile[i]: 
			element = element.split("/",numberOfSlashAppear)[numberOfSlashAppear]
		element = element.replace("-","_")
		element = element.replace(" ","_")
		writeDefContent(file,"#define","INDEX_OF_"+element.upper()+"_"+extension.upper(),"",i)
		file.write("\n")

def writeIndexOfAnimationFrameToFile(file,listFile,postfix) :
	for i in range(0,len(listFile)):
		element = listFile[i]
		element = element.replace("-","_")
		element = element.replace(" ","_")
		element = element[:-4]
		if ("0000" in element) :
			element = element[:-4]
			writeDefContent(file,"#define","INDEX_OF_ANIMATION_"+element.upper()+str(postfix),"",i)
		else : 
			writeDefContent(file,"#define","INDEX_OF_IMAGE_" +element.upper()+str(postfix),"",i)
		file.write("\n")

def initNumberOfFrameAnimationToFile(file,listContent,length,varName):
	file.write("const int " + varName +"[]"+" =")
	file.write("\n{")
	file.write("\n")
	if len(listContent) > 0:
		for content in listContent[:-1] :
			file.write("\t"+str(content)+",")
			file.write("\n")
		file.write("\t"+str(listContent[-1]))
	else:
		file.write("\t"+"-999")
	file.write("\n")
	file.write("};")
	

root = os.getcwd()

# You modify it to change number of file extensions
patterns =['*.png','*.plist','*.mp3','*.wav','*.ogg','*.ttf','*.otf','*.fnt','*.json']

# Create a file
filenameHeader = "../Classes/Resources.h"
header = open(filenameHeader,'w') # w is new , a is override

filenameCpp = "../Classes/Resources.cpp"
cpp =open(filenameCpp,'w')

# Number of file in cur and sub folders ( Extends : <extension>_count)
png_count   = len(glob.glob1(root,"*.png"))
plist_count = len(glob.glob1(root,"*.plist"))
mp3_count   = len(glob.glob1(root,"*.mp3"))
wav_count   = len(glob.glob1(root,"*.wav"))
ogg_count	= len(glob.glob1(root,"*.ogg"))
ttf_count	= len(glob.glob1(root,"*.ttf"))
otf_count	= len(glob.glob1(root,"*.otf"))
fnt_count	= len(glob.glob1(root,"*.fnt"))
json_count  = len(glob.glob1(root,"*.json"))

# List of plist file and animation first frame
firstFrameList  = []
bodyJSONList	= []

# List of file in cur and sub folders ( Extends : <extension>LstFile )
pngLstFile    = []
plistLstFile  = []
mp3LstFile    = []
wavLstFile    = []
oggLstFile    = []
ttfLstFile    = []
otfLstFile	  = []
fntLstFile	  = []
jsonLstFile	  = []

# List of number of frame animation
animationNumberOfFrameList = []
animationItemCheckedList   = []

subdirList = filter(None,scan_dir(root))

for subdir in subdirList :
	for patternElement in patterns :
		execCmd = patternElement[2:]+'_count' + '+= len(glob.glob1(subdir,' + '"' + patternElement +'"' +')' +')'
		exec(execCmd)
	
for pattern in patterns :
	for path, subdirs, files in os.walk(root) :
		for name in files :
			if fnmatch(name,pattern) :
				fullPath = os.path.join(path,name)
				absPath = re.search('\Resources(.+)',fullPath).group(1)
				absPath = absPath.replace("\\","/")
				absPath = absPath[1:]
				execCmd = pattern[2:] + 'LstFile' + '.append' +'("' + absPath +'")'
				exec(execCmd)
	# Add File (Sorted by time) to List
	execCmdSortList = pattern[2:] + 'LstFile' + '.sort(key=lambda s: os.path.getmtime(s))'
	exec(execCmdSortList)

for plEle in plistLstFile:
	#pl = plistlib.readPlist(plEle)
	if 'data' in plEle:
		plistLstFile.remove(plEle)
		plist_count = plist_count - 1
	
# Add number of frame Animation , animation object (first frame)  to seperate List
for plEle in plistLstFile :
	pl = plistlib.readPlist(plEle)
	if 'frames' in pl:
		item = pl["frames"]
		listKeySorted = sorted(item.iterkeys())
		for key,keyNext in zip(listKeySorted,listKeySorted[1:]) :
			keyRemovedExtension = key[:-4]
			keyWithLast4Char    = keyRemovedExtension[-4:]
			if (keyWithLast4Char == "0000" and keyRemovedExtension not in firstFrameList ) :
				firstFrameList.append(keyRemovedExtension)
			elif (keyWithLast4Char.isdigit() ) :
				if ((key[:-8] != keyNext[:-8])  and ((key[:-8] + "0000") not in animationItemCheckedList)):
					animationNumberOfFrameList.append(int(keyWithLast4Char)+1)
					animationItemCheckedList.append(key[:-8] + "0000")
				elif ((keyNext == listKeySorted[-1] ) and ((keyNext[:-8] + "0000") not in animationItemCheckedList)):
					keyNextRemovedExtension = keyNext[:-4]
					animationNumberOfFrameList.append(int(keyNextRemovedExtension[-4:])+1)
					animationItemCheckedList.append(keyNext[:-8] + "0000")
				
# Read json file
for jsonEle in jsonLstFile:
	with open(jsonEle) as data_file:
		data = json.load(data_file)
	removedPrefixData = data["rigidBodies"][0]["name"].encode('utf-8')
	bodyJSONList.append(removedPrefixData)
#print(bodyJSONList)
				
# Add image object in sprite sheet to list
for plEle in plistLstFile :
	pl = plistlib.readPlist(plEle)
	if 'frames' in pl:
		item = pl["frames"]
		for key in item.iterkeys() :
			keyRemovedExtension = key[:-4]
			keyWithLast4Char    = keyRemovedExtension[-4:]
			keyFilter = re.sub(r'[^0-9]','',keyWithLast4Char)
			if ( not keyFilter and keyRemovedExtension not in firstFrameList ) :
				firstFrameList.append(keyRemovedExtension)


for i in range(0,len(firstFrameList)) :
	firstFrameList[i] = firstFrameList[i]+".png"

# Write Resources.h
			
header.write("#ifndef\t__RESOURCES_H__")
header.write("\n")
header.write("#define\t__RESOURCES_H__")
header.write("\n\n")

for patternElement in patterns :
	execCmd = 'writeIndexOfContentToFile(header,'+patternElement[2:]+'LstFile)'
	exec(execCmd)
	header.write("\n")

writeIndexOfAnimationFrameToFile(header,firstFrameList,"_FRAME_NAME")
header.write("\n")

for patternElement in patterns :
	patternElement = patternElement[2:]
	execCmd = 'numElement = '+'str(' + patternElement+'_count'+')'
	#ljust : canh le trai , bao nhieu ky tu cho truoc
	exec(execCmd)
	writeDefContent(header,"const int","NUMBER_OF_"+patternElement.upper()+"_FILE","=  ",numElement)
	header.write(";")
	header.write("\n")

header.write("\n")				
writeDefContent(header,"const int","NUMBER_OF_OBJECT_IN_PLIST_FILE","=  ",len(firstFrameList))
header.write(";")
header.write("\n")
writeDefContent(header,"const int","NUMBER_OF_ANIMATION","=  ",len(animationNumberOfFrameList))
header.write(";")
header.write("\n\n")

for patternElement in patterns :
	patternElement = patternElement[2:]
	writeDefContent(header,"extern const char\t*",patternElement+"ResourcesList[]"," ","")
	header.write(";")
	header.write("\n")

writeDefContent(header,"extern const char\t*","objectNameInPlistFileList[]"," ","")
header.write(";")
header.write("\n")
writeDefContent(header,"extern const int\t","numberOfFrameAnimationList[]","","")	
header.write(";")
header.write("\n")
writeDefContent(header,"extern const char\t*","objectInJSONList[]"," ","")
header.write(";")
header.write("\n")
header.write("#endif")



# Write cpp file
cpp.write("#include \"Resources.h\"")	
cpp.write("\n\n")

# Write definition to Resources.cpp
# Pattern 
for patternE in patterns :
	execCmd = 'initResourceConstArray(cpp,' + patternE[2:] + 'LstFile,' + '"' + patternE[2:] + 'ResourcesList'+'"' + ')\n'
	exec(execCmd)
	cpp.write("\n")
	
execCmd = 'initResourceConstArray(cpp,' + 'firstFrameList,' + '"' + 'objectNameInPlistFileList'+'"'+')\n'
exec(execCmd)

# Number Of Frame In Animation
cpp.write("\n")
initNumberOfFrameAnimationToFile(cpp,animationNumberOfFrameList,len(animationNumberOfFrameList),"numberOfFrameAnimationList")

# Number Of JSON object
cpp.write("\n")
execCmd = 'initResourceConstArray(cpp,' + 'bodyJSONList,' + '"' + 'objectInJSONList'+'"'+')\n'
exec(execCmd)

"""
for distKey in firstFrameList[:-1] :
	distKey = distKey[:-4] +'0000' + distKey[-4:]
	header.write(distKey)
	header.write("\n")
header.write(firstFrameList[-1])
header.write("\n")
"""

header.close()
cpp.close()
