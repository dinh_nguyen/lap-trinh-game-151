import itertools, random
from __init__ import *
# from ass_02 import *
import Physic

os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (200, 70)

class Time(object):
    deltaTime = FPSCLOCK = None
    FPS = 30

    @staticmethod
    def init(clock):
        Time.FPSCLOCK = clock
        Time.deltaTime = 0
        Time.unscaleDeltaTime = 0
        Time.timeScale = 1

    @staticmethod
    def Update():
        Time.unscaleDeltaTime = Time.FPSCLOCK.tick(Time.FPS) / 1000.0
        Time.deltaTime = Time.unscaleDeltaTime * Time.timeScale


class ButtonManager(object):

    @staticmethod
    def Menu_BtnPlay_Press(button):
        Global.GameOver = False
        Global.ConfPress = False
        Global.Player1Score = 0
        Global.Player2Score = 0
        button.arg0 = False

        # a = b = 0
        # while abs(a) < 1.5 and abs(b) < 1.5:
        #     a = random.randint(-30, 30)/10.0
        #     b = random.randint(-30, 30)/10.0

        #PlayingScene.pokeballSprite.rd.setVelocity(Vector2(a, b))
        PlayingScene.reset()
        PlayingScene.init()
        runGame()

    @staticmethod
    def Menu_BtnAbout_Press(button):
        button.arg0 = False
        about()

    @staticmethod
    def BtnMenu_Press(button):
        button.arg0 = False
        menu()
    @staticmethod
    def BtnOption_Press(button):
        button.arg0 = False
        option()
    @staticmethod
    def BtnConfig_Press(button):
        if not Global.ConfPress:
            Global.ConfPress = True
            Global.ConfKeyPress = False
            button.arg0 = False
            config()
    @staticmethod
    def BtnConfirm_Press(button):
        Global.ConfPress = False
        Global.ConfKeyPress = True
        Global.ConfigCheckbox = button.Checkbox
    @staticmethod
    def BtnCancel_Press(button):
        Global.ConfPress = False
        Global.ConfKeyPress = True

class CheckBox(Drawable):
    check = False
    disable = False
    def __init__(self, lstTexture, normalSpriteName, backGroundColor = (255,255,255)):
        self.sprite = None
        self.touch = False
        self.onTouchUpInside = None
        self.bakColor = backGroundColor
        self.UncheckState = None
        self.CheckedState = None

        self.SetNormalStateSprite(lstTexture,normalSpriteName)

        self.position = Vector2()
        self.size = Size(self.normalSprite.get_width(), self.normalSprite.get_height())
        self.rect = pygame.Rect(self.position.toTuple(), self.size.toTuple())

    def Update(self):

        if not self.disable:
            if self.sprite == None:
                self.sprite = self.normalSprite
            if InputManager.mouseClick == InputManager.ONMOUSEDOWN:
                if self.rect.collidepoint(InputManager.mouses):
                    self.touch = True
            elif InputManager.mouseClick == InputManager.ONMOUSEUP:
                if self.touch and self.rect.collidepoint(InputManager.mouses):
                    self.check = not self.check
                    self.onTouchUpInside(self)
                self.touch = False

    def SetNormalStateSprite(self, lstTexture, name):
        self.normalSprite = Resource.Load(lstTexture, name)
        assert self.normalSprite != None, 'Invalid normal State Image Name'

    def Draw(self):
        pygame.draw.rect(Window.displaySurface,self.bakColor,self.rect)
        if self.check:
            Window.Draw(self.normalSprite,self.rect)

class Wall:

    def __init__(self, position, halfSize):
        self.halfSize = halfSize
        self.SetPosition(position)

        self.collider = Global.world.addWallAABB(self)
        self.collider.halfSize = halfSize
        self.rd.staticObject = True
        self.rd.restitution = 1.1

    def GetPosition(self):
        return self.position

    def SetPosition(self, position):
        self.position = position
        self.topleft = (self.position - self.halfSize).toTuple()


class Pokemon(Image):
    def __init__(self, player, indexBar, indexPosition, numberOfFrame, aRect):
    #listGridRect):
        self.currentClip = []
        self.currentFrame = 0
        self.player = int(player)
        self.indexBar = int(indexBar)
        self.indexPosition = int(indexPosition)
        self.numberOfFrame = int(numberOfFrame) + 1
        self.timeToChangeFrame = 0.0
        self.rect = aRect
        self.colliderRect = self.rect.copy()

        for i in xrange(0, self.numberOfFrame):
            self.currentClip.append(Resource.listTexture["PKM-{}-{}-{}-{}".format(
                self.player, self.indexBar, self.indexPosition, i)])

        self.sprite = self.currentClip[self.currentFrame]
        self.timePerFrame = Constant.FOOS_PLAYER_ANIMATION_TIME / self.numberOfFrame
        self.updateSprite()

        pokemoncollider = Global.world.addPokemonCollider(self)
        pokemoncollider.halfSize = Vector2(self.colliderRect.width/2, self.colliderRect.height/2)
        self.rd.gravityScale = 0
        self.rd.staticObject = True

    def draw(self):
        Window.displaySurface.blit(self.sprite, self.rect)

    def drawWithOpacity(self, opacity):
        image = self.sprite.copy()
        image.fill((255, 255, 255, opacity), None, pygame.BLEND_RGBA_MULT)
        Window.displaySurface.blit(image, self.rect)

    def drawActive(self):
        self.draw()

    def drawDeactive(self):
        self.drawWithOpacity(150)

    def updateTimeLife(self):
        self.timeToChangeFrame += Time.deltaTime

        if self.timeToChangeFrame > self.timePerFrame:
            self.timeToChangeFrame = 0
            self.currentFrame = (self.currentFrame + 1) % self.numberOfFrame
            self.updateSprite()

    def updateSprite(self):
        tmpSprite = self.currentClip[self.currentFrame]
        self.colliderRect.size = tmpSprite.get_size()
        self.colliderRect.topleft = self.rect.topleft

        offsetX = (Constant.GRID_X  - self.colliderRect.width) / 2
        offsetY = (Constant.GRID_Y  - self.colliderRect.height) / 2
        surfaceTmp = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
        surfaceTmp.blit(tmpSprite, (offsetX, offsetY))
        self.sprite = surfaceTmp
        self.colliderRect.left += offsetX
        self.colliderRect.top += offsetY
        self.position = Vector2(self.rect.centerx, self.rect.centery)

    def moveY(self, velocity):
        self.rect.top += velocity
        self.position.x = self.rect.centerx
        self.position.y = self.rect.centery

    def moveX(self, velocity):
        self.rect.left += velocity
        self.position.x = self.rect.centerx
        self.position.y = self.rect.centery


    def GetPosition(self):
        return self.position

    def SetPosition(self, position):
        self.position = position
        self.rect.center = self.position.toTuple()

class Pokeball(Image):
    def __init__(self, listTexture, name):
        super(Pokeball, self).__init__(listTexture, name)
        self.SetPosition(Vector2(self.rect.centerx, self.rect.centery))
        self.size = self.sprite.get_width()/2

        c = Global.world.addPokeballCollider(self)
        c.radius = self.size
        self.rd.gravityScale = 0
        self.rd.restitution = 1.1

    def Draw(self):
        Window.Draw(self.sprite, self.rect)

    def GetPosition(self):
        return self.position

    def SetPosition(self, position):
        self.position = position
        self.rect.center = self.position.toTuple()

class RotateBar:
    def __init__(self, player, index, listPokemon):
        self.listPokemon = listPokemon
        self.player = int(player)
        self.index = int(index)
        self.isActive = False

    def update(self):
        self.updateMove()
        self.updateActive()

    def drawSelected(self):
        image = None
        if self.player == Constant.PLAYER_1:
            image = Resource.Load(Resource.listTexture,'Blue')
        elif self.player == Constant.PLAYER_2:
            image = Resource.Load(Resource.listTexture,'Red')
        pokemonIndex = self.listPokemon[0]
        pokemonRect = pokemonIndex.rect.copy()
        pokemonRect.left = pokemonIndex.rect.left + 30
        if pokemonIndex.indexPosition == 0 and pokemonIndex.indexBar == 0:
            pokemonRect.top  = 390
            if self.player == Constant.PLAYER_1:
                pokemonRect.left = pokemonIndex.rect.left + 15
            elif self.player == Constant.PLAYER_2:
                pokemonRect.left = pokemonIndex.rect.left + 60
        else:
            pokemonRect.top  = 550
        #image.rect = pokemonIndex.rect
        Window.displaySurface.blit(image, pokemonRect)


    def draw(self):
        for pokemon in self.listPokemon:
            if self.isActive == True:
                pokemon.drawActive()
                self.drawSelected()
            else:
                pokemon.drawDeactive()

    def moveY(self, velocity):
        height = Window.displaySurface.get_size()[1]
        if (velocity > 0 and self.getBorderBot() > height):
            return
        elif (velocity < 0 and self.getBorderTop() < 0):
            return
        for pokemon in self.listPokemon:
            pokemon.moveY(velocity)
            pokemon.rd.setVelocity(Vector2(0, velocity))

    def moveX(self, velocity):
        for pokemon in self.listPokemon:
            pokemon.moveX(velocity)

    def getBorderTop(self):
        tmpPokemon = min(self.listPokemon, key=lambda pokemon: pokemon.rect.top)
        return tmpPokemon.rect.top

    def getBorderBot(self):
        tmpPokemon = max(self.listPokemon, key=lambda pokemon: pokemon.rect.bottom)
        return tmpPokemon.rect.bottom

    def updateMove(self):
        if (self.isActive == True):
            # Border player with goal board (remove if collision detection)
            if (len(self.listPokemon) == 1): # Goal Keeper moving
                if(self.getBorderTop() <= 190 and (InputManager.isPlayer1Moving == Constant.MOVE_UP or InputManager.isPlayer2Moving == Constant.MOVE_UP)):
                    return
                if(self.getBorderBot() >= 410 and (InputManager.isPlayer1Moving == Constant.MOVE_DOWN or InputManager.isPlayer2Moving == Constant.MOVE_DOWN)):
                    return

            if (self.player == Constant.PLAYER_1):
                self.moveY(InputManager.isPlayer1Moving * Constant.VELOCITY_PER_FRAME_PLAYER)
                if not InputManager.isPlayer1Moving:
                    for pokemon in self.listPokemon:
                        pokemon.rd.setVelocity(Vector2(0, 0))

            else:  # player 2 active
                self.moveY(InputManager.isPlayer2Moving * Constant.VELOCITY_PER_FRAME_PLAYER)
                if not InputManager.isPlayer2Moving:
                    for pokemon in self.listPokemon:
                        pokemon.rd.setVelocity(Vector2(0, 0))


    def updateActive(self):
        for pokemon in self.listPokemon:
            if self.isActive == True:
                pokemon.updateTimeLife()

    def setActive(self, isActive):
        self.isActive = isActive

    # def updateMove

    def move(self):
        pass

class myWorld(Physic.World):

    def __init__(self):
        super(myWorld, self).__init__()
        self.listHalfSpace = []
        self.listWallAABB = []
        self.listPokemon = []
        self.pokeball = None

    def reset(self):
        super(myWorld, self).reset()
        del self.listHalfSpace [:]
        del self.listWallAABB [:]
        del self.listPokemon [:]
        self.pokeball = None

    def Update(self):
        super(myWorld, self).Update()

        for wallHalfPlane in self.listHalfSpace:
            Physic.ColliderDetector.CirclevsHalfPlane(self.pokeball, wallHalfPlane, self.cData)

        for wallAABB in self.listWallAABB:
            Physic.ColliderDetector.AABBvsCircle(wallAABB, self.pokeball, self.cData)

        for pokemon in self.listPokemon:
            Physic.ColliderDetector.AABBvsCircle(pokemon, self.pokeball, self.cData)

        Physic.ColliderResolve.resolveCollision(self.cData.listOfContact)

    def addPokemonCollider(self, pokemon):
        c = self.addCollider(Physic.AABB, pokemon)
        self.listPokemon.append(c)
        return c

    def addPokeballCollider(self, pokeball):
        c = self.addCollider(Physic.Circle, pokeball)
        self.pokeball = c
        return c

    def addWallHalfSpace(self, direction, offset):
        ground = Physic.HalfSpace()
        ground.offset = offset
        ground.direction = direction
        self.listHalfSpace.append(ground)
        self.listOfCollider.append(ground)

    def addWallAABB(self, wall):
        c = self.addCollider(Physic.AABB, wall)
        self.listPokemon.append(c)
        return c

def checkForQuit():
    for event in pygame.event.get(QUIT):  # get all the QUIT events
        terminate()  # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP):  # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate()  # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event)  # put the other KEYUP event objects back


def terminate():
    pygame.quit()
    sys.exit()


class Global:
    # Test
    testButton = "OptionBG"
    # Test

    # listFoosPlayerRect = []
    # listRotateBar = []

    GameOver = False
    Player1Score = 0
    Player2Score = 0
    lstButton = []
    ConfPress = False
    ConfKeyPress = False
    musicVolume = 0.5
    soundVolume = 0.5
    ConfigCheckbox = [False,False] #[music,sound]

    world = myWorld()#Physic.World()


class PlayingScene:
    listFoosPlayerRect = []
    listFoosPlayer = []
    listRotateBar = []
    listWall = []
    pokeballSprite = None

    @staticmethod
    def reset():
        del PlayingScene.listFoosPlayerRect[:]
        del PlayingScene.listFoosPlayer[:]
        del PlayingScene.listRotateBar[:]

    @staticmethod
    def init():
        Global.world.reset()

        PlayingScene.listWall.append(Wall(Vector2(9, 108), Vector2(9, 108)))
        PlayingScene.listWall.append(Wall(Vector2(9, 492), Vector2(9, 108)))
        PlayingScene.listWall.append(Wall(Vector2(791, 108), Vector2(9, 108)))
        PlayingScene.listWall.append(Wall(Vector2(791, 492), Vector2(9, 108)))

        Global.world.addWallHalfSpace(Vector2(0, 1), 18)
        Global.world.addWallHalfSpace(Vector2(0, -1), -Constant.WINDOWHEIGHT+18)

        # Init player rect
        PlayingScene.listFoosPlayerRect = Util.CalculateScreenGrid((700, 500),
                                                                   (Constant.GRID_X, Constant.GRID_Y),
                                                                   Constant.MARGIN_WINDOW)

        GoalKeeperRectP1 = Rect(0, 0, Constant.GRID_X, Constant.GRID_Y)
        GoalKeeperRectP1.center = (Window.displaySurface.get_size()[0] - Constant.GRID_X / 2,
                                   Window.displaySurface.get_size()[1] / 2)
        GoalKeeperRectP1.x += Constant.OFFSET_GOALKEEPER_PLAYER1_X

        GoalKeeperRectP2 = Rect(0, 0, Constant.GRID_X, Constant.GRID_Y)
        GoalKeeperRectP2.center = (Constant.GRID_X / 2, Window.displaySurface.get_size()[1] / 2)
        GoalKeeperRectP2.x -= Constant.OFFSET_GOALKEEPER_PLAYER2_X

        PlayingScene.listFoosPlayerRect.append(GoalKeeperRectP1)
        PlayingScene.listFoosPlayerRect.append(GoalKeeperRectP2)


        #listGridRect[(2 * (self.indexBar - 1) + self.player) * Constant.MAX_NUMBER_OF_PLAYER_COLUMN + self.indexPosition]

        # Init player (Pokemon) from data
        for tupleKey, value in Resource.pokemonData.items():
            typeOfPlayer = int(tupleKey[0])
            indexBar = int(tupleKey[1])
            indexPosition = int(tupleKey[2])

            if indexBar != 0:
                index = (2 * (indexBar - 1) + typeOfPlayer) * Constant.MAX_NUMBER_OF_PLAYER_COLUMN + indexPosition
                pokemonNotGoalKeeper = Pokemon(typeOfPlayer,
                                               indexBar,
                                               indexPosition,
                                               int(value),
                                               PlayingScene.listFoosPlayerRect[index])
                PlayingScene.listFoosPlayer.append(pokemonNotGoalKeeper)

            else:
                listRectPlayers = PlayingScene.listFoosPlayerRect[-2:]
                pokemonGoalKeeper = Pokemon(typeOfPlayer,
                                            indexBar,
                                            indexPosition,
                                            value,
                                            listRectPlayers[typeOfPlayer])
                PlayingScene.listFoosPlayer.append(pokemonGoalKeeper)

        # Group player(pokemon) object by tuple (player, indexColumn)
        critetion = lambda t: (t.player, t.indexBar)
        for key, group in itertools.groupby(sorted(PlayingScene.listFoosPlayer, key=critetion), critetion):
            tmpBar = RotateBar(key[0], key[1], [])
            for pokemon in group:
                tmpBar.listPokemon.append(pokemon)
            PlayingScene.listRotateBar.append(tmpBar)

        # Move column to right format
        for bar in PlayingScene.listRotateBar:
            numOfPLayerInColumn = len(bar.listPokemon)
            if numOfPLayerInColumn > 1 and numOfPLayerInColumn < 4:
                bar.moveY(Constant.GRID_Y / 2)

        for bar in PlayingScene.listRotateBar:
            numOfPLayerInColumn = len(bar.listPokemon)
            if (numOfPLayerInColumn == 4):
                if (bar.player == 0):
                    bar.moveX(-45)
                elif(bar.player == 1):
                    bar.moveX(45)
            elif (numOfPLayerInColumn == 3):
                if (bar.player == 0):
                    bar.moveX(-5)
                elif(bar.player == 1):
                    bar.moveX(5)
            elif (numOfPLayerInColumn == 2):
                if (bar.player == 0):
                    bar.moveX(25)
                elif (bar.player == 1):
                    bar.moveX(-25)

        pokemonMidTop = [foosPlayer for foosPlayer in PlayingScene.listFoosPlayer if foosPlayer.indexBar == 2 and foosPlayer.indexPosition == 0 ]
        for pkm in pokemonMidTop:
            pkm.moveY(-60)
        pokemonMidBot = [foosPlayer for foosPlayer in PlayingScene.listFoosPlayer if foosPlayer.indexBar == 2 and foosPlayer.indexPosition == 2 ]
        for pkm in pokemonMidBot:
            pkm.moveY(60)

        # Init pokeball
        PlayingScene.pokeballSprite = Pokeball(Resource.listTexture,'Poke_Ball')
        PlayingScene.pokeballSprite.SetPosition(Vector2(Constant.WINDOWWIDTH/2, Constant.WINDOWHEIGHT/2))

        selectedDir = 0
        a=0
        while math.fabs(a) <= 2:
            a = random.randint(-40, 40)
            a = float(a)/10.0
            selectedDir = random.randint(0,1)
        if (selectedDir == 0):
            b = a
        else:
            b = - a
        PlayingScene.pokeballSprite.rd.setVelocity(Vector2(a, b))

    @staticmethod
    def update():
        PlayingScene.updatePlayers()

    @staticmethod
    def draw():
        for verticalBar in PlayingScene.listRotateBar:
            verticalBar.draw()
        PlayingScene.pokeballSprite.Draw()

    @staticmethod
    def updatePlayers():
        for verticalBar in PlayingScene.listRotateBar:

            if verticalBar.player == Constant.PLAYER_1:
                if verticalBar.index == InputManager.player1SelectedColumn:
                    verticalBar.setActive(True)
                else:
                    verticalBar.setActive(False)

            if verticalBar.player == Constant.PLAYER_2:
                if verticalBar.index == InputManager.player2SelectedColumn:
                    verticalBar.setActive(True)
                else:
                    verticalBar.setActive(False)

            verticalBar.update()


def main():
    pygame.init()
    Time.init(pygame.time.Clock())
    Resource.init()
    AudioManage.init()
    Window.init()
    InputManager.Init()

    Window.CoordinationForUI()
    PlayingScene.init()
    Physic.Util.display = Window.displaySurface

    while True:
        checkForQuit()
        intro()
        # InputManager.Update(pygame.event.get())
        #
        # InputManager.UpdateMoveState()
        # InputManager.UpdateSelectedState()
        #
        # Window.displaySurface.fill((255, 255, 255, 0))
        #
        # PlayingScene.update()
        # Global.world.Update()
        #
        # PlayingScene.draw()

        # for i in range(0, len(Global.listFoosPlayerRect)):
        #     flag = (i / 4) % 2
        #     if i == (len(Global.listFoosPlayerRect) - 2):
        #         aColor = Constant.RED
        #     elif flag == 0 or i == (len(Global.listFoosPlayerRect) - 1):
        #         aColor = Constant.BLUE
        #     else:
        #         aColor = Constant.RED
        #     pygame.draw.rect(Window.displaySurface, aColor, Global.listFoosPlayerRect[i], 1)

        # pygame Call
        pygame.display.update()
        Time.Update()

def intro():
    Intro = Image(Resource.listTexture,'IntroBG')
    Intro.SetPositionValue(0,0)
    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Intro.Draw()

        if InputManager.mouseClick == 1 or InputManager.key != None:
            menu()
            break

        # pygame Call
        pygame.display.update()
        Time.Update()

def animateGameStart():
    startTime = 2
    BG = Image(Resource.listTexture,'Field')
    BG.SetPositionValue(0,0)
    BGBorder = Image(Resource.listTexture,'Border')
    BGBorder.SetPositionValue(0,0)
    Pokeball = Resource.listTexture['Shop_Poke_ball'].get_rect()

    while startTime > 0.125:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        BG.DrawWithOpacity(200)
        BGBorder.DrawWithOpacity(255 * (1 - startTime/2))
        tmp = pygame.transform.scale(Resource.listTexture['Shop_Poke_ball'],(int(Pokeball.width * startTime * 2),int(Pokeball.height * startTime * 2)))
        tmp_rect = tmp.get_rect()
        tmp_rect.center = BG.rect.center
        Window.displaySurface.blit(tmp,tmp_rect)

        # pygame Call
        pygame.display.update()
        Time.Update()
        startTime -= Time.deltaTime

def runGame():
    animateGameStart()
    lstTmp = []
    BG = Image(Resource.listTexture,'Field')
    BG.SetPositionValue(0,0)
    lstTmp.append(BG)
    BGBorder = Image(Resource.listTexture,'Border')
    BGBorder.SetPositionValue(0,0)
    lstTmp.append(BGBorder)
    PlayingScene.pokeballSprite.rect.center = BG.rect.center

    Global.lstButton = lstTmp
    game_Font = pygame.font.Font('font/UVNThanhPhoB.TTF',50)

    timer = 1.5 * 60 # 3 minutes



    while not Global.GameOver and timer > 0.0:

        if InputManager.key == K_p:
            Global.ConfPress = True
            Global.ConfKeyPress = False
            config()

        # Check condition player goal
        if (PlayingScene.pokeballSprite.rect.right <= BG.rect.left and PlayingScene.pokeballSprite.rect.top >= 200 and PlayingScene.pokeballSprite.rect.top <= 400 ):
            Global.Player2Score += 1
            goal()
            PlayingScene.reset()
            PlayingScene.init()
        elif (PlayingScene.pokeballSprite.rect.left >= BG.rect.right and PlayingScene.pokeballSprite.rect.top >= 200 and PlayingScene.pokeballSprite.rect.top <= 400):
            Global.Player1Score += 1
            goal()
            PlayingScene.reset()
            PlayingScene.init()
        checkForQuit()
        InputManager.Update(pygame.event.get())
        InputManager.UpdateMoveState()
        InputManager.UpdateSelectedState()
        for button in lstTmp:
            button.Update()
            button.Draw()
        strTimer = "{0}:{1}".format(int(timer/60),int(timer%60))
        Timer_Score = game_Font.render(strTimer,True,(0,0,0))
        Timer_rect  = Timer_Score.get_rect()
        Timer_rect.center = BG.rect.center
        Timer_rect.top = BG.rect.height * 0.04
        Window.displaySurface.blit(Timer_Score,Timer_rect)
        P1_Score = game_Font.render(str(Global.Player1Score),True,(255,0,0))
        P1_rect = P1_Score.get_rect()
        P1_rect.topleft = (BG.rect.width * 0.05,BG.rect.height * 0.04)
        Window.displaySurface.blit(P1_Score,P1_rect)
        P2_Score = game_Font.render(str(Global.Player2Score),True,(0,0,255))
        P2_rect = P2_Score.get_rect()
        P2_rect.topleft = (BG.rect.width * 0.95 - P2_rect.width,BG.rect.height * 0.04)
        Window.displaySurface.blit(P2_Score,P2_rect)



        # Window.displaySurface.fill((255, 255, 255, 0))

        PlayingScene.update()
        Global.world.Update()
        PlayingScene.draw()

        # for i in range(0, len(PlayingScene.listFoosPlayerRect)):
        #     flag = (i / 4) % 2
        #     if i == (len(PlayingScene.listFoosPlayerRect) - 2):
        #         aColor = Constant.RED
        #     elif flag == 0 or i == (len(PlayingScene.listFoosPlayerRect) - 1):
        #         aColor = Constant.BLUE
        #     else:
        #         aColor = Constant.RED
        #     pygame.draw.rect(Window.displaySurface, aColor, PlayingScene.listFoosPlayerRect[i], 1)

        # Test game score
        #PlayingScene.pokeballSprite.rect.x+=5
        timer -= Time.deltaTime
        #Global.Player1Score += 1
     #Global.Player2Score += 1

     # pygame Call
        pygame.display.update()
        Time.Update()
    gameOver()

def config():
    Global.ConfPress = True
    Global.ConfKeyPress = False

    lstTmp = Global.lstButton
    BG = Image(Resource.listTexture,'Field')
    BG.SetPositionValue(0,0)
    Config = Image(Resource.listTexture,'Configure_BG')
    Config.rect.center = BG.rect.center
    lstTmp.append(Config)

    ButtonCancel = Button(Resource.listTexture,'ButtonCancel')
    ButtonCancel.rect.bottomleft = (Config.rect.left + Config.rect.width * 0.5,Config.rect.bottom * 0.97)
    ButtonCancel.onTouchUpInside = ButtonManager.BtnCancel_Press
    lstTmp.append(ButtonCancel)

    ButtonConfirm = Button(Resource.listTexture,'ButtonConfirm')
    ButtonConfirm.rect.bottomleft = (Config.rect.left + Config.rect.width * 0.5,ButtonCancel.rect.top)
    ButtonConfirm.onTouchUpInside = ButtonManager.BtnConfirm_Press
    lstTmp.append(ButtonConfirm)

    ButtonMenu = Button(Resource.listTexture,'ButtonMenu')
    ButtonMenu.rect.bottomleft = (Config.rect.left + Config.rect.width * 0.2 ,Config.rect.bottom * 0.92)
    ButtonMenu.arg0 = True
    lstTmp.append(ButtonMenu)
    ButtonMenu.onTouchUpInside = ButtonManager.BtnMenu_Press

    game_Font = pygame.font.Font('font/UVNThanhPhoB.TTF',50)
    P1_Score = game_Font.render(str(Global.Player1Score),True,(255,0,0))
    P1_rect = P1_Score.get_rect()
    P1_rect.topleft = (BG.rect.width * 0.05,BG.rect.height * 0.04)

    P2_Score = game_Font.render(str(Global.Player2Score),True,(0,0,255))
    P2_rect = P2_Score.get_rect()
    P2_rect.topleft = (BG.rect.width * 0.95 - P2_rect.width,BG.rect.height * 0.04)

    # game_Font = pygame.font.Font('font/VHOPTI_I_0.TTF',35)
    # P1_Score = game_Font.render(str(Global.Player1Score),True,(255,0,0))
    # P1_rect = P1_Score.get_rect()
    # P1_rect.topleft = (BG.rect.width * 0.03,BG.rect.height * 0.3)
    # P2_Score = game_Font.render(str(Global.Player2Score),True,(255,0,0))
    # P2_rect = P2_Score.get_rect()
    # P2_rect.topleft = (BG.rect.width * 0.97 - P2_rect.width,BG.rect.height * 0.3)

    Config_font = pygame.font.Font('font/VHOPTI_I_0.TTF',20)
    Config_font.set_bold
    Config_Music = Config_font.render('Music:',True,(0,0,0))
    Config_Music_Rect = Config_Music.get_rect()
    Config_Music_Rect.bottomleft = (Config.rect.left + Config.rect.width / 4,Config.rect.top + Config.rect.height * 0.4)
    Config_Sound = Config_font.render('Sound:',True,(0,0,0))
    Config_Sound_Rect = Config_Sound.get_rect()
    Config_Sound_Rect.bottomleft = (Config.rect.left + Config.rect.width / 4,Config.rect.top + Config.rect.height * 0.6)

    checkMusic = CheckBox(Resource.listTexture,'deep-sea-tooth')
    checkMusic.check = Global.ConfigCheckbox[0]
    checkMusic.rect.center = Config_Music_Rect.center
    checkMusic.rect.left = Config.rect.right - Config.rect.width * 0.3
    checkMusic.CheckedState = 0.0
    checkMusic.UncheckState = 1.0
    checkMusic.onTouchUpInside = Set_musicVolume
    lstTmp.append(checkMusic)

    checkSound = CheckBox(Resource.listTexture,'deep-sea-tooth')
    checkSound.check = Global.ConfigCheckbox[1]
    checkSound.rect.center = Config_Sound_Rect.center
    checkSound.rect.left = Config.rect.right - Config.rect.width * 0.3
    checkSound.CheckedState = 0.0
    checkSound.UncheckState = 1.0
    checkSound.onTouchUpInside = Set_soundVolume
    lstTmp.append(checkSound)
    while not Global.ConfKeyPress:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        for button in lstTmp:
            button.Update()
            button.Draw()
        ButtonConfirm.Checkbox = [checkMusic.check,checkSound.check]
        Window.displaySurface.blit(P1_Score,P1_rect)
        Window.displaySurface.blit(P2_Score,P2_rect)
        Window.displaySurface.blit(Config_Music,Config_Music_Rect)
        Window.displaySurface.blit(Config_Sound,Config_Sound_Rect)

        # pygame Call
        pygame.display.update()
        Time.Update()
    lstTmp.remove(Config)
    lstTmp.remove(ButtonCancel)
    lstTmp.remove(ButtonConfirm)
    lstTmp.remove(ButtonMenu)
    lstTmp.remove(checkMusic)
    lstTmp.remove(checkSound)

def menu():
    lstTmp = []
    BLACK = (0,0,0)
    menu_Font = pygame.font.Font('font\ETHNOCEN.TTF',35)

    Menu = Image(Resource.listTexture,'Menu_screen')
    Menu.SetPositionValue(0,0)
    menu = Menu.rect
    lstTmp.append(Menu)
    btnPlay = Button(Resource.listTexture,'Menu_Play')
    btnPlay.rect.topright = (menu.right,menu.bottom * 0.2)
    btnPlay.arg0 = True
    btnPlay.onTouchUpInside = ButtonManager.Menu_BtnPlay_Press
    lstTmp.append(btnPlay)
    menu_Label_Play = menu_Font.render('Play',True,BLACK)
    lblPlay = menu_Label_Play.get_rect()
    lblPlay.center = btnPlay.rect.center
    lblPlay.left = menu.width * 0.8

    btnAbout = Button(Resource.listTexture,'Menu_About')
    btnAbout.rect.topright = (menu.right,menu.bottom * 0.45)
    btnAbout.arg0 = True
    btnAbout.onTouchUpInside = ButtonManager.Menu_BtnAbout_Press
    lstTmp.append(btnAbout)
    menu_Label_About = menu_Font.render('About',True,BLACK)
    lblAbout = menu_Label_About.get_rect()
    lblAbout.center = btnAbout.rect.center
    lblAbout.left = menu.width * 0.7

    btnOption = Button(Resource.listTexture,'Menu_Option')
    btnOption.rect.topright = (menu.right,menu.bottom * 0.7)
    btnOption.arg0 = True
    btnOption.onTouchUpInside = ButtonManager.BtnOption_Press
    lstTmp.append(btnOption)
    menu_Label_Option = menu_Font.render('Option',True,BLACK)
    lblOption = menu_Label_Option.get_rect()
    lblOption.center = btnOption.rect.center
    lblOption.left = menu.width * 0.7

    while (btnPlay.arg0 and btnAbout.arg0 and btnOption.arg0):
        checkForQuit()
        InputManager.Update(pygame.event.get())
        for button in lstTmp:
            button.Update()
            button.Draw()

        Window.displaySurface.blit(menu_Label_Play,lblPlay)
        Window.displaySurface.blit(menu_Label_About,lblAbout)
        Window.displaySurface.blit(menu_Label_Option,lblOption)

        # pygame Call
        pygame.display.update()
        Time.Update()

def option():
    lstTmp = []
    OptionBG = Image(Resource.listTexture,'OptionBG')
    OptionBG.SetPositionValue(0,0)
    lstTmp.append(OptionBG)
    Option = Image(Resource.listTexture,'Option')
    Option.rect.topright = (OptionBG.rect.right,OptionBG.rect.bottom * 0.2)
    lstTmp.append(Option)
    ButtonMenu = Button(Resource.listTexture,'ButtonMenu')
    ButtonMenu.rect.bottomleft = (Option.rect.left * 1.1 ,Option.rect.bottom * 0.97)
    ButtonMenu.arg0 = True
    lstTmp.append(ButtonMenu)
    ButtonMenu.onTouchUpInside = ButtonManager.BtnMenu_Press

    while ButtonMenu.arg0:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        for button in lstTmp:
            button.Update()
            button.Draw()
        # pygame Call
        pygame.display.update()
        Time.Update()

def about():
    lstTmp = []
    AboutM = Image(Resource.listTexture,'AboutM')
    AboutM.SetPositionValue(0,0)
    lstTmp.append(AboutM)
    AboutB = Image(Resource.listTexture,'AboutB')
    AboutB.rect.topright = (AboutM.rect.right,AboutM.rect.height * 0.15)
    lstTmp.append(AboutB)
    ButtonMenu = Button(Resource.listTexture,'ButtonMenu')
    ButtonMenu.rect.bottomleft = (AboutB.rect.left * 1.1 ,AboutB.rect.bottom * 0.95)
    ButtonMenu.arg0 = True
    lstTmp.append(ButtonMenu)
    ButtonMenu.onTouchUpInside = ButtonManager.BtnMenu_Press

    while ButtonMenu.arg0:
        checkForQuit()
        InputManager.Update(pygame.event.get())

        for button in lstTmp:
            button.Update()
            button.Draw()

        pygame.display.update()
        Time.Update()

def goal():
    PlayingScene.pokeballSprite.rd.setVelocity(Vector2(0, 0))
    lstTmp = Global.lstButton
    BG = Image(Resource.listTexture,'Field')
    BG.SetPositionValue(0,0)
    Goal = Image(Resource.listTexture,'Goal')
    Goal.rect.center = BG.rect.center
    lstTmp.append(Goal)
    timeGoal = 0.0

    while timeGoal < 2.0:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        timeGoal += Time.deltaTime

        for button in lstTmp:
            button.Update()
            button.Draw()

        pygame.display.update()
        Time.Update()

    PlayingScene.pokeballSprite.position.x = BG.rect.centerx
    PlayingScene.pokeballSprite.position.y = BG.rect.centery
    Goal.SetVisible(False)
    # a = 0
    # b = 0
    # selectedDir = 0
    # while math.fabs(a) <= 3:
    #     a = random.randint(-7, 7)
    #     selectedDir = random.randint(0,1)
    # if (selectedDir == 0):
    #     b = a
    # else:
    #     b = - a
    # PlayingScene.pokeballSprite.rd.setVelocity(Vector2(a, b))


def gameOver():
    Global.GameOver = True
    Global.ConfPress = True
    lstTmp = Global.lstButton
    BG = Image(Resource.listTexture,'Field')
    BG.SetPositionValue(0,0)

    playerLabel = None
    winLabel = None
    # Create win label depend on score
    if (Global.Player1Score > Global.Player2Score):
        playerLabel = Image(Resource.listTexture,'Label-Player1')
        winLabel = Image(Resource.listTexture,'Label-Win-Red')
        winLabel.rect.center  = BG.rect.center
        playerLabel.rect.center = (BG.rect.center[0],BG.rect.center[1] - 100)
    elif (Global.Player1Score < Global.Player2Score):
        playerLabel = Image(Resource.listTexture,'Label-Player2')
        winLabel = Image(Resource.listTexture,'Label-Win-Blue')
        winLabel.rect.center  = BG.rect.center
        playerLabel.rect.center = (BG.rect.center[0],BG.rect.center[1] - 100)
    else:
        playerLabel = Image(Resource.listTexture,'Gameover')
        playerLabel.rect.center = BG.rect.center

    if (winLabel != None): lstTmp.append(winLabel)
    if (playerLabel != None): lstTmp.append(playerLabel)

    PlayingScene.pokeballSprite.rd.setVelocity(Vector2(0, 0))
    PlayingScene.pokeballSprite.position.x = BG.rect.centerx
    PlayingScene.pokeballSprite.position.y = BG.rect.centery

    ButtonMenu = Button(Resource.listTexture,'ButtonMenu')
    ButtonMenu.rect.bottomright = (playerLabel.rect.right + playerLabel.rect.width * 0.3,playerLabel.rect.bottom * 2.0)
    ButtonMenu.arg0 = True
    lstTmp.append(ButtonMenu)
    ButtonMenu.onTouchUpInside = ButtonManager.BtnMenu_Press

    ButtonReplay = Button(Resource.listTexture,'Replay')
    ButtonReplay.rect.bottomleft = (playerLabel.rect.left - playerLabel.rect.width * 0.3,playerLabel.rect.bottom * 2.0)
    ButtonReplay.arg0 = True
    lstTmp.append(ButtonReplay)
    ButtonReplay.onTouchUpInside = ButtonManager.Menu_BtnPlay_Press

    while (ButtonMenu.arg0 and ButtonReplay.arg0):
        checkForQuit()
        InputManager.Update(pygame.event.get())

        for button in lstTmp:
            button.Update()
            button.Draw()

        pygame.display.update()
        Time.Update()

def Set_musicVolume(checkbox):
    if checkbox.check:
        Global.musicVolume = checkbox.CheckedState
    else:
        Global.musicVolume = checkbox.UncheckState

def Set_soundVolume(checkbox):
    if checkbox.check:
        Global.soundVolume = checkbox.CheckedState
    else:
        Global.soundVolume = checkbox.UncheckState

if __name__ == '__main__':
    main()


# Test

if __name__ == '__main__':
    main()
