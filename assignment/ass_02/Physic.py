from __init__ import *
import math, pygame, sys, copy
from pygame.locals import *

debug = False

class Utils(object):

    @staticmethod
    def distance(a, b):
        return math.sqrt( (a.x - b.x)**2 + (a.y - b.y)**2 )

    @staticmethod
    def clampInt(minimum, value, maximum):
        return min(maximum, max(value, minimum))

class World(object):

    def __init__(self):
        self.gravity = Vector2(0, 9.81/30)
        self.cData = ColliderData()
        self.listOfCollider = []
        self.listOfBody = []
        self.customCollisionDetect = None

    def reset(self):
        self.cData.reset()
        del self.listOfBody[:]
        del self.listOfCollider[:]

    def Update(self):

        for body in self.listOfBody:
            body.Update()

        for c in self.listOfCollider:
            c.calculate()

        del self.cData.listOfContact[:]

    def addRigidBody(self, obj):
        rd = RigidBody(obj)
        rd.world = self
        obj.rd = rd
        self.listOfBody.append(rd)

    def addCollider(self, collider, obj, offset = None):
        if not hasattr(obj, 'rd'):
            self.addRigidBody(obj)

        c = collider()
        c.body = obj.rd
        if offset != None:
            c.offset = offset
        self.listOfCollider.append(c)

        return c

class ForceGenerator:

    def __init__(self):
        self.force = None

class RigidBody:

    def __init__(self, obj):
        self.world = None
        self.obj = obj

        self.inv_mass = 0.01
        self.staticObject = False

        self.velocity = Vector2()
        self.acceleration = Vector2()

        self.forceAccum = Vector2()
        self.gravityScale = 1

        self.restitution = 1
        self.listOfForceGenerator = []

    def Update(self):
        if self.obj != None:
            position = self.getPosition()

            if not self.staticObject:
                self.forceAccum.addScaledVector(self.world.gravity, self.gravityScale/self.inv_mass)
                for force in self.listOfForceGenerator:
                    self.forceAccum.addScaledVector(force.force, 1)
                self.acceleration.addScaledVector(self.forceAccum, self.inv_mass)
                self.velocity.addScaledVector(self.acceleration, 1)
                position.addScaledVector(self.velocity, 1)
            else:
                self.velocity.clear()

            self.setPosition(position)

            self.acceleration.clear()
            self.forceAccum.clear()

    def setVelocity(self, other):
        self.velocity.x = other.x
        self.velocity.y = other.y

    def getPosition(self):
        return self.obj.GetPosition()

    def setPosition(self, position):
        self.obj.SetPosition(position)

    def positionAddScaleVector(self, vector, scale):
        position = self.getPosition()
        position.addScaledVector(vector, scale)
        self.setPosition(position)

class Collider(object):

    def __init__(self):
        self.body = None
        self.offset = Vector2()

    def calculate(self):
        self.position = self.body.getPosition() + self.offset

class AABB(Collider):

    def __init__(self, halfSize=Vector2(1,1)):
        super(AABB, self).__init__()
        self.halfSize = halfSize

    def calculate(self):
        super(AABB, self).calculate()
        self.min = self.position - self.halfSize
        self.max = self.position + self.halfSize

        if debug:
            pygame.draw.rect(Util.display, (255, 0, 0), pygame.Rect((self.min.x, self.min.y), (self.halfSize.x*2, self.halfSize.y*2)), 1)

class Circle(Collider):

    def __init__(self):
        super(Circle, self).__init__()
        self.radius = 1

    def calculate(self):
        super(Circle, self).calculate()

        if debug:
            pygame.draw.circle(Util.display, (255, 0, 0), (int(self.position.x), int(self.position.y)), self.radius+1, 1)

class HalfSpace(Collider):

    def __init__(self):
        super(HalfSpace, self).__init__()
        self.direction = None
        self.offset = 0

    def calculate(self):
        if debug :
            pygame.draw.line(Util.display, (255, 0, 0), (0,abs(self.offset)), (800,abs(self.offset)))


class ColliderDetector(object):

    @staticmethod
    def CirclevsHalfPlane(circle, plane, data):
        distance = (plane.direction * circle.position) - circle.radius - plane.offset

        if distance >= 0:
            return False

        contact = Contact()
        contact.normal = plane.direction
        contact.penetration = -distance
        contact.contactPoint = circle.position - plane.direction * (distance + circle.radius)
        contact.a = circle.body
        contact.b = None
        contact.restitution = circle.body.restitution

        data.listOfContact.append(contact)

    @staticmethod
    def AABBvsCircle(aabb, circle, data):
        aabbPos = aabb.position
        circlePos = circle.position

        if abs(circlePos.x - aabbPos.x) - circle.radius > aabb.halfSize.x or \
            abs(circlePos.y - aabbPos.y) - circle.radius > aabb.halfSize.y:
                return False

        closePt = Vector2()
        dist = circlePos.x
        closePt.x = Utils.clampInt(aabbPos.x-aabb.halfSize.x, dist, aabbPos.x+aabb.halfSize.x)
        dist = circlePos.y
        closePt.y = Utils.clampInt(aabbPos.y-aabb.halfSize.y, dist, aabbPos.y+aabb.halfSize.y)

        dist = (closePt - circlePos).squareMagnitude()
        if dist > circle.radius*circle.radius:
            return False

        contact = Contact()
        contact.normal = closePt - circlePos
        contact.normal.normalize()
        contact.contactPoint = closePt
        contact.penetration = circle.radius - math.sqrt(dist)
        contact.restitution = min(aabb.body.restitution, circle.body.restitution)
        contact.a = aabb.body
        contact.b = circle.body

        data.listOfContact.append(contact)
        return True

class Contact:

    def __init__(self):
        self.a = None
        self.b = None

        self.contactPoint = None
        self.normal = 0

        self.penetration = 0
        self.restitution = 1

class ColliderData:

    def __init__(self):
        self.listOfContact = []

    def reset(self):
        self.listOfContact = []

class ColliderResolve:

    @staticmethod
    def resolveCollision(listOfContact):
        ColliderResolve.prepareContact(listOfContact)
        ColliderResolve.applyVelocityChange(listOfContact)
        ColliderResolve.applyPositionChange(listOfContact)

    @staticmethod
    def prepareContact(listOfContact):
        pass

    @staticmethod
    def applyVelocityChange(listOfContact):
        for contact in listOfContact:
            VB = Vector2() if contact.b == None else contact.b.velocity

            rv = VB - contact.a.velocity
            velAlongNormal = rv * contact.normal

            if velAlongNormal != 0:
                e = contact.restitution

                j = -(1+e)*velAlongNormal
                j /= contact.a.inv_mass + 0 if contact.b == None else contact.b.inv_mass

                impulse = contact.normal * j
                if not contact.a.staticObject:
                    contact.a.velocity.addScaledVector(impulse, -contact.a.inv_mass)
                if contact.b != None and not contact.b.staticObject:
                    contact.b.velocity.addScaledVector(impulse, contact.b.inv_mass)

    @staticmethod
    def applyPositionChange(listOfContact):
        for contact in listOfContact:
            percent = 0.2
            slop = 0.01
            sum_inverseMass = 0
            if not contact.a.staticObject:
                sum_inverseMass += contact.a.inv_mass
            if contact.b != None and not contact.b.staticObject:
                sum_inverseMass += contact.b.inv_mass

            correction =  contact.normal * percent * (max(contact.penetration - slop, 0.0)/sum_inverseMass)
            if not contact.a.staticObject:
                contact.a.positionAddScaleVector(correction, contact.a.inv_mass)
            if contact.b != None and not contact.b.staticObject:
                contact.b.positionAddScaleVector(correction,-contact.b.inv_mass)