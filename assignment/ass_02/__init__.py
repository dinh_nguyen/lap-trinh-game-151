import pygame
import sys
import os
import math
import copy
import re
from pygame.locals import *

class Constant:
    # Application properties
    APP_NAME = "Ass2"
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600

    # Path resource
    TEXTURE = 'res/Texture.png'
    TEXTURE_DES = 'res/Texture.txt'
    SOUND = 'res/Sound'
    MUSIC = 'res/Music'

    # Color
    GRAY = (100, 100, 100)
    NAVYBLUE = (60, 60, 100)
    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    ORANGE = (255, 128, 0)
    PURPLE = (255, 0, 255)
    CYAN = (0, 255, 255)

    # Enum rotate bar
    GOALKEEPER_BAR_P1 = 1
    DEFENDER_BAR_P1 = 2
    MIDFIELDER_BAR_P1 = 3
    ATTACKER_BAR_P1 = 4

    GOALKEEPER_BAR_P2 = 'M'
    DEFENDER_BAR_P2 = ','
    MIDFIELDER_BAR_P2 = '.'
    ATTACKER_BAR_P2 = '/'

    ROTATE_BAR = (GOALKEEPER_BAR_P1, DEFENDER_BAR_P1, MIDFIELDER_BAR_P1, ATTACKER_BAR_P1,
                  GOALKEEPER_BAR_P2, DEFENDER_BAR_P2, MIDFIELDER_BAR_P2, ATTACKER_BAR_P2)

    # Enum player
    PLAYER_1 = 0
    PLAYER_2 = 1

    PLAYER = (PLAYER_1, PLAYER_2)

    # Index Position
    POSITION_GOALKEEPER = 0

    POSITION_DEFENDER_TOP = 0
    POSITION_DEFENDER_MID = 1
    POSITION_DEFENDER_BOT = 2

    POSITION_MIDFIELDER_TOP = 0
    POSITION_MIDFIELDER_MID_1 = 1
    POSITION_MIDFIELDER_MID_2 = 2
    POSITION_MIDFIELDER_BOT = 3

    POSITION_ATTACKER_TOP = 0
    POSITION_ATTACKER_MID = 1
    POSITION_ATTACKER_BOT = 2

    # Time
    FOOS_PLAYER_ANIMATION_TIME = 0.75

    #GRID
    GRID_X = 100
    GRID_Y = 100

    # Move Direction
    MOVE_UP = -1
    MOVE_DOWN = 1
    NOT_MOVE = 0
    MOVE = (NOT_MOVE, MOVE_UP, MOVE_DOWN)

    # OFFSET X POSITION GOALKEEPER
    OFFSET_GOALKEEPER_PLAYER1_X = 35
    OFFSET_GOALKEEPER_PLAYER2_X = 32

    # Velocity
    VELOCITY_PER_FRAME_PLAYER = 10

    # Keyboard using in game
    KEY_VERTICAL_LIST = (K_UP,K_DOWN)
    KEY_VERTICAL_LIST2 = (K_w,K_s)

    # OFFSET_WINDOW
    OFFSET_WINDOW_TOP = 100
    OFFSET_WINDOW_BOT = 100
    OFFSET_WINDOW_LEFT = 100
    OFFSET_WINDOW_RIGHT = 100
    MARGIN_WINDOW = (OFFSET_WINDOW_LEFT,OFFSET_WINDOW_RIGHT,OFFSET_WINDOW_TOP,OFFSET_WINDOW_BOT)

    # Another
    MAX_NUMBER_OF_PLAYER_COLUMN = 4

    KEYS_USED = (K_w, K_a, K_s, K_d, K_UP, K_DOWN, K_LEFT, K_RIGHT , K_p)
    KEYS_P1 = (K_UP, K_DOWN, K_LEFT, K_RIGHT)
    KEYS_P2 = (K_w, K_a, K_s, K_d)

class Window(object):

    @staticmethod
    def init():
        Window.displaySurface = pygame.display.set_mode((Constant.WINDOWWIDTH, Constant.WINDOWHEIGHT))
        Window.rect = Window.displaySurface.get_rect()
        pygame.display.set_caption(Constant.APP_NAME)

    @staticmethod
    def ChangeCursorWithName(name):
        Window.cursor = Resource.listTexture[name]

    @staticmethod
    def ChangeIcon(name):
        pygame.display.set_icon(Resource.listTexture[name])

    @staticmethod
    def CoordinationForUI():
        pass

    @staticmethod
    def Draw(source, dest, area=None, special_flag=0):
        Window.displaySurface.blit(source, dest, area, special_flag)

class InputManager(object):
    ONMOUSEDOWN = 1
    ONMOUSEUP = 2
    MOUSENORMAL = 0

    # Dinh's add

    isPlayer1Moving = Constant.NOT_MOVE
    isPlayer2Moving = Constant.NOT_MOVE
    player1SelectedColumn = 2
    player2SelectedColumn = 2
    keyMoveVertical = None
    keyMoveVertical2 = None

    @staticmethod
    def Init():
        InputManager.mouses = (0, 0)
        InputManager.mouseClick = InputManager.MOUSENORMAL
        InputManager.key = None

    @staticmethod
    def Update(event):
        InputManager.key = None
        InputManager.mouseClick = InputManager.MOUSENORMAL
        InputManager.mouses = pygame.mouse.get_pos()

        # Check if key P1,P2 or two player press same time
        keys = pygame.key.get_pressed()
        if keys[K_UP]:
            InputManager.keyMoveVertical = K_UP
        elif keys[K_DOWN]:
            InputManager.keyMoveVertical = K_DOWN
        if keys[K_w]:
            InputManager.keyMoveVertical2 = K_w
        elif keys[K_s]:
            InputManager.keyMoveVertical2 = K_s

        for ev in event:
            if ev.type == pygame.KEYDOWN and ev.key in Constant.KEYS_USED:
                InputManager.key = ev.key

            # If we don't press anymore, set it None
            if ev.type == pygame.KEYUP and ev.key == InputManager.keyMoveVertical:
                InputManager.keyMoveVertical = None
            if ev.type == pygame.KEYUP and ev.key == InputManager.keyMoveVertical2:
                InputManager.keyMoveVertical2 = None
            if ev.type == pygame.MOUSEBUTTONDOWN:
                InputManager.mouseClick = InputManager.ONMOUSEDOWN
            elif ev.type == pygame.MOUSEBUTTONUP:
                InputManager.mouseClick = InputManager.ONMOUSEUP

    #Dinh's add
    @staticmethod
    def UpdateSelectedState():
        if InputManager.key == K_a:
            InputManager.player2SelectedColumn = (InputManager.player2SelectedColumn + 3 ) % 4
        elif InputManager.key == K_d:
            InputManager.player2SelectedColumn = (InputManager.player2SelectedColumn + 1 ) % 4
        elif InputManager.key == K_LEFT:
            InputManager.player1SelectedColumn = (InputManager.player1SelectedColumn + 3 ) % 4
        elif InputManager.key == K_RIGHT:
            InputManager.player1SelectedColumn = (InputManager.player1SelectedColumn + 1 ) % 4

    #Dinh's add
    @staticmethod
    def UpdateMoveState():
        if InputManager.keyMoveVertical2 == K_w:
            InputManager.isPlayer2Moving = Constant.MOVE_UP
        elif InputManager.keyMoveVertical2 == K_s:
            InputManager.isPlayer2Moving = Constant.MOVE_DOWN
        else:
            InputManager.isPlayer2Moving = Constant.NOT_MOVE
        if InputManager.keyMoveVertical == K_UP:
            InputManager.isPlayer1Moving = Constant.MOVE_UP
        elif InputManager.keyMoveVertical == K_DOWN:
            InputManager.isPlayer1Moving = Constant.MOVE_DOWN
        else:
            InputManager.isPlayer1Moving = Constant.NOT_MOVE
        # else:
        #     InputManager.isPlayer1Moving = Constant.NOT_MOVE
        #     InputManager.isPlayer2Moving = Constant.NOT_MOVE

        # if (InputManager.isPlayer1Moving != Constant.NOT_MOVE):
        #     InputManager.isPlayer2Moving = Constant.NOT_MOVE

        # if (InputManager.isPlayer2Moving != Constant.NOT_MOVE):
        #     InputManager.isPlayer1Moving = Constant.NOT_MOVE


class AudioManage(object):

    @staticmethod
    def init():
        pygame.mixer.quit()
        pygame.mixer.init(22050, -16, 1, 1024)

        AudioManage.musicVolume = 1.0
        AudioManage.soundVolume = 1.0
        AudioManage.listMusic = {}
        AudioManage.listSound = {}
        AudioManage.LoadMusic()
        AudioManage.LoadSound()

    @staticmethod
    def LoadMusic():
        for f in os.listdir(Constant.MUSIC):
            AudioManage.listMusic[os.path.splitext(f)[0]] = os.path.join(Constant.MUSIC,f)

    @staticmethod
    def LoadSound():
        for f in os.listdir(Constant.SOUND):
            AudioManage.listSound[os.path.splitext(f)[0]] = pygame.mixer.Sound(os.path.join(Constant.SOUND,f))

    @staticmethod
    def SetMusicVolume(volume):
        AudioManage.musicVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.music.set_volume(AudioManage.musicVolume)

    @staticmethod
    def SetSoundVolume(volume):
        AudioManage.soundVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.Sound.set_volume(volume)

    @staticmethod
    def PlayMusic(name):
        path = AudioManage.listMusic[name]
        pygame.mixer.music.load(path)
        pygame.mixer.music.set_volume(AudioManage.musicVolume)
        pygame.mixer.music.play(-1)

    @staticmethod
    def PauseMusic():
        pygame.mixer.music.pause()

    @staticmethod
    def ResumeMusic():
        pygame.mixer.music.unpause()

    @staticmethod
    def PlaySound(name):
        sound = AudioManage.listSound[name]
        sound.set_volume(AudioManage.soundVolume)
        sound.play(0)

class Resource(object):

    @staticmethod
    def init():
        listRect = Util.ReadTextureDescription(Constant.TEXTURE_DES)
        texture = pygame.image.load(Constant.TEXTURE)
        Resource.listTmpTexture = Util.ReadSpriteSheet(texture, listRect)
        Resource.listTexture = Resource.changePokemonToRightDirection(Resource.listTmpTexture)
        Resource.pokemonData = Util.AnalyzePlayerData(listRect.keys())

    @staticmethod
    def changePokemonToRightDirection(lstTexture):
        result = {}
        for (name,sprite) in lstTexture.iteritems():
            if name.startswith("PKM-1"):
                sprite = pygame.transform.flip(sprite,True,False)
            result[name] = sprite
        return result
        #pass
        #for


    @staticmethod
    def Load(lstTexture,name):
        try:
            return lstTexture[name]
        except KeyError:
            return None

class Vector3(object):

    __MATH_FLOAT_SMALL = 10.0**(-37)

    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __iadd__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        self.x += other.x
        self.y += other.y
        self.z += other.z

        return self

    def __add__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        return Vector3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __isub__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z

        return self

    def __sub__(self, other):
        assert isinstance(other, Vector3), 'Vector3 instance required'
        return Vector3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __imul__(self, other):
        value = float(other)
        self.x *= value
        self.y *= value
        self.z *= value

        return self

    def __mul__(self, other):
        if isinstance(other, Vector3):
            return self.x*other.x + self.y*other.y + self.z*other.z
        else:
            value = float(other)
            return Vector3(self.x * value, self.y * value, self.z * value)

    @staticmethod
    def initFromPoints(p1, p2):
        assert (isinstance(p1, Point3) and isinstance(p2,Point3)), 'p1 and p2 must be Point3 instance'
        pTmp = p2-p1
        return Vector3(pTmp.x, pTmp.y, pTmp.z)

    # Return the angle (in radians) between  2 vectors
    @staticmethod
    def getAngle(v1, v2):
        assert (isinstance(v1, Vector3) and isinstance(v2,Vector3)), 'v1 and v2 must be Vector3 instance'
        dx = float(v1.x * v2.z - v1.z * v2.y);
        dy = float(v1.z * v2.x - v1.x * v2.z);
        dz = float(v1.x * v2.y - v1.y * v2.x);

        return math.atan2( math.sqrt(dx*dx + dy*dy + dz*dz) + Vector3.__MATH_FLOAT_SMALL, Vector3.scalarProduct(v1,v2) )

    @staticmethod
    def scalarProduct(v1,v2):
        assert (isinstance(v1, Vector3) and isinstance(v2, Vector3)), 'Vector3 instance required'
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z

    @staticmethod
    def componentProduct(v1, v2):
        assert (isinstance(v1, Vector3) and isinstance(v2, Vector3)), 'Vector3 instance required'
        return Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)

    #################################################################################################################

    def setWithValue(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def setWithVector(self,other):
        assert isinstance(other,Vector3), 'other must be Vector3 instance'
        self.setWithValue(other.x,other.y,other.z)

    def getAngle(self, v2):
        assert isinstance(v2,Vector3), 'v1 and v2 must be Vector3 instance'
        dx = float(self.x * v2.z - self.z * v2.y);
        dy = float(self.z * v2.x - self.x * v2.z);
        dz = float(self.x * v2.y - self.y * v2.x);

        return math.atan2( math.sqrt(dx*dx + dy*dy + dz*dz) + Vector3.__MATH_FLOAT_SMALL, Vector3.scalarProduct(self,v2) )

    def componentProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return Vector3(self.x * vector.x, self.y * vector.y, self.z * vector.z)

    def componentProductUpdate(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        self.x *= vector.x
        self.y *= vector.y
        self.z *= vector.z

    def crossProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return Vector3(self.y * vector.z - self.z * vector.y,
                       self.z * vector.x - self.x * vector.z,
                       self.x * vector.y - self.y * vector.x)

    def scalarProduct(self, vector):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        return self.x * vector.x + self.y * vector.y + self.z * vector.z

    def addScaledVector(self, vector, scale):
        assert isinstance(vector, Vector3), 'Vector3 instance required'
        s = float(scale)
        self.x += vector.x * s
        self.y += vector.y * s
        self.z += vector.z * s

    def magnitude(self):
        return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)

    def squareMagnitude(self):
        return self.x*self.x + self.y*self.y + self.z*self.z

    def trim(self, size):
        if self.squareMagnitude() > size*size:
            self.normalize()
            self.x *= size
            self.y *= size

    def normalize(self):
        l = self.magnitude()
        if l>0:
            self.x *= (float(1)/l)
            self.y *= (float(1)/l)
            self.z *= (float(1)/l)

    def getNormalized(self):
        tmp = copy.deepcopy(self)
        tmp.normalize()
        return tmp

    def equal(self, other):
        if isinstance(other, Vector3):
            return self.x == other.x and self.y == other.y and self.z == other.z
        else:
            return False

    def clear(self):
        self.x = self.y = self.z = 0.0

    def flip(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z

    def toTuple(self):
        return (self.x, self.y, self.z)

    def toList(self):
        return [self.x, self.y, self.z]

    # Dinh's add
    def display(self):
        print(self.toTuple())

class Vector2(Vector3):

    __FLT_EPSILON      = 1.192092896 * ( 10 ** (-7) )

    def __init__(self, x = 0.0, y = 0.0):
        super(Vector2, self).__init__(x, y, 0.0)

    # Return the angle (in radians) between 2 vectors
    @staticmethod
    def getAngle(v1, v2):
        unitV1 = v1.getNormalized()
        unitV2 = v2.getNormalized()
        angle  = math.atan2(Vector2.crossProduct(unitV1,unitV2),Vector3.scalarProduct(unitV1,unitV2))
        if math.fabs(angle) < Vector2.__FLT_EPSILON : return 0.0
        else: return angle

    # # Rotate this vector by angle (radians) around the given point
    # def rotate(self,point, angle):
    #     assert isinstance(point, Point2), 'Point2 instance required'
    #     sinAngle = math.sin(angle)
    #     cosAngle = math.cos(angle)
    #     if ( point.equal(Point2(0,0))):
    #         tmpX = self.x * cosAngle - self.y * sinAngle
    #         self.y = self.y * cosAngle + self.x * sinAngle
    #         self.x = tmpX
    #     else:
    #         tmpX = self.x - point.x
    #         tmpY = self.y - point.y
    #
    #         x = tmpX * cosAngle - tmpY * sinAngle + point.x
    #         y = tmpY * cosAngle + tmpX * sinAngle + point.y

    # In Vector2, x and y after crossed = 0 => cross value = z
    def crossProduct(self, vector):
        vTmp = super(Vector2,self).crossProduct(vector)
        return float(vTmp.z)

    def getPerp(self):
        return Vector2( -self.y, self.x )

    def toTuple(self):
        return (self.x, self.y)

    def toList(self):
        return [self.x, self.y]

class Size(object):
    def __init__(self,w=0.0,h=0.0):
        self.width  = float(w)
        self.height = float(h)

    def __iadd__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        self.width  += other.width
        self.height += other.height

        return self

    def __add__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        return Size(self.width + other.width, self.height + other.height)

    def __isub__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        self.width  -= other.width
        self.height -= other.height

        return self

    def __sub__(self, other):
        assert isinstance(other, Size), 'Size instance required'
        return Size(self.width - other.width, self.height - other.height)

    def __imul__(self, other):
        value = float(other)
        self.width *= value
        self.height *= value

        return self

    def __mul__(self, other):
        value = float(other)
        return Size(self.width * value, self.height * value)

    def setSizeValue(self,w=0.0,h=0.0):
        self.width  = float(w)
        self.height = float(h)

    def setSize(self,otherSize):
        assert isinstance(otherSize, Size), 'Size instance required'
        self.setSizeValue(otherSize.width,otherSize.height)

    def setWidth(self,w):
        self.width  = float(w)

    def setHeight(self,h):
        self.height = float(h)

    def equal(self, otherSize):
        if isinstance(otherSize, Size):
            return self.width == otherSize.width and self.height == otherSize.height
        else:
            return False

    def clear(self):
        self.width = self.height = 0.0

    def toTuple(self):
        return (self.width,self.height)

    def toList(self):
        return [self.width,self.height]

    def display(self):
        print(self.toTuple())

class Point3(object):

    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __sub__(self, other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __isub__(self, other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3(self.x - other.x, self.y - other.y, self.z - other.z)

    def setWithValue(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def setWithPoint(self,other):
        assert isinstance(other,Point3), 'other must be Point3 instance'
        self.setWithValue(other.x,other.y,other.z)

    def equal(self, other):
        if isinstance(other, Point3):
            return self.x == other.x and self.y == other.y and self.z == other.z
        else:
            return False

    def clear(self):
        self.x = self.y = self.z = 0.0

    def flip(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z

    def toTuple(self):
        return (self.x, self.y, self.z)

    def toList(self):
        return [self.x, self.y, self.z]

    def display(self):
        print(self.toTuple())

    def getMidPoint(self,other):
        assert isinstance(other, Point3), 'Point3 instance required'
        return Point3( (self.x + other.x) / 2.0,(self.y + other.y) / 2.0,(self.z + other.z) / 2.0)

    @staticmethod
    def getMidPoint(p1,p2):
        assert (isinstance(p1, Point3) and isinstance(p2, Point3)), 'Point3 instance required'
        return Point3( (p1.x + p2.x) / 2.0,(p1.y + p2.y) / 2.0,(p1.z + p2.z) / 2.0)

class Point2(Point3):
    def __init__(self, x = 0.0, y = 0.0):
        super(Point2, self).__init__(x, y, 0.0)

    def toTuple(self):
        return (self.x, self.y)

    def toList(self):
        return [self.x, self.y]

class Drawable(object):
    isVisible = True
    position = Vector2(0,0)
    #size = Size(0,0)

    def Draw(self):
        raise RuntimeError("Method Draw not declared")

    def SetVisible(self, visible):
        self.isVisible = visible

    def SetPosition(self, pos):
        assert isinstance(pos, Vector2), 'Vector2 instance reuqired'
        self.position.setWithVector(pos)
        self.rect.topleft = pos.toTuple()

    def SetPositionValue(self, posX, posY):
        self.position.setWithValue(posX, posY)
        self.rect.topleft = (posX, posY)

    def GetPosition(self):
        return self.position

    def GetPositionX(self):
        return self.position.x

    def GetPositionY(self):
        return self.position.y

    def SetSize(self, size):
        assert isinstance(size, Size), 'Size instance reuqired'
        self.size = size
        self.rect.size = size.toTuple()
    def Update(self):
        pass

    def GetContentSize(self):
        return self.size


class Image(Drawable):

    def __init__(self, lstTexture, name):
        self.position = Vector2()
        self.SetSprite(lstTexture,name)

    def Draw(self):
        if self.isVisible == True :
            Window.Draw(self.sprite, self.rect)

    def DrawWithOpacity(self, opacity):
        if self.isVisible == True:
            image = self.sprite.copy()
            image.fill((255, 255, 255, opacity), None, pygame.BLEND_RGBA_MULT)
            Window.displaySurface.blit(image, self.rect)

    def SetSprite(self, lstTexture, name):
        self.sprite = Resource.Load(lstTexture, name)
        assert self.sprite != None, 'Invalid Image Name'

        self.size= Size(self.sprite.get_width(), self.sprite.get_height())
        self.rect = pygame.Rect(self.position.toTuple(), self.size.toTuple())

class Label(Drawable):
    ALIGN_X_LEFT   = 'align_x_left'
    ALIGN_X_MID    = 'align_x_mid'
    ALIGN_X_RIGHT  = 'align_x_right'
    ALIGN_Y_TOP    = 'align_y_top'
    ALIGN_Y_MID    = 'align_y_mid'
    ALIGN_Y_BOTTOM = 'align_y_bottom'
    ALIGN_X = (ALIGN_X_LEFT, ALIGN_X_MID, ALIGN_X_RIGHT)
    ALIGN_Y = (ALIGN_Y_BOTTOM, ALIGN_Y_MID, ALIGN_Y_TOP)

    def __init__(self, text):
        self.text = text
        self.position = Vector2()
        self.size= Size(100, 100)
        self.rect = pygame(self.position.toTuple(), self.size.toTuple())

        self.font = None
        self.fontName = None
        self.fontSize = 14
        self.isBold = False
        self.isItalic = False
        self.textColor = (255, 255, 255, 255)
        self.antialiasing = True

        self.alignHoriz = Label.ALIGN_X_LEFT
        self.alignVert  = Label.ALIGN_Y_TOP

        self.listOfSingleLineText = []
        self.multipleLine = False

        self.sprite = None
        self.SetFont(self.fontName, self.fontSize, self.isBold, self.isItalic, self.textColor, self.antialiasing)

    def Draw(self):
        if self.sprite == None:
            raise TypeError('Sprite Property in Label Component is missing')
        if self.isVisible == True:
            Window.blit(self.sprite, self.rect)

    def SetFont(self, fontName, fontSize = 14, isBold = False, isItalic = False, textColor = False, antialiasing = False, shouldRender = True):
        self.fontName = fontName
        self.fontSize = fontSize
        self.isBold = isBold
        self.isItalic = isItalic
        self.textColor = textColor
        self.antialiasing = antialiasing
        self.font = pygame.font.Font(self.fontName, self.fontSize)
        self.font.set_bold(isBold)
        self.font.set_italic(isItalic)
        if shouldRender:
            self.Render()

    def SetAlignX(self, alignX, shouldRender = True):
        self.SetAlign(self, alignX, self.alignVert, shouldRender)

    def SetAlignY(self, alignY, shouldRender = True):
        self.SetAlign(self, self.alignHoriz, alignY, shouldRender)

    def SetAlign(self, alignX, alignY, shouldRender = True):
        assert alignX in Label.ALIGN_X, 'AlignX Wrong Format'
        assert alignY in Label.ALIGN_Y, 'AlignY Wrong Format'
        if self.alignHoriz != alignX or self.alignVert != alignY:
            self.alignHoriz = alignX
            self.alignVert  = alignY
            if self.font != None and shouldRender:
                self.Render()

    def SetText(self, text, shouldRender = True):
        assert isinstance(text, basestring), 'String Needed'
        if self.text != text:
            self.text = text

            if "\n" in self.text:
                self.multipleLine = True
            else:
                self.multipleLine = False

            if self.font != None and shouldRender:
                self.Render()

    def Render(self):
        rect = pygame.Rect((0, 0), self.rect.size)
        self.sprite = pygame.Surface(rect.size, pygame.SRCALPHA, 32)

        if self.multipleLine == True:
            self.CalculateForMultiple()

            if self.alignVert != Label.ALIGN_Y_TOP:
                sumsize = 0
                for line in self.listOfSingleLineText:
                    sumsize += self.font.size(line)[1]

                rect.height -= sumsize
                if self.alignVert == Label.ALIGN_Y_MID:
                    rect.top += rect.height/2
                elif self.alignVert == Label.ALIGN_Y_BOTTOM:
                    rect.top += rect.height

            for line in self.listOfSingleLineText:
                size = self.font.size(line)

                if size[1] > rect.height:
                    return;
                else:
                    render = self.font.render(line, self.antialiasing, self.textColor).convert_alpha()
                    if size[0] > rect.width:
                        self.sprite.blit(render, rect.topleft, pygame.Rect((0, 0), rect.size))
                    else:
                        render_rect = render.get_rect()
                        # align x
                        if self.alignHoriz == Label.ALIGN_X_LEFT:
                            render_rect.left = rect.left
                        elif self.alignHoriz == Label.ALIGN_X_MID:
                            render_rect.centerx = rect.centerx
                        else:
                            render_rect.right = rect.right

                        # align y
                        render_rect.top = rect.top

                        self.sprite.blit(render, render_rect)
                    rect.height -= render.get_height()
                    rect.top += render.get_height()
            pass
        else:
            size = self.font.size(self.text)

            if size[1] > rect.height:
                return;
            else:
                render = self.font.render(self.text, self.antialiasing, self.textColor).convert_alpha()
                if size[0] > rect.width:
                    self.sprite.blit(render, (0, 0), pygame.Rect((0, 0), rect.size))
                else:
                    render_rect = render.get_rect()
                    # align x
                    if self.alignHoriz == Label.ALIGN_X_LEFT:
                        render_rect.left = rect.left
                    elif self.alignHoriz == Label.ALIGN_X_MID:
                        render_rect.centerx = rect.centerx
                    else:
                        render_rect.right = rect.right

                    # align y
                    if self.alignVert == Label.ALIGN_Y_TOP:
                        render_rect.top = rect.top
                    elif self.alignVert == Label.ALIGN_Y_MID:
                        render_rect.centery = rect.centery
                    else:
                        render_rect.bottom = rect.bottom

                    self.sprite.blit(render, render_rect)

    def CalculateForMultiple(self):
        if self.multipleLine == False:
            return;

        self.listOfSingleLineText = []
        listAfterSplitNewLine = self.text.splitlines()
        desiredWidth = self.rect.width
        desiredHeight = self.rect.height

        for line in listAfterSplitNewLine:
            if self.font.size(line)[0] > desiredWidth:
                words = line.split(' ')

                # if each word too big for a row, then dont' draw anythin
                for word in words:
                    if self.font.size(word)[0] > desiredWidth:
                        print('Label, Word Too Big for a Row, PLease Increase Width')
                        return;
                accumulated_line = ""
                for word in words:
                    test_line = accumulated_line + word + ' '
                    if self.font.size(test_line)[0] < desiredWidth:
                        accumulated_line = test_line
                    else:
                        self.listOfSingleLineText.append(accumulated_line)
                        accumulated_line = word + ' '
                accumulated_line.strip()
                if accumulated_line != '':
                    self.listOfSingleLineText.append(accumulated_line)
            else:
                self.listOfSingleLineText.append(line)


# Using this, assume normal, pressed and disable image have same content size
class Button(Drawable):
    BUTTON_NORMAL = 'button_normal'
    BUTTON_PRESSED = 'button_pressed'
    BUTTON_DISABLE = 'button_disable'
    BUTTON_STATE = (BUTTON_NORMAL, BUTTON_PRESSED, BUTTON_DISABLE)

    def __init__(self, lstTexture, normalSpriteName, pressSpriteName = "", disableSpriteName = ""):
        self.sprite = None
        self.touch = False
        self.onTouchUpInside = None

        self.state = Button.BUTTON_NORMAL
        self.SetNormalStateSprite(lstTexture,normalSpriteName)
        self.SetPressStateSprite(lstTexture,pressSpriteName)
        self.SetDisableStateSprite(lstTexture,disableSpriteName)

        self.position = Vector2()
        self.size = Size(self.normalSprite.get_width(), self.normalSprite.get_height())
        self.rect = pygame.Rect(self.position.toTuple(), self.size.toTuple())

        self.SetState(Button.BUTTON_NORMAL)

    def Update(self):
        if self.state != Button.BUTTON_DISABLE:
            if self.sprite == None:
                self.sprite = self.normalSprite
            if InputManager.mouseClick == InputManager.ONMOUSEDOWN:
                if self.rect.collidepoint(InputManager.mouses):
                    self.touch = True
                    self.SetState(Button.BUTTON_PRESSED)
            elif InputManager.mouseClick == InputManager.ONMOUSEUP:
                if self.touch and self.rect.collidepoint(InputManager.mouses):
                    self.onTouchUpInside(self)
                self.touch = False
                self.SetState(Button.BUTTON_NORMAL)

    # Never use this method
    def SetState(self, state):
        assert state in Button.BUTTON_STATE, 'Invalid state'

        self.state = state

        if state == Button.BUTTON_NORMAL:
            self.sprite = self.normalSprite
            self.size = Size(self.normalSprite.get_width(), self.normalSprite.get_height())
            self.rect.size = self.size.toTuple()
        elif state == Button.BUTTON_PRESSED and self.pressSprite != None:
            self.sprite = self.pressSprite
            self.size = Size(self.pressSprite.get_width(), self.pressSprite.get_height())
            self.rect.size = self.size.toTuple()
        elif state == Button.BUTTON_DISABLE and self.disableSprite != None:
            self.sprite = self.disableSprite
            self.size = Size(self.disableSprite.get_width(), self.disableSprite.get_height())
            self.rect.size = self.size.toTuple()

    def SetNormalStateSprite(self, lstTexture, name):
        self.normalSprite = Resource.Load(lstTexture, name)
        assert self.normalSprite != None, 'Invalid normal State Image Name'

    def SetPressStateSprite(self, lstTexture, name):
        self.pressSprite = Resource.Load(lstTexture, name)

    def SetDisableStateSprite(self, lstTexture, name):
        self.disableSprite = Resource.Load(lstTexture, name)

    def Draw(self):
        if self.isVisible == True :
            Window.Draw(self.sprite, self.rect)

class Util(object):
    @staticmethod
    def ReadTextureDescription(directory):
        result = {}
        with open(directory) as f:
            for line in f:
                list = line.split()
                rect_attr = [int(i) for i in list[1:]]
                rect = pygame.Rect(rect_attr[0], rect_attr[1], rect_attr[2], rect_attr[3])
                result[list[0]] = rect
        return result

    @staticmethod
    def ReadSpriteSheet(image, description):
        results = {}
        for (name, rect) in description.iteritems():
            sprite = pygame.Surface(rect.size, pygame.SRCALPHA, 32)
            sprite.blit(image, (0, 0), rect)
            results[name] = sprite
        return results

    @staticmethod
    def CreateText(text, textColor, BGColor, fontName, fontSize):
        font = pygame.font.Font(fontName, fontSize)
        fontSurface = font.render(text, True, textColor)
        return (fontSurface, fontSurface.get_rect())

    @staticmethod
    def CalculateScreenGrid(screenSize, gridSize, margin):
        actualSize = list(screenSize)
        delx = screenSize[0] - margin[0] - margin[1]
        dely = screenSize[1] - margin[2] - margin[3]
        if delx % gridSize[0] != 0:
            actualSize[0] = delx / gridSize[0] * gridSize[0]
        if dely % gridSize[1] != 0:
            actualSize[1] = dely / gridSize[1] * gridSize[1]

        result = []
        for i in range(margin[0], actualSize[0], gridSize[0]):
            for j in range(margin[2], actualSize[1], gridSize[1]):
                rect = pygame.Rect((i, j), gridSize)
                result.append(rect)
        return result

    @staticmethod
    def AnalyzePlayerData(listOfName):
        listOfFoosPlayer = [v for v in listOfName if v.startswith('PKM')]
        result = {}
        for pokemon in listOfFoosPlayer:
            filterName = filter(None, re.split("PKM|-|.png", pokemon))
            indexMaxFrame = result.get((filterName[0], filterName[1], filterName[2]), 0)
            indexMaxFrame = max(filterName[3], indexMaxFrame)
            result[filterName[0], filterName[1], filterName[2]] = indexMaxFrame
        return result

    @staticmethod
    def Clamp(minimum, maximum, value):
        return max(minimum, min(value, maximum))
