#-------------------------------------------------------------------------------
# Name:        Ass1
# Purpose:
#
# Author:      chha963
#
# Created:     31/08/2015
# Copyright:   (c) chha963 2015
#-------------------------------------------------------------------------------

import random, pygame, sys, re, os
from pygame.locals import *
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (200,70)

class Time(object):
    deltaTime = FPSCLOCK = None
    FPS = 30

    @staticmethod
    def init():
        Time.FPSCLOCK = pygame.time.Clock()
        Time.deltaTime = 0
        Time.unscaleDeltaTime = 0
        Time.timeScale = 1

    @staticmethod
    def Update():
        Time.unscaleDeltaTime = Time.FPSCLOCK.tick(Constant.FPS)/1000.0
        Time.deltaTime = Time.unscaleDeltaTime * Time.timeScale #Time.FPSCLOCK.tick(Constant.FPS)/1000.0

class Window(object):

    @staticmethod
    def init():
        Window.displaySurface = pygame.display.set_mode((Constant.WINDOWWIDTH, Constant.WINDOWHEIGHT))
        Window.rect = Window.displaySurface.get_rect()
        pygame.display.set_caption(Constant.APP_NAME)
        pygame.display.set_icon(Resource.listTexture['icon'])
        pygame.mouse.set_visible(False)
        Window.ChangeCursorWithName('Poke_Ball')

    @staticmethod
    def ChangeCursorWithName(name):
        Window.cursor = Resource.listTexture[name]

    @staticmethod
    def CoordinationForUI():
        Footer_Rect = Resource.listTexture['Map_Footer'].get_rect()
        Footer_Rect.bottomleft = Window.rect.bottomleft
        Header_Rect = Resource.listTexture['Map_Header'].get_rect()
        Header_Rect.topleft = Window.rect.topleft
        GreenLeft_Rect = Resource.listTexture['Green_leaf'].get_rect()
        GreenLeft_Rect.bottomleft = Footer_Rect.bottomleft
        Window.GreenLeaf_TopLeft = GreenLeft_Rect.topleft

        Window.Font = pygame.font.Font('font/VHOPTI_1.TTF',20)
        Window.Menu_Font = pygame.font.Font('font/VHOPTI_I_0.TTF',20)
        Window.Menu_Font.set_bold

        Menu_Poke_Ball = Resource.listTexture['Poke_Ball'].get_rect()
        Menu_Poke_Ball.center = Footer_Rect.center
        Menu_Poke_Ball.left = Constant.WINDOWWIDTH * 0.01
        Window.btnPokeBall = Button('Menu_PokeBall','Poke_Ball',Menu_Poke_Ball)

        Menu_Chosen_Ball = Resource.listTexture['Border_Choose_Ball'].get_rect()
        Menu_Chosen_Ball.center = Footer_Rect.center
        Menu_Chosen_Ball.left = Menu_Poke_Ball.left * 0.7
        Window.btnChosenBall = Button('ChosenBall','Border_Choose_Ball',Menu_Chosen_Ball)
        Window.btnPokeBall.Chosen = True

        Menu_Great_Ball = Resource.listTexture['Great_Ball'].get_rect()
        Menu_Great_Ball.center = Footer_Rect.center
        Menu_Great_Ball.left = Constant.WINDOWWIDTH * 0.08
        Window.btnGreatBall = Button('Menu_GreatBall','Great_Ball',Menu_Great_Ball)

        Menu_Ultra_Ball = Resource.listTexture['Ultra_Ball'].get_rect()
        Menu_Ultra_Ball.center = Footer_Rect.center
        Menu_Ultra_Ball.left = Constant.WINDOWWIDTH * 0.15
        Window.btnUltraBall = Button('Menu_UltraBall','Ultra_Ball',Menu_Ultra_Ball)

        Menu_Timer_Ball = Resource.listTexture['Timer_Ball'].get_rect()
        Menu_Timer_Ball.center = Footer_Rect.center
        Menu_Timer_Ball.left = Constant.WINDOWWIDTH * 0.22
        Window.btnTimerBall = Button('Menu_TimerBall','Timer_Ball',Menu_Timer_Ball)

        Menu_Luxury_Ball = Resource.listTexture['Luxury_Ball'].get_rect()
        Menu_Luxury_Ball.center = Footer_Rect.center
        Menu_Luxury_Ball.left = Constant.WINDOWWIDTH * 0.29
        Window.btnLuxuryBall = Button('Menu_LuxBall','Luxury_Ball',Menu_Luxury_Ball)

        Menu_Priceless_Master = Resource.listTexture['Priceless_Master'].get_rect()
        Menu_Priceless_Master.center = Footer_Rect.center
        Menu_Priceless_Master.left = Constant.WINDOWWIDTH * 0.36
        Window.btnMasterBall = Button('Menu_MasBall','Priceless_Master',Menu_Priceless_Master)

        Menu_Item_Bag = Resource.listTexture['Item_Menu_Bag'].get_rect()
        Menu_Item_Bag.center = Footer_Rect.center
        Menu_Item_Bag.left = Constant.WINDOWWIDTH * 0.85
        Window.btnBag = Button('Bag','Item_Menu_Bag',Menu_Item_Bag)

        Menu_Item_Conf = Resource.listTexture['Item_Menu_Config'].get_rect()
        Menu_Item_Conf.center = Footer_Rect.center
        Menu_Item_Conf.left = Constant.WINDOWWIDTH * 0.95
        Window.btnConfig = Button('Configure','Item_Menu_Config',Menu_Item_Conf)

        Menu_Catch_Count = Window.Font.render(str(Global.CatchCount),True,Constant.BLACK)
        Menu_Catch_Count_Rect = Menu_Catch_Count.get_rect()
        Menu_Catch_Count_Rect.centery = Footer_Rect.centery
        Menu_Catch_Count_Rect.right = Constant.WINDOWWIDTH * 0.77
        Window.lblCatch = Label('Catch',Menu_Catch_Count,Menu_Catch_Count_Rect)

        Menu_Pokemon = Resource.listTexture['Item_Menu_pkm'].get_rect()
        Menu_Pokemon.center = (Constant.WINDOWWIDTH * 0.8, Footer_Rect.centery)
        Window.btnPokemon = Button('Pokemon','Item_Menu_pkm',Menu_Pokemon)

        Menu_Combo_Count = Window.Menu_Font.render(str(Global.ComboCount),True,Constant.BLACK)
        Menu_Combo_Count_Rect = Menu_Combo_Count.get_rect()
        Menu_Combo_Count_Rect.center = (Constant.WINDOWWIDTH * 0.8,Header_Rect.centery - 5)
        Window.lblComboCount = Label('ComboCount',Menu_Combo_Count,Menu_Combo_Count_Rect)
        Window.Combo = Window.Menu_Font.render('Combo',True,Constant.BLACK)
        Window.Combo_Rect = Window.Combo.get_rect()
        Window.Combo_Rect.center = Menu_Combo_Count_Rect.center
        Window.Combo_Rect.left = Constant.WINDOWWIDTH * 0.82
        Window.lblCombo = Label('Combo',Window.Combo,Window.Combo_Rect)

        #Config:
        Config = Resource.listTexture['Configure_BG'].get_rect()
        Config.center = Window.rect.center
        Window.Config = Config

        Window.Config_Confirm = Resource.listTexture['ButtonConfirm'].get_rect()
        Window.Config_Confirm.center = (Config.left + Config.width/3,Config.bottom - Config.height/6)
        Window.Config_Cancel = Resource.listTexture['ButtonCancel'].get_rect()
        Window.Config_Cancel.center = (Config.right - Config.width/3,Config.bottom - Config.height/6)
        Config_Bar1 = Resource.listTexture['Bar'].get_rect()
        Config_Bar1.center = (Config.centerx,Config.top + Config.height*3/8 - 1)
        Window.Config_Bar1 = Config_Bar1
        Config_Bar2 = Resource.listTexture['Bar'].get_rect()
        Config_Bar2.center = (Config.centerx,Config.top + Config.height*7/12 + 1)
        Window.Config_Bar2 = Config_Bar2

        Config_font = Window.Menu_Font
        Window.Config_Music = Config_font.render('Music:',True,Constant.BLACK)
        Window.Config_Music_Rect = Window.Config_Music.get_rect()
        Window.Config_Music_Rect.bottomleft = (Config_Bar1.left + 10,Config_Bar1.top - Config_Bar1.height*1.5)
        Window.Config_Sound = Config_font.render('Sound:',True,Constant.BLACK)
        Window.Config_Sound_Rect = Window.Config_Sound.get_rect()
        Window.Config_Sound_Rect.bottomleft = (Config_Bar2.left + 10,Config_Bar2.top - Config_Bar2.height*1.5)
        #Shop:

    #Duy_End
        Window.Footer_TopLeft = Footer_Rect.topleft
        Window.Menu_Poke_Ball_TopLeft = Menu_Poke_Ball.topleft
        Window.Menu_Great_Ball_TopLeft = Menu_Great_Ball.topleft
        Window.Menu_Ultra_Ball_TopLeft = Menu_Ultra_Ball.topleft
        Window.Menu_Timer_Ball_TopLeft = Menu_Timer_Ball.topleft
        Window.Menu_Luxury_Ball_TopLeft = Menu_Luxury_Ball.topleft
        Window.Menu_Priceless_Master_TopLeft = Menu_Priceless_Master.topleft
        Window.GamePlay_Timer_Rect = pygame.Rect( (Constant.WINDOWWIDTH - 20, 200), Resource.listTexture['Timer'].get_size())

        #GameOver
        gameoverBoardRect = pygame.Rect((0, 0), Resource.listTexture['Gameover'].get_size())
        gameoverBoardRect.center = (Constant.WINDOWWIDTH/2, Constant.WINDOWHEIGHT/2)
        Window.GameOver_TopLeft = gameoverBoardRect.topleft
        Window.GameOver_Catch_TopLeft = (gameoverBoardRect.left + 20, gameoverBoardRect.top + 70)
        Window.GameOver_Miss_TopLeft = (gameoverBoardRect.left + 20, gameoverBoardRect.top + 120)
        Window.GameOver_MissRate_TopLeft = (gameoverBoardRect.left + 20, gameoverBoardRect.top + 170)
        Window.GameOver_MaxCombo_TopLeft = (gameoverBoardRect.left + 20, gameoverBoardRect.top + 220)
        Window.GameOver_Score_TopLeft = (gameoverBoardRect.left + 20, gameoverBoardRect.top + 270)

        Window.GameOver_ButtonMenu_Rect = pygame.Rect((gameoverBoardRect.left + 70, gameoverBoardRect.top + 325), Resource.listTexture['Menu'].get_size())
        Window.GameOver_ButtonReplay_Rect = pygame.Rect((gameoverBoardRect.left + 280, gameoverBoardRect.top + 325), Resource.listTexture['Replay'].get_size())

class InputManager(object):

    @staticmethod
    def Init():
        InputManager.mouses = (0, 0)
        InputManager.mouseClick = 0
        InputManager.key = None

    @staticmethod
    def Update(event):
        InputManager.key = None
        InputManager.mouseClick = 0
        InputManager.mouses = pygame.mouse.get_pos()
        for ev in event:
            if ev.type == pygame.KEYDOWN:
                InputManager.key = ev.key
            if ev.type == pygame.MOUSEBUTTONDOWN:
                InputManager.mouseClick = 1
            elif ev.type == pygame.MOUSEBUTTONUP:
                InputManager.mouseClick = 2

class AudioManage(object):

    @staticmethod
    def init():
        pygame.mixer.quit()
        pygame.mixer.init(22050, -16, 1, 1024)

        AudioManage.musicVolume = 1.0
        AudioManage.soundVolume = 1.0
        AudioManage.listMusic = {}
        AudioManage.listSound = {}
        AudioManage.LoadMusic()
        AudioManage.LoadSound()

    @staticmethod
    def LoadMusic():
        for f in os.listdir(Constant.MUSIC):
            AudioManage.listMusic[os.path.splitext(f)[0]] = os.path.join(Constant.MUSIC,f)

    @staticmethod
    def LoadSound():
        for f in os.listdir(Constant.SOUND):
            AudioManage.listSound[os.path.splitext(f)[0]] = pygame.mixer.Sound(os.path.join(Constant.SOUND,f))

    @staticmethod
    def SetMusicVolume(volume):
        AudioManage.musicVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.music.set_volume(AudioManage.musicVolume)

    @staticmethod
    def SetSoundVolume(volume):
        AudioManage.soundVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.Sound.set_volume(volume)

    @staticmethod
    def PlayMusic(name):
        path = AudioManage.listMusic[name]
        pygame.mixer.music.load(path)
        pygame.mixer.music.set_volume(AudioManage.musicVolume)
        pygame.mixer.music.play(-1)

    @staticmethod
    def PauseMusic():
        pygame.mixer.music.pause()

    @staticmethod
    def ResumeMusic():
        pygame.mixer.music.unpause()

    @staticmethod
    def PlaySound(name):
        sound = AudioManage.listSound[name]
        sound.set_volume(AudioManage.soundVolume)
        sound.play(0)


class Util(object):

    @staticmethod
    def ReadTextureDescription(directory):
        result = {}
        with open(directory) as f:
            for line in f:
                list = line.split()
                rect_attr = [int(i) for i in list[1:]]
                rect = pygame.Rect(rect_attr[0], rect_attr[1], rect_attr[2], rect_attr[3])
                result[list[0]] = rect
        return result

    @staticmethod
    def ReadSpriteSheet(image, description):
        results = {}
        for (name, rect) in description.iteritems():
            sprite = pygame.Surface(rect.size, pygame.SRCALPHA, 32)
            sprite.blit(image, (0, 0), rect)
            results[name] = sprite
        return results

    @staticmethod
    def CreateText(text, textColor, BGColor, fontName, fontSize):
        font = pygame.font.Font(fontName, fontSize)
        fontSurface = font.render(text, True, textColor)
        return (fontSurface, fontSurface.get_rect())

    @staticmethod
    def AnalyzePokemonData(listOfName):
        listOfPokemon = [v for v in listOfName if v.startswith('PKM')]
        result = {}
        for pokemon in listOfPokemon:
            filterName = filter(None, re.split("PKM|-|.png", pokemon))
            numberOfLevel = int(filterName[1])
            level = int(filterName[2])-1

            data = result.get(filterName[0], [0]*numberOfLevel)
            data[level] = max(int(filterName[3]), data[level])
            result[filterName[0]] = data
        return result

    @staticmethod
    def CalculateScreenGrid(screenSize, gridSize, margin):
        actualSize = list(screenSize)
        delx = screenSize[0] - margin[0] - margin[1]
        dely = screenSize[1] - margin[2] - margin[3]
        if  delx % gridSize[0] != 0:
            actualSize[0] = delx/gridSize[0] * gridSize[0]
        if  dely % gridSize[1] != 0:
            actualSize[1] = dely/gridSize[1] * gridSize[1]

        result = []
        for i in range(margin[0], actualSize[0], gridSize[0]):
            for j in range(margin[2], actualSize[1], gridSize[1]):
                rect = pygame.Rect((i, j), gridSize)
                result.append(rect)
        return result

    @staticmethod
    def Clamp(minimum, maximum, value):
        return max(minimum, min(value, maximum))

class Constant:

    # Application Variable
    FPS = 30
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    APP_NAME = 'Ass1'

    # Color
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)

    # Pokemon
    NUMBER_OF_POKEMON = 10

    GamePlayTime = 40
    MISS_TIME_PUNISH = 2
    POKEMON_LIVINGTIME_MIN = 4
    POKEMON_LIVINGTIME_MAX = 5
    POKEMON_SPAWN_TIME_INTERVAL = 0.5
    POKEMON_ANIMATION_TIME = 1.5

    POKEMON_ENTRY = 'pokemon_entry'
    POKEMON_LIVE = 'pokemon_live'
    POKEMON_HIDE = 'pokemon_hide'
    POKEMON_CAPTURE = 'pokemon_capture'
    POKEMON_DISAPEAR = 'pokemon_disapear'

    POKEMON_ENTRY_DIR_UP = 'pokemon_entry_dir_up'
    POKEMON_ENTRY_DIR_DOWN = 'pokemon_entry_dir_down'
    POKEMON_ENTRY_DIR_RIGHT = 'pokemon_entry_dir_right'
    POKEMON_ENTRY_DIR_LEFT = 'pokemon_entry_dir_left'
    POKEMON_ENTRY_DIR = (POKEMON_ENTRY_DIR_UP, POKEMON_ENTRY_DIR_DOWN, POKEMON_ENTRY_DIR_RIGHT, POKEMON_ENTRY_DIR_LEFT)

    POKEMON_ENTRY_TIME = 0.5
    POKEMON_EXIT_TIME = 0.5
    POKEMON_CAPTURE_TIME = 0.5

    POKEBALL_FALLING = 'pokeball_falling'
    POKEBALL_FADING = 'pokeball_fading'
    POKEBALL_DISAPEAR = 'pokeball_disapear'
    POKEBALL_FADING_TIME = 1

    # SCREEN
    GRID_X = 80
    GRID_Y = 80

    # PHYSIC
    GRAVITY_ACCELORATION = 9.81
    ELASTICITY = 0.5
    FICTION = 0.9
    AIRDRAG = 1

    #path
    MUSIC = 'res/Music'
    SOUND = 'res/Sound'
    TEXTURE = 'res/Texture.png'
    TEXTURE_DES = 'res/Texture.txt'

    @staticmethod
    def GetPKMCountMax():
        return Global.ComboCount / 8 + 1

    @staticmethod
    def GetTimeBonusForCombo():
        return int(Global.ComboCount / 21 + 1)

    @staticmethod
    def GetBonusCountForTimeBonus():
        return 7 if Global.ComboCount < 20 else int(Global.ComboCount/ 10 + 5)

class Global:
    isPause = False

    GameOver = False
    ShowStartScreen = True
    GamePlayTime = 0

    pokemonCount = 0
    allowSpawnPokemon = True
    listEmptySpot = []
    listPokemonSpot = []
    listPokemonAboutToDisapear = []
    listPokeBall = []
    listPokeBallAboutToDisapear = []
    listButton = []

    isHitPKMIfMousePress = False
    CatchCount = 0
    ComboCount = 0
    MaxComboCount = 0
    MissCount = 0
    TotalCount = 0

class Resource(object):

    @staticmethod
    def init():
        listRect = Util.ReadTextureDescription(Constant.TEXTURE_DES)
        texture = pygame.image.load(Constant.TEXTURE)
        Resource.listTexture = Util.ReadSpriteSheet(texture, listRect)
        Resource.pokemonData = Util.AnalyzePokemonData(listRect.keys())

class Pokemon:

    def __init__(self, index):
        self.state = Constant.POKEMON_ENTRY
        self.entryDir = random.choice(Constant.POKEMON_ENTRY_DIR)
        self.LivingTime = random.uniform(Constant.POKEMON_LIVINGTIME_MIN, Constant.POKEMON_LIVINGTIME_MAX)
        self.startTime = 0
        self.timeToChangeFrame = 0

        # DINH's added
        self.kindOfPokemon = str(random.randint(1, Constant.NUMBER_OF_POKEMON))
        self.maxlevel = len(Resource.pokemonData[self.kindOfPokemon])
        self.hitPoint = random.randint(1, self.maxlevel)

    def init(self):
        self.colliderRect = self.rect.copy()
        self.rectForDraw = self.rect.copy()
        # DINH's modified
        self.ChangeToClip()

    def ChangeToClip(self):
        self.currentFrame = 0
        self.currentClip = []

        # DINH's modified
        levelMaxFrame = Resource.pokemonData[self.kindOfPokemon][self.hitPoint-1]
        for i in xrange(1, levelMaxFrame + 1):
            self.currentClip.append(Resource.listTexture["PKM{}-{}-{}-{}".format(self.kindOfPokemon, self.maxlevel, self.hitPoint, i)])

        self.maxFrame = len(self.currentClip)
        self.timePerFrame = Constant.POKEMON_ANIMATION_TIME / self.maxFrame
        self.UpdateSprite()

    def Update(self):
        self.UpdateTimeLife()

        if self.state == Constant.POKEMON_ENTRY:
            self.DrawEntryState()
        elif self.state == Constant.POKEMON_LIVE:
            self.DrawLivingState()
            self.CheckMouseCollider()
        elif self.state == Constant.POKEMON_CAPTURE:
            self.DrawCaptureState()
        elif self.state == Constant.POKEMON_HIDE:
            self.DrawHideState()

        if self.state == Constant.POKEMON_DISAPEAR:
            self.AboutToDestroy()

    def UpdateTimeLife(self):
        self.startTime += Time.deltaTime

        if self.state == Constant.POKEMON_LIVE:
            self.timeToChangeFrame += Time.deltaTime

            if self.timeToChangeFrame > self.timePerFrame:
                self.timeToChangeFrame = 0
                self.currentFrame = (self.currentFrame + 1) % self.maxFrame
                self.UpdateSprite()

        if self.startTime > self.LivingTime + Constant.POKEMON_ENTRY_TIME and self.state == Constant.POKEMON_LIVE:
            self.state = Constant.POKEMON_HIDE
            self.timeTmp = self.startTime

    def UpdateSprite(self):
        self.sprite = self.currentClip[self.currentFrame]
        self.colliderRect.size = self.sprite.get_size()
        self.colliderRect.topleft = self.rect.topleft

        offsetX = (Constant.GRID_X - self.colliderRect.width)/2
        offsetY = (Constant.GRID_Y - self.colliderRect.height)/2
        surfaceTmp = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
        surfaceTmp.blit(self.sprite, (offsetX, offsetY))
        self.sprite = surfaceTmp
        self.colliderRect.left += offsetX
        self.colliderRect.top += offsetY


    def DrawLivingState(self):
        Window.displaySurface.blit(self.sprite, self.rect)

    def DrawEntryState(self):
        width = self.rect.width * self.startTime/Constant.POKEMON_ENTRY_TIME
        width_convert = self.rect.width * (Constant.POKEMON_ENTRY_TIME - self.startTime)/Constant.POKEMON_ENTRY_TIME
        height = self.rect.height * self.startTime/Constant.POKEMON_ENTRY_TIME
        height_convert = self.rect.height * (Constant.POKEMON_ENTRY_TIME - self.startTime)/Constant.POKEMON_ENTRY_TIME
        top = self.rect.top
        left = self.rect.left

        if self.entryDir == Constant.POKEMON_ENTRY_DIR_LEFT:
            self.rectForDraw.width = width
            self.rectForDraw.topleft = (max(0, width_convert), 0)
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_RIGHT:
            self.rectForDraw.width = width
            self.rectForDraw.topleft = (0, 0)
            left += width_convert
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_UP:
            self.rectForDraw.height = height
            self.rectForDraw.topleft = (0, 0)
            top += height_convert
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_DOWN:
            self.rectForDraw.height = height
            self.rectForDraw.topleft = (0, max(0, height_convert))

        Window.displaySurface.blit(self.sprite, (left, top), self.rectForDraw)

        if self.startTime > Constant.POKEMON_ENTRY_TIME:
            self.state = Constant.POKEMON_LIVE

    def DrawHideState(self):
        time = self.startTime - self.timeTmp

        width = self.rect.width * time/Constant.POKEMON_EXIT_TIME
        width_convert = self.rect.width * (Constant.POKEMON_EXIT_TIME - time)/Constant.POKEMON_EXIT_TIME
        height = self.rect.height * time/Constant.POKEMON_EXIT_TIME
        height_convert = self.rect.height * (Constant.POKEMON_EXIT_TIME - time)/Constant.POKEMON_EXIT_TIME
        top = self.rect.top
        left = self.rect.left

        if self.entryDir == Constant.POKEMON_ENTRY_DIR_LEFT:
            self.rectForDraw.width = max(0, width_convert)
            self.rectForDraw.topleft = (0, 0)
            left += width
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_RIGHT:
            self.rectForDraw.width = width_convert
            self.rectForDraw.topleft = (max(0, width), 0)
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_UP:
            self.rectForDraw.height = height_convert
            self.rectForDraw.topleft = (0, max(0, height))
        elif self.entryDir == Constant.POKEMON_ENTRY_DIR_DOWN:
            self.rectForDraw.height = max(0, height_convert)
            self.rectForDraw.topleft = (0, 0)
            top += height

        Window.displaySurface.blit(self.sprite, (left, top), self.rectForDraw)

        if time > Constant.POKEMON_EXIT_TIME:
            self.state = Constant.POKEMON_DISAPEAR

    def DrawCaptureState(self):
        percentOfFade = min(1, (self.startTime - self.timeTmp)/Constant.POKEMON_CAPTURE_TIME)
        image = self.sprite.copy()
        image.fill((241, 142, 56, (1 - percentOfFade) * 255), None, pygame.BLEND_RGBA_MULT)
        Window.displaySurface.blit(image, self.rect)

        if percentOfFade >= 1:
            self.state = Constant.POKEMON_DISAPEAR
            pokeball = PokeBall(self.colliderRect.center)
            Global.listPokeBall.append(pokeball)


    def CheckMouseCollider(self):
        if InputManager.mouseClick == 1 and self.colliderRect.collidepoint(InputManager.mouses):
            Global.isHitPKMIfMousePress = True
            Global.ComboCount += 1
            Global.MaxComboCount = max(Global.ComboCount, Global.MaxComboCount)

            if Global.ComboCount % Constant.GetBonusCountForTimeBonus() == 0:
                AudioManage.PlaySound('bonus-combo')
                Global.GamePlayTime =  max(0, Global.GamePlayTime - Constant.GetTimeBonusForCombo())

            Global.TotalCount += 1
            Window.lblComboCount.UpdateLbl()
            if self.hitPoint <= 1:
                self.state = Constant.POKEMON_CAPTURE
                self.timeTmp = self.startTime
                AudioManage.PlaySound('hit_disappear')
            else:
                self.hitPoint -= 1;
                # Dinh's edit
                self.ChangeToClip()
                AudioManage.PlaySound('hit_not_disappear')

    def AboutToDestroy(self):
        Global.listPokemonAboutToDisapear.append(self)


    def DestroyPokemon(self):
        self.Destroy = True
        Global.listEmptySpot.append(self.rect)
        Global.listPokemonSpot.remove(self)

class PokeBall:

    def __init__(self, center):
        self.sprite = Window.cursor.copy()
        self.rect = self.sprite.get_rect()
        self.rect.center = center
        self.startTime = 0
        self.velocity = [random.randint(-20, 20), random.randint(-30, 5)]
        self.startRotate = False
        self.rotate = 0
        self.state = Constant.POKEBALL_FALLING

    def Update(self):
        self.startTime += Time.deltaTime

        if self.state == Constant.POKEBALL_FALLING:
            self.rect.move_ip(self.velocity[0], self.velocity[1])

            if self.rect.bottom >= Constant.WINDOWHEIGHT - 50:
                self.rect.bottom = Constant.WINDOWHEIGHT - 50
                self.velocity[1] = -(self.velocity[1] * Constant.ELASTICITY)
            elif self.rect.top <= 50:
                self.rect.top = 50
                self.velocity[1] = -(self.velocity[1] * Constant.ELASTICITY)

            if self.rect.left <= 0:
                self.rect.left = 0
                self.velocity[0] = -(self.velocity[0] * Constant.ELASTICITY)
            elif self.rect.right >= Constant.WINDOWWIDTH:
                self.rect.right = Constant.WINDOWWIDTH
                self.velocity[0] = -(self.velocity[0] * Constant.ELASTICITY)

            self.velocity[1] += Constant.GRAVITY_ACCELORATION ** self.startTime

            self.velocity[0] *= Constant.AIRDRAG
            self.velocity[1] *= Constant.AIRDRAG
            if self.rect.bottom >= 540:
                self.velocity[0] *= Constant.FICTION

            if self.rect.bottom >= 540 and abs(self.velocity[0]) <= 0.09:
                self.state = Constant.POKEBALL_FADING
                self.startTime = 0
            self.DrawFallingState()
        elif self.state == Constant.POKEBALL_FADING:
            self.DrawFadingState()
        elif self.state == Constant.POKEBALL_DISAPEAR:
                self.AboutToDestroy()

    def DrawFallingState(self):
        image = pygame.transform.rotate(self.sprite, self.rotate)
        Window.displaySurface.blit(image, self.rect)

    def DrawFadingState(self):
        percentOfFade = min(1, self.startTime / Constant.POKEBALL_FADING_TIME)
        image = self.sprite.copy()
        image.fill((255, 255, 255, (1 - percentOfFade) * 255), None, pygame.BLEND_RGBA_MULT)
        Window.displaySurface.blit(image, self.rect)

        if self.startTime > Constant.POKEBALL_FADING_TIME:
            self.state = Constant.POKEBALL_DISAPEAR

    def AboutToDestroy(self):
        Global.listPokeBallAboutToDisapear.append(self)

    def Destroy(self):
        Global.CatchCount += 1
        Window.lblCatch.UpdateLbl()
        Global.listPokeBall.remove(self)


class Button:

    def __init__(self,name,img_name,rect):
        self.Chosen = False
        self.sprite = Resource.listTexture[img_name]
        self.topleft = rect.topleft
        self.rect = rect
        self.name = name
        Global.listButton.append(self)

    def Update(self):

        if InputManager.mouseClick == 1 and self.rect.collidepoint(InputManager.mouses):
            Global.isHitPKMIfMousePress = True
            if self.name == 'Menu_PokeBall':
                Window.btnChosenBall.rect.left = Window.btnPokeBall.rect.left - 5
                Window.ChangeCursorWithName('Poke_Ball')
            elif self.name == 'Menu_GreatBall':
                Window.btnChosenBall.rect.left = Window.btnGreatBall.rect.left - 5
                Window.ChangeCursorWithName('Great_Ball')
            elif self.name == 'Menu_UltraBall':
                Window.btnChosenBall.rect.left = Window.btnUltraBall.rect.left - 5
                Window.ChangeCursorWithName('Ultra_Ball')
            elif self.name == 'Menu_TimerBall':
                Window.btnChosenBall.rect.left = Window.btnTimerBall.rect.left - 5
                Window.ChangeCursorWithName('Timer_Ball')
            elif self.name == 'Menu_LuxBall':
                Window.btnChosenBall.rect.left = Window.btnLuxuryBall.rect.left - 5
                Window.ChangeCursorWithName('Luxury_Ball')
            elif self.name == 'Menu_MasBall':
                Window.btnChosenBall.rect.left = Window.btnMasterBall.rect.left  - 5
                Window.ChangeCursorWithName('Priceless_Master')
            elif self.name == 'Configure' and self.Chosen == False:
                self.Chosen = True
                config()
    def Contain(self,list):
        for button in list:
            if button == self: return True
        return False
    def Draw(self):
        Window.displaySurface.blit(self.sprite,self.rect)

    def Remove(self):
        Global.listButton.remove(self)

class Label(Button):
    def __init__(self,name,image,rect):
        self.name = name
        self.sprite = image
        self.rect = rect
        Global.listButton.append(self)

    def UpdateLbl(self):
        if self.name == 'Catch':
            tmp = self.rect
            new = Window.Menu_Font.render(str(Global.CatchCount),True,Constant.BLACK)
            new_rect = new.get_rect()
            new_rect.bottomright = tmp.bottomright
            self.Remove()
            Window.lblCatch = Label('Catch',new,new_rect)
        elif self.name == 'ComboCount':
            tmp = self.rect
            new = Window.Menu_Font.render(str(Global.ComboCount),True,Constant.BLACK)
            new_rect = new.get_rect()
            new_rect.bottomright = tmp.bottomright
            self.Remove()
            Window.lblComboCount = Label('ComboCount',new,new_rect)

class ProgressBar(object):

    def __init__(self, ProgressBarBG, ProgressBarFull, rect):
        self.spriteBG = Resource.listTexture[ProgressBarBG]
        self.spriteFull = Resource.listTexture[ProgressBarFull]
        self.percent = 1
        self.rectBG = rect.copy()
        self.rectFull = rect.copy()

    def Draw(self):
        self.rectFull.height = self.rectBG.height * self.percent
        self.rectFull.bottom = self.rectBG.bottom
        timer = pygame.Surface(self.rectBG.size, pygame.SRCALPHA, 32)
        timer.blit(self.spriteFull, (0, 0), pygame.Rect((0, self.rectBG.height - self.rectFull.height), self.rectBG.size))

        Window.displaySurface.blit(self.spriteBG, self.rectBG)
        Window.displaySurface.blit(timer, self.rectFull)

    def SetPercent(self, percent):
        self.percent =  Util.Clamp(0 , 1, percent)

def checkForQuit():
    for event in pygame.event.get(QUIT): # get all the QUIT events
        terminate() # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP): # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate() # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event) # put the other KEYUP event objects back

def terminate():
    pygame.quit()
    sys.exit()

def DrawCursor():
    rect = Window.cursor.get_rect()
    rect.center = InputManager.mouses
    Window.displaySurface.blit(Window.cursor, rect)

def main():
    pygame.init()
    Time.init()
    Resource.init()
    AudioManage.init()
    Window.init()
    InputManager.Init()

    Window.CoordinationForUI()

    while True:
        if Global.ShowStartScreen:
            Intro()
        RunGame()
        GameOver()

def RunGame():
    AudioManage.PlayMusic('gameplay')
    AnimationAtStart()

    Global.listEmptySpot = Util.CalculateScreenGrid((Constant.WINDOWWIDTH, Constant.WINDOWHEIGHT), (Constant.GRID_X, Constant.GRID_Y), (50, 50, 50, 50))

    Global.GamePlayTime = 0
    spawnTime = 0
    Global.CatchCount = 0
    Global.ComboCount = 0
    Global.MaxComboCount = 0
    Global.MissCount = 0
    Global.TotalCount = 0
    Window.lblComboCount.UpdateLbl()
    Window.lblCatch.UpdateLbl()

    nextSpawnTime = random.uniform(0, Constant.POKEMON_SPAWN_TIME_INTERVAL)
    timer = ProgressBar('Timer_blank', 'Timer', Window.GamePlay_Timer_Rect)
    timer.SetPercent(1)

    while not Global.GameOver:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        if Global.GamePlayTime < Constant.GamePlayTime:
            Global.isHitPKMIfMousePress = False

            if len(Global.listPokemonSpot) >= Constant.GetPKMCountMax():
                Global.allowSpawnPokemon = False
            else:
                Global.allowSpawnPokemon = True

            Window.displaySurface.blit(Resource.listTexture['BG'], (0,0))

            for pokemon in Global.listPokemonSpot:
                pokemon.Update()
            for pokemon in Global.listPokemonAboutToDisapear:
                pokemon.DestroyPokemon()
            del Global.listPokemonAboutToDisapear[:]

            for pokeball in Global.listPokeBall:
                pokeball.Update()
            for pokeball in Global.listPokeBallAboutToDisapear:
                pokeball.Destroy()
            del Global.listPokeBallAboutToDisapear[:]

            #forground
            Window.displaySurface.blit(Resource.listTexture['Map_Header'], (0, 0))
            Window.displaySurface.blit(Resource.listTexture['Green_leaf'], Window.GreenLeaf_TopLeft)
            Window.displaySurface.blit(Resource.listTexture['Map_Footer'], Window.Footer_TopLeft)

            for button in Global.listButton:
                button.Draw()
                button.Update()

            if InputManager.mouseClick == 1 and Global.isHitPKMIfMousePress == False and Time.timeScale == 1:
                Global.MissCount += 1
                Global.TotalCount += 1
                Global.ComboCount = 0
                Window.lblComboCount.UpdateLbl()
                Global.GamePlayTime += Constant.MISS_TIME_PUNISH
                AudioManage.PlaySound('miss')

            timer.SetPercent(1- Global.GamePlayTime/Constant.GamePlayTime)
            timer.Draw()

            DrawCursor()

            # pygame Call
            pygame.display.update()
            Time.Update()

            Global.GamePlayTime += Time.deltaTime
            if Global.allowSpawnPokemon:
                spawnTime += Time.deltaTime

            if spawnTime > nextSpawnTime and Global.allowSpawnPokemon:
                nextSpawnTime = random.uniform(0, Constant.POKEMON_SPAWN_TIME_INTERVAL)
                spawnTime = 0

                index = random.randrange(0,len(Global.listEmptySpot))
                pokemon = Pokemon(index)
                pokemon.rect = Global.listEmptySpot[index]
                pokemon.init()

                del Global.listEmptySpot[index]
                Global.listPokemonSpot.append(pokemon)
        else:
            Global.GameOver = True

def GameOver():
    AudioManage.PauseMusic()
    AudioManage.PlaySound('game_over')

    catch = Global.CatchCount
    miss =  Global.MissCount
    missRate = miss / float(Global.TotalCount) * 100 if Global.TotalCount != 0 else 100
    maxCombo = Global.MaxComboCount
    score = int(catch * missRate + 100 * (1 + maxCombo/100))

    del Global.listPokemonAboutToDisapear[:]
    del Global.listPokemonSpot[:]
    del Global.listPokeBall[:]
    del Global.listPokeBallAboutToDisapear[:]

    gameoverFont = pygame.font.Font('font/UVNGIOMAY.TTF', 20)
    catchLabel = gameoverFont.render("Catched: {}".format(catch), True, (0, 0, 0))
    missLabel = gameoverFont.render("Miss ball: {}".format(miss), True, (0, 0, 0))
    missRateLabel = gameoverFont.render("Miss rate: {0:.2f}%".format(missRate), True, (0, 0, 0))
    maxComboLabel = gameoverFont.render("Max combo: {}".format(maxCombo), True, (0, 0, 0))
    scoreLabel = gameoverFont.render("Score: {}".format(score), True, (0, 0, 0))
    timer = ProgressBar('Timer_blank', 'Timer', Window.GamePlay_Timer_Rect)
    timer.SetPercent(0)
    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())

        Window.displaySurface.blit(Resource.listTexture['BG'], (0,0))
        Window.displaySurface.blit(Resource.listTexture['Map_Header'], (0, 0))
        Window.displaySurface.blit(Resource.listTexture['Map_Footer'], Window.Footer_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Poke_Ball'], Window.Menu_Poke_Ball_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Great_Ball'], Window.Menu_Great_Ball_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Ultra_Ball'], Window.Menu_Ultra_Ball_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Timer_Ball'], Window.Menu_Timer_Ball_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Luxury_Ball'], Window.Menu_Luxury_Ball_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Priceless_Master'], Window.Menu_Priceless_Master_TopLeft)
        Window.displaySurface.blit(Resource.listTexture['Item_Menu_pkm'], Window.btnPokemon.rect)
        Window.displaySurface.blit(Resource.listTexture['Item_Menu_Config'], Window.btnConfig.rect)
        timer.Draw()
        Window.lblComboCount.Draw()
        Window.lblCombo.Draw()
        Window.lblCatch.Draw()

        Window.displaySurface.blit(Resource.listTexture['Gameover'], Window.GameOver_TopLeft)
        Window.displaySurface.blit(catchLabel, Window.GameOver_Catch_TopLeft)
        Window.displaySurface.blit(missLabel, Window.GameOver_Miss_TopLeft)
        Window.displaySurface.blit(missRateLabel, Window.GameOver_MissRate_TopLeft)
        Window.displaySurface.blit(maxComboLabel, Window.GameOver_MaxCombo_TopLeft)
        Window.displaySurface.blit(scoreLabel, Window.GameOver_Score_TopLeft)

        Window.displaySurface.blit(Resource.listTexture['Menu'], Window.GameOver_ButtonMenu_Rect)
        Window.displaySurface.blit(Resource.listTexture['Replay'], Window.GameOver_ButtonReplay_Rect)

        if InputManager.mouseClick == 1:
            if Window.GameOver_ButtonMenu_Rect.collidepoint(InputManager.mouses):
                Global.ShowStartScreen = True
                Global.GameOver = False
                break
            if Window.GameOver_ButtonReplay_Rect.collidepoint(InputManager.mouses):
                Global.ShowStartScreen = False
                Global.GameOver = False
                break;

        DrawCursor()

        # pygame Call
        pygame.display.update()
        Time.Update()

def AnimationAtStart():
    AnimateHeaderAndFooter()
    AnimateMenuItem()

def AnimateHeaderAndFooter():
    startTime = 0
    headerRect = Resource.listTexture['Map_Header'].get_rect()
    footerRect = Resource.listTexture['Map_Footer'].get_rect()

    while startTime < 1.5:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        headerRect.width = footerRect.width = Constant.WINDOWWIDTH * startTime/1.5
        Window.displaySurface.blit(Resource.listTexture['BG'], (0,0))
        Window.displaySurface.blit(Resource.listTexture['Map_Header'], (0, 0), headerRect)
        Window.displaySurface.blit(Resource.listTexture['Map_Footer'], Window.Footer_TopLeft, footerRect)
        DrawCursor()
        # pygame Call
        pygame.display.update()
        Time.Update()

        startTime += Time.deltaTime

def AnimateMenuItem():
    startTime = 0

    while startTime < 1:
        checkForQuit()
        InputManager.Update(pygame.event.get())

        Window.displaySurface.blit(Resource.listTexture['BG'], (0,0))
        Window.displaySurface.blit(Resource.listTexture['Map_Header'], (0, 0))
        Window.displaySurface.blit(Resource.listTexture['Map_Footer'], Window.Footer_TopLeft)

        if startTime > 0.1:
            Window.displaySurface.blit(Resource.listTexture['Poke_Ball'], Window.Menu_Poke_Ball_TopLeft)
        if startTime > 0.2:
            Window.displaySurface.blit(Resource.listTexture['Great_Ball'], Window.Menu_Great_Ball_TopLeft)
        if startTime > 0.3:
            Window.displaySurface.blit(Resource.listTexture['Ultra_Ball'], Window.Menu_Ultra_Ball_TopLeft)
        if startTime > 0.4:
            Window.displaySurface.blit(Resource.listTexture['Timer_Ball'], Window.Menu_Timer_Ball_TopLeft)
        if startTime > 0.5:
            Window.displaySurface.blit(Resource.listTexture['Luxury_Ball'], Window.Menu_Luxury_Ball_TopLeft)
        if startTime > 0.6:
            Window.displaySurface.blit(Resource.listTexture['Priceless_Master'], Window.Menu_Priceless_Master_TopLeft)

        DrawCursor()
        # pygame Call
        pygame.display.update()
        Time.Update()

        startTime += Time.deltaTime

def Intro():
    Global.ShowStartScreen = False
    AudioManage.PlayMusic('intro')
    imageIntro = Resource.listTexture['IntroBG']

    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Window.displaySurface.blit(imageIntro, (0, 0))

        DrawCursor()

        if InputManager.mouseClick == 1 or InputManager.key != None:
            menu()
            break

        # pygame Call
        pygame.display.update()
        Time.Update()

def menu():
    menuScreenBG = Resource.listTexture['Menu_screen']

    menu_Font = pygame.font.Font('font\ETHNOCEN.TTF',35)
    menu_Label_Play = menu_Font.render('Play',True,Constant.BLACK)
    menu_Label_About = menu_Font.render('About',True,Constant.BLACK)
    menu_Label_Option = menu_Font.render('Option',True,Constant.BLACK)

    menu_Button_Play = Resource.listTexture['Menu_Play']
    menu_Button_About = Resource.listTexture['Menu_About']
    menu_Button_Option = Resource.listTexture['Menu_Option']

    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Window.displaySurface.blit(menuScreenBG, (0, 0))
        Play = menu_Button_Play.get_rect()
        Play.topright = (Window.rect.right,Constant.WINDOWHEIGHT / 8)
        Window.displaySurface.blit(menu_Button_Play, Play)

        lblPlay = menu_Label_Play.get_rect()
        lblPlay.center = Play.center
        lblPlay.left = Constant.WINDOWWIDTH * 0.8
        Window.displaySurface.blit(menu_Label_Play,lblPlay)

        About = menu_Button_About.get_rect()
        About.topright = (Window.rect.right,Constant.WINDOWHEIGHT * 3 / 8)
        Window.displaySurface.blit(menu_Button_About, About)

        lblAbout = menu_Label_About.get_rect()
        lblAbout.center = About.center
        lblAbout.left = Constant.WINDOWWIDTH * 0.75
        Window.displaySurface.blit(menu_Label_About, lblAbout)

        Option = menu_Button_Option.get_rect()
        Option.topright = (Window.rect.right,Constant.WINDOWHEIGHT * 5 / 8)
        Window.displaySurface.blit(menu_Button_Option, Option)

        lblOption = menu_Label_Option.get_rect()
        lblOption.center = Option.center
        lblOption.left = Constant.WINDOWWIDTH * 0.7
        Window.displaySurface.blit(menu_Label_Option, lblOption)

        DrawCursor()
        pygame.display.update()
        Time.Update()
        if InputManager.mouseClick == 1:
            if  Play.collidepoint(InputManager.mouses):
                break
            elif About.collidepoint(InputManager.mouses):
                about()
                break
            elif Option.collidepoint(InputManager.mouses):
                option()
                break

def about():
    aboutM = Resource.listTexture['AboutM']
    aboutB = Resource.listTexture['AboutB']
    ButtonMenu = Resource.listTexture['ButtonMenu']

    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Window.displaySurface.blit(aboutM, (0, 0))

        Board = aboutB.get_rect()
        Board.topright = (Window.rect.right,Window.rect.height / 8)
        Window.displaySurface.blit(aboutB, Board)

        btnMenu = ButtonMenu.get_rect()
        btnMenu.bottomleft = (Board.left * 1.1, Board.bottom * 0.96)
        Window.displaySurface.blit(ButtonMenu, btnMenu)

        DrawCursor()
        pygame.display.update()
        Time.Update()
        if InputManager.mouseClick == 1 and btnMenu.collidepoint(InputManager.mouses):
            menu()
            break

def option():
    optionBG = Resource.listTexture['OptionBG']
    optionB = Resource.listTexture['Option']
    ButtonMenu = Resource.listTexture['ButtonMenu']

    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Window.displaySurface.blit(optionBG , (0, 0))

        Board = optionB.get_rect()
        Board.topright = (Window.rect.right,Window.rect.height / 8)
        Window.displaySurface.blit(optionB, Board)

        btnMenu = ButtonMenu.get_rect()
        btnMenu.bottomleft = (Board.left * 1.1, Board.bottom * 0.96)
        Window.displaySurface.blit(ButtonMenu, btnMenu)


        DrawCursor()
        pygame.display.update()
        Time.Update()
        if InputManager.mouseClick == 1 and btnMenu.collidepoint(InputManager.mouses):
            menu()
            break

def config():
    Chosen = Resource.listTexture['Border_Choose_Configure'].get_rect()
    Chosen.center = Window.btnConfig.rect.center
    Window.btnChosen = Button('Chosen','Border_Choose_Configure',Chosen)
    Window.btnConfig.Draw()
    Window.btnConfigBG = Button('Config_BG','Configure_BG',Window.Config)
    Window.btnConfigConfirm = Button('Config_Confirm','ButtonConfirm',Window.Config_Confirm)
    Window.btnConfigCancel = Button('Config_Cancel','ButtonCancel',Window.Config_Cancel)
    Window.btnConfigBar1 = Button('Config_Bar1','Bar',Window.Config_Bar1)
    Window.btnConfigBar2 = Button('Config_Bar2','Bar',Window.Config_Bar2)
    Window.lblConfigMusic = Label('Config_Music',Window.Config_Music,Window.Config_Music_Rect)
    Window.lblConfigSound = Label('Config_Sound',Window.Config_Sound,Window.Config_Sound_Rect)

    deep_sea_tooth = Resource.listTexture['deep-sea-tooth']
    button_pressed = False

    while True:
        checkForQuit()
        InputManager.Update(pygame.event.get())
        Window.displaySurface.blit(Resource.listTexture['BG'], (0,0))
        Window.displaySurface.blit(Resource.listTexture['Map_Header'], (0, 0))
        Window.displaySurface.blit(Resource.listTexture['Map_Footer'], Window.Footer_TopLeft)
        for button in Global.listButton:
            button.Draw()

        CheckBoard1 = deep_sea_tooth.get_rect()
        CheckBoard1.center = (Window.Config_Cancel.centerx,Window.Config_Bar1.centery)
        Window.displaySurface.blit(deep_sea_tooth, CheckBoard1)

        CheckBoard2 = CheckBoard1
        CheckBoard2.center = (Window.Config_Cancel.centerx,Window.Config_Bar2.centery)
        Window.displaySurface.blit(deep_sea_tooth,CheckBoard2)

        DrawCursor()
        pygame.display.update()
        Time.Update()
        if InputManager.mouseClick == 1:
            if Window.Config_Confirm.collidepoint(InputManager.mouses):
                button_pressed = True
            elif Window.Config_Cancel.collidepoint(InputManager.mouses):
                button_pressed = True

        if InputManager.mouseClick == 2 and button_pressed:
            Global.isHitPKMIfMousePress = True
            if Window.Config_Confirm.collidepoint(InputManager.mouses):
                break
            elif Window.Config_Cancel.collidepoint(InputManager.mouses):
                break


    Window.btnConfig.Chosen = False
    Window.btnChosen.Remove()
    Window.btnConfigBG.Remove()
    Window.btnConfigConfirm.Remove()
    Window.btnConfigCancel.Remove()
    Window.btnConfigBar1.Remove()
    Window.btnConfigBar2.Remove()
    Window.lblConfigMusic.Remove()
    Window.lblConfigSound.Remove()


if __name__ == '__main__':
    main()
