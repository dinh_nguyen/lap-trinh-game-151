class GameInfo(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self.playerLife = 3
        self.keyCollect = 0
        self.money = 0
        self.doorOpen = True
        self.roundDone = False
        self.win = False