import pygame
from .. import Constant

class Sprite(pygame.sprite.Sprite):

    def __init__(self, tag=Constant.UNTAGGED):
        super(Sprite, self).__init__()

        self.tag = tag
        self.visible = True
        self.hook = False

        self.image = None
        self.rect = None
        self.isEnabled = True

    def setVisible(self, visible):
        if self.visible != visible:
            self.visible = visible

            if self.visible:
                self.onBecameVisible()
            else:
                self.onBecameInvisible()

    def setPositionTopLeft(self, top, left):
        if self.rect != None:
            self.rect.topleft = top, left
            return self

    def setPosition(self, x, y):
        if self.rect != None:
            self.rect.center = x, y
            return self

    def update(self):
        pass

    def onBecameVisible(self):
        pass

    def onBecameInvisible(self):
        pass

    def onCollision(self, other):
        pass
