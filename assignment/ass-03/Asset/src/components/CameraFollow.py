from ..base import Camera, utils

class CameraFollow(Camera.Camera):

    def __init__(self, obj, offsetX = 0):
        super(CameraFollow, self).__init__()

        self.obj = obj
        self.offsetX = offsetX

    def clone(self):
        camera = CameraFollow(self.obj, self.offsetX)
        camera.rect = self.rect.copy()
        camera.width, camera.height = self.width, self.height
        return camera

    def update(self):
        self.rect.centerx = utils.util.Clamp(400, 7600, self.obj.rect.centerx + self.offsetX)