import Sprite
from .. import Setup

class Image(Sprite.Sprite):

    def setSprite(self, sourceImage, nameImage=None):
        self.image = Setup.Resource.load(sourceImage, nameImage)
        assert self.image != None

        if self.rect == None:
            self.rect = self.image.get_rect()
        else:
            center = self.rect.center
            self.rect = self.image.get_rect()
            self.rect.center = center

        return self