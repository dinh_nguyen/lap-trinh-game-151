import pygame, Sprite
from .. import Constant

class Land(Sprite.Sprite):
    def __init__(self, x, y, width, height):
        super(Land, self).__init__(Constant.LAND)
        self.image = pygame.Surface((width, height)).convert()
        self.rect = self.image.get_rect()
        self.rect.topleft = x, y