import Sprite
from .. import Setup
from ..base import InputManager

class Button(Sprite.Sprite):
    BUTTON_NORMAL = 'button_normal'
    BUTTON_PRESSED = 'button_pressed'
    BUTTON_DISABLE = 'button_disable'
    BUTTON_STATE = (BUTTON_NORMAL, BUTTON_PRESSED, BUTTON_DISABLE)
    hoverOn = False

    def __init__(self):
        super(Button, self).__init__()

        self.image = None
        self.rect = None

        self.touch = False
        self.onTouchUpInside = []

        self.normalSprite = None
        self.pressSprite = None
        self.disablelSprite = None

        self.isPlayedSoundHover = False
        self.state = Button.BUTTON_NORMAL

    def update(self):
        if self.state != Button.BUTTON_DISABLE:

            if self.image == None:
                self.image = self.normalSprite

            if Setup.Input.mouseClick == Setup.Input.ONMOUSEDOWN:
                point = Setup.Window.camera.transformInversePoint(Setup.Input.mouses)
                if self.rect.collidepoint(point):
                    self.touch = True
                    self.setState(Button.BUTTON_PRESSED)
            elif Setup.Input.mouseClick == Setup.Input.ONMOUSEUP:
                point = Setup.Window.camera.transformInversePoint(Setup.Input.mouses)
                if self.touch and self.rect.collidepoint(point):
                    for cb in self.onTouchUpInside:
                        cb(self)
                    self.touch = False
                    self.setState(Button.BUTTON_NORMAL)

            #Simple hover
            if self.hoverOn:
                tmpx,tmpy = Setup.Input.mouses
                if not (tmpx in range(self.rect.left,self.rect.right) and tmpy in range(self.rect.top,self.rect.bottom)):
                    tmp = self.image.copy()
                    tmp.fill((255,255,255,0))
                    self.image = tmp
                    self.isEnabled = True
                    self.isPlayedSoundHover = True
                else:
                    if self.isPlayedSoundHover:
                        Setup.Audio.PlaySound("hover-menu")
                        self.isPlayedSoundHover = False
                    self.image = self.normalSprite

    def setState(self, state):
        assert state in Button.BUTTON_STATE

        if self.state != state:
            self.state = state

            if state == Button.BUTTON_NORMAL:
                self.image = self.normalSprite
            elif state == Button.BUTTON_PRESSED and self.pressSprite != None:
                self.image = self.pressSprite
            elif state == Button.BUTTON_DISABLE and self.disablelSprite != None:
                self.image = self.disablelSprite

            if self.rect == None:
                self.rect = self.image.get_rect()
            else:
                center = self.rect.center
                self.rect = self.image.get_rect()
                self.rect.center = center

    def setNormalSprite(self, sourceImage, nameImage=None):
        self.normalSprite = Setup.Resource.load(sourceImage, nameImage)
        assert self.normalSprite != None

        if self.state == Button.BUTTON_NORMAL:
            self.image = self.normalSprite

            if self.rect == None:
                self.rect = self.image.get_rect()
            else:
                center = self.rect.center
                self.rect = self.image.get_rect()
                self.rect.center = center

        return self

    def setPressSprite(self, sourceImage, nameImage=None):
        self.pressSprite = Setup.Resource.load(sourceImage, nameImage)

        if self.state == Button.BUTTON_PRESSED and self.pressSprite != None:
            self.image = self.pressSprite

            if self.rect == None:
                self.rect = self.image.get_rect()
            else:
                center = self.rect.center
                self.rect = self.image.get_rect()
                self.rect.center = center

        return self

    def setDisableSprite(self, sourceImage, nameImage=None):
        self.disablelSprite = Setup.Resource.load(sourceImage, nameImage)

        if self.state == Button.BUTTON_DISABLE and self.disablelSprite != None:
            self.image = self.disablelSprite

            if self.rect == None:
                self.rect = self.image.get_rect()
            else:
                center = self.rect.center
                self.rect = self.image.get_rect()
                self.rect.center = center

        return self

    def addTouchEventListener(self, listener):
        self.onTouchUpInside.append(listener)