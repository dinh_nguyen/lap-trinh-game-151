import Item
from .. import Constant, Setup

class Door(Item.Item):

    def __init__(self):
        super(Door, self).__init__()

        self.tag = Constant.DOOR
        self.open = False

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Gate_Close")
        self.rect = self.image.get_rect()

    def update(self):
        if not self.open and Setup.Info.doorOpen == True:
                self.image = Setup.Resource.load("UI", "Gate_Open")
                top, left = self.rect.topleft
                self.rect = self.image.get_rect()
                self.rect.topleft = top, left

                self.open = True
