import pygame
import Sprite
from .. import Constant, Setup

class PlayerFireBall(Sprite.Sprite):

    def __init__(self, x, y, facingRight, level):
        super(PlayerFireBall, self).__init__(Constant.PLAYERFIREBALl)

        self.setup(x, y, facingRight, level)

    def setup(self, x, y, facingRight, level):
        self.setupInfo(facingRight, level)
        self.setupSprite()
        self.resetCounter()

        self.rect.topleft = x, y

    def setupInfo(self, facingRight, level):
        self.level = level

        if facingRight:
            self.direction = Constant.RIGHT
            self.xVel = Constant.PLAYERFIREBALL_XSTARTVEL
        else:
            self.direction = Constant.LEFT
            self.xVel = -Constant.PLAYERFIREBALL_XSTARTVEL

        self.yVel = 0
        self.gravity = 0

    def setupSprite(self):
        self.clip = [Setup.Resource.load("Player", 'Player_Fire_1'),
                     Setup.Resource.load("Player", 'Player_Fire_2'),
                     Setup.Resource.load("Player", 'Player_Fire_3'),
                     Setup.Resource.load("Player", 'Player_Fire_4'),
                     Setup.Resource.load("Player", 'Player_Fire_5'),
                     Setup.Resource.load("Player", 'Player_Fire_6'),
                     Setup.Resource.load("Player", 'Player_Fire_7'),
                     Setup.Resource.load("Player", 'Player_Fire_8'),
                     Setup.Resource.load("Player", 'Player_Fire_9')]

        if self.level != 1:
            self.clip = [pygame.transform.scale(x,
                                        (int(x.get_width() *Constant.PLAYERFIREBALL_MULTIFIER_BYLEVEL[self.level-1]),
                                        int(x.get_height()*Constant.PLAYERFIREBALL_MULTIFIER_BYLEVEL[self.level-1]))) for x in self.clip]
            for x in self.clip:
                x.fill(Constant.PLAYERFIREBALL_FILLCOLOR_BYLEVEL[self.level-1],
                                None,
                                pygame.BLEND_RGBA_MULT)

        if self.direction != Constant.RIGHT:
            self.clip = [pygame.transform.flip(s, True, False) for s in self.clip]

        self.maxFrame = 9
        self.timePerFrame = Constant.PLAYER_FIRE_ANIMATION_TIME / self.maxFrame

        self.image = self.clip[0]
        self.rect = self.image.get_rect()

    def resetCounter(self):
        self.frameIndex = 0
        self.liveTime = 0
        self.animationTime = 0

    def update(self):
        self.liveTime += Setup.Time.deltaTime
        self.animationTime += Setup.Time.deltaTime
        self.updateInfo()
        self.animation()

    def updateInfo(self):
        self.rect.move_ip(self.xVel, self.yVel)

    def animation(self):
        if self.animationTime > self.timePerFrame:
            self.frameIndex = (self.frameIndex+1)%self.maxFrame
            self.animationTime = 0
            self.calculateSprite()

    def calculateSprite(self):
        self.image = self.clip[self.frameIndex]
        if self.direction == Constant.RIGHT:
            top, right = self.rect.topright
            self.rect = self.image.get_rect()
            self.rect.topright = top, right
        else:
            top, left = self.rect.topleft
            self.rect = self.image.get_rect()
            self.rect.topleft = top, left

    def onBecameInvisible(self):
        self.kill()

    def onCollision(self, other):
        if other.tag == Constant.LAND:
            if ((self.rect.height + other.rect.height)/2 - abs(self.rect.centery - other.rect.centery)) > self.rect.height/2:
                self.kill()
        elif other.tag == Constant.ENEMY:
            other.startDead()
            self.kill()
