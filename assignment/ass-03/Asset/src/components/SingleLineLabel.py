import Sprite, pygame
from .. import Setup

class SingleLineLabel(Sprite.Sprite):

    def __init__(self):
        super(SingleLineLabel, self).__init__(self)

        self.text = ""

        self.font = None
        self.fontName = None
        self.fontSize = 14
        self.isBold = False
        self.isItalic = False
        self.textColor = (255, 255, 255, 255)
        self.antialiasing = True

        self.setupFont()
        self.render()

    def setText(self, text):
        self.text = text
        self.render()

        return self

    def setFont(self, fontName, fontSize = 14, textColor = (255, 255, 255, 255), isBold = False, isItalic = False, antialiasing = False):
        self.fontName = fontName
        self.fontSize = fontSize
        self.isBold = isBold
        self.isItalic = isItalic
        self.textColor = textColor
        self.antialiasing = antialiasing

        self.setupFont()

        self.render()

        return self

    def setupFont(self):
        if self.fontName != None:
            self.font = pygame.font.Font( self.fontName , self.fontSize)
            self.font.set_bold(self.isBold)
            self.font.set_italic(self.isItalic)

    def render(self):
        if self.font != None:
            self.image = self.font.render(self.text, self.antialiasing, self.textColor).convert_alpha()
            self.rect = self.image.get_rect()