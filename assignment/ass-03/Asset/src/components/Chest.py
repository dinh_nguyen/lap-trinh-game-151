import Item
from .. import Setup

class Chest(Item.Item):

    def __init__(self, floatingGroup, item):
        super(Chest, self).__init__()

        self.deadTime = 0.0
        self.open = False
        self.floatingGroup = floatingGroup
        self.item = item

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Treasure_Close")
        self.rect = self.image.get_rect()

    def itemEffect(self, other):
        if not self.open and Setup.Info.keyCollect > 0:
            Setup.Audio.PlaySound("coin")
            self.image = Setup.Resource.load("UI", "Treasure_Open")
            bottom, left = self.rect.bottomleft
            self.rect = self.image.get_rect()
            self.rect.bottomleft = bottom, left

            Setup.Info.keyCollect -= 1
            self.open = True

            item = self.item(other).setPosition(self.rect.centerx, self.rect.centery)
            self.floatingGroup.add(item)

    def update(self):
        if self.open:
            self.deadTime += Setup.Time.deltaTime
            if self.deadTime >= 1.5:
                self.kill()
