import Sprite
from .. import Constant

class Item(Sprite.Sprite):

    def __init__(self):
        super(Item, self).__init__(Constant.ITEM)

        self.setupSprite()

    def setupSprite(self):
        pass

    def itemEffect(self, other):
        pass