import FloatingItem
from .. import Constant, Setup

class Life(FloatingItem.FloatingItem):

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Life")
        self.rect = self.image.get_rect()

    def itemEffect(self):
        self.player.life = min(self.player.life + 1, 3)