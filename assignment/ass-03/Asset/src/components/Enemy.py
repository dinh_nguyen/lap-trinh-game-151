import Sprite, pygame
from .. import Constant, Setup

class Enemy(Sprite.Sprite):

    def __init__(self):
        super(Enemy, self).__init__(Constant.ENEMY)

        self.setup(Constant.LEFT)

    def setup(self, direction):

        self.setupInfo(direction)
        self.setupCounter()
        self.setupSprite()

        self.image = self.currentClip[self.frameIndex]
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def setupInfo(self, direction):
        self.direction = direction
        self.state = Constant.WALK

        if self.direction == Constant.RIGHT:
            self.xVel = -2
        else:
            self.xVel = 2
        self.yVel = 0

    def setupCounter(self):
        self.liveTime = 0
        self.frameIndex = 0
        self.animateTimer = 0
        self.fireTimer = 0
        self.deathTimer = 0

    def setupSprite(self):
        self.currentClip = []
        self.fireBullet = None

    def update(self, fireGroup):
        self.liveTime += Setup.Time.deltaTime

        self.handleState(fireGroup)
        self.updateInfo()
        self.animation()

    def handleState(self, fireGroup):
        if self.state == Constant.WALK:
            self.walking(fireGroup)
        elif self.state == Constant.DEAD:
            self.dead()

    def walking(self, fireGroup):
        if self.liveTime - self.animateTimer > 0.5:
            self.frameIndex = (self.frameIndex+1) % (len(self.currentClip))
            self.animateTimer = self.liveTime
        if self.liveTime - self.fireTimer > 3.0:
            self.fire(fireGroup)

    def fire(self, fireGroup):
        fireGroup.add(self.fireBullet(self.rect.right, self.rect.top, self.direction))
        self.fireTimer = self.liveTime

    def startDead(self):
        self.deathTimer = self.liveTime
        self.state = Constant.DEAD

    def dead(self):
        alpha = max(0, 1- ((self.liveTime - self.deathTimer)/0.5))
        newImage = self.currentClip[self.frameIndex].copy()
        newImage.fill((0, 0, 0, alpha * 255), None, pygame.BLEND_RGBA_MULT)
        self.image = newImage

        if self.liveTime - self.deathTimer > 0.5:
            self.kill()

    def animation(self):
        if self.state !=Constant.DEAD:
            self.image = self.currentClip[self.frameIndex]

    def updateInfo(self):
        pass


