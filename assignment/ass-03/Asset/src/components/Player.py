import pygame

import Sprite, PlayerFireBall
from .. import Setup, Constant

class Player(Sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__(Constant.PLAYER)

        self.setup()

        self.state = Constant.STAND
        self.image = self.currentClip[self.frameIndex]
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def setup(self):
        self.setupInfo()
        self.setupSprite()
        self.resetCounter()
        self.resetFlag()

    def setupInfo(self):
        self.invincible = 3.0
        self.coolDown = 1.0
        self.transition = 2.0
        self.transitionChangeSprite = 0.2
        self.maxHP = 100
        self.maxLife = 3
        self.maxMoney = 99999
        self.maxJump = 1
        self.maxLevel = 3

        self.fireCoolDown = 1.0
        self.numberOfFire1 = 1
        self.numberOfFire2 = 3
        self.numberOfFire3 = 5
        self.changingSprite = [None, None]

        self.x_vel = 0
        self.y_vel = 0
        self.max_x_vel = Constant.MAX_WALK_SPEED
        self.max_y_vel = Constant.MAX_Y_VEL
        self.x_accel = Constant.WALK_ACCEL
        self.jumpVel = Constant.JUMP_VEL
        self.gravity = Constant.GRAVITY

    def setupSprite(self):
        self.leftClip_Level1 = [Setup.Resource.load("Player", "Player-Normal0"),
                                Setup.Resource.load("Player", "Player-Normal1"),
                                Setup.Resource.load("Player", "Player-Normal2"),
                                Setup.Resource.load("Player", "Player-Normal3"),
                                Setup.Resource.load("Player", "Player-Normal4"),
                                Setup.Resource.load("Player", "Player-Normal5")
                                ]

        self.leftClip_Level2 = [Setup.Resource.load("Player", "Player-Evolution0"),
                                Setup.Resource.load("Player", "Player-Evolution1"),
                                Setup.Resource.load("Player", "Player-Evolution2"),
                                Setup.Resource.load("Player", "Player-Evolution3"),
                                Setup.Resource.load("Player", "Player-Evolution4"),
                                Setup.Resource.load("Player", "Player-Evolution5")
                                ]

        self.leftClip_Level3 = [Setup.Resource.load("Player", "Player-Mega-Evolution0"),
                                Setup.Resource.load("Player", "Player-Mega-Evolution1"),
                                Setup.Resource.load("Player", "Player-Mega-Evolution2"),
                                Setup.Resource.load("Player", "Player-Mega-Evolution3"),
                                Setup.Resource.load("Player", "Player-Mega-Evolution4"),
                                Setup.Resource.load("Player", "Player-Mega-Evolution5")
                                ]

        # Create Right Sprite
        self.rightClip_Level1 = [pygame.transform.flip(s, True, False) for s in self.leftClip_Level1]

        self.rightClip_Level2 = [pygame.transform.flip(s, True, False) for s in self.leftClip_Level2]

        self.rightClip_Level3 = [pygame.transform.flip(s, True, False) for s in self.leftClip_Level3]

        self.leftClip = [self.leftClip_Level1, self.leftClip_Level2, self.leftClip_Level3]
        self.rightClip = [self.rightClip_Level1, self.rightClip_Level2, self.rightClip_Level3]

        self.currentClip = self.rightClip_Level1;

    def resetCounter(self):
        self.liveTime = 0.0
        self.walkingTime = 0.0
        self.lastFireTime = 0.0
        self.invincibleTime = 0.0
        Time = 0.0
        self.alpha = 0.0
        self.fireCount = 0
        self.transitionTime = 0.0
        self.hp = 100.0
        self.life = 3
        self.money = 0
        self.jump = 0
        self.level = 1
        self.frameIndex = 0

    def resetFlag(self):
        self.facingRight = True
        self.isInvincible = False
        self.allowFire = True
        self.allowJump = True
        self.inTransitionState = False

    def update(self, fireGroup):
        keys = Setup.Input.keys

        self.liveTime += Setup.Time.deltaTime

        self.handleState(keys, fireGroup)
        self.updateInfo()
        self.updateInvincible()
        self.animation()

    def handleState(self, keys, fireGroup):
        if self.state == Constant.STAND:
            self.standing(keys, fireGroup)
        elif self.state == Constant.WALK:
            self.walking(keys, fireGroup)
        elif self.state == Constant.JUMP:
            self.jumping(keys, fireGroup)
        elif self.state == Constant.FALL:
            self.falling(keys, fireGroup)
        elif self.state == Constant.LEVEL1TO2:
            self.Changing(1, 2)
        elif self.state == Constant.LEVEL2TO3:
            self.Changing(2, 3)
        elif self.state == Constant.LEVEL3TO2:
            self.Changing(3, 2)
        elif self.state == Constant.LEVEL2TO1:
            self.Changing(2, 1)
        else:
            self.dead()

    def standing(self, keys, fireGroup):
        self.checkAllowJump(keys)
        self.checkAllowFire(keys)

        #self.frame_index = 0
        self.x_vel = 0
        self.y_vel = 0

        if keys[Constant.keybinding['a']] or keys[Constant.keybinding['left']]:
            self.facingRight = False
            self.state = Constant.WALK
            self.walkingTime = self.liveTime
            self.calculateCurrentClip()
        elif keys[Constant.keybinding['d']] or keys[Constant.keybinding['right']]:
            self.facingRight = True
            self.state = Constant.WALK
            self.walkingTime = self.liveTime
            self.calculateCurrentClip()
        elif keys[Constant.keybinding['w']] or keys[Constant.keybinding['up']]:
            if self.allowJump:
                self.state = Constant.JUMP
                self.y_vel = self.jumpVel
                Setup.Audio.PlaySound("big_jump")
        else:
            self.state = Constant.STAND

        if keys[Constant.keybinding['space']]:
            if self.allowFire:
                self.Fire(fireGroup)

    def checkAllowJump(self, keys):
        if not keys[Constant.keybinding['w']] and not keys[Constant.keybinding['up']]:
            self.allowJump = True

    def checkAllowFire(self, keys):
        if not keys[Constant.keybinding['space']]:
            self.allowFire = True

    def Fire(self, fireGroup):
        self.fireCount = self.countPlayerFireBall(fireGroup)

        if (self.liveTime - self.lastFireTime) > self.coolDown:
            if self.fireCount < self.getMaxNumberOfFire():
                self.allowFire = False
                fireGroup.add(PlayerFireBall.PlayerFireBall(self.rect.right, self.rect.top, self.facingRight, self.level))
                self.frameIndex = len(self.currentClip)-1
                self.lastFireTime = self.liveTime
                Setup.Audio.PlaySound("fireball")

    def countPlayerFireBall(self, fireGroup):
        return len([x for x in fireGroup if x.tag == Constant.PLAYERFIREBALl])

    def getMaxNumberOfFire(self):
        if self.level == 1:
            return self.numberOfFire1
        elif self.level == 2:
            return self.numberOfFire2
        else:
            return self.numberOfFire3

    def walking(self, keys, fireGroup):
        self.checkAllowJump(keys)
        self.checkAllowFire(keys)

        if self.liveTime - self.walkingTime > self.calculateAnimationSpeed():
            self.frameIndex = (self.frameIndex+1)%(len(self.currentClip)-1)

        if keys[Constant.keybinding['space']]:
            if self.allowFire:
                self.Fire(fireGroup)

        if keys[Constant.keybinding['w']] or keys[Constant.keybinding['up']]:
            if self.allowJump:
                self.state = Constant.JUMP

                # Jump little lower when walk fast
                if abs(self.x_vel) > 4.5:
                    self.y_vel = Constant.JUMP_VEL - .5
                else:
                    self.y_vel = Constant.JUMP_VEL

        if keys[Constant.keybinding['a']] or keys[Constant.keybinding['left']]:
            self.facingRight = False
            self.calculateCurrentClip()
            if self.x_vel > 0:
                self.x_accel = Constant.TURNAROUND_ACCEL
            else:
                self.x_accel = Constant.WALK_ACCEL

            if self.x_vel > (self.max_x_vel * -1):
                self.x_vel -= self.x_accel
            elif self.x_vel < (self.max_x_vel * -1):
                self.x_vel += self.x_accel

        elif keys[Constant.keybinding['d']] or keys[Constant.keybinding['right']]:
            self.facingRight = True
            self.calculateCurrentClip()
            if self.x_vel < 0:
                self.x_accel = Constant.TURNAROUND_ACCEL
            else:
                self.x_accel = Constant.WALK_ACCEL

            if self.x_vel < self.max_x_vel:
                self.x_vel += self.x_accel
            elif self.x_vel > self.max_x_vel:
                self.x_vel -= self.x_accel

        else:
            if self.facingRight:
                if self.x_vel > 0:
                    self.x_vel -= self.x_accel
                else:
                    self.x_vel = 0
                    self.state = Constant.STAND
            else:
                if self.x_vel < 0:
                    self.x_vel += self.x_accel
                else:
                    self.x_vel = 0
                    self.state = Constant.STAND


    def calculateAnimationSpeed(self):
        if self.x_vel == 0:
            animation_speed = 0.130
        elif self.x_vel > 0:
            animation_speed = 0.130 - (self.x_vel * (13))
        else:
            animation_speed = 0.130 - (self.x_vel * (13) * -1)

        return animation_speed


    def jumping(self, keys, fireGroup):
        self.allowJump = False
        self.gravity = Constant.JUMP_GRAVITY
        self.y_vel += self.gravity
        self.checkAllowFire(keys)

        if self.y_vel >= 0 and self.y_vel < self.max_y_vel:
            self.gravity = Constant.GRAVITY
            self.state = Constant.FALL

        if keys[Constant.keybinding['a']] or keys[Constant.keybinding['left']]:
            self.facingRight = False
            self.calculateCurrentClip()
            if self.x_vel > (self.max_x_vel * - 1):
                self.x_vel -= self.x_accel

        elif keys[Constant.keybinding['d']] or keys[Constant.keybinding['right']]:
            self.facingRight = True
            self.calculateCurrentClip()
            if self.x_vel < self.max_x_vel:
                self.x_vel += self.x_accel

        if not keys[Constant.keybinding['w']] and not keys[Constant.keybinding['up']]:
            self.gravity = Constant.GRAVITY
            self.state = Constant.FALL

        if keys[Constant.keybinding['space']]:
            if self.allowFire:
                self.Fire(fireGroup)


    def falling(self, keys, fireGroup):
        self.checkAllowFire(keys)
        if self.y_vel < Constant.MAX_Y_VEL:
            self.y_vel += self.gravity

        if keys[Constant.keybinding['a']] or keys[Constant.keybinding['left']]:
            self.facingRight = False
            self.calculateCurrentClip()
            if self.x_vel > (self.max_x_vel * - 1):
                self.x_vel -= self.x_accel
        elif keys[Constant.keybinding['d']] or keys[Constant.keybinding['right']]:
            self.facingRight = True
            self.calculateCurrentClip()
            if self.x_vel < self.max_x_vel:
                self.x_vel += self.x_accel

        if keys[Constant.keybinding['space']]:
            if self.allowFire:
                self.Fire(fireGroup)

    def Changing(self, beforeLevel, afterLevel):

        if self.inTransitionState == False:
            self.inTransitionState = True
            self.transitionTime = 0
            self.calculateForChangingSprite(beforeLevel, afterLevel)
            self.x_vel = 0
            self.y_vel = 0
            Setup.Audio.PlaySound("powerup")
        elif self.transitionTime > self.transition:
            self.image = self.changingSprite[1]
            self.adjustRect()
            self.inTransitionState = False
            self.state = Constant.WALK
            self.level = afterLevel
            self.transitionTime = 0
            self.calculateCurrentClip()
        else:
            self.transitionTime += Setup.Time.deltaTime
            self.image = self.changingSprite[int(round((self.transitionTime % self.transitionChangeSprite)/self.transitionChangeSprite))]
            self.adjustRectMidBottom()

    def adjustRect(self):
        left, bottom = self.rect.bottomleft
        self.rect = self.image.get_rect()
        self.rect.bottomleft = left, bottom

    def adjustRectMidBottom(self):
        midbottom = self.rect.midbottom
        self.rect = self.image.get_rect()
        self.rect.midbottom = midbottom

    def calculateForChangingSprite(self, beforeLevel, afterLevel):
        if self.facingRight:
            self.changingSprite = [self.rightClip[beforeLevel-1][0], self.rightClip[afterLevel -1][0]]
        else:
            self.changingSprite = [self.leftClip[beforeLevel-1][0] , self.leftClip[afterLevel -1][0] ]

    def calculateCurrentClip(self, resetFrameIndex=False):
        if self.facingRight:
            self.currentClip = self.rightClip[self.level-1]
        else:
            self.currentClip = self.leftClip[self.level-1]

        if resetFrameIndex:
            self.frameIndex = 0

    def startDead(self):
        self.state = Constant.DEAD
        self.deadTime = 0
        self.x_vel = 0
        self.y_vel = -7.1
        self.gravity = 0

    def dead(self):
        self.deadTime += Setup.Time.deltaTime
        if self.deadTime > 0.22 and self.y_vel < 0:
            self.gravity = 2.0
            self.y_vel = 8.1

    def updateInvincible(self):
        if self.isInvincible:
            if self.invincibleTime > self.invincible:
                self.isInvincible = False
                self.invincibleTime = 0
            else:
                self.invincibleTime += Setup.Time.deltaTime
                self.alpha = 0.5 if self.alpha == 0 or self.alpha == 1 else 1

    def animation(self):
        if self.state == Constant.DEAD \
            or self.state == Constant.LEVEL1TO2 \
            or self.state == Constant.LEVEL2TO3 \
            or self.state == Constant.LEVEL3TO2 \
            or self.state == Constant.LEVEL2TO1:
            pass
        else:
            self.image = self.currentClip[self.frameIndex]
            self.adjustRect()

            if self.isInvincible:
                newImage = self.image.copy()
                newImage.fill((255, 255, 255, self.alpha * 255), None, pygame.BLEND_RGBA_MULT)
                self.image = newImage

    def updateInfo(self):
        if self.isInvincible and self.invincibleTime < 0.75:
            self.rect.move_ip(0, self.y_vel)
        else:
            self.rect.move_ip(self.x_vel, self.y_vel)

    def checkIfFalling(self, groundGroup):
        self.rect.top += 1

        if pygame.sprite.spritecollideany(self, groundGroup) is None:
            if self.state != Constant.JUMP and \
               self.state != Constant.DEAD and \
               self.state != Constant.LEVEL1TO2 and \
               self.state != Constant.LEVEL2TO1 and \
               self.state != Constant.LEVEL2TO3 and \
               self.state != Constant.LEVEL3TO2 :
                self.state = Constant.FALL

        self.rect.top -= 1

    def onCollision(self, other):
        if other.tag == Constant.LAND:
            self.resolveCollisionWithLand(other)
        elif other.tag == Constant.ENEMY:
            self.resolveCollisionWithEnemy(other)
        elif other.tag == Constant.ITEM:
            if self.state != Constant.DEAD:
                other.itemEffect(self)
        elif other.tag == Constant.DOOR:
            if self.state != Constant.DEAD and Setup.Info.doorOpen == True:
                Setup.Info.roundDone = True

    def resolveCollisionWithLand(self, other):
        if self.state != Constant.DEAD:
            penetrationX = (self.rect.width + other.rect.width)/2 - abs(self.rect.centerx - other.rect.centerx)
            penetrationY = (self.rect.height + other.rect.height)/2 - abs(self.rect.centery - other.rect.centery)

            if penetrationX < penetrationY:
                if self.rect.left < other.rect.left:
                    self.rect.right = other.rect.left
                else:
                    self.rect.left = other.rect.right
                self.x_vel = 0
            else:
                if self.rect.top > other.rect.top:
                    self.y_vel = 7
                    self.rect.top = other.rect.bottom
                    self.state = Constant.FALL
                else:
                    self.y_vel = 0
                    self.rect.bottom = other.rect.top
                    self.state = Constant.WALK

    def resolveCollisionWithEnemy(self, other):
        if other.state != Constant.DEAD and self.isInvincible == False and self.state != Constant.DEAD:
            if self.y_vel > 0:
                other.startDead()
                self.rect.bottom = other.rect.top
                self.state = Constant.JUMP
                self.y_vel = -7
                Setup.Audio.PlaySound("stomp")
            else:
                self.checkHit(other)

    def checkHit(self, other):
        if self.state != Constant.DEAD and \
            self.state != Constant.LEVEL1TO2 and \
            self.state != Constant.LEVEL2TO1 and \
            self.state != Constant.LEVEL2TO3 and \
            self.state != Constant.LEVEL3TO2 and \
            self.isInvincible == False:
                if self.level == 3:
                    self.state = Constant.LEVEL3TO2
                elif self.level == 2:
                    self.state = Constant.LEVEL2TO1
                else:
                    if self.life > 1:
                        self.isInvincible = True
                        self.invincibleTime = 0
                        self.life -= 1
                    else:
                        self.startDead()
                        return None

                if self.rect.left < other.rect.left:
                    self.rect.left -= 25

                Setup.Audio.PlaySound("bump")

    def onBecameInvisible(self):
        Setup.Info.roundDone = True