import Sprite
from .. import Constant

class FloatingItem(Sprite.Sprite):

    def __init__(self, player):
        super(FloatingItem, self).__init__()

        self.player = player
        self.setupSprite()
        self.beginY = None

    def setupSprite(self):
        pass

    def update(self):
        if self.beginY == None:
            self.beginY = self.rect.top

        self.rect.top -= 1.5
        if self.beginY - self.rect.top > 130:
            self.itemEffect()
            self.kill()

    def itemEffect(self):
        pass