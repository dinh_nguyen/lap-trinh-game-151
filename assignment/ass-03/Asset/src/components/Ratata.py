import Enemy, EnemyAttack1, pygame
from .. import Setup, Constant

class Ratata(Enemy.Enemy):

    def setupSprite(self):
        self.currentClip = [Setup.Resource.load("Enemy", "Ratata_0"),
                            Setup.Resource.load("Enemy", "Ratata_1")]

        if self.direction != Constant.LEFT:
            self.currentClip = [pygame.transform.flip(s, True, False) for s in self.currentClip]

        self.fireBullet = EnemyAttack1.EnemyAttack1