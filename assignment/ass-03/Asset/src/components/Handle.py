import Item
from .. import Constant, Setup

class Handle(Item.Item):

    def __init__(self):
        super(Handle, self).__init__()

        self.handle = False

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Left_Handle")
        self.rect = self.image.get_rect()

    def itemEffect(self, other):
        if not self.handle:
            self.image = Setup.Resource.load("UI", "Right_Handle")
            bottom, left = self.rect.bottomleft
            self.rect = self.image.get_rect()
            self.rect.bottomleft = bottom, left

            Setup.Info.handle = self.handle = True
