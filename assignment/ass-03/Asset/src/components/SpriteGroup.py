import pygame
from .. import Setup

class SpriteGroup(pygame.sprite.Group):

    def draw(self):
        sprites = self.sprites()
        visible = True

        for spr in sprites:
            if spr.isEnabled :
                self.spritedict[spr], visible = Setup.Window.blit(spr.image, spr.rect, not spr.hook)
                spr.setVisible(visible)
        self.lostsprites = []