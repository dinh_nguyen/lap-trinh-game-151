import Item
from .. import Constant, Setup

class Key(Item.Item):

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Key")
        self.rect = self.image.get_rect()

    def itemEffect(self, other):
        Setup.Audio.PlaySound("coin")
        Setup.Info.keyCollect += 1
        self.kill()