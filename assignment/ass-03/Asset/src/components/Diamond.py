import Item
from .. import Constant, Setup

class Diamond(Item.Item):

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Diamond")
        self.rect = self.image.get_rect()

    def itemEffect(self, other):
        Setup.Audio.PlaySound("coin")
        Setup.Info.money += Constant.MONEYREWARD

        self.kill()