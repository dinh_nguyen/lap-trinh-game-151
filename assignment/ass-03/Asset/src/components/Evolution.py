import FloatingItem
from .. import Constant, Setup

class Evolution(FloatingItem.FloatingItem):

    def setupSprite(self):
        self.image = Setup.Resource.load("UI", "Evolution")
        self.rect = self.image.get_rect()

    def itemEffect(self):
        if self.player.level == 1:
            self.player.state = Constant.LEVEL1TO2
        else:
            self.player.state = Constant.LEVEL2TO3