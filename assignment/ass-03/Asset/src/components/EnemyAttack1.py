import Sprite, pygame
from .. import Constant, Setup

class EnemyAttack1(Sprite.Sprite):

    def __init__(self, x, y, direction):
        super(EnemyAttack1, self).__init__(Constant.ENEMYFIREBALL)

        self.setup(x, y, direction)

    def setup(self, x, y, direction):
        self.setupInfo(direction)
        self.setupSprite()
        self.resetCounter()

        self.rect.topleft = x, y

    def setupInfo(self, direction):

        self.direction = direction
        if self.direction == Constant.RIGHT:
            self.xVel = Constant.PLAYERFIREBALL_XSTARTVEL
        else:
            self.xVel = -Constant.PLAYERFIREBALL_XSTARTVEL

        self.yVel = 0
        self.gravity = 0

    def setupSprite(self):
        self.image = Setup.Resource.load("Enemy", 'Attack1')

        if self.direction != Constant.RIGHT:
            self.image = pygame.transform.flip(self.image, True, False)

        self.rect = self.image.get_rect()

    def resetCounter(self):
        pass

    def update(self):
        self.updateInfo()

    def updateInfo(self):
        self.rect.move_ip(self.xVel, self.yVel)

    def onBecameInvisible(self):
        self.kill()

    def onCollision(self, other):
        if other.tag == Constant.LAND:
            if ((self.rect.height + other.rect.height)/2 - abs(self.rect.centery - other.rect.centery)) > self.rect.height/2:
                self.kill()
        elif other.tag == Constant.PLAYER:
            other.checkHit(self)
            self.kill()