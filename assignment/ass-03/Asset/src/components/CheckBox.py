import Sprite
from .. import Setup
from ..base import InputManager

class CheckBox(Sprite.Sprite):
    check = False
    disable = False

    def __init__(self):
        super(CheckBox,self).__init__()
        self.touch = False
        self.onTouchUpInside = None

        self.image = None
        self.rect = None

    def update(self):
        if not self.disable:
            if self.image == None:
                self.image = self.normalSprite
            if Setup.Input.mouseClick == Setup.Input.ONMOUSEDOWN:
                point = Setup.Window.camera.transformInversePoint(Setup.Input.mouses)
                if self.rect.collidepoint(point):
                    self.touch = True
            elif Setup.Input.mouseClick == Setup.Input.ONMOUSEUP:
                point = Setup.Window.camera.transformInversePoint(Setup.Input.mouses)
                if self.touch and self.rect.collidepoint(point):
                    self.check = not self.check
                    if not (self.onTouchUpInside == None):
                        self.onTouchUpInside(self)
                self.touch = False
            if self.check:
                self.image = self.normalSprite
            else:
                self.image = self.uncheckSprite

    def setNormalSprite(self, sourceImage, nameImage=None):
        self.normalSprite = Setup.Resource.load(sourceImage, nameImage)
        assert self.normalSprite != None
        img = self.normalSprite.copy()
        img.fill((255,255,255))
        self.uncheckSprite = img

        self.image = self.uncheckSprite

        if self.rect == None:
            self.rect = self.image.get_rect()
        else:
            center = self.rect.center
            self.rect = self.image.get_rect()
            self.rect.center = center

        return self

    def setCheck(self, isChecking):
        self.check = isChecking