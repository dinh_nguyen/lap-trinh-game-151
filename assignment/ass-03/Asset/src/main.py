import SceneController
from collections import OrderedDict
from scene import IntroScene, MainMenu_Scene, About_Scene, Option_Scene, Config_Scene, GamePlay_Scene, GameOver_Scene
from . import Constant

def main():

    sceneList = OrderedDict([(Constant.INTRO_SCENE, IntroScene.Intro()),
                             (Constant.MAINMENU_SCENE, MainMenu_Scene.MainMenu()),
                             (Constant.GAMEPLAY_SCENE, GamePlay_Scene.GamePlay()),
                             (Constant.ABOUT_SCENE, About_Scene.About()),
                             (Constant.OPTION_SCENE, Option_Scene.Option()),
                             (Constant.GAMEOVER_SCENE, GameOver_Scene.GameOver()),
                             (Constant.CONFIG_SCENE, Config_Scene.Config())])
    app = SceneController.Control(sceneList)
    app.run()