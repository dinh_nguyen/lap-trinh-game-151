from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager
from ..components import Button, Image, SingleLineLabel, SpriteGroup

class MainMenu(SceneController.Scene):

    def __init__(self):
        super(MainMenu, self).__init__()

    def start(self):
        super(MainMenu, self).start()

        self.camera = Camera.Camera()

        Setup.Audio.PlayMusic("intro")

        background = Image.Image().setSprite("BG", "BG_MainMenu").setPositionTopLeft(0, 0)

        btnPlay = Button.Button().setNormalSprite("UI", "Button_Menu_Play").setPositionTopLeft(508, 120)
        btnPlay.isEnabled = False
        btnPlay.hoverOn = True
        btnPlay.addTouchEventListener(self.buttonPlayPress)

        btnAbout = Button.Button().setNormalSprite("UI", "Button_Menu_About").setPositionTopLeft(418, 270)
        btnAbout.isEnabled = False
        btnAbout.hoverOn = True
        btnAbout.addTouchEventListener(self.buttonAboutPress)

        btnOption = Button.Button().setNormalSprite("UI", "Button_Menu_Option").setPositionTopLeft(400, 420)
        btnOption.isEnabled = False
        btnOption.hoverOn = True
        btnOption.addTouchEventListener(self.buttonOptionPress)

        lblPlay    = SingleLineLabel.SingleLineLabel().setText("Play")  .setFont("Asset/Font/ethnocen.ttf", 35, (0, 0, 0, 255)).setPositionTopLeft(640, 143.5)
        lblAbout   = SingleLineLabel.SingleLineLabel().setText("About") .setFont("Asset/Font/ethnocen.ttf", 35, (0, 0, 0, 255)).setPositionTopLeft(560, 292)
        lblOption  = SingleLineLabel.SingleLineLabel().setText("Option").setFont("Asset/Font/ethnocen.ttf", 35, (0, 0, 0, 255)).setPositionTopLeft(560, 443.5)

        self.BGGroup = SpriteGroup.SpriteGroup(background)
        self.buttonGroup = SpriteGroup.SpriteGroup(btnPlay, btnOption, btnAbout)
        self.lableGroup = SpriteGroup.SpriteGroup(lblPlay, lblAbout, lblOption)

        self.finishInitialize()

    def clear(self):
        super(MainMenu, self).clear()

        self.BGGroup = SpriteGroup.SpriteGroup()
        self.buttonGroup = SpriteGroup.SpriteGroup()
        self.lableGroup = SpriteGroup.SpriteGroup()

    def update(self):
        super(MainMenu, self).update()

        self.BGGroup.draw()
        self.buttonGroup.draw()
        self.lableGroup.draw()

        self.buttonGroup.update()

    def buttonPlayPress(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.GAMEPLAY_SCENE)

    def buttonOptionPress(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.OPTION_SCENE)

    def buttonAboutPress(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.ABOUT_SCENE)















