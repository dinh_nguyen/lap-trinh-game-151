import pygame
from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager
from ..components import SpriteGroup, Player, Land, CameraFollow, Door, Diamond, Key, Handle, Chest, Ratata, Life, Evolution
from ..TileMap.pytmx import util_pygame
from ..TileMap.pyscroll import data, group, renderer

class GamePlay(SceneController.Scene):

    def __init__(self):
        super(GamePlay, self).__init__()

    def start(self):
        super(GamePlay, self).start()

        Setup.Audio.PlayMusic("gameplay")

        self.setup()

        self.camera = CameraFollow.CameraFollow(self.player, 0)

        self.finishInitialize()

    def setup(self):
        self.setupInfo()
        self.setupTMXMap()
        self.setupEnemy()
        self.setupPlayer()
        self.setupItem()

    def setupInfo(self):
        self.playerFireColliderGroup= SpriteGroup.SpriteGroup()
        self.enemyFireColliderGroup = SpriteGroup.SpriteGroup()
        self.floatingItemGroup = SpriteGroup.SpriteGroup()
        Setup.Info.reset()

    def setupTMXMap(self):

        #load data from pytmx
        tmxData = util_pygame.load_pygame(Constant.TILEMAP_PATH)

        # create new data source for pyscroll
        map_data = data.TiledMapData(tmxData)

        map_layer = renderer.BufferedRenderer(map_data, Constant.SCREEN_SIZE, clamp_camera=True)
        self.group = group.PyscrollGroup(map_layer=map_layer, default_layer=0)
        self.temp_surface = pygame.Surface(Constant.SCREEN_SIZE).convert()

        self.landColliderGroup = SpriteGroup.SpriteGroup()

        for obj in tmxData:
            if obj.name == "LandCollider":
                self.landColliderGroup.add(Land.Land(obj.x, obj.y, obj.width, obj.height))

    def setupEnemy(self):
        ratata1 = Ratata.Ratata().setPositionTopLeft(1150, 480)
        ratata2 = Ratata.Ratata().setPositionTopLeft(3169, 480)
        ratata3 = Ratata.Ratata().setPositionTopLeft(4318, 432)
        ratata4 = Ratata.Ratata().setPositionTopLeft(5560, 313)
        ratata5 = Ratata.Ratata().setPositionTopLeft(7119, 480)

        ratata6 = Ratata.Ratata().setPositionTopLeft(1344, 426)
        ratata7 = Ratata.Ratata().setPositionTopLeft(2720, 420)
        ratata8 = Ratata.Ratata().setPositionTopLeft(3585, 485)
        ratata9 = Ratata.Ratata().setPositionTopLeft(4830, 475)
        ratata10 = Ratata.Ratata().setPositionTopLeft(4900, 475)

        ratata11 = Ratata.Ratata().setPositionTopLeft(4970, 475)
        ratata12 = Ratata.Ratata().setPositionTopLeft(5040, 475)

        self.enemyColliderGroup = SpriteGroup.SpriteGroup(ratata1, ratata2, ratata3, ratata4, ratata5, ratata6, ratata7, ratata8, ratata9, ratata10,
                                                            ratata11, ratata12)

    def setupPlayer(self):
        self.player = Player.Player().setPositionTopLeft(550, 480)

    def setupItem(self):
        diamond1 = Diamond.Diamond().setPositionTopLeft(730, 490)
        diamond2 = Diamond.Diamond().setPositionTopLeft(890, 430)
        diamond3 = Diamond.Diamond().setPositionTopLeft(1000, 400)
        diamond4 = Diamond.Diamond().setPositionTopLeft(1020, 380)
        diamond5 = Diamond.Diamond().setPositionTopLeft(1040, 400)

        diamond6 = Diamond.Diamond().setPositionTopLeft(1575, 490)
        diamond7 = Diamond.Diamond().setPositionTopLeft(1750, 490)
        diamond8 = Diamond.Diamond().setPositionTopLeft(1750, 370)
        diamond9 = Diamond.Diamond().setPositionTopLeft(2245, 390)
        diamond10 = Diamond.Diamond().setPositionTopLeft(2245, 370)

        diamond11 = Diamond.Diamond().setPositionTopLeft(2245, 350)
        diamond12 = Diamond.Diamond().setPositionTopLeft(2265, 370)
        diamond13 = Diamond.Diamond().setPositionTopLeft(2688, 462)
        diamond14 = Diamond.Diamond().setPositionTopLeft(2630, 370)
        diamond15 = Diamond.Diamond().setPositionTopLeft(2650, 370)

        diamond16 = Diamond.Diamond().setPositionTopLeft(2650, 350)
        diamond17 = Diamond.Diamond().setPositionTopLeft(3095, 400)
        diamond18 = Diamond.Diamond().setPositionTopLeft(2975, 277)
        diamond19 = Diamond.Diamond().setPositionTopLeft(3225, 365)
        diamond20 = Diamond.Diamond().setPositionTopLeft(3294, 491)

        diamond21 = Diamond.Diamond().setPositionTopLeft(3624, 393)
        diamond22 = Diamond.Diamond().setPositionTopLeft(3795, 377)
        diamond23 = Diamond.Diamond().setPositionTopLeft(3815, 377)
        diamond24 = Diamond.Diamond().setPositionTopLeft(3815, 357)
        diamond25 = Diamond.Diamond().setPositionTopLeft(3795, 357)

        diamond26 = Diamond.Diamond().setPositionTopLeft(4068, 379)
        diamond27 = Diamond.Diamond().setPositionTopLeft(4314, 442)
        diamond28 = Diamond.Diamond().setPositionTopLeft(4334, 442)
        diamond29 = Diamond.Diamond().setPositionTopLeft(4354, 442)
        diamond30 = Diamond.Diamond().setPositionTopLeft(4314, 422)

        diamond31 = Diamond.Diamond().setPositionTopLeft(4334, 422)
        diamond32 = Diamond.Diamond().setPositionTopLeft(4354, 422)
        diamond33 = Diamond.Diamond().setPositionTopLeft(5028, 200)
        diamond34 = Diamond.Diamond().setPositionTopLeft(5008, 200)
        diamond35 = Diamond.Diamond().setPositionTopLeft(4988, 200)

        diamond36 = Diamond.Diamond().setPositionTopLeft(4968, 200)
        diamond37 = Diamond.Diamond().setPositionTopLeft(5008, 180)
        diamond38 = Diamond.Diamond().setPositionTopLeft(4988, 180)
        diamond39 = Diamond.Diamond().setPositionTopLeft(5212, 327)
        diamond40 = Diamond.Diamond().setPositionTopLeft(5214, 160)

        diamond41 = Diamond.Diamond().setPositionTopLeft(5284, 426)
        diamond42 = Diamond.Diamond().setPositionTopLeft(4773, 166)
        diamond43 = Diamond.Diamond().setPositionTopLeft(5595, 351)
        diamond44 = Diamond.Diamond().setPositionTopLeft(5615, 371)
        diamond45 = Diamond.Diamond().setPositionTopLeft(5635, 391)

        diamond46 = Diamond.Diamond().setPositionTopLeft(6389, 324)
        diamond47 = Diamond.Diamond().setPositionTopLeft(6369, 324)
        diamond48 = Diamond.Diamond().setPositionTopLeft(6349, 324)
        diamond49 = Diamond.Diamond().setPositionTopLeft(6369, 324)
        diamond50 = Diamond.Diamond().setPositionTopLeft(6680, 304)

        diamond51 = Diamond.Diamond().setPositionTopLeft(6874, 317)

        key1 = Key.Key().setPositionTopLeft(1349, 432)
        key2 = Key.Key().setPositionTopLeft(5460, 493)
        key3 = Key.Key().setPositionTopLeft(6278, 381)

        handle = Handle.Handle().setPositionTopLeft(6088, 491)

        chest1 = Chest.Chest(self.floatingItemGroup, Evolution.Evolution).setPositionTopLeft(1416, 490)
        chest2 = Chest.Chest(self.floatingItemGroup, Evolution.Evolution).setPositionTopLeft(3237, 486)
        chest3 = Chest.Chest(self.floatingItemGroup, Life.Life).setPositionTopLeft(5158, 301)
        chest4 = Chest.Chest(self.floatingItemGroup, Life.Life).setPositionTopLeft(6178, 522)
        chest5 = Chest.Chest(self.floatingItemGroup, Life.Life).setPositionTopLeft(7123, 490)

        door = Door.Door().setPositionTopLeft(7450, 422)

        self.itemColliderGroup = SpriteGroup.SpriteGroup(diamond1 , diamond2 , diamond3 , diamond4 , diamond5 , diamond6 , diamond7 , diamond8 , diamond9 , diamond10,
                                                         diamond11, diamond12, diamond13, diamond14, diamond15, diamond16, diamond17, diamond18, diamond19, diamond20,
                                                         diamond21, diamond22, diamond23, diamond24, diamond25, diamond26, diamond27, diamond28, diamond29, diamond30,
                                                         diamond31, diamond32, diamond33, diamond34, diamond35, diamond36, diamond37, diamond38, diamond39, diamond40,
                                                         diamond41, diamond42, diamond43, diamond44, diamond45, diamond46, diamond47, diamond48, diamond49, diamond50, diamond51,
                                                         key1, key2, key3,
                                                         #handle,
                                                         chest1, chest2, chest3, chest4, chest5,
                                                         door)

    def setupToggleLand(self):
        self.toggleLandColliderGroup = SpriteGroup.SpriteGroup()

    def drawAll(self):
        self.drawMap(self.temp_surface)
        Setup.Window.blit(self.temp_surface, (0, 0))

        self.player.setVisible(Setup.Window.blit(self.player.image, self.player.rect, True)[1])

        self.enemyColliderGroup.draw()

        self.itemColliderGroup.draw()

        self.floatingItemGroup.draw()

        self.playerFireColliderGroup.draw()

        self.enemyFireColliderGroup.draw()

    def drawMap(self, surface):
		# center map / screen on player
        self.group.center(self.camera.rect.center)

		# draw the map and all sprites
        self.group.draw(surface)


    def update(self):
        self.updateAllSprite()
        self.checkCollision()
        self.additionCheckAfterCollision()
        self.camera.update()
        self.drawAll()
        self.checkGameInfo()

    def clear(self):
        super(GamePlay, self).clear()

    def checkGameInfo(self):
        if Setup.Info.roundDone:
            self.loadScene(Constant.GAMEOVER_SCENE)

    def updateAllSprite(self):
##        self.toggleLandColliderGroup.update()
        self.player.update(self.playerFireColliderGroup)
        self.enemyColliderGroup.update(self.enemyFireColliderGroup)
        self.itemColliderGroup.update()
        self.playerFireColliderGroup.update()
        self.enemyFireColliderGroup.update()
        self.floatingItemGroup.update()
        pass

    def checkCollision(self):
        self.playerAndLand()
##        self.playerAndToggleLand()
        self.playerAndEnemy()
        self.playerAndItem()
        self.playerAndEnemyFire()
        self.playerFireAndEnemy()
        self.playerFireAndLand()
        self.enemyFireAndLand()

    def playerAndLand(self):
        result = pygame.sprite.spritecollideany(self.player, self.landColliderGroup)

        if result:
            self.player.onCollision(result)

    def playerAndToggleLand(self):
        sprList = self.toggleLandColliderGroup.sprites()

        for spr in sprList:
            if spr.enable and pygame.sprite.collide_mask(self.player, spr):
                self.player.onCollision(spr)

    def playerAndEnemy(self):
        result = pygame.sprite.spritecollideany(self.player, self.enemyColliderGroup)

        if result:
            self.player.onCollision(result)

    def playerAndItem(self):
        result = pygame.sprite.spritecollideany(self.player, self.itemColliderGroup)

        if result:
            self.player.onCollision(result)

    def playerAndEnemyFire(self):
        result = pygame.sprite.spritecollideany(self.player, self.enemyFireColliderGroup)

        if result:
            result.onCollision(self.player)

    def playerFireAndEnemy(self):
        spr_dict = pygame.sprite.groupcollide(self.playerFireColliderGroup, self.enemyColliderGroup, False, False)

        for spr1, spr2List in spr_dict.iteritems():
            for spr2 in spr2List:
                spr1.onCollision(spr2)

    def playerFireAndLand(self):
        spr_dict = pygame.sprite.groupcollide(self.playerFireColliderGroup, self.landColliderGroup, False, False)

        for spr1, spr2List in spr_dict.iteritems():
            for spr2 in spr2List:
                spr1.onCollision(spr2)

    def enemyFireAndLand(self):
        spr_dict = pygame.sprite.groupcollide(self.enemyFireColliderGroup, self.landColliderGroup, False, False)

        for spr1, spr2List in spr_dict.iteritems():
            for spr2 in spr2List:
                spr1.onCollision(spr2)

    def additionCheckAfterCollision(self):
        self.player.checkIfFalling(self.landColliderGroup)