from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager
from ..components import Button, Image, SpriteGroup

class About(SceneController.Scene):

    def __init__(self):
        super(About, self).__init__()

    def start(self):
        super(About, self).start()

        self.camera = Camera.Camera()

        background = Image.Image().setSprite("BG", "BG_About").setPositionTopLeft(0, 0)
        about = Image.Image().setSprite("BG","BG_About_Info")
        about.rect.topright = background.rect.right, background.rect.height * 0.1

        btnMenu = Button.Button().setNormalSprite("UI", "ok") #Change "Key" to "ButtonMenu"
        btnMenu.rect.bottomleft = (about.rect.left + 10, about.rect.bottom - 10)
        btnMenu.addTouchEventListener(self.buttonMenuPressed)

        self.BGGroup = SpriteGroup.SpriteGroup(background)
        self.buttonGroup = SpriteGroup.SpriteGroup(btnMenu)
        self.lableGroup = SpriteGroup.SpriteGroup(about)

        self.finishInitialize()

    def clear(self):
        super(About, self).clear()

        self.BGGroup = SpriteGroup.SpriteGroup()
        self.buttonGroup = SpriteGroup.SpriteGroup()
        self.lableGroup = SpriteGroup.SpriteGroup()

    def update(self):
        super(About, self).update()

        self.BGGroup.draw()
        self.lableGroup.draw()
        self.buttonGroup.draw()

        self.buttonGroup.update()

    def buttonMenuPressed(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.MAINMENU_SCENE)