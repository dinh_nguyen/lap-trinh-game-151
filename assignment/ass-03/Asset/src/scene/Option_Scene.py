from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager, WindowManager, AudioManager
from ..components import Button, CheckBox, Image, SpriteGroup

class Option(SceneController.Scene):

    def __init__(self):
        super(Option, self).__init__()

    def start(self):
        super(Option, self).start()

        self.camera = Camera.Camera()

        background = Image.Image().setSprite("BG", "BG_Option").setPositionTopLeft(0, 0)
        option = Image.Image().setSprite("BG","BG_Option_Setting")
        option.rect.topright = (background.rect.right, background.rect.height * 0.1)

        boundingBoxMusic = Image.Image().setSprite("UI","ButtonMenu")
        boundingBoxMusic.rect.bottomleft = option.rect.left + option.rect.width * 0.39, option.rect.height * 0.43

        cbxMusic = CheckBox.CheckBox().setNormalSprite("UI","Evolution")
        cbxMusic.rect.bottomleft = option.rect.left + option.rect.width * 0.4, option.rect.height * 0.42
        cbxMusic.UncheckState = 0.0
        cbxMusic.setCheck(AudioManager.isMusicOn)
        cbxMusic.onTouchUpInside = self.setMusic

        boundingBoxSound = Image.Image().setSprite("UI","ButtonMenu")
        boundingBoxSound.rect.bottomleft = option.rect.left + option.rect.width * 0.39, option.rect.height * 0.76

        cbxSound = CheckBox.CheckBox().setNormalSprite("UI","Evolution")
        cbxSound.rect.bottomleft = option.rect.left + option.rect.width * 0.4, option.rect.height * 0.75
        cbxSound.UncheckState = 0.0
        cbxSound.setCheck(AudioManager.isSoundEffectOn)
        cbxSound.onTouchUpInside = self.setSound

        btnMenu = Button.Button().setNormalSprite("UI", "ok") #Change "Key" to "ButtonMenu"
        btnMenu.rect.bottomleft = (option.rect.left + 10, option.rect.bottom - 10)
        btnMenu.addTouchEventListener(self.buttonMenuPressed)

        self.BGGroup = SpriteGroup.SpriteGroup(background)
        self.buttonGroup = SpriteGroup.SpriteGroup(btnMenu,cbxMusic,cbxSound,boundingBoxSound,boundingBoxMusic)
        self.lableGroup = SpriteGroup.SpriteGroup(option)

        self.finishInitialize()

    def clear(self):
        super(Option, self).clear()

        self.BGGroup = SpriteGroup.SpriteGroup()
        self.buttonGroup = SpriteGroup.SpriteGroup()
        self.lableGroup = SpriteGroup.SpriteGroup()

    def update(self):
        super(Option, self).update()

        self.BGGroup.draw()
        self.lableGroup.draw()
        self.buttonGroup.draw()

        self.buttonGroup.update()

    def buttonMenuPressed(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.MAINMENU_SCENE)

    def setMusic(self,checkbox):
        AudioManager.isMusicOn = not AudioManager.isMusicOn
        if not AudioManager.isMusicOn:
            Setup.Audio.StopMusic()
    def setSound(self,checkbox):
        AudioManager.isSoundEffectOn = not AudioManager.isSoundEffectOn
        Setup.Audio.PlaySound("click")