from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager
from ..components import Button, CheckBox, Image, SingleLineLabel, SpriteGroup

class Config(SceneController.Scene):

    def __init__(self):
        super(Config, self).__init__()

    def start(self):
        super(Config, self).start()

        self.camera = Camera.Camera()

        background = Image.Image().setSprite("Texture", "Field").setPositionTopLeft(0, 0)

        option = Image.Image().setSprite("BG","BG_Option_Setting")
        option.rect.center = background.rect.center

        cbxMusic = CheckBox.CheckBox().setNormalSprite("UI","Evolution")
        cbxMusic.rect.bottomleft = option.rect.left + option.rect.width * 0.4, option.rect.top + option.rect.height * 0.27
        cbxMusic.UncheckState = 0.0
        self.CheckMusic = cbxMusic
        #cbxMusic.CheckedState = ??
        #cbxMusic.onTouchUpInside = self.setMusic

        cbxSound = CheckBox.CheckBox().setNormalSprite("UI","Evolution")
        cbxSound.rect.bottomleft = option.rect.left + option.rect.width * 0.4, option.rect.top + option.rect.height * 0.6
        cbxSound.UncheckState = 0.0
        self.CheckSound = cbxSound
        #cbxSound.CheckedState = ??
        #cbxSound.onTouchUpInside = self.setSound

        btnMenu = Button.Button().setNormalSprite("Texture", "ButtonMenu") #Change "Key" to "ButtonMenu"
        btnMenu.rect.bottomleft = (option.rect.left + option.rect.width * 0.25, option.rect.bottom - 20)
        btnMenu.addTouchEventListener(self.buttonMenuPressed)

        btnCancel = Button.Button().setNormalSprite("Texture", "ButtonCancel")
        btnCancel.rect.bottomleft = (btnMenu.rect.right + 5, option.rect.bottom - 10)
        btnCancel.addTouchEventListener(self.buttonCancelPressed)

        btnConfirm = Button.Button().setNormalSprite("Texture", "ButtonConfirm")
        btnConfirm.rect.bottomleft = btnCancel.rect.topleft
        btnConfirm.addTouchEventListener(self.buttonConfirmPressed)

        self.BGGroup = SpriteGroup.SpriteGroup(background)
        self.buttonGroup = SpriteGroup.SpriteGroup(btnMenu,btnCancel,btnConfirm,cbxMusic,cbxSound)
        self.lableGroup = SpriteGroup.SpriteGroup(option)

        self.finishInitialize()

    def clear(self):
        super(Config, self).clear()

        self.BGGroup = SpriteGroup.SpriteGroup()
        self.buttonGroup = SpriteGroup.SpriteGroup()
        self.lableGroup = SpriteGroup.SpriteGroup()

    def update(self):
        super(Config, self).update()

        self.BGGroup.draw()
        self.lableGroup.draw()
        self.buttonGroup.draw()

        self.buttonGroup.update()

    def buttonMenuPressed(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.MAINMENU_SCENE)

    def buttonCancelPressed(self, button):
        #self.animation()
        #self.loadScene(Constant.GAMEPLAY_SCENE)
        pass

    def buttonConfirmPressed(self, button):
        if self.CheckMusic.check:
            self.setMusic(self.CheckMusic)
        if self.CheckSound.check:
            self.setSound(self.CheckSound)
        #self.animation()
        #self.loadScene(Constant.GAMEPLAY_SCENE)
        pass

    def animation(self):
        self.clear()
        startTime = 5
        i = 3
        BG = Image.Image().setSprite("Texture", "Field").setPositionTopLeft(0, 0)
        self.BGGroup = SpriteGroup.SpriteGroup(BG)
        while startTime > 0:
            self.update()
            if i > 0:
                lblCount = SingleLineLabel.SingleLineLabel().setText(str(i))  .setFont("Asset/Font/ethnocen.ttf", 100, (0, 0, 0))
            else:
                if i == 0: lblCount = SingleLineLabel.SingleLineLabel().setText("GO")  .setFont("Asset/Font/ethnocen.ttf", 100, (0, 0, 0))
            lblCount.rect.center = BG.rect.center
            Setup.Window.blit(lblCount.image,lblCount.rect,True)

            Setup.Time.tick()
            i -= 1
            startTime -= Setup.Time.deltaTime

    def setMusic(self,checkbox): pass
    def setSound(self,checkbox): pass