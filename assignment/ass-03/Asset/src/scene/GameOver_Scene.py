from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager
from ..components import Button, Image, SpriteGroup, SingleLineLabel

class GameOver(SceneController.Scene):
    def __init__(self):
        super(GameOver, self).__init__()

    def start(self):
        super(GameOver, self).start()

        self.camera = Camera.Camera()

        Setup.Audio.StopMusic()
        Setup.Audio.PlaySound("game_over")

        background = Image.Image().setSprite("BG", "BG_Play").setPositionTopLeft(0, 0)

        btnMenu = Button.Button().setNormalSprite("UI", "ButtonMenuBack")
        btnMenu.addTouchEventListener(self.buttonMenuPressed)
        btnReplay = Button.Button().setNormalSprite("UI", "Replay")
        btnReplay.addTouchEventListener(self.buttonGameOverPressed)

        btnMenu.setPositionTopLeft(275,250)
        btnReplay.setPositionTopLeft(425,250)

        lblPlay    = SingleLineLabel.SingleLineLabel().setText("Game Over").setFont("Asset/Font/ethnocen.ttf", 35, (255, 0, 0, 255)).setPositionTopLeft(250, 143.5)

        self.BGGroup = SpriteGroup.SpriteGroup(background)
        self.LabelGroup = SpriteGroup.SpriteGroup(lblPlay)
        self.BtnGroup = SpriteGroup.SpriteGroup(btnMenu, btnReplay)

        self.finishInitialize()

    def clear(self):
        super(GameOver, self).clear()

        self.BGGroup = SpriteGroup.SpriteGroup()
        self.LabelGroup = SpriteGroup.SpriteGroup()

    def update(self):
        super(GameOver, self).update()

        self.BGGroup.draw()
        self.LabelGroup.draw()
        self.BtnGroup.draw()

        self.BtnGroup.update()

    def buttonMenuPressed(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.MAINMENU_SCENE)

    def buttonGameOverPressed(self, button):
        Setup.Audio.PlaySound("click")
        self.loadScene(Constant.GAMEPLAY_SCENE)