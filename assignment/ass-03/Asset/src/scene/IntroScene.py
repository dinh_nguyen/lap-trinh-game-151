from .. import SceneController, Setup, Constant
from ..base import Camera, InputManager

class Intro(SceneController.Scene):

    def __init__(self):
        super(Intro, self).__init__()

    def start(self):
        super(Intro, self).start()

        Setup.Audio.PlayMusic("intro")

        self.camera = Camera.Camera()

        self.background = Setup.Resource.load('BG', 'BG_Intro')

        self.finishInitialize()

    def update(self):
        Setup.Window.blit(self.background, (0, 0))

        if Setup.Input.keyDown != None or Setup.Input.mouseClick == InputManager.Input.ONMOUSEDOWN:
            self.loadScene(Constant.MAINMENU_SCENE)