import os, pygame

SCREEN_HEIGHT = 600
SCREEN_WIDTH = 800

SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)

APP_NAME = "Assignment3 - FRIENDS - Find Treasure"

FPS = 60

# RESOURCE
MUSIC_PATH = os.path.join("Asset", "Music")
SOUND_PATH = os.path.join("Asset", "Sound")
FONT_PATH  = os.path.join("Asset", "Font" )
LISTOFTEXTURE = [(None                                          , os.path.join("Asset", "Texture", "BG_Map.png")),
                 (os.path.join("Asset", "Texture", "Player.txt"), os.path.join("Asset", "Texture", "Player.png")),
                 (os.path.join("Asset", "Texture", "UI.txt")    , os.path.join("Asset", "Texture", "UI.png")    ),
                 (os.path.join("Asset", "Texture", "BG.txt")    , os.path.join("Asset", "Texture", "BG.png")    ),
                 (os.path.join("Asset", "Texture", "Enemy.txt") , os.path.join("Asset", "Texture", "Enemy.png") ),
                ]
TILEMAP_PATH = os.path.join("Asset", "TileMap", "Map_Level_1.tmx")

# KEY
keybinding = {
    'w'     : pygame.K_w,
    'a'     : pygame.K_a,
    's'     : pygame.K_s,
    'd'     : pygame.K_d,
    'left'  : pygame.K_LEFT,
    'right' : pygame.K_RIGHT,
    'up'    : pygame.K_UP,
    'down'  : pygame.K_DOWN,
    'space' : pygame.K_SPACE
}

# SCENE FOR ENTIRE GAME
INTRO_SCENE      = "intro_scene"
MAINMENU_SCENE   = "mainmenu_scene"
GAMEPLAY_SCENE   = 'gameplay_scene'
OPTION_SCENE     = 'option_scene'
ABOUT_SCENE      = 'about_scene'
CONFIG_SCENE     = 'config_scene'
GAMEOVER_SCENE   = 'gameover_scene'

# PLAYER STATE
STAND = 'standing'
WALK = 'walk'
JUMP = 'jump'
FALL = 'fall'
DEAD = 'dead'
LEVEL1TO2 = "level1to2"
LEVEL2TO3 = "level2to3"
LEVEL3TO2 = "level3to2"
LEVEL2TO1 = "level2to1"

# PLAYER FIRE INFO
LEFT = 'left'
RIGHT = 'right'
PLAYERFIREBALL_XSTARTVEL = 12
PLAYERFIREBALL_MULTIFIER_BYLEVEL = (1, 1.1, 1.2)
PLAYERFIREBALL_FILLCOLOR_BYLEVEL = ((255, 255, 255, 255), (0, 0, 255, 255), (255, 0, 0, 255))
PLAYER_FIRE_ANIMATION_TIME = 0.2

# PLAYER INFO
WALK_ACCEL = .15
TURNAROUND_ACCEL = .35

GRAVITY = 1.01
JUMP_GRAVITY = .31
JUMP_VEL = -10
FAST_JUMP_VEL = -12.5
MAX_Y_VEL = 11

MAX_WALK_SPEED = 6

# ENEMY STATE
JUMPON = 'jumpon'

# ITEM INFO
MONEYREWARD = 100

# Tag
UNTAGGED = "untagged"
PLAYER = "player"
LAND = "land"
ENEMY = "enemy"
PLAYERFIREBALl = 'playerfireball'
ENEMYFIREBALL = 'enemyfireball'
ITEM = 'item'
DOOR = 'door'



