import pygame, os, sys
import Setup
from pygame.locals import *

class Control(object):
    def __init__(self, sceneDict):
        self.scene_dict = sceneDict
        self.scene_name = self.scene_dict.items()[0][0]
        self.scene = self.scene_dict[self.scene_name]
        self.scene.start()

    def update(self):
        if self.scene.nextScene != None:
            self.change_scene()
        self.scene.update()

    def change_scene(self):
        self.scene_name = self.scene.nextScene
        self.scene.clear()
        self.scene = self.scene_dict[self.scene_name]
        self.scene.start()

    def checkForQuit(self):
        for event in pygame.event.get(QUIT):  # get all the QUIT events
            self.terminate()  # terminate if any QUIT events are present
        for event in pygame.event.get(KEYUP):  # get all the KEYUP events
            if event.key == K_ESCAPE:
                self.terminate()  # terminate if the KEYUP event was for the Esc key
            pygame.event.post(event)  # put the other KEYUP event objects back

    def terminate(self):
        pygame.quit()
        sys.exit()

    def run(self):

        while True:
            # Check for Quit Event
            self.checkForQuit()

            # Update input
            Setup.Input.Update(pygame.event.get())

            # Update Scene
            self.update()

            # Update Pyame
            pygame.display.update()

            # Update Time
            Setup.Time.tick()


class Scene(object):
    def __init__(self):
        self.nextScene = None
        self.camera = None

    def finishInitialize(self):
        assert self.camera != None

        self.initialCamera = self.camera.clone()

        Setup.Window.setScene(self)

    def start(self):
        pass

    def clear(self):
        self.nextScene = None
        self.resetCamera()

    def resetCamera(self):
        self.camera = self.initialCamera

    def update(self):
        self.camera.update()

    def loadScene(self, name):
        self.nextScene = name










