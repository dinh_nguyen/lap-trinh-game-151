import pygame, os
import Constant, GameInfo
from .base import ResourceManager, AudioManager, TimeManager, InputManager, WindowManager

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

Window = WindowManager.Window()
Resource = ResourceManager.Resource(Constant.LISTOFTEXTURE)
Audio = AudioManager.Audio(Constant.MUSIC_PATH, Constant.SOUND_PATH)
Input = InputManager.Input()
Time = TimeManager.Time(Constant.FPS, pygame.time.Clock())
Info = GameInfo.GameInfo()