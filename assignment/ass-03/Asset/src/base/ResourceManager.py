import os, pygame, sys
from .. import Constant
from utils import *
from pygame.locals import *

class Resource(object):

    def __init__(self, listOfTexture):
        self.listTexture = {}

        for description, imageName in listOfTexture:
            listRect = util.ReadTextureDescription(description)
            name =  os.path.splitext(os.path.basename(imageName))[0]
            image = pygame.image.load(imageName)

            if listRect == None:
                if not (name in self.listTexture):
                    self.listTexture[name] = {}
                self.listTexture[name][name] = image
            else:
                self.listTexture[name] = util.ReadSpriteSheet(image, listRect)

    def load(self, sourceName, imageName = None):
        if imageName == None:
            imageName = sourceName

        try:
            return self.listTexture[sourceName][imageName]
        except KeyError:
            return None