class Time(object):

    def __init__(self, fps, clock):
        self.FPS = fps
        self.clock = clock

        self.deltaTime = 0
        self.unscaleDeltaTime = 0
        self.timeScale = 1

    def tick(self):
        self.unscaleDeltaTime = self.clock.tick(self.FPS) / 1000.0
        self.deltaTime = self.unscaleDeltaTime * self.timeScale