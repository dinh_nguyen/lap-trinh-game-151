import pygame, sys
from pygame.locals import *

class Input(object):
    ONMOUSEDOWN = 1
    ONMOUSEUP = 2
    MOUSENORMAL = 0

    def __init__(self):
        self.mouses = (0, 0)
        self.mouseClick = Input.MOUSENORMAL
        self.key = None

    def Update(self, event):
        self.keyDown = None
        self.keyUp = None
        self.keys = pygame.key.get_pressed()
        self.mouseClick = Input.MOUSENORMAL
        self.mouses = pygame.mouse.get_pos()

        for ev in event:
            if ev.type == pygame.KEYDOWN:
                self.keyDown = ev.key
            elif ev.type == pygame.KEYUP:
                self.keyUp = ev.key

            if ev.type == pygame.MOUSEBUTTONDOWN:
                self.mouseClick = Input.ONMOUSEDOWN
            elif ev.type == pygame.MOUSEBUTTONUP:
                self.mouseClick = Input.ONMOUSEUP