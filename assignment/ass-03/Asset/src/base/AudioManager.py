import pygame, sys, os
from pygame.locals import *

isMusicOn = True
isSoundEffectOn = True

class Audio(object):

    def __init__(self, musicPath, soundPath):
        pygame.mixer.quit()
        pygame.mixer.init(22050, -16, 1, 1024)

        self.musicVolume = 1.0
        self.soundVolume = 1.0
        self.listMusic = {}
        self.listSound = {}
        self.LoadMusic(musicPath)
        self.LoadSound(soundPath)

    def LoadMusic(self, musicPath):
        for f in os.listdir(musicPath):
            self.listMusic[os.path.splitext(f)[0]] = os.path.join(musicPath, f)

    def LoadSound(self, soundPath):
        for f in os.listdir(soundPath):
            self.listSound[os.path.splitext(f)[0]] = pygame.mixer.Sound(os.path.join(soundPath, f))

    def SetMusicVolume(self, volume):
        self.musicVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.music.set_volume(self.musicVolume)


    def SetSoundVolume(self, volume):
        self.soundVolume = Util.Clamp(0.0, volume, 1.0)
        pygame.mixer.Sound.set_volume(self.soundVolume)

    def PlayMusic(self, name):
        if (isMusicOn):
            self.StopMusic()
            path = self.listMusic[name]
            pygame.mixer.music.load(path)
            pygame.mixer.music.set_volume(self.musicVolume)
            pygame.mixer.music.play(-1)

    def StopMusic(self):
        pygame.mixer.music.stop()

    def RestartMusic(self):
        pygame.mixer.music.rewind()

    def PlaySound(self, name):
        if (isSoundEffectOn):
            sound = self.listSound[name]
            sound.set_volume(self.soundVolume)
            sound.play(0)

    def PauseMusic(self):
        pygame.mixer.music.pause()

    def ResumeMusic(self):
        pygame.mixer.music.unpause()