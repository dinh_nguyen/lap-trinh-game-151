import pygame, sys
from pygame.locals import *

class util(object):
    @staticmethod
    def ReadTextureDescription(directory):
        if directory == None:
            return None
        else:
            result = {}
            with open(directory) as f:
                for line in f:
                    list = line.split()
                    rect_attr = [int(i) for i in list[1:]]
                    rect = pygame.Rect(rect_attr[0], rect_attr[1], rect_attr[2], rect_attr[3])
                    result[list[0]] = rect
            return result

    @staticmethod
    def ReadSpriteSheet(image, description):
        results = {}
        for (name, rect) in description.iteritems():
            sprite = pygame.Surface(rect.size, pygame.SRCALPHA, 32)
            sprite.blit(image, (0, 0), rect)
            results[name] = sprite
        return results

    @staticmethod
    def Clamp(minimum, maximum, value):
        return max(minimum, min(value, maximum))

    @staticmethod
    def pingpong(value, maximum):
        l = 2 * maximum
        t = t % l
        return l - t
