import pygame
from .. import Constant

class Window(object):

    def __init__(self):
        self.camera = None

        self.displaySurface = pygame.display.set_mode(Constant.SCREEN_SIZE)
        pygame.display.set_caption(Constant.APP_NAME)

    def setScene(self, scene):
        self.camera = scene.camera

    def blit(self, source, dest, tranformToLocal=False, area=None, special_flag=0):
        if not tranformToLocal:
            return (self.displaySurface.blit(source, dest, area, special_flag), True)
        else:
            destination = None
            if isinstance(dest, pygame.Rect):
                destination = self.camera.transformRect(dest)
            elif isinstance(dest, (tuple, list)):
                destination = pygame.Rect(self.camera.transformPoint(dest), dest.get_size())

            assert destination != None

            visible = True if pygame.Rect((0, 0), self.camera.rect.size).colliderect(destination) else False

            if visible:
                return (self.displaySurface.blit(source, destination, area, special_flag), visible)

            return (None, visible)
