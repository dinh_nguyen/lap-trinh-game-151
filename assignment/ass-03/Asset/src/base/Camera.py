import pygame
from .. import Constant

class Camera(object):

    def __init__(self):
        self.width, self.height = Constant.SCREEN_SIZE
        self.rect = pygame.Rect(0, 0, self.width, self.height)

    def clone(self):
        camera = Camera()
        camera.rect = self.rect.copy()
        camera.width, camera.height = self.width, self.height
        return camera

    def update(self):
        pass

    def transformRect(self, rect):
        return pygame.Rect((rect.left - self.rect.left, rect.top - self.rect.top), rect.size)

    def transformPoint(self, point):
        return (point[0] - self.rect.left, point[1] - self.rect.top)

    def transformInversePoint(self, point):
        return (point[0] + self.rect.left, point[1] + self.rect.top)