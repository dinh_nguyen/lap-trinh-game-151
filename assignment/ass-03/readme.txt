Người dùng sử dụng các phím :
- Mũi tên lên/xuống/trái/phải hoặc w/s/a/d tương ứng để di chuyển
- Ấn phím space để bắn
Ngoài ra:
- Giữ phím lên hoặc w lâu sẽ nhảy cao hơn
- Giữ phím trái phải hoặc a/s lâu thì di chuyển càng nhanh
- Ăn các chìa khóa để mở rương, nếu số chìa khóa = 0 thì 0 thể mở rương.
- Tiêu diệt quái bằng cách bắn đạn hoặc nhảy lên đầu chúng.