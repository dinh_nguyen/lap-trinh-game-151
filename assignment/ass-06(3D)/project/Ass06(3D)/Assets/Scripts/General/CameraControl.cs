﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{

    #region Inspector Parameters

    public float PanSpeed = 5;
    public float ZoomSpeed = 5;

    public int MousePanningZoneWidth = 10;

    public bool EnableMouseZoom = true;
    public bool EnableMouseRotate = true;
    public bool EnableMousePanning = true;
    public bool EnableKeyPanning = true;

    public bool AvoidClipping = false;

    public float MinPosX = -10;
    public float MaxPosX = 10;

    public float MinPosZ = -10;
    public float MaxPosZ = 10;

    public float MinZoomDistance = 8;
    public float MaxZoomDistance = 30;

    public float MinRotateAngle = 10;
    public float MaxRotateAngle = 89;

    public bool ShowGizmo = true;

    #endregion

    #region Public Parameters

    public static CameraControl Instance;

    [HideInInspector] public Transform CamT;

    #endregion

    #region Private Members

    private Transform thisT;

    private float _initialMousePosX;
    private float _initialMousePosY;

    private float _initialRotX;
    private float _initialRotY;

    private float _currentZoom;

    private float _deltaT;

    private bool _obstacle;

    #if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
    private Vector3 _lastTouchPos;
    private Vector3 _moveDir;
			
	private float _touchZoomSpeed;
    #endif

    #endregion

    #region Constructor and Init

    void Awake()
    {
        Instance = this;

        thisT = transform;

        CamT = Camera.main.transform;

    #if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
        _lastTouchPos = new Vector3(9999, 9999, 9999);
    
        _moveDir = Vector3.zero;
    #endif
    }

    void Start()
    {
        MinRotateAngle = Mathf.Max(10, MinRotateAngle);
        MaxRotateAngle = Mathf.Min(89, MaxRotateAngle);

        MinZoomDistance = Mathf.Max(1, MinZoomDistance);

        _currentZoom = CamT.localPosition.z;
    }

    #endregion

    #region Internal Functionality

    void Update()
    {
        if (Time.timeScale == 1) _deltaT = Time.deltaTime;
        else if (Time.timeScale > 1) _deltaT = Time.deltaTime / Time.timeScale;
        else _deltaT = 1 / 60f;

    #if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
		
    #elif UNITY_EDITOR || !(UNITY_IPHONE && UNITY_ANDROID && UNITY_WP8 && UNITY_BLACKBERRY)
        if(EnableMouseRotate)
        {
            if(Input.GetMouseButtonDown(1)) // Right Mouse 1st Down
            {
                _initialMousePosX = Input.mousePosition.x;
                _initialMousePosY = Input.mousePosition.y;
                _initialRotX = thisT.eulerAngles.y;
                _initialRotY = thisT.eulerAngles.x;
            }

            if(Input.GetMouseButton(1))
            {
                float deltaX = Input.mousePosition.x - _initialMousePosX;
                float deltaRotX = (_initialRotX / Screen.width) / 10f;
                float rotX = deltaX + deltaRotX;

                float deltaY = _initialMousePosY -  Input.mousePosition.y;
                float deltaRotY = -(_initialRotY / Screen.height) - 10f;
                float rotY = deltaY + deltaRotY;
                float y = rotY + _initialRotY;

                if(y > MaxRotateAngle)
                {
                    _initialRotY -= ((rotY + _initialRotY) - MaxRotateAngle);
                    y = MaxRotateAngle;
                }
                else if(y < MinRotateAngle)
                {
                    _initialRotY += (MinRotateAngle - (rotY + _initialRotY));
                    y = MinRotateAngle;
                }

                thisT.rotation = Quaternion.Euler(y, rotX + _initialRotX, 0);
            }
        }

        Quaternion direction = Quaternion.Euler(0, thisT.eulerAngles.y, 0);

        if(EnableKeyPanning)
        {
            if(Input.GetButton("Horizontal"))
            {
                Vector3 dir = transform.InverseTransformDirection(direction * Vector3.right);
                thisT.Translate(dir * PanSpeed * _deltaT * Input.GetAxisRaw("Horizontal"));
            }

            if (Input.GetButton("Vertical"))
            {
                Vector3 dir = transform.InverseTransformDirection(direction * Vector3.forward);
                thisT.Translate(dir * PanSpeed * _deltaT * Input.GetAxisRaw("Vertical"));
            }
        }

        if(EnableMousePanning)
        {
            Vector3 mousePos = Input.mousePosition;
            Vector3 dirHor = transform.InverseTransformDirection(direction * Vector3.right);
            Vector3 dirVer = transform.InverseTransformDirection(direction * Vector3.forward);

            if (mousePos.x <= 0)
                thisT.Translate(dirHor * PanSpeed * _deltaT * -3);
            else if (mousePos.x <= MousePanningZoneWidth)
                thisT.Translate(dirHor * PanSpeed * _deltaT * -1);
            else if (mousePos.x >= Screen.width)
                thisT.Translate(dirHor * PanSpeed * _deltaT *  3);
            else if (mousePos.x >= Screen.width - MousePanningZoneWidth)
                thisT.Translate(dirHor * PanSpeed * _deltaT *  1);

            if (mousePos.y <= 0)
                thisT.Translate(dirVer * PanSpeed * _deltaT * -3);
            else if (mousePos.y <= MousePanningZoneWidth)
                thisT.Translate(dirVer * PanSpeed * _deltaT * -1);
            else if (mousePos.y >= Screen.height)
                thisT.Translate(dirVer * PanSpeed * _deltaT *  3);
            else if (mousePos.y >= Screen.height - MousePanningZoneWidth)
                thisT.Translate(dirVer * PanSpeed * _deltaT *  1);
        }

        if(EnableMouseZoom)
        {
            float zoomInput = Input.GetAxis("Mouse ScrollWheel");
            if(zoomInput != 0)
            {
                _currentZoom += ZoomSpeed * zoomInput;
                _currentZoom = Mathf.Clamp(_currentZoom, -MaxZoomDistance, -MinZoomDistance);
            }
        }
    #endif

        if(AvoidClipping)
        {
            Vector3 aPos = thisT.TransformPoint(new Vector3(0, 0, _currentZoom));
            Vector3 dirC = aPos - thisT.position;

            float dist = Vector3.Distance(aPos, thisT.position);
            
            RaycastHit hitInfo;
            _obstacle = Physics.Raycast(thisT.position, dirC, out hitInfo, dist);
            if(!_obstacle)
            {
                float camZ = Mathf.Lerp(CamT.localPosition.z, _currentZoom, Time.deltaTime * 4);
                CamT.localPosition = new Vector3(CamT.localPosition.x, CamT.localPosition.y, camZ);
            }
            else
            {
                dist = Vector3.Distance(hitInfo.point, thisT.position) * 0.85f;
                float camZ = Mathf.Lerp(CamT.localPosition.z, -dist, Time.deltaTime * 50);
                CamT.localPosition = new Vector3(CamT.localPosition.x, CamT.localPosition.y, camZ);
            }
        }
        else
        {
            float camZ = Mathf.Lerp(CamT.localPosition.z, _currentZoom, Time.deltaTime * 4);
            CamT.localPosition = new Vector3(CamT.localPosition.x, CamT.localPosition.y, camZ);
        }

        float x = Mathf.Clamp(thisT.position.x, MinPosX, MaxPosX);
        float z = Mathf.Clamp(thisT.position.z, MinPosZ, MaxPosZ);

        thisT.position = new Vector3(x, thisT.position.y, z);
    }

    #endregion

    #region Gizmo Draw

    void OnDrawGizmos()
    {
        if(ShowGizmo)
        {
            Vector3 p1 = new Vector3(MinPosX, transform.position.y, MaxPosZ);
            Vector3 p2 = new Vector3(MaxPosX, transform.position.y, MaxPosZ);
            Vector3 p3 = new Vector3(MaxPosX, transform.position.y, MinPosZ);
            Vector3 p4 = new Vector3(MinPosX, transform.position.y, MinPosZ);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(p1, p2);
            Gizmos.DrawLine(p2, p3);
            Gizmos.DrawLine(p3, p4);
            Gizmos.DrawLine(p4, p1);
        }
    }

    #endregion
}
