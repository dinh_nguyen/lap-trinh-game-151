﻿using UnityEngine;
using System.Collections;
using Chida;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : SingletonMonoBehaviour<AudioManager>
{

    #region Inspector Members

    [SerializeField]
    protected SpawnPool m_MusicPool;

    [SerializeField]
    protected GameObject m_AudioObject;

    #endregion

    #region Private Members

    private AudioSource _musicSource;

    #endregion

    #region Constructor and Init

    void AwakeSingleton()
    {
        _musicSource = GetComponent<AudioSource>();
        _musicSource.loop = true;

        DontDestroyOnLoad(gameObject);
    }

    #endregion

    #region AudioManager Functionality

    #region Music
    public void PlayMusic(AudioClip clip)
    {
        if(clip != null && _musicSource.clip != clip)
        {
            _musicSource.clip = clip;
            _musicSource.Play();
        }
    }
    #endregion

    #region Sound

    public void PlaySound2D(AudioClip clip)
    {
        PlaySound3D(clip, Camera.main.transform.position, 1.0f);
    }

    public void PlaySound3D(AudioClip clip, Vector3 position)
    {
        PlaySound3D(clip, position, 1.0f);
    }

    public void PlaySound3D(AudioClip clip, Vector3 position, float pitch)
    {
        if (clip != null)
        {
            Transform obj = m_MusicPool.Spawn(m_AudioObject, position, Quaternion.identity);

            AudioSource src = obj.GetComponent<AudioSource>();
            src.clip = clip;
            src.pitch = pitch;
            src.Play();

            m_MusicPool.Despawn(obj, clip.length);
        }
    }

    #endregion

    #endregion
}
