﻿using UnityEngine;
using System.Collections;
using Chida;

public class LayerManager : SingletonMonoBehaviour<LayerManager> {

    public static int GridLayer; 
    public static int GridMask;  
    public static int TowerLayer;
    public static int TowerMask; 
    public static int WorldLayer; 
    public static int WorldMask;
    public static int EnemyMask; 
    public static int EnemyLayer; 

    void AwakeSingleton()
    {
        EnemyLayer = LayerMask.NameToLayer("Enemies");
        GridLayer = LayerMask.NameToLayer("Grid");
        TowerLayer = LayerMask.NameToLayer("Tower");
        WorldLayer = LayerMask.NameToLayer("WorldLimit");

        GridMask = 1 << LayerMask.NameToLayer("Grid"); 
        TowerMask = 1 << LayerMask.NameToLayer("Tower"); 
        WorldMask = 1 << LayerMask.NameToLayer("WorldLimit"); 
        EnemyMask = 1 << LayerMask.NameToLayer("Enemies");
    }

}
