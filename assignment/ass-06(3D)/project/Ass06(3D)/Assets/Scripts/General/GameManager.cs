﻿using UnityEngine;
using System.Collections;
using Chida;
using UnityEngine.SceneManagement;


namespace Friends_Tower
{

    public class GameManager : MonoBehaviour
    {
        #region Inspector Parameters

        public string NextScene;

        public string GameOverScene;

        public int MaxGameHealth;

        public int StartMoney;

        #endregion

        #region Public Parameters

        public static GameObject Selection;
        public static GameObject GridSelection;

        public static Vector3 outOfView = new Vector3(0, -300, 0);

        public static string WaveCount = "1";

        public static float GameHealth = 1f;
        public static float MaxHealth = 1f;

        public static int EnemiesAlive;
        public static int EnemiesKill;
        public static int Wave = 0;
        public static float Money = 0;

        public static bool ShowUpgrade;
        public static bool ShowExit;
        public static bool GameOver = false;

        #endregion

        #region Private Members

        #endregion

        #region Constructor && Init

        void Awake()
        {
            GameHealth = MaxHealth = MaxGameHealth;
            Money = StartMoney;
            EnemiesAlive = EnemiesKill = 0;
            Wave = 0;
            WaveCount = "";
        }

        void Start()
        {
            InvokeRepeating("CheckGameState", 1f, 1f);
        }

        void CheckGameState()
        {
            if(GameHealth <= 0 && !GameOver)
            {
                Debug.Log("You Lose");

                GameHealth = 0;
                GameOver = true;
            }
            else if (GameHealth > 0 && GameOver)
            {
                Debug.Log("You Won!!");

                GameOver = true;

                StartCoroutine(IELoadGameOverScene());
            }
        }

        #endregion

        #region Functionality

        public static void AddMoney(int amount)
        {
            Money += amount;
        }

        public static void AddEnemy(int amount)
        {
            EnemiesAlive += amount;
        }

        public static void AddHealth(int amount)
        {
            GameHealth += amount;
            if (GameHealth > MaxHealth)
                GameHealth = MaxHealth;
        }

        public static void DealDamage(float dmg)
        {
            if (GameOver)
                return;

            GameHealth -= dmg;
            EnemiesAlive--;
        }

        public static void EnemyWasKilled()
        {
            EnemiesAlive--;
            EnemiesKill++;
        }

        public static void IncreaseWave()
        {
            Wave++;
        }
        

        #endregion

        #region Load Scene Functionality

        IEnumerator IELoadGameOverScene()
        {
            CancelInvoke("CheckGameState");

            if (GameOver || string.IsNullOrEmpty(NextScene))
                NextScene = GameOverScene;

            if (NextScene.Equals(GameOverScene))
            {
                yield return new WaitForSeconds(5);

                transform.parent = null;
            }

            SceneManager.LoadScene(NextScene);
        }

        #endregion
    }
}