﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Setting {

    public SettingValue Type;

    public float Value;

    public bool Enable;
}
