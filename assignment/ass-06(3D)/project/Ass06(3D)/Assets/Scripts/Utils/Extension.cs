﻿using UnityEngine;
using System.Collections;

public static class StringExtension  {

    public static string DeCamel(this string s)
    {
        if (string.IsNullOrEmpty(s)) return string.Empty;

        System.Text.StringBuilder newStr = new System.Text.StringBuilder();

        char c;
        for (int i = 0; i < s.Length; i++)
        {
            c = s[i];

            // Handle spaces and underscores. 
            //   Do not keep underscores
            //   Only keep spaces if there is a lower case letter next, and 
            //       capitalize the letter
            if (c == ' ' || c == '_')
            {
                // Only check the next character is there IS a next character
                if (i < s.Length - 1 && char.IsLower(s[i + 1]))
                {
                    // If it isn't the first character, add a space before it
                    if (newStr.Length != 0)
                    {
                        newStr.Append(' ');  // Add the space
                        newStr.Append(char.ToUpper(s[i + 1]));
                    }
                    else
                    {
                        newStr.Append(s[i + 1]);  // Stripped if first char in string
                    }

                    i++;  // Skip the next. We already used it
                }
            }  // Handle uppercase letters
            else if (char.IsUpper(c))
            {
                // If it isn't the first character, add a space before it
                if (newStr.Length != 0)
                {
                    newStr.Append(' ');
                    newStr.Append(c);
                }
                else
                {
                    newStr.Append(c);
                }
            }
            else  // Normal character. Store and move on.
            {
                newStr.Append(c);
            }
        }

        return newStr.ToString();
    }
}

public static class FloatExtensions
{
	public const float TOLERANCE = .1f;
    public static bool NearlyEqual(this float a, float b)
	{
            return Mathf.Abs(a - b) < TOLERANCE;
        
	}
}