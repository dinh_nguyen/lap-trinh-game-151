﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour
{

    #region Inspector Parameters

    public GameObject GridPrefab;

    public Material GridFreeMat;
    public Material GridFullMat;

    public int GridSize = 8;
    public float offsetX = 0f;
    public float offsetY = 0f;

    public int Width = 2;
    public int Height = 2;

    public float GridHeight = 1f;

    public List<string> GridList = new List<string>();

    #endregion

    #region Private Properties

    private bool _gridVisible = true;

    #endregion

    #region Constructor and Init

    void Start()
    {
        ToggleVisibility(false);
    }

    #endregion
    
    #region Utility Functions

    public void ToggleVisibility(bool visible)
    {
        if (_gridVisible == visible) return;
        _gridVisible = visible;

        foreach (Transform trans in transform)
        {
            trans.GetComponent<Renderer>().enabled = _gridVisible;
        }
    }

    #endregion


}
