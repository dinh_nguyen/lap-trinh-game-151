﻿using UnityEngine;
using System.Collections;

namespace Friends_Tower
{
    public class Projectile : MonoBehaviour
    {

        #region Inspector Parameters

        public bool FollowEnemy = true;

        public FireMode FlyMode = FireMode.Straight;

        public float LobHeight = 0f;

        public float Speed = 0f;

        public bool ContinuousCollision = false;

        public AudioClip ImpaceSound;

        public bool SingleDamage = true;

        public float TimeToLast = 0f;

        //Effect

        #endregion

        #region Public Parameters

        [HideInInspector]
        public Transform Target;

        [HideInInspector]
        public Vector3 StartPos = Vector3.zero;

        [HideInInspector]
        public Vector3 EndPos = Vector3.zero;

        [HideInInspector]
        public float Damage = 0f;

        #endregion

        #region Private
        private Vector3 _previousPosition;

        private Rigidbody _rigidbody;

        private float _minimumBounds;

        private float _sqrMinimumBounds;

        private float _time = 0f;

        private bool _executed;

        private bool _cacheFollowEnemy;

        private float _distance;

        #endregion

        #region Constructor and Init

        void OnSpawned()
        {
            if(_rigidbody == null)
            {
                _rigidbody = GetComponent<Rigidbody>();
                _minimumBounds = GetComponent<Collider>().bounds.size.z;
                _sqrMinimumBounds = _minimumBounds * _minimumBounds;
            }

            Initialize();
        }

        void OnDespawned()
        {
            Target = null;
            FollowEnemy = _cacheFollowEnemy;
        }

        void Initialize()
        {
            _executed = false;

            StartPos = transform.position;

            _previousPosition = _rigidbody.position;

            _time = 0f;

            _cacheFollowEnemy = FollowEnemy;

            if (Target && !FollowEnemy)
                EndPos = Target.position;

            transform.LookAt(EndPos);

            switch (FlyMode)
            {
                case FireMode.Straight:
                    StartCoroutine(IEMoveStraight());
                    break;

                case FireMode.Lob:
                    StartCoroutine(IEMoveLob());
                    break;
            }
        }

        #endregion

        #region Movement

        IEnumerator IEMoveStraight()
        {
            while (!_executed)
            {
                if (FollowEnemy && Target != null &&
                Target.gameObject.activeInHierarchy)
                {
                    EndPos = Target.position;
                    transform.LookAt(EndPos);

                    transform.Translate(Vector3.forward * Speed * Time.deltaTime);

                    if (ContinuousCollision)
                    {
                        if (CheckContinuousCollision())
                            yield break;
                    }
                    else
                    {
                        _distance = Vector3.Distance(StartPos, EndPos);
                        if (Vector3.Distance(transform.position, StartPos) > _distance)
                        {
                            StartCoroutine(IEHandleImpact());
                        }
                    }
                }
                else if (Target != null &&
                !Target.gameObject.activeInHierarchy)
                {
                    StartCoroutine(IEHandleImpact());
                }

                yield return null;
            }
        }

        IEnumerator IEMoveLob()
        {
            yield return null;
        }

        bool CheckContinuousCollision()
        {
            Vector3 movementThisStep = _rigidbody.position - _previousPosition;
            float movementSqrMagnitude = movementThisStep.sqrMagnitude;

            if (movementSqrMagnitude > _sqrMinimumBounds)
            {
                float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
                RaycastHit[] hitInfo = Physics.RaycastAll(_previousPosition, movementThisStep, movementMagnitude, LayerManager.EnemyMask);

                for (int i = 0; i < hitInfo.Length; i++)
                    StartCoroutine(OnTriggerEnter(hitInfo[i].collider));

                _previousPosition = _rigidbody.position;

                if (hitInfo.Length > 0)
                    return true;
            }

            _previousPosition = _rigidbody.position;
            return false;
        }

        IEnumerator OnTriggerEnter(Collider other)
        {
            if (_executed)
                yield break;

            GameObject otherObj = other.gameObject;

            if (otherObj.layer == LayerManager.EnemyLayer)
            {
                if (Target == null)
                    Target = otherObj.transform;
                else if (FollowEnemy && Target != other.transform)
                    yield break;

                if (SingleDamage)
                {
                    CreepProperty prop = otherObj.GetComponent<CreepProperty>();
                    prop.Hit(Damage);
                }
            }
            else if (otherObj.layer != LayerManager.WorldLayer)
            {
                yield break;
            }

            yield return StartCoroutine(IEHandleImpact());
        }

        IEnumerator IEHandleImpact()
        {
            _executed = true;

#warning Play Sound

#warning Effect

            if (TimeToLast > 0)
            {
                float timer = Time.time + TimeToLast;
                while (Time.time < timer)
                {
                    yield return null;
                }
            }

            MyPool.Instance.DespawnProjectile(gameObject);
        }

        #endregion

        #region Effect

        #endregion
    }
}