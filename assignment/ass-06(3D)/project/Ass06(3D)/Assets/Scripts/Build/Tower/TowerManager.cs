﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Chida;

namespace Friends_Tower
{
    public class TowerManager : SingletonMonoBehaviour<TowerManager>
    {

        #region Inspector Parameters

        public List<string> TowerNames = new List<string>();

        public List<GameObject> TowerPrefabs = new List<GameObject>();

        public List<TowerBase> TowerBaseList = new List<TowerBase>();

        public List<Upgrade> TowerUpgradeList = new List<Upgrade>();

        public int SellLoss;

        #endregion

        #region Constructor and Init

        void Start()
        {
            for (int i = 0; i < TowerNames.Count; i++)
            {
                if (TowerPrefabs[i] == null)
                    return;

                GameObject tower = Instantiate(TowerPrefabs[i], new Vector3(-1000, -1000, -1000), Quaternion.identity) as GameObject;

                tower.name = TowerNames[i];
                tower.transform.parent = transform;

                TowerBase towerBase = tower.GetComponentInChildren<TowerBase>();
                TowerBaseList.Add(towerBase);

                Upgrade upgrade = tower.GetComponentInChildren<Upgrade>();
                TowerUpgradeList.Add(upgrade);
                towerBase.Upgrade = upgrade;

                tower.SetActive(false);
            }
        }

        #endregion

    }
}