﻿using UnityEngine;
using System.Collections;

namespace Friends_Tower
{
    public class RangeTrigger : MonoBehaviour
    {

        #region Inspector Parameters

        public TowerBase towerBase;

        #endregion

        #region Collision Functionality

        void OnTriggerEnter(Collider other)
        {
            GameObject colGO = other.gameObject;

            if (colGO.layer != LayerManager.EnemyLayer ||
                !colGO.activeInHierarchy ||
                colGO.GetComponent<CreepProperty>().Health <= 0)
                return;

            if (towerBase.MyTargetType == EnemyType.Both ||
                colGO.CompareTag(towerBase.tag))
            {
                towerBase.InRangeList.Add(colGO);
                colGO.SendMessage("AddTower", towerBase);
            }

            if (towerBase.InRangeList.Count == 1)
            {
                towerBase.StartInvoke(0f);
            }
        }

        void OnTriggerExit(Collider other)
        {
            GameObject colGO = other.gameObject;

            if (towerBase.InRangeList.Contains(colGO))
            {
                towerBase.InRangeList.Remove(colGO);

                colGO.SendMessage("RemoveTower", towerBase);
            }
        }

        #endregion
    }
}