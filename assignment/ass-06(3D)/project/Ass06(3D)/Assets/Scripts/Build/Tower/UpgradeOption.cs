﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UpgradeOption
{

    #region Public Parameters

    public float Cost;

    #region After Upgrade

    public float radius = 5;

    public float Damage = 1;

    public float ShootDelay = 3;

    public int TargetCount = 1;

    public GameObject SwapModel;

    #endregion

    #endregion

}
