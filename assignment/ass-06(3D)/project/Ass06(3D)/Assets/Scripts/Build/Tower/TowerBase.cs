﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Friends_Tower
{
    public class TowerBase : MonoBehaviour
    {

        #region Inspector Parameters

        public GameObject Projectile;

        public Transform ShotPos;

        public AudioClip ShotSound;

        public GameObject ShotFx;

        public GameObject RangeIndicator;

        public Transform Turret;

        public float ShotAngle = 0f;

        public EnemyType MyTargetType = EnemyType.Both;

        public ShootOrder shootOrder = ShootOrder.FirstIn;

        public float ControlMultiplier = 1f;

        public float ProjectSpacing = 3;

        #endregion

        #region Public Parameters

        [HideInInspector]
        public List<GameObject> InRangeList = new List<GameObject>();

        [HideInInspector]
        public float LastShot = 0f;

        [HideInInspector]
        public Transform CurrentTarget;

        [HideInInspector]
        public Upgrade Upgrade;

        #endregion

        #region Private Members

        private Animator _animator;

        #endregion

        #region Constructor and Init

        void Awake()
        {
            this.enabled = false;

            _animator = GetComponent<Animator>();
        }

        void Start()
        {
            Initialize();
        }

        void Initialize()
        {
            ShotPos.localRotation = Quaternion.identity;

            if (Turret != null)
            {
                if (ShotAngle == 0)
                    Debug.LogError(gameObject.name + "Had zero shot angle");

                TowerTurretRotation turrentRot = Turret.gameObject.GetComponent<TowerTurretRotation>();

                if(turrentRot != null)
                    turrentRot.TowerBaseScript = this;
            }

            StartInvoke(0f);
        }

        #endregion

        #region RangeCheck && Attack

        public void StartInvoke(float delay)
        {
            if (this.enabled && !IsInvoking("CheckRange"))
            {
                InvokeRepeating("CheckRange", delay + 0.1f, Upgrade.Options[Upgrade.CurrentLevel].ShootDelay);
            }
        }

        void CheckRange()
        {
            if (InRangeList.Count == 0)
            {
                CancelInvoke("CheckRange");

                CurrentTarget = null;

                return;
            }

            AutoAttack();
        }

        void AutoAttack()
        {
            LastShot = Time.time;

            List<Transform> targets = GetTargets();

            if (targets.Count == 0)
            {
                CancelInvoke("CheckRange");
                InvokeRepeating("CheckRange", 0.5f, Upgrade.Options[Upgrade.CurrentLevel].ShootDelay);
                return;
            }

            for (int i = 0; i < targets.Count; i++)
                InstantiateProjectile(targets[i]);

            ShotEffect();
        }

        List<Transform> GetTargets()
        {
            int targetCount = Upgrade.Options[Upgrade.CurrentLevel].TargetCount;

            List<Transform> targets = new List<Transform>();

            if (InRangeList.Count > 0)
            {
                switch (shootOrder)
                {
                    case ShootOrder.FirstIn:
                        CurrentTarget = InRangeList[0].transform;

                        for (int i = 0; i < InRangeList.Count; i++)
                        {
                            if (targets.Count == targetCount)
                                break;

                            Transform enemy = InRangeList[0].transform;

                            if (Turret == null || (Turret != null && CheckAngle(enemy)))
                                targets.Add(enemy);
                        }
                        break;
#warning Need More Code
                    default:
                        break;
                }
            }

            return targets;
        }

        void InstantiateProjectile(Transform target)
        {
            GameObject ProjectileObj = MyPool.Instance.SpawnProjectile(Projectile, ShotPos.position, ShotPos.rotation);

            Projectile proj = ProjectileObj.GetComponent<Projectile>();

            proj.Damage = Upgrade.Options[Upgrade.CurrentLevel].Damage;

            proj.Target = target;
        }

        void ShotEffect()
        {
            _animator.SetTrigger("Shot");

            if(ShotFx != null)
            {
                Quaternion rot = Quaternion.identity;
                if (Turret != null)
                    rot = Turret.rotation;
                GameObject fx = MyPool.Instance.SpawnProjectile(ShotFx, ShotPos.position, rot);
                fx.transform.parent = ShotPos;
            }
        }

        bool CheckAngle(Transform target)
        {
            Vector2 targetVec = new Vector2(target.position.x, target.position.z);
            Vector2 shootVec = new Vector2(ShotPos.position.x, ShotPos.position.z);

            Vector2 targetDir = targetVec - shootVec;
            Vector2 forward = new Vector2(ShotPos.forward.x, ShotPos.forward.y);
            float angle = Vector2.Angle(forward, targetDir);

            if (angle <= ShotAngle / 2)
                return true;
            else
                return false;
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;

            if (!this.enabled)
                return;

            if(InRangeList.Count > 0)
            {
                foreach (GameObject enemy in InRangeList)
                    Gizmos.DrawLine(ShotPos.position, enemy.transform.position);
            }

            if (Turret == null)
                return;

            Gizmos.color = Color.green;

            float currentRadius = Upgrade.Options[Upgrade.CurrentLevel].radius;
            float halfShootAngle = ShotAngle / 2;

            Vector3 forwarddir = Turret.TransformDirection(Vector3.forward) * currentRadius;
            Gizmos.DrawRay(Turret.position, forwarddir);

            Quaternion locRight = Quaternion.AngleAxis(halfShootAngle, Vector3.up);
            Vector3 rightDir = Turret.TransformDirection(locRight * Vector3.forward) * currentRadius;
            Gizmos.DrawRay(Turret.position, rightDir);

            Quaternion locLeft = Quaternion.AngleAxis(halfShootAngle, Vector3.down);
            Vector3 leftDir = Turret.TransformDirection(locLeft * Vector3.forward) * currentRadius;
            Gizmos.DrawRay(Turret.position, leftDir);
        }

        void OnMouseEnter()
        {
            if (GameManager.Selection == null && !GameManager.ShowUpgrade && 
                !GameManager.ShowExit)
                RangeIndicator.GetComponent<Renderer>().enabled = true;
        }

        void OnMouseExit()
        {
            if (GameManager.Selection == null && !GameManager.ShowUpgrade 
                && !GameManager.ShowExit)
                RangeIndicator.GetComponent<Renderer>().enabled = false;
        } 

        #endregion
    }
}