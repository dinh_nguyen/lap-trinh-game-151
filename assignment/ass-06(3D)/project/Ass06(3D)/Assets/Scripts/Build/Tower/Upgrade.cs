﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Friends_Tower
{
    public class Upgrade : MonoBehaviour
    {

        #region Inspector Parameters

        public List<UpgradeOption> Options = new List<UpgradeOption>();

        #endregion

        #region Public Parameters

        [HideInInspector]
        public int CurrentLevel = 0;

        #endregion

        #region Private Members

        private Transform _towerParent;
        private SphereCollider _sphereCollider; // For Range Indicator
        private Transform _rangeIndicator;
        private TowerBase _towerBaseScripts;

        #endregion

        #region Constructor and Init

        void Start()
        {
            Initalize();
        }

        void Initalize()
        {
            if (Options.Count == 0)
            {
                Debug.LogError(gameObject.name + "has no Upgrade");
                return;
            }

            _towerParent = GameObject.FindWithTag(UnityConstants.Tags.TowerPool).transform;
            _towerBaseScripts = GetComponent<TowerBase>();
            _sphereCollider = transform.parent.FindChild("RangeTrigger").gameObject.GetComponent<SphereCollider>();
            _rangeIndicator = transform.parent.FindChild("RangeIndicator");

            if (_rangeIndicator != null)
                RangeChange();
            else
                _sphereCollider.radius = Options[CurrentLevel].radius;
        }

        void RangeChange()
        {
            if (_sphereCollider == null)
            {
                Initalize();
                return;
            }

            float radius = Options[CurrentLevel].radius;

            _sphereCollider.radius = radius;

            Vector3 currentSize = _rangeIndicator.GetComponent<Renderer>().bounds.size;

            Vector3 scale = _rangeIndicator.transform.localScale;
            scale.z = radius * 2 * scale.z / currentSize.z;
            scale.x = radius * 2 * scale.x / currentSize.x;

            _rangeIndicator.transform.localScale = scale;

        }

        void TowerChange()
        {
            GameObject newTower = MyPool.Instance.SpawnTower(Options[CurrentLevel].SwapModel, transform.position, transform.rotation, _towerParent);

            TowerBase newTowerBaseScript = newTower.GetComponentInChildren<TowerBase>();

            GameObject newTowerBase = newTowerBaseScript.gameObject;
            newTower.name = newTowerBase.name = transform.name;

            Upgrade newUpgrade = newTowerBase.AddComponent<Upgrade>();
            newUpgrade.CurrentLevel = CurrentLevel;
            newUpgrade.Options = Options;

            newTowerBaseScript.Upgrade = newUpgrade;

            newTowerBaseScript.enabled = true;

            newTowerBaseScript.CancelInvoke("CheckRange");

            float LastShot = _towerBaseScripts.LastShot;
            float InvokeInSecond = Options[CurrentLevel].ShootDelay + LastShot - Time.time;
            if (InvokeInSecond < 0)
                InvokeInSecond = 0;

            _towerBaseScripts.StartInvoke(InvokeInSecond);

            //Update GUI

            foreach (GameObject enemy in _towerBaseScripts.InRangeList)
            {
                enemy.GetComponent<CreepProperty>().RemoveTower(_towerBaseScripts);
            }

            if (newTowerBaseScript.Turret != null)
            {
                if (_towerBaseScripts.Turret != null)
                    newTowerBaseScript.Turret.rotation = _towerBaseScripts.Turret.rotation;
                else
                {
                    Ray ray = new Ray(transform.position + new Vector3(0, 0.5f, 0), -transform.up);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo, 20, LayerManager.GridMask))
                    {
                        Transform grid = hitInfo.transform;
                        newTowerBaseScript.Turret.rotation = grid.rotation;
                    }
                }

                newTowerBaseScript.ShotPos.localRotation = Quaternion.identity;

                newTowerBaseScript.Turret.GetComponent<TowerTurretRotation>().enabled = true;
            }

            MyPool.Instance.DespawnTower(transform.parent.gameObject);
        }

        #endregion

        #region Upgrade Functions

        public void UpgradeLevel()
        {
            CurrentLevel++;

            if (Options[CurrentLevel].SwapModel != null)
            {
                TowerChange();
                return;
            }

            RangeChange();

            _towerBaseScripts.CancelInvoke("CheckRange");

            float LastShot = _towerBaseScripts.LastShot;
            float InvokeInSecond = Options[CurrentLevel].ShootDelay + LastShot - Time.time;
            if (InvokeInSecond < 0)
                InvokeInSecond = 0;

            _towerBaseScripts.StartInvoke(InvokeInSecond);
        }

        #endregion
    }
}