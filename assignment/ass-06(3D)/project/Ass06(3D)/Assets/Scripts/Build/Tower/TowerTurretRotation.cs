﻿using UnityEngine;
using System.Collections;

namespace Friends_Tower
{
    public class TowerTurretRotation : MonoBehaviour
    {

        #region Inspector Parameters

        public float Damping = 0f;

        #endregion

        #region Public Parameters

        [HideInInspector]
        public TowerBase TowerBaseScript;

        #endregion

        #region Constructor and Init

        void OnSpawned()
        {
            Initialize();
        }

        void Initialize()
        {
            this.enabled = false;

            if (Damping == 0f)
            {
                Debug.LogError("Damping must different zero");
                Damping = 0.00001f;
            }
        }

        #endregion

        #region Internal Work

        void Update()
        {
            if (Damping == 0f)
                return;

            Transform rotateTo = TowerBaseScript.CurrentTarget;

            if (rotateTo != null && !TowerBaseScript.InRangeList.Contains(rotateTo.gameObject))
            {
                if (TowerBaseScript.InRangeList.Count > 0)
                    rotateTo = TowerBaseScript.InRangeList[0].transform;
                else
                    rotateTo = null;
            }

            if (rotateTo == null)
                return;

            Vector3 dir = rotateTo.position - transform.position;

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime / (Damping / 10));

            transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
        }

        #endregion

    }
}