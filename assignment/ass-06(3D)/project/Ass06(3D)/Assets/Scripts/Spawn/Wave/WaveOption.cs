﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class WaveOption
{

    #region Public Field

    public List<GameObject> EnemyPrefabs = new List<GameObject>();

    public List<PathManager> Path = new List<PathManager>();

    public List<float> EnemyHP = new List<float>();
    public List<float> EnemyShield = new List<float>();
    public List<float> StartDelayMin = new List<float>();
    public List<float> StartDelayMax = new List<float>();
    public List<float> DelayBetweenMin = new List<float>();
    public List<float> DelayBetweenMax = new List<float>();

    public List<int> EnemyCount = new List<int>();

    #endregion

}
