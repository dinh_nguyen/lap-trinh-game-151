﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Friends_Tower
{
    public class CreepProperty : MonoBehaviour
    {

        #region Public Field

        [HideInInspector]
        public List<TowerBase> NearTower = new List<TowerBase>();

        [HideInInspector]
        public float MaxHealth;

        [HideInInspector]
        public bool IsDead;

        public Animator Anim;

        public GameObject HitEffect;
        public GameObject DeathEffect;

        public AudioClip HitSound;
        public AudioClip DeathSound;

        public Slider HealthBar;
        public Shield2 Shield;

        public float PointsToEarn;
        public float Health = 100;

        public int DamageToDeal = 1;

        #endregion

        #region Protected Field

        protected RectTransform m_BarParentT;

        protected IEnumerator IERegenerateShieldCoroutine;

        protected float m_Time;

        #endregion

        #region Private Field

        private const string WalkParameter = "Walk";
        private const string HitParameter = "Hit";
        private const string DeadParameter = "Dead";
        private const string SuccessParameter = "Success";

        #endregion

        #region Public Method

        public void AddTower(TowerBase tower)
        {
            NearTower.Add(tower);
        }

        public void RemoveTower(TowerBase tower)
        {
            NearTower.Remove(tower);
        }

        public void Hit(float damage)
        {
            if (!IsAlive())
                return;

            if (Shield.Enabled)
            {
                damage = HitShield(damage);
                StopCoroutine(IERegenerateShieldCoroutine);
                StartCoroutine(IERegenerateShieldCoroutine);
            }

            Health -= damage;

            if (Health > 0 && Time.time > m_Time + 2)
                OnHit();
            else if (Health <= 0)
            {
                GameManager.Money += PointsToEarn;
                GameManager.EnemyWasKilled();

                OnDeath();
            }

            UpdateSlider();
        }

        public void UpdateSlider()
        {
            if (HealthBar)
                HealthBar.value = Health / MaxHealth;
            if (Shield.Bar)
                Shield.Bar.value = Shield.Value / Shield.MaxValue;
        }

        public bool IsAlive()
        {
            if (Health <= 0 || !gameObject.activeInHierarchy || IsDead)
                return false;
            else
                return true;
        }

        public void ReachDestination()
        {
            GameManager.DealDamage(DamageToDeal);

            OnDeath();
        }

        public void Dead()
        {
            Health = MaxHealth;
            Shield.Value = Shield.MaxValue;

            if (HealthBar != null)
                HealthBar.value = 1;
            if (Shield.Bar != null)
                Shield.Bar.value = 1;

            MyPool.Instance.DespawnEnemy(gameObject);
        }

        #endregion

        #region Protectecd Method

        protected void Start()
        {
            if (HealthBar)
                m_BarParentT = HealthBar.transform.parent.GetComponent<RectTransform>();
            else if (Shield.Bar)
                m_BarParentT = Shield.Bar.transform.parent.GetComponent<RectTransform>();

            IERegenerateShieldCoroutine = IERegenerateShield();
        }

        private void LateUpdate()
        {
            Quaternion rot = Camera.main.transform.rotation;

            if (m_BarParentT != null)
                m_BarParentT.rotation = rot;
        }

        #endregion

        #region Private Method

        private void OnSpawned(SpawnPool pool)
        {
            Anim.SetTrigger(WalkParameter);

            if (m_BarParentT != null)
            {
                RectTransform[] rects = GetComponentsInChildren<RectTransform>();
                for (int i = 0; i < rects.Length; i++)
                    rects[i].anchoredPosition = Vector2.zero;
            }

            IsDead = false;

            MaxHealth = Health;
            Shield.MaxValue = Shield.Value;
        }

        private void OnHit()
        {
            m_Time = Time.time;

            AudioManager.Instance.PlaySound3D(HitSound, transform.position, Random.Range(0.8f, 1.1f));

            if (HitEffect)
                MyPool.Instance.SpawnParticle(HitEffect, transform.position, Quaternion.identity);
        }

        private void OnDeath()
        {
            StopAllCoroutines();

            for (int i = 0; i < NearTower.Count; i++)
                NearTower[i].InRangeList.Remove(gameObject);

            NearTower.Clear();

            if (Health <= 0)
            {
                if (DeathEffect != null)
                    MyPool.Instance.SpawnParticle(DeathEffect, transform.position, Quaternion.identity);

                AudioManager.Instance.PlaySound3D(DeathSound, transform.position);

                Anim.SetTrigger(DeadParameter);
            }
            else
            {
                Anim.SetTrigger(SuccessParameter);
            }

            IsDead = true;
        }

        private float HitShield(float damage)
        {
            float currentValue = Shield.Value;

            Shield.Value -= damage;

            if (Shield.Value < 0)
                Shield.Value = 0;

            damage -= currentValue;
            if (damage < 0)
                damage = 0;

            return damage;
        }

        private IEnumerator IERegenerateShield()
        {
            if (Shield.RegenValue <= 0)
                yield break;

            yield return new WaitForSeconds(Shield.Delay);

            while (Shield.Value < Shield.MaxValue)
            {
                if (Shield.RegenType == SettingValue.Fix)
                    Shield.Value += Shield.RegenValue;
                else
                    Shield.Value += Mathf.Round(Shield.MaxValue * Shield.RegenValue * 100f) / 100f;

                Shield.Value = Mathf.Clamp(Shield.Value, 0f, Shield.MaxValue);

                Shield.Bar.value = Shield.Value / Shield.MaxValue;

                yield return new WaitForSeconds(Shield.Interval);

            }
        }

        #endregion
    }
}