﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EndlessOption
{

    #region Public Field

    public Setting IncreaseHP = new Setting();

    public Setting IncreaseShield = new Setting();

    public Setting IncreaseAmount = new Setting();

    #endregion
}
