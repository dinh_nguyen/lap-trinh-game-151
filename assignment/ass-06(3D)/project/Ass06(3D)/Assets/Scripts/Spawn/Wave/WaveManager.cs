﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Friends_Tower
{
    [System.Serializable]
    public class WaveManager : MonoBehaviour
    {

        #region Public Field

        [HideInInspector]
        public List<WaveOption> Options = new List<WaveOption>();

        [HideInInspector]
        public float SecondTillWave = 0f;

        [HideInInspector]
        public bool IsUserInput = true;

        public WaveMode Mode = WaveMode.Normal;

        public WaveStartOption StartOption = WaveStartOption.WaveCleared;

        public EndlessOption WaveEndlessOption = new EndlessOption();
        public WaveAnimation Anims = new WaveAnimation();
        public WaveSound Sounds = new WaveSound();
        public MyPool pool;

        public int SecondBetweenWaves;
        public int SecondIncrement;

        public bool IsAutoStart;
        public bool IsAutoCleanup;

        #endregion

        #region Private Field

        private int _waveIndex = 0;

        #endregion

        #region Public Field

        public void StartWaves()
        {
            StartCoroutine(IELaunchWave());
        }

        #endregion

        #region Protected Method

        protected void Start()
        {
            if (Mode == WaveMode.Normal)
                GameManager.WaveCount = Options.Count.ToString();

            AudioManager.Instance.PlayMusic(Sounds.PauseMusic);

            if (IsAutoStart)
                StartWaves();
        }

        protected void CheckStatus()
        {
            if (GameManager.GameHealth <= 0)
            {
                CancelInvoke("CheckStatus");
                return;
            }

            if (_waveIndex < Options.Count)
            {
                if (GameManager.EnemiesAlive > 0)
                    return;

                switch (StartOption)
                {
                    case WaveStartOption.WaveCleared:
                        if (GameManager.Wave > 0)
                            SecondBetweenWaves += SecondIncrement;

                        StartCoroutine(IEWaveTimer(SecondBetweenWaves));

                        Invoke("StartWaves", SecondBetweenWaves);

                        break;

                    default:
                        break;
                }

                Debug.Log("Wave Defeated!!");

                AudioManager.Instance.PlayMusic(Sounds.PauseMusic);

                AudioManager.Instance.PlaySound2D(Sounds.WaveEndtSound);
            }
        }

        protected void PlaySpawnEndAnimation()
        {
            if (Anims.SpawnEnd != null)
                Anims.ObjectToAnimate.GetComponent<Animation>().Play(Anims.SpawnEnd.name);
        }

        protected void ToggleInput()
        {
            IsUserInput = true;
        }

        protected void SpawnEnemy(int waveNo, int index)
        {
            GameObject prefab = Options[waveNo].EnemyPrefabs[index];
            Vector3 position = Options[waveNo].Path[index].WayPoints[0].position;

            GameObject enemy = pool.SpawnEnemy(prefab, position, Quaternion.identity);

            if (enemy == null)
                return;

            enemy.GetComponentInChildren<CreepMove>().Initialize(Options[waveNo].Path[index]);

            CreepProperty prop = enemy.GetComponentInChildren<CreepProperty>();

            if (Options[waveNo].EnemyHP[index] > 0)
                prop.Health = Options[waveNo].EnemyHP[index];
            if (Options[waveNo].EnemyShield[index] > 0)
                prop.Shield.Value = Options[waveNo].EnemyShield[index];

            GameManager.AddEnemy(1);
        }

        #endregion

        #region Private Method

        private IEnumerator IELaunchWave()
        {
            Debug.Log(string.Format("Wave {0} launched! Startup Time: {1}", _waveIndex, Time.time));

            if (IsAutoCleanup)
                PoolManager2.Pools.DestroyAll();

            if (GameManager.Wave > 0)
                IncreaseSettings();

            AudioManager.Instance.PlayMusic(Sounds.BattleMusic);

            AudioManager.Instance.PlaySound2D(Sounds.WaveStartSound);

            if (Anims.SpawnStart != null)
            {
                Anims.ObjectToAnimate.GetComponent<Animation>().Play(Anims.SpawnStart.name);
                yield return new WaitForSeconds(Anims.SpawnStart.length);
            }

            for (int i = 0; i < Options[_waveIndex].EnemyPrefabs.Count; i++)
            {
                StartCoroutine(IESpawnEnemy(i));
            }

            float lastSpawn = GetLastSpawnTime(_waveIndex);

            if (IsInvoking("CheckStatus"))
                CancelInvoke("CheckStatus");

            switch (StartOption)
            {
                case WaveStartOption.Interval:
                    if (GameManager.Wave > 0)
                        SecondBetweenWaves += SecondIncrement;

                    if (Mode != WaveMode.Normal || _waveIndex + 1 < Options.Count)
                    {
                        StartCoroutine(IEWaveTimer(SecondBetweenWaves));

                        Invoke("StartWaves", SecondBetweenWaves);
                    }

                    break;

                case WaveStartOption.UserInput:
                    IsUserInput = false;
                    Invoke("ToggleInput", lastSpawn);

                    break;
                default:
                    break;
            }

            InvokeRepeating("CheckStatus", lastSpawn, 2f);

            Invoke("PlaySpawnEndAnimation", lastSpawn);

            GameManager.IncreaseWave();

            switch (Mode)
            {
                case WaveMode.Normal:
                    _waveIndex++;
                    break;

                case WaveMode.Endless:
                    _waveIndex = (_waveIndex + 1) % Options.Count;
                    break;

                case WaveMode.RandomEndless:
                    int rnd = _waveIndex;
                    while (rnd == _waveIndex)
                        _waveIndex = Random.Range(0, Options.Count);
                    break;
            }
        }

        private IEnumerator IEWaveTimer(float second)
        {
            float timer = Time.time + second;

            while (Time.time < timer)
            {
                SecondTillWave = Mathf.Round((timer - Time.time) * 100f) / 100f;
                yield return null;
            }

            SecondTillWave = 0f;
        }

        private IEnumerator IESpawnEnemy(int index)
        {
            int waveNo = _waveIndex;

            yield return new WaitForSeconds(Random.Range(Options[waveNo].StartDelayMin[index],
                                                          Options[waveNo].StartDelayMax[index]));

            for (int j = 0; j < Options[waveNo].EnemyCount[index]; j++)
            {
                if (Options[waveNo].EnemyPrefabs[index] == null)
                {
                    yield return null;
                }

                if (Options[waveNo].Path[index] == null)
                {
                    break;
                }

                SpawnEnemy(waveNo, index);

                yield return new WaitForSeconds(Random.Range(Options[waveNo].DelayBetweenMin[index],
                                                              Options[waveNo].DelayBetweenMax[index]));
            }
        }

        private float GetLastSpawnTime(int wave)
        {
            float lastSpawn = 1;

            for (int i = 0; i < Options[wave].EnemyCount.Count; i++)
            {
                float result = Options[wave].StartDelayMax[i] + (Options[wave].EnemyCount[i] - 1) * Options[wave].DelayBetweenMax[i];

                if (result > lastSpawn)
                    lastSpawn = result + 0.25f;
            }

            return lastSpawn;
        }

        private void IncreaseSettings()
        {
            switch (Mode)
            {
                case WaveMode.Normal:
                    return;

                case WaveMode.Endless:
                    if (GameManager.Wave % Options.Count != 0)
                        return;
                    break;

                default:
                    break;
            }

            for (int i = 0; i < Options.Count; i++)
            {
                WaveOption option = Options[i];

                for (int j = 0; j < option.EnemyPrefabs.Count; j++)
                {
                    if (WaveEndlessOption.IncreaseAmount.Enable)
                    {
                        if (WaveEndlessOption.IncreaseAmount.Type == SettingValue.Fix)
                            option.EnemyCount[j] += (int)WaveEndlessOption.IncreaseAmount.Value;
                        else
                            option.EnemyCount[j] = Mathf.CeilToInt(option.EnemyCount[i] * WaveEndlessOption.IncreaseAmount.Value);
                    }

                    if (WaveEndlessOption.IncreaseHP.Enable)
                    {
                        if (option.EnemyHP[j] == 0)
                        {
                            option.EnemyHP[j] = option.EnemyPrefabs[j].GetComponent<CreepProperty>().Health;
                        }

                        if (WaveEndlessOption.IncreaseHP.Type == SettingValue.Fix)
                        {
                            option.EnemyHP[j] += WaveEndlessOption.IncreaseHP.Value;
                        }
                        else
                        {
                            option.EnemyHP[j] = Mathf.Round(option.EnemyHP[j] * WaveEndlessOption.IncreaseHP.Value * 100f) / 100f;
                        }
                    }

                    if (WaveEndlessOption.IncreaseShield.Enable)
                    {
                        if (option.EnemyShield[j] == 0)
                        {
                            CreepProperty prop = option.EnemyPrefabs[j].GetComponent<CreepProperty>();

                            if (prop.Shield.Enabled)
                                option.EnemyShield[j] = prop.Shield.Value;
                            else
                                continue;
                        }

                        if (WaveEndlessOption.IncreaseShield.Type == SettingValue.Fix)
                        {
                            option.EnemyShield[j] += WaveEndlessOption.IncreaseShield.Value;
                        }
                        else
                        {
                            option.EnemyShield[j] = Mathf.Round(option.EnemyShield[j] * WaveEndlessOption.IncreaseShield.Value * 100f) / 100f;
                        }
                    }
                }
            }
        }

        #endregion

    }
}