﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Shield2
{

    #region Public Field

    [HideInInspector]
    public float MaxValue;

    public bool Enabled = false;
    public Slider Bar;
    public float Value = 10;
    public SettingValue RegenType = SettingValue.Fix;
    public float RegenValue = 0;
    public float Interval = 1;
    public float Delay = 1;

    #endregion
}
