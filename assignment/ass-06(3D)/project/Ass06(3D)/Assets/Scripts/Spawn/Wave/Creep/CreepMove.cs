﻿using UnityEngine;
using System.Collections;

namespace Friends_Tower
{
    public class CreepMove : MonoBehaviour
    {

        #region Public Field

        [HideInInspector]
        public PathManager PathContain;

        [HideInInspector]
        public CreepProperty Property;

        public float MoveSpeed = 2;

        public float RotateSpeed = 10;

        #region Get/Set

        private Vector3 _pathDynamicOffset;
        public Vector3 PathDynamicOffset { get { return _pathDynamicOffset; } }


        private float _distFromDestination = 0;
        public float DistFromDestination { get { return _distFromDestination; } }

        #endregion

        #endregion

        #region Protected Field

        protected float m_MaxSpeed;

        protected int m_WayPointID;

        #endregion

        #region Public Method

        public void Initialize(PathManager path)
        {
            PathContain = path;

            float dynamicX = Random.Range(-path.DynamicOffset, path.DynamicOffset);
            float dynamicZ = Random.Range(-path.DynamicOffset, path.DynamicOffset);
            _pathDynamicOffset = new Vector3(dynamicX, 0, dynamicZ);

            transform.position += _pathDynamicOffset;

            _distFromDestination = CalculateDistFromDestination();

            m_WayPointID = 0;
        }

        public void ResetToNormalSpeed()
        {
            MoveSpeed = m_MaxSpeed;
        }

        #endregion

        #region Protected Method

        protected void Start()
        {
            m_MaxSpeed = MoveSpeed;

            Property = GetComponent<CreepProperty>();
        }

        protected void FixedUpdate()
        {
            if (!Property.IsDead)
            {
                if (MoveToPoint(PathContain.WayPoints[m_WayPointID].position))
                {
                    m_WayPointID++;
                    if (m_WayPointID >= PathContain.WayPoints.Length)
                        ReachDestination();
                }
            }
        }

        #endregion

        #region Private Method

        private bool MoveToPoint(Vector3 point)
        {
            point += PathDynamicOffset;

            float dist = Vector3.Distance(point, transform.position);

            if (dist < 0.005f)
                return true;

            if (MoveSpeed > 0)
            {
                Quaternion wantedRot = Quaternion.LookRotation(point - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, wantedRot, RotateSpeed * Time.deltaTime);
            }

            Vector3 dir = (point - transform.position).normalized;
            transform.Translate(dir * Mathf.Min(dist, MoveSpeed * Time.fixedDeltaTime), Space.World);

            _distFromDestination -= (MoveSpeed * Time.fixedDeltaTime);

            return false;
        }

        private void ReachDestination()
        {
            Property.ReachDestination();
        }

        private float CalculateDistFromDestination()
        {
            float dist = Vector3.Distance(transform.position, PathContain.WayPoints[m_WayPointID].position);
            for (int i = m_WayPointID + 1; i < PathContain.WayPoints.Length; i++)
            {
                dist += Vector3.Distance(PathContain.WayPoints[i - 1].position, PathContain.WayPoints[i].position);
            }

            return dist;
        }

        #endregion
    }
}