﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WaveSound
{

    #region Public Field

    public AudioClip BattleMusic;

    public AudioClip PauseMusic;

    public AudioClip WaveStartSound;

    public AudioClip WaveEndtSound;

    #endregion
}
