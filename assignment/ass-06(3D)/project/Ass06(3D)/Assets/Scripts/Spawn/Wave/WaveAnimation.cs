﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WaveAnimation
{

    #region Public Field

    public GameObject ObjectToAnimate;

    public AnimationClip SpawnStart;

    public AnimationClip SpawnEnd;

    #endregion

}
