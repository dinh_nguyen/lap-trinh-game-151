﻿using UnityEngine;
using System.Collections;

namespace Friends_Tower
{
    public class CreepModel : MonoBehaviour
    {

        public CreepProperty Property;

        public void Dead()
        {
            Property.Dead();
        }
    }
}