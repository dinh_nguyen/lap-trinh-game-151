﻿using UnityEngine;
using System.Collections.Generic;

public class PathManager : MonoBehaviour
{

    #region Public Field

    public Transform[] WayPoints;

    public Color Color1 = new Color(1, 0, 1, 0.5f);

    public Color Color2 = new Color(1, 235 / 255f, 4 / 255f, 0.5f);

    public bool IsDrawStraight = true;

    public bool IsDrawCurved = true;

    public float DynamicOffset = 1;

    #endregion

    #region Protected Field

    protected Vector3 m_Size = new Vector3(0.7f, 0.7f, 0.7f);

    protected float m_Radius = 0.4f;

    protected Vector3[] m_Points;

    #endregion

    #region Protected Method

    #region Gizmos Draw

    protected void OnDrawGizmos()
    {
        foreach(Transform child in transform)
        {
            if(child.CompareTag(UnityConstants.Tags.WayPoint))
            {
                Gizmos.color = Color2;
                Gizmos.DrawWireSphere(child.position, m_Radius);
            }
            else
            {
                Gizmos.color = Color1;
                Gizmos.DrawWireCube(child.position, m_Size);
            }
        }

        if (IsDrawStraight)
            DrawStraight();

        if (IsDrawCurved)
            DrawCurved();
    }

    protected void DrawStraight()
    {
        Gizmos.color = Color2;
        for (int i = 0; i < WayPoints.Length - 1; i++)
            Gizmos.DrawLine(WayPoints[i].position, WayPoints[i + 1].position);
    }

    protected void DrawCurved()
    {
        if (WayPoints.Length < 2)
            return;

        m_Points = new Vector3[WayPoints.Length + 2];

        for (int i = 0; i < WayPoints.Length; i++)
            m_Points[i + 1] = WayPoints[i].position;

        m_Points[0] = m_Points[1];
        m_Points[m_Points.Length - 1] = m_Points[m_Points.Length - 2];

        Gizmos.color = Color2;
        Vector3[] drawPs;
        Vector3 currPt;

        int subdivisions = m_Points.Length * 10;
        drawPs = new Vector3[subdivisions + 1];
        for(int i=0; i<subdivisions; i++)
        {
            float pm = i / (float)subdivisions;
            currPt = GetPoint(pm);
            drawPs[i] = currPt;
        }

        Vector3 prevPt = drawPs[0];
        for(int i=1; i<drawPs.Length; i++)
        {
            currPt = drawPs[i];
            Gizmos.DrawLine(currPt, prevPt);
            prevPt = currPt;
        }
    }

    #endregion

    #endregion

    #region Private Method

    private Vector3 GetPoint(float t)
    {
        int numSections = m_Points.Length - 3;
        int tSec = (int)Mathf.Floor(t * numSections);
        int currPt = numSections - 1;
        if (currPt > tSec)
            currPt = tSec;

        float u = t * numSections - currPt;

        Vector3 a = m_Points[currPt];
        Vector3 b = m_Points[currPt + 1];
        Vector3 c = m_Points[currPt + 2];
        Vector3 d = m_Points[currPt + 3];

        return .5f * (
                       (-a + 3f * b - 3f * c + d) * (u * u * u)
                       + (2f * a - 5f * b + 4f * c - d) * (u * u)
                       + (-a + c) * u
                       + 2f * b
                   );
    }

    #endregion
}
