﻿using UnityEngine;
using System.Collections.Generic;

public class WayPointManager : MonoBehaviour
{

    #region Public Field

    public static readonly Dictionary<string, PathManager> Paths = new Dictionary<string, PathManager>();

    #endregion

    #region Public Method

    public static void AddPath(GameObject path)
    {
        if(path.name.Contains("Clone"))
        {
            path.name = path.name.Replace("(Clone)", "");
        }

        if (Paths.ContainsKey(path.name))
            return;

        PathManager pathManager = path.GetComponent<PathManager>();

        if (pathManager == null)
            return;

        Paths.Add(path.name, pathManager);
    }

    #endregion

    #region Protected Method

    protected void Awake()
    {
        Paths.Clear();

        foreach (Transform child in transform)
            AddPath(child.gameObject);
    }

    #endregion

}
