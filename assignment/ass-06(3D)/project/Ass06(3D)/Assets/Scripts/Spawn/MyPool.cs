﻿using UnityEngine;
using System.Collections;
using Chida;

public class MyPool : SingletonMonoBehaviour<MyPool> {

    public SpawnPool EnemyPool;
    public SpawnPool TowerPool;
    public SpawnPool ProjectilePool;
    public SpawnPool ParticlePool;

    public GameObject SpawnEnemy(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        return EnemyPool.Spawn(prefab, pos, rot).gameObject;
    }

    public void DespawnEnemy(GameObject instance)
    {
        EnemyPool.Despawn(instance.transform);
    }

    public GameObject SpawnTower(GameObject prefab, Vector3 pos, Quaternion rot, Transform parent)
    {
        return TowerPool.Spawn(prefab, pos, rot, parent).gameObject;
    }

    public void DespawnTower(GameObject instance)
    {
        TowerPool.Despawn(instance.transform);
    }

    public GameObject SpawnProjectile(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        ParticleSystem par = prefab.GetComponent<ParticleSystem>();
        if(par != null)
            return ProjectilePool.Spawn(par, pos, rot).gameObject;
        else
            return ProjectilePool.Spawn(prefab, pos, rot).gameObject;
    }

    public void DespawnProjectile(GameObject instance)
    {
        ProjectilePool.Despawn(instance.transform);
    }

    public void SpawnParticle(GameObject obj, Vector3 pos, Quaternion rot)
    {
        ParticleSystem par = obj.GetComponent<ParticleSystem>();
        ParticlePool.Spawn(par, pos, rot);
    }
}
