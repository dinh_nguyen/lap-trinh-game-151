﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Friends_Tower
{
    public class UIGameInfo : MonoBehaviour
    {

        public WaveManager wave;

        public Text LabelWave;
        public Text LabelAlive;
        public Text LabelKill;
        public Text LabelGold;
        public Text LabelWaveTimer;
        public Text LabelHealth;

        void Update()
        {
            LabelWave.text = GameManager.Wave + " / " + GameManager.WaveCount;
            LabelAlive.text = GameManager.EnemiesAlive.ToString();
            LabelKill.text = GameManager.EnemiesKill.ToString();

            LabelGold.text = GameManager.Money.ToString();
            LabelHealth.text = GameManager.GameHealth.ToString();

            if (wave.SecondTillWave == 0)
                LabelWaveTimer.text = "";
            else
                LabelWaveTimer.text = wave.SecondTillWave.ToString();
        }

        public void NextWave()
        {
            wave.StartWaves();
        }
    }
}