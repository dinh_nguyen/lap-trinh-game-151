﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Friends_Tower
{
    public class UILogic : MonoBehaviour
    {

        #region Inspector Parameters

        public TowerManager TowerScript;

        public GridManager gridScript;

        public Camera RaycastCam;

        public Transform towerContainer;

        public GameObject[] InvisibleWidgets;

        public Text ErrorText;

        #endregion

        #region Public Parameters

        [HideInInspector]
        public TowerBase towerBase;

        [HideInInspector]
        public Upgrade upgrade;

        [HideInInspector]
        public GameObject CurrentGrid;

        [HideInInspector]
        public GameObject CurrentTower;

        #endregion

        #region Private Members

        private Ray ray;
        private RaycastHit gridHitInfo;
        private RaycastHit towerHitInfo;

        private int _index;

        #endregion

        #region Constructor && Init

        void Start()
        {
            foreach (GameObject obj in InvisibleWidgets)
                obj.SetActive(false);
        }

        #endregion

        #region Interal Functionality

        void Update()
        {
            ray = RaycastCam.ScreenPointToRay(Input.mousePosition);

            RaycastGrid();

            RaycastTower();
        }

        void RaycastGrid()
        {
            if (Physics.Raycast(ray, out gridHitInfo, 300, LayerManager.GridMask | LayerManager.TowerMask))
            {
                GameObject hit = gridHitInfo.transform.gameObject;

                if (hit.layer == LayerManager.GridLayer)
                    CurrentGrid = hit;
            }
            else
                CurrentGrid = null;
        }

        void RaycastTower()
        {
            if (Physics.Raycast(ray, out towerHitInfo, 300, LayerManager.TowerMask))
            {
                CurrentTower = towerHitInfo.transform.gameObject;
            }
            else
                CurrentTower = null;
        }

        #endregion

        #region Functionality

        public void CancelSelection(bool destroySelection)
        {
            if (destroySelection && GameManager.Selection != null)
                Destroy(GameManager.Selection);

            CurrentGrid = null;
            CurrentTower = null;
            GameManager.Selection = null;
            GameManager.GridSelection = null;

            gridScript.ToggleVisibility(false);
        }

        public bool CheckIfGridIsFree()
        {
            if (CurrentGrid == null || gridScript.GridList.Contains(CurrentGrid.name))
                return false;
            return true;
        }

        public void SetTowerComponents(GameObject tower)
        {
            upgrade = tower.GetComponent<Upgrade>();
            towerBase = tower.GetComponent<TowerBase>();
        }

        public bool AvailableUpgrade()
        {
            if (upgrade == null)
                return false;

            bool available = true;
            int curLvl = upgrade.CurrentLevel;
            if (curLvl >= upgrade.Options.Count - 1)
                available = false;

            return available;
        }

        public bool AffordableUpgrade()
        {
            if (upgrade == null)
                return false;

            bool affordable = true;
            int curLvl = upgrade.CurrentLevel;
            if(AvailableUpgrade())
            {
                if (GameManager.Money < upgrade.Options[curLvl + 1].Cost)
                    affordable = false;
            }

            return affordable;
        }

        public float GetUpgradePrice()
        {
            float upgradePrice = 0;
            int curLvl = upgrade.CurrentLevel;

            if (AvailableUpgrade())
                upgradePrice = upgrade.Options[curLvl + 1].Cost;

            return upgradePrice;
        }

        public float GetSellPrice()
        {
            float sellPrice = 0;
            int curLvl = upgrade.CurrentLevel;

            if (AvailableUpgrade())
                sellPrice = upgrade.Options[curLvl + 1].Cost * (1f - (TowerScript.SellLoss / 100f));

            return sellPrice;
        }

        public void InstantiateTower(int index)
        {
            _index = index;

            if (GameManager.Selection != null)
            {
                CurrentGrid = null;
                CurrentTower = null;
                Destroy(GameManager.Selection);
            }

            if (gridScript.GridList.Count == gridScript.transform.childCount)
                return;

            float price = 0;
            UpgradeOption opt = TowerScript.TowerUpgradeList[_index].Options[0];

            price = opt.Cost;

            if (GameManager.Money < price)
            {
                if (GameManager.GridSelection)
                {
                    GameObject grid = GameManager.GridSelection;
                    CancelSelection(true);
                    GameManager.GridSelection = grid;
                    grid.GetComponent<Renderer>().enabled = true;
                }
                else
                    CancelSelection(true);
                return;
            }

            GameManager.Selection = Instantiate(TowerScript.TowerPrefabs[_index], GameManager.outOfView, Quaternion.identity) as GameObject;
            GameManager.Selection.name = TowerScript.TowerNames[_index];

            towerBase = GameManager.Selection.GetComponentInChildren<TowerBase>();

            towerBase.gameObject.name = TowerScript.TowerNames[_index];

            GameManager.Selection.transform.parent = towerContainer;

            upgrade = GameManager.Selection.GetComponentInChildren<Upgrade>();
            towerBase.Upgrade = upgrade;

            towerBase.enabled = false;

            if(GameManager.GridSelection == null)
            {
                gridScript.ToggleVisibility(true);
            }
        }

        public void UpgradeTower()
        {
            upgrade.UpgradeLevel();

            GameManager.Money -= upgrade.Options[upgrade.CurrentLevel].Cost;
        }

        public void BuyTower()
        {
            GameManager.Money -= TowerScript.TowerUpgradeList[_index].Options[0].Cost;

            if(GameManager.GridSelection != null)
            {
                CurrentGrid = GameManager.GridSelection;
                GameManager.GridSelection.GetComponent<Renderer>().enabled = false;
            }

            gridScript.GridList.Add(CurrentGrid.name);

            CurrentGrid.transform.GetComponent<Renderer>().material = gridScript.GridFullMat;

            towerBase.enabled = true;
            towerBase.RangeIndicator.GetComponent<Renderer>().enabled = false;

            if (towerBase.Turret != null)
                towerBase.Turret.gameObject.GetComponent<TowerTurretRotation>().enabled = true;
        }

        public void SellTower(GameObject tower)
        {
            float sellPrice = GetSellPrice();

            GameManager.Money += sellPrice;

            Ray ray = new Ray(tower.transform.position + new Vector3(0f, 0.5f, 0f), transform.up);
            RaycastHit hitInfo;

            if(Physics.Raycast(ray, out hitInfo, 20, LayerManager.GridMask))
            {
                Transform grid = hitInfo.transform;

                gridScript.GridList.Remove(grid.name);
                grid.GetComponent<Renderer>().material = gridScript.GridFreeMat;
            }
        }

        public IEnumerator FadeIn(GameObject gObj)
        {
            float duration = 0.2f;

            if (!gObj.activeInHierarchy)
                gObj.SetActive(true);
            else
                yield break;

            float alpha = 1f;

            Graphic[] graphics = gObj.GetComponentsInChildren<Graphic>(true);

            foreach(Graphic g in graphics)
            {
                g.canvasRenderer.SetAlpha(0f);
                g.CrossFadeAlpha(alpha, duration, true);
            }
        }

        public IEnumerator FadeOut(GameObject gObj)
        {
            //fade out within 0.2 seconds
            float duration = 0.2f;

            //if gameobject is already inactive, do nothing
            if (!gObj.activeInHierarchy)
                yield break;

            //create alpha value
            float alpha = 0f;

            Graphic[] graphics = gObj.GetComponentsInChildren<Graphic>(true);

            //loop through widgets and set their alpha value to 0 using a color tween
            foreach (Graphic g in graphics)
            {
                g.CrossFadeAlpha(alpha, duration, true);
            }

            //wait till fade out was complete
            yield return new WaitForSeconds(duration);

            //disable UI elements
            gObj.SetActive(false);
        }

        
        #endregion

    }
}