﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Friends_Tower
{
    public enum _BuildMode
    {
        Tower,
        Grid
    }

    public class UIImpl : MonoBehaviour
    {

        #region Inspector Parameters

        public GameObject BuildFX;
        public GameObject UpgradeFX;

        public _BuildMode buildMode = _BuildMode.Tower;

        public MyPanel Panels;
        public MyButton Buttons;
        public MyLabel Labels;
        public MyControl Controls;
        public MySound Sounds;

        #endregion

        #region Public Parameters

        #endregion

        #region Private Members

        private UILogic _uiLogic;

        private float _time;

        private Vector3 _initRot;

        private Toggle _selectedCheckbox = null;

        #endregion

        #region Constructor && Init

        void Awake()
        {
            _uiLogic = GetComponent<UILogic>();
        }

        void Start()
        {
            _initRot = _uiLogic.RaycastCam.transform.eulerAngles;

            Controls.CrossHair = (GameObject)Instantiate(Controls.CrossHair, GameManager.outOfView, Quaternion.identity);
            Controls.aimIndicator = (GameObject)Instantiate(Controls.aimIndicator, GameManager.outOfView, Quaternion.identity); 

        }

        #endregion

        #region Internal Functionality

        void Update()
        {
            CheckESC();

            if (EventSystem.current.IsPointerOverGameObject() && GameManager.GridSelection == null || GameManager.ShowExit)
                return;
            else if(Input.GetMouseButtonUp(0) &&
                    (_uiLogic.CurrentTower == null || GameManager.Selection != null && _uiLogic.CurrentTower.transform.parent.gameObject != GameManager.Selection) &&
                    (_uiLogic.CurrentGrid == null || GameManager.ShowUpgrade && _uiLogic.CurrentGrid != null || buildMode == _BuildMode.Grid) &&
                    (GameManager.Selection != null && _uiLogic.CurrentGrid != GameManager.GridSelection))
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return;

                DisableMenus();

                if (buildMode == _BuildMode.Grid)
                    _uiLogic.StartCoroutine("FadeOut", Buttons.TowerButons);
            }

            ProcessGrid();
            ProcessTower();
        }

        void UpdateUpgradeMenu()
        {
            int CurrentLevel = _uiLogic.upgrade.CurrentLevel;

            UpgradeOption upgradeOption = _uiLogic.upgrade.Options[CurrentLevel];

            Labels.HeaderName.text = _uiLogic.upgrade.gameObject.name;
            Labels.Properties.text = "Level:" + "\n" +
                                     "Radius:" + "\n" +
                                     "Damage:" + "\n" +
                                     "Delay:" + "\n" +
                                     "Targets:";
            Labels.Stats.text = CurrentLevel + "\n" +
                               (Mathf.Round(upgradeOption.radius * 100f) / 100f) + "\n" +
                               (Mathf.Round(upgradeOption.Damage * 100f) / 100f) + "\n" +
                               (Mathf.Round(upgradeOption.ShootDelay * 100f) / 100f) + "\n" +
                               upgradeOption.TargetCount;

            if(Labels.UpgradeInfo != null)
            {
                if (CurrentLevel < _uiLogic.upgrade.Options.Count - 1)
                {
                    UpgradeOption nextUpg = _uiLogic.upgrade.Options[CurrentLevel + 1];
                    Labels.UpgradeInfo.text = "= " + (CurrentLevel + 1) + "\n" +
                                              "= " + (Mathf.Round(nextUpg.radius * 100f) / 100f) + "\n" +
                                              "= " + (Mathf.Round(nextUpg.Damage * 100f) / 100f) + "\n" +
                                              "= " + (Mathf.Round(nextUpg.ShootDelay * 100f) / 100f) + "\n" +
                                              "= " + nextUpg.TargetCount;
                }
                else
                    Labels.UpgradeInfo.text = "";
            }

            float sellPrice = _uiLogic.GetSellPrice();
            float upgradePrice = _uiLogic.GetUpgradePrice();

            bool affordable = true;

            Labels.SellPrice.text = "$" + sellPrice;

            if (!_uiLogic.AvailableUpgrade())
            {
                affordable = false;
                Labels.Price.text = "";
            }
            else
                Labels.Price.text = "$" + upgradePrice;

            if (affordable)
                affordable = _uiLogic.AffordableUpgrade();

            if (affordable)
                Buttons.UpgradeButtons.SetActive(true);
            else
                Buttons.UpgradeButtons.SetActive(false);
        }

        void CheckESC()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                _uiLogic.StartCoroutine("FadeIn", Panels.Main);

                _uiLogic.StartCoroutine("FadeOut", Buttons.TowerButons);

                DisableMenus();

                GameManager.ShowExit = true;

                Time.timeScale = 0.0001f;
            }
        }

        void ProcessGrid()
        {
            switch(buildMode)
            {
                case _BuildMode.Tower:
                    if (GameManager.Selection == null)
                        return;
                    else if(!_uiLogic.CheckIfGridIsFree() || _uiLogic.CurrentTower != null 
                        &&  _uiLogic.CurrentTower.transform.parent.gameObject != GameManager.Selection)
                    {
                        GameManager.Selection.transform.position = GameManager.outOfView;
                    }
                    else
                    {
                        GameManager.Selection.transform.position = _uiLogic.CurrentGrid.transform.position;

                        if (_uiLogic.towerBase.Turret != null)
                            _uiLogic.towerBase.Turret.localRotation = _uiLogic.CurrentGrid.transform.rotation;

                        if(Input.GetMouseButtonUp(0))
                        {
                            BuyTower();
                        }
                    }

                    break;

                case _BuildMode.Grid:
                    if(Input.GetMouseButtonUp(0) && _uiLogic.CheckIfGridIsFree() &&
                       GameManager.Selection == null)
                    {
                        if (GameManager.GridSelection != null)
                            GameManager.GridSelection.GetComponent<Renderer>().enabled = false;

                        GameObject grid = _uiLogic.CurrentGrid;

                        if (!Buttons.TowerButons.activeInHierarchy)
                            ShowButtons();

                        GameManager.GridSelection = grid;
                        GameManager.GridSelection.GetComponent<Renderer>().enabled = true;
                    }

                    if(GameManager.GridSelection != null)
                    {
                        Vector3 pos = GameManager.GridSelection.transform.position;
                        RepositionTowerButton(pos);
                    }

                    break;
            }
        }

        void ProcessTower()
        {
            GameObject tower = _uiLogic.CurrentTower;

            if (GameManager.Selection != null || tower == null)
                return;

            if(Input.GetMouseButtonUp(0) && _time + 0.5f < Time.time)
            {
                //ShowUpgradeMenu(tower);
            }
        }

        void BuyTower()
        {
            if(BuildFX != null)
                MyPool.Instance.SpawnParticle(BuildFX, GameManager.Selection.transform.position, Quaternion.identity);

            _uiLogic.BuyTower();

            _uiLogic.CancelSelection(false);

            if (_uiLogic.towerBase != null)
                _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = false;

            _uiLogic.StartCoroutine("FadeOut", Panels.ToolTip);

            _time = Time.time;
        }

        void RepositionTowerButton(Vector3 pos)
        {
            Transform towerBtn = Buttons.TowerButons.transform;
            Camera cam = Camera.main;

            if(cam != null)
            {
                Vector3 mPos = cam.WorldToScreenPoint(pos);

                if (mPos.x < 0 || mPos.x > Screen.width || mPos.y < 0 || mPos.y > Screen.height)
                    ShowButtons();

                mPos.z = 0;
                towerBtn.position = mPos;
                mPos = towerBtn.localPosition;
                mPos.x = Mathf.Round(mPos.x);
                mPos.y = Mathf.Round(mPos.y);
                towerBtn.localPosition = mPos;
            }
        }

        #endregion

        #region Functionality

        public void DisableMenus()
        {
            GameManager.ShowUpgrade = false;
            CancelInvoke("UpdateUpgradeMenu");

            _uiLogic.StartCoroutine("FadeOut", Panels.UpgradeMenu);

            if (_uiLogic.towerBase != null)
                _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = false;
            if (GameManager.GridSelection != null)
                GameManager.GridSelection.GetComponent<Renderer>().enabled = false;

            _uiLogic.CancelSelection(true);
        }

        public void ShowButtons()
        {
            if (Panels.Main.activeInHierarchy)
                return;

            _uiLogic.StartCoroutine("FadeOut", Buttons.TowerButons);
            Buttons.TowerButons.SetActive(true);

            DisableMenus();
        }

        public void ShowUpgradeMenu(GameObject tower)
        {
            _uiLogic.StartCoroutine("FadeIn", Panels.ToolTip);
            _uiLogic.StartCoroutine("FadeIn", Panels.UpgradeMenu);

            GameManager.ShowUpgrade = true;

            if (_uiLogic.towerBase != null)
                _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = false;
            if(GameManager.GridSelection != null)
            {
                GameManager.GridSelection.GetComponent<Renderer>().enabled = false;
                _uiLogic.StartCoroutine("FadeOut", Buttons.TowerButons);
            }
            GameManager.GridSelection = null;

            _uiLogic.SetTowerComponents(tower);

            _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = true;

            UpdateUpgradeMenu();

            if (!IsInvoking("UpdateUpgradeMenu"))
                InvokeRepeating("UpdateUpgradeMenu", .5f, 1f);
        }

        public void ShowTooltipMenu(int index)
        {
            _uiLogic.StartCoroutine("FadeIn", Panels.ToolTip);
            _uiLogic.StartCoroutine("FadeOut", Panels.UpgradeMenu);

            GameManager.ShowUpgrade = false;

            CancelInvoke("UpdateUpgradeMenu");

            TowerBase baseOptions = null;
            UpgradeOption upgradeOptions = null;

            if(GameManager.Selection != null)
            {
                baseOptions = _uiLogic.towerBase;
                upgradeOptions = _uiLogic.upgrade.Options[0];
            }
            else
            {
                baseOptions = _uiLogic.TowerScript.TowerBaseList[index];
                upgradeOptions = _uiLogic.TowerScript.TowerUpgradeList[index].Options[0];
            }

            Labels.HeaderName.text = _uiLogic.TowerScript.TowerNames[index];
            Labels.Properties.text = "Projectile:" + "\n" +
                                     "Radius:" + "\n" +
                                     "Damage:" + "\n" +
                                     "Delay:" + "\n" +
                                     "Targets:";
            Labels.Stats.text = baseOptions.Projectile.name + "\n" +
                                upgradeOptions.radius + "\n" +
                                upgradeOptions.Damage + "\n" +
                                upgradeOptions.ShootDelay + "\n" +
                                baseOptions.MyTargetType;
            Labels.Price.text = "$" + upgradeOptions.Cost;
        }

        public void Resume()
        {
            GameManager.ShowExit = false;
            Time.timeScale = 1;
            _uiLogic.StartCoroutine("FadeOut", Panels.Main);
        }

        public void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public void CreateTower(int index)
        {
            Transform button = Buttons.TowerButons.transform.GetChild(index);
            Toggle checkbox = button.GetComponent<Toggle>();

            if (buildMode == _BuildMode.Grid && !GameManager.GridSelection)
            {
                return;
            }

            if (GameManager.ShowUpgrade && _uiLogic.towerBase)
            {
                _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = false;
                GameManager.ShowUpgrade = false;
                CancelInvoke("UpdateUpgradeMenu");
            }

            if (checkbox == null || checkbox.isOn) _uiLogic.InstantiateTower(index);

            ShowTooltipMenu(index);

            if (GameManager.Selection)
                _uiLogic.towerBase.RangeIndicator.GetComponent<Renderer>().enabled = true;

            if (buildMode == _BuildMode.Grid)
            {
                if (GameManager.Selection)
                {
                    GameManager.Selection.transform.position = GameManager.GridSelection.transform.position;

                    if (_uiLogic.towerBase.Turret)
                        _uiLogic.towerBase.Turret.localRotation = GameManager.GridSelection.transform.rotation;
                }

                if (checkbox.isOn)
                    _selectedCheckbox = checkbox;
                else if (GameManager.Selection && !checkbox.isOn && checkbox == _selectedCheckbox)
                {
                    _uiLogic.StartCoroutine("FadeOut", Buttons.TowerButons);
                    BuyTower();
                }

                Toggle[] allToggles = Buttons.TowerButons.GetComponentsInChildren<Toggle>(true);
                for (int i = 0; i < allToggles.Length; i++)
                    if (allToggles[i] != _selectedCheckbox) allToggles[i].isOn = false;
            }
        }

        public void Upgrade()
        {
            GameObject tower = _uiLogic.upgrade.gameObject;

            if(UpgradeFX != null)
                MyPool.Instance.SpawnParticle(UpgradeFX, tower.transform.position, Quaternion.identity);

            _uiLogic.UpgradeTower();

            UpdateUpgradeMenu();

        }

        public void SellTower()
        {
            GameObject obj = _uiLogic.upgrade.transform.parent.gameObject;

            _uiLogic.SellTower(obj);
            GameManager.Selection = obj;

            DisableMenus();
        }

        #endregion

    }

    [System.Serializable]
    public class MyButton
    {
        public GameObject MainButton;
        public GameObject TowerButons;
        public GameObject SellButton;
        public GameObject CancelButton;
        public GameObject UpgradeButtons;
        public GameObject BuyButton;
        public GameObject ExitButton;
    }

    [System.Serializable]
    public class MyControl
    {
        public GameObject CrossHair;

        public GameObject aimIndicator;

        public float towerHeight = 10f;

        public Slider ReloadingSlider;

        public Image ReloadingSprite;
    }

    [System.Serializable]
    public class MyPanel
    {
        public GameObject Main;
        public GameObject UpgradeMenu;
        public GameObject ToolTip;
        public GameObject Control;
    }

    [System.Serializable]
    public class MyLabel
    {
        public Text HeaderName;
        public Text Properties;

        public Text Stats;
        public Text UpgradeInfo;

        public Text Price;
        public Text SellPrice;
    }

    [System.Serializable]
    public class MySound
    {
        public AudioClip Build;
        public AudioClip Sell;
        public AudioClip Upgrade;
    }
}