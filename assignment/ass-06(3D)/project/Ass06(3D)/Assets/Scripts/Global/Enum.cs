﻿using UnityEngine;
using System.Collections;


public enum WaveMode
{
    Normal,       
    Endless,        
    RandomEndless   
};

public enum WaveStartOption
{
    WaveCleared,
    Interval,
    UserInput,
    RoundBased
};

public enum SettingValue
{
    Fix,
    Percentual
}

public enum EnemyType
{
    Ground,
    Air,
    Both
}

public enum ShootOrder
{
    FirstIn,
    LastIn,
    Strongest,
    Weakest
}

public enum FireMode
{
    Straight,
    Lob,
}