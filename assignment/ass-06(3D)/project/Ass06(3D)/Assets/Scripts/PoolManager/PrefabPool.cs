﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PrefabPool
{

    #region Public Field

    public Transform PrefabT;

    public int PreLoadAmount = 1;

    public bool PreloadTime = false;

    public int PreLoadFrame = 2;

    public float PreLoadDelay = 0;

    public bool LimitInstance = false;

    public int LimitAmount = 100;

    public bool LimitFIFO = false;

    public bool CullDespawned = false;
    public bool CullingActive = false;

    public int CullAbove = 50;

    public int CullDelay = 60;

    public int CullMaxPerPass = 5;

    public SpawnPool spawnPool;

    #region Get/Set

    private bool _logMessage = false;
    public bool LogMessage
    {
        get
        {
            if (_forceLoggingSilent) 
                return false;

            if (spawnPool.LogMessage)
                return spawnPool.LogMessage;
            else
                return _logMessage;
        }
    }

    private int TotalCount {
        get {
            return _spawned.Count + _despawned.Count;
        }
    }

    private bool _preloaded = false;
    public bool Preloaded
    {
        get { return _preloaded; }
        private set { _preloaded = value; }
    }

    internal List<Transform> _spawned = new List<Transform>();
    public List<Transform> Spawned { get { return new List<Transform>(_spawned); } }

    internal List<Transform> _despawned = new List<Transform>();
    public List<Transform> Despawned { get { return new List<Transform>(_despawned); } }

    #endregion

    #endregion

    #region Private Field

    internal GameObject _prefabPool;

    private bool _forceLoggingSilent = false;

    #endregion

    #region Public Method

    #region Constructor

    public PrefabPool() { }

    public PrefabPool(Transform prefab)
    {
        this.PrefabT = prefab;
        this._prefabPool = prefab.gameObject;
    }

    #endregion

    public Transform SpawnNew()
    {
        return SpawnNew(Vector3.zero, Quaternion.identity);
    }

    public Transform SpawnNew(Vector3 pos, Quaternion rot)
    {
        if (LimitInstance && TotalCount >= LimitAmount)
        {
            if(LogMessage)
                Debug.Log(string.Format("SpawnPool {0} ({1}): " +
                                "LIMIT REACHED! Not creating new instances! (Returning null)",
                            spawnPool.PoolName,
                            PrefabT.name
                         ));

            return null;
        }

        if (pos == Vector3.zero)
            pos = spawnPool.Group.position;
        if (rot == Quaternion.identity)
            rot = spawnPool.Group.rotation;

        GameObject poolObj = spawnPool.InstantiatePrefab(_prefabPool, pos, rot);
        Transform poolT = poolObj.transform;

        NameInstance(poolT);

        if(!spawnPool.DontReparent)
        {
            bool worldPositionStays = !(poolT is RectTransform);
            poolT.SetParent(spawnPool.Group, worldPositionStays);
        }

        if (spawnPool.MatchPoolScale)
            poolT.localScale = Vector3.one;
        if (spawnPool.MatchPoolLayer)
            SetRecursively(poolT, spawnPool.gameObject.layer);

        _spawned.Add(poolT);

        if (LogMessage)
            Debug.Log(string.Format("SpawnPool {0} ({1}): Spawned new instance '{2}'.",
                                    spawnPool.PoolName,
                                    PrefabT.name,
                                    poolT.name
                                   ));

        return poolT;
    }

    public bool Contains(Transform instance)
    {
        if(_prefabPool == null)
            Debug.LogError(string.Format("SpawnPool {0}: PrefabPool.prefabGO is null",
                                         spawnPool.PoolName));

        bool contains;

        contains = _spawned.Contains(instance);
        if (contains)
            return true;

        contains = _despawned.Contains(instance);
        if (contains)
            return true;

        return false;
    }

    #endregion

    #region Private Method

    internal void InspectorInstanceConstructor()
    {
        _prefabPool = this.PrefabT.gameObject;
        _spawned = new List<Transform>();
        _despawned = new List<Transform>();
    }

    internal void SeldDestruct()
    {
        PrefabT = null;
        _prefabPool = null;
        spawnPool = null;

        foreach (Transform pool in _despawned)
            if (pool != null && spawnPool != null)
                spawnPool.DestroyInstance(pool.gameObject);

        foreach (Transform pool in _spawned)
            if (pool != null && spawnPool != null)
                spawnPool.DestroyInstance(pool.gameObject);

        _spawned.Clear();
        _despawned.Clear();
    }

    internal bool DespawnInstance(Transform instance)
    {
        return DespawnInstance(instance, true);
    }

    internal bool DespawnInstance(Transform instance, bool sendEventMessage)
    {
        if (LogMessage)
            Debug.Log(string.Format("SpawnPool {0} ({1}): Despawning '{2}'",
                                    spawnPool.PoolName,
                                    PrefabT.name,
                                    instance.name
                                    ));

        _spawned.Remove(instance);
        _despawned.Add(instance);

        if(sendEventMessage)
        {
            instance.gameObject.BroadcastMessage("OnDespawned",
                                                  spawnPool,
                                                  SendMessageOptions.DontRequireReceiver);
        }

        instance.gameObject.SetActive(false);

        if (!CullingActive && CullDespawned &&
            TotalCount > CullAbove)
        {
            CullingActive = false;
            spawnPool.StartCoroutine(IECullDespawned());
        }

        return true;
    }

    internal IEnumerator IECullDespawned()
    {
        if (LogMessage)
            Debug.Log(string.Format("SpawnPool {0} ({1}): CULLING TRIGGERED! " +
                                          "Waiting {2}sec to begin checking for despawns...",
                                    spawnPool.PoolName,
                                    PrefabT.name,
                                    CullDelay
                                    ));

        yield return new WaitForSeconds(CullDelay);

        while(TotalCount > CullAbove)
        {
            for(int i=0; i<CullMaxPerPass; i++)
            {
                if (TotalCount <= CullAbove)
                    break;

                if(_despawned.Count > 0)
                {
                    Transform pool = _despawned[0];
                    _despawned.RemoveAt(0);
                    spawnPool.DestroyInstance(pool.gameObject);

                    if(LogMessage)
                        Debug.Log(string.Format("SpawnPool {0} ({1}): " +
                                                    "CULLING to {2} instances. Now at {3}.",
                                   spawnPool.PoolName,
                                   PrefabT.name,
                                   CullAbove,
                                   TotalCount
                                   ));
                }
                else if (LogMessage)
                {
                    Debug.Log(string.Format("SpawnPool {0} ({1}): " +
                                                    "CULLING waiting for despawn. " +
                                                    "Checking again in {2}sec",
                                   spawnPool.PoolName,
                                   PrefabT.name,
                                   CullDelay
                                   ));
                    break;
                }
            }

            yield return new WaitForSeconds(CullDelay);
        }

        if(LogMessage)
            Debug.Log(string.Format("SpawnPool {0} ({1}): CULLING FINISHED! Stopping",
                                   spawnPool.PoolName,
                                   PrefabT.name
                                   ));

        CullingActive = true;
        yield return null;
    }

    internal Transform SpawnInstance(Vector3 pos, Quaternion rot)
    {
        if(LimitInstance && LimitFIFO &&
            _spawned.Count > LimitAmount)
        {
            Transform firstPool = _spawned[0];

            if(LogMessage)
                Debug.Log(string.Format("SpawnPool {0} ({1}): " +
                            "LIMIT REACHED! FIFO=True. Calling despawning for {2}...",
                                   spawnPool.PoolName,
                                   PrefabT.name,
                                   firstPool.name
                                   ));

            DespawnInstance(firstPool);
            spawnPool._spawned.Remove(firstPool);
        }

        Transform pool;

        if(_despawned.Count == 0)
        {
            pool = SpawnNew(pos, rot);
        }
        else
        {
            pool = _despawned[0];
            _despawned.RemoveAt(0);
            _spawned.Add(pool);

            if(pool == null)
            {
                throw new MissingReferenceException("Despawn Instance is null ??");
            }

            if (LogMessage)
                Debug.Log(string.Format("SpawnPool {0} ({1}): respawning '{2}'.",
                                   spawnPool.PoolName,
                                   PrefabT.name,
                                   pool.name
                                   ));
            pool.position = pos;
            pool.rotation = rot;
            pool.gameObject.SetActive(true);
        }

        return pool;
    }

    private void SetRecursively(Transform instance, int layer)
    {
        instance.gameObject.layer = layer;
        foreach (Transform child in instance)
            SetRecursively(child, layer);
    }

    internal void AddUnpooled(Transform instance, bool despawn)
    {
        NameInstance(instance);

        if(despawn)
        {
            instance.gameObject.SetActive(true);

            _despawned.Add(instance);
        }
        else
        {
            _spawned.Add(instance);
        }
    }

    internal void PreLoadInstance()
    {
        if (Preloaded)
        {
            Debug.Log(string.Format("SpawnPool {0} ({1}): " +
                                          "Already preloaded! You cannot preload twice. " +
                                          "If you are running this through code, make sure " +
                                          "it isn't also defined in the Inspector.",
                                        spawnPool.PoolName,
                                        PrefabT.name));

            return;
        }

        Preloaded = true;

        if(_prefabPool == null)
        {
            Debug.LogError(string.Format("SpawnPool {0} ({1}): Prefab cannot be null.",
                                             spawnPool.PoolName,
                                             PrefabT.name));

            return;
        }

        if(LimitInstance && PreLoadAmount > LimitAmount)
        {
            Debug.LogWarning(string.Format("SpawnPool {0} ({1}): " +
                            "You turned ON 'Limit Instances' and entered a " +
                            "'Limit Amount' greater than the 'Preload Amount'! " +
                            "Setting preload amount to limit amount.",
                         spawnPool.PoolName,
                         PrefabT.name
                    )
                );

            PreLoadAmount = LimitAmount;
        }

        if(CullDespawned && PreLoadAmount > CullAbove)
        {
            Debug.LogWarning(string.Format("SpawnPool {0} ({1}): " +
                    "You turned ON Culling and entered a 'Cull Above' threshold " +
                    "greater than the 'Preload Amount'! This will cause the " +
                    "culling feature to trigger immediatly, which is wrong " +
                    "conceptually. Only use culling for extreme situations. " +
                    "See the docs.",
                    spawnPool.PoolName,
                    PrefabT.name
                ));
        }

        if (PreloadTime)
        {
            if (PreLoadFrame > PreLoadAmount)
            {
                Debug.LogWarning(string.Format("SpawnPool {0} ({1}): " +
                    "Preloading over-time is on but the frame duration is greater " +
                    "than the number of instances to preload. The minimum spawned " +
                    "per frame is 1, so the maximum time is the same as the number " +
                    "of instances. Changing the preloadFrames value...",
                    spawnPool.PoolName,
                    PrefabT.name
                ));

                PreLoadFrame = PreLoadAmount;
            }

            spawnPool.StartCoroutine(IEPreLoadOverTime());
        }
        else
        {
            _forceLoggingSilent = true;

            Transform instance;
            while (TotalCount < PreLoadAmount)
            {
                instance = SpawnNew();
                DespawnInstance(instance, false);
            }

            _forceLoggingSilent = false;
        }
    }

    private IEnumerator IEPreLoadOverTime()
    {
        yield return new WaitForSeconds(PreLoadDelay);

        Transform instance;

        int amount = PreLoadAmount - TotalCount;
        if (amount <= 0)
            yield break;

        int remainder = amount % PreLoadFrame;
        int numPerFrame = amount / PreLoadFrame;

        _forceLoggingSilent = true;

        int numThisFrame;
        for (int i = 0; i < PreLoadFrame; i++)
        {
            numThisFrame = numPerFrame;
            if(i == PreLoadFrame - 1)
            {
                numThisFrame += remainder;
            }

            for(int j=0; j<numThisFrame; j++)
            {
                instance = SpawnNew();
                if (instance != null)
                    DespawnInstance(instance, false);

                yield return null;
            }

            if (TotalCount > PreLoadAmount)
                break;
        }

        _forceLoggingSilent = false;
    }

    private void NameInstance(Transform instance)
    {
        instance.name += (TotalCount + 1).ToString("#000");
    }

    #endregion

}
