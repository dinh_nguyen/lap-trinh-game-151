﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPoolsDict : IDictionary<string, SpawnPool>
{
    #region Public Field

    #region Delegate

    public delegate void OnCreatePoolDelegate(SpawnPool pool);

    #endregion

    #region Get/Set

    public SpawnPool this[string key]
    {
        get
        {
            return _pools[key];
        }
        set
        {
            throw new System.NotImplementedException();
        }
    }

    public ICollection<string> Keys
    {
        get
        {
            throw new System.NotImplementedException();
        }
    }

    public ICollection<SpawnPool> Values
    {
        get
        {
            throw new System.NotImplementedException();
        }
    }

    public int Count { get { return _pools.Count; } }

    bool ICollection<KeyValuePair<string, SpawnPool>>.IsReadOnly { get { return true; } }

    #endregion

    #endregion

    #region Private Field

    internal Dictionary<string, OnCreatePoolDelegate> onCreatePools = new Dictionary<string, OnCreatePoolDelegate>();
    private Dictionary<string, SpawnPool> _pools = new Dictionary<string, SpawnPool>();

    #endregion

    #region Public Method

    public void AddOnCreatePoolDelegate(string poolName, OnCreatePoolDelegate createPoolDelegate)
    {
        if(!onCreatePools.ContainsKey(poolName))
        {
            onCreatePools.Add(poolName, createPoolDelegate);
            return;
        }

        onCreatePools[poolName] += createPoolDelegate;
    }

    public void RemoveOnCreatePoolDelegate(string poolName, OnCreatePoolDelegate createPoolDelegate)
    {
        if (!onCreatePools.ContainsKey(poolName))
        {
            throw new KeyNotFoundException("No OnCreatedDelegates found for pool name '" + poolName + "'.");
        }

        onCreatePools[poolName] -= createPoolDelegate;
    }

    public SpawnPool Create(string poolName)
    {
        GameObject obj = new GameObject(poolName + "Pool");
        return obj.AddComponent<SpawnPool>();
    }

    public SpawnPool Create(string poolName, GameObject owner)
    {
        if (!IsAssertValidPoolName(poolName))
            return null;

        string ownerName = owner.gameObject.name;

        try
        {
            owner.gameObject.name = poolName;

            return owner.AddComponent<SpawnPool>();
        }
        finally
        {
            owner.gameObject.name = ownerName;
        }
    }

    public bool Destroy(string poolName)
    {
        SpawnPool spawnPool;
        if(!_pools.TryGetValue(poolName, out spawnPool))
        {
            return false;
        }

        Object.Destroy(spawnPool.gameObject);

        return true;
    }

    public void DestroyAll()
    {
        foreach (KeyValuePair<string, SpawnPool> pair in _pools)
            Object.Destroy(pair.Value.gameObject);

        _pools.Clear();
    }

    public bool ContainsKey(string poolName)
    {
        return _pools.ContainsKey(poolName);
    }

    public bool TryGetValue(string poolName, out SpawnPool spawnPool)
    {
        return _pools.TryGetValue(poolName, out spawnPool);
    }

    public IEnumerator<KeyValuePair<string, SpawnPool>> GetEnumerator()
    {
        return _pools.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _pools.GetEnumerator();
    }

    public override string ToString()
    {
        string[] keysArray = new string[_pools.Count];
        _pools.Keys.CopyTo(keysArray, 0);

        return string.Format("[{0}]", string.Join(", ", keysArray));
    }

    public void Add(string key, SpawnPool value)
    {
        throw new System.NotImplementedException();
    }

    public void Add(KeyValuePair<string, SpawnPool> item)
    {
        throw new System.NotImplementedException();
    }

    public bool Remove(string poolName)
    {
        throw new System.NotImplementedException();
    }

    public bool Remove(KeyValuePair<string, SpawnPool> item)
    {
        throw new System.NotImplementedException();
    }

    public bool Contains(KeyValuePair<string, SpawnPool> item)
    {
        throw new System.NotImplementedException();
    }

    public void Clear()
    {
        throw new System.NotImplementedException();

    }

    #endregion

    #region Private Method

    private bool IsAssertValidPoolName(string poolName)
    {
        string temp;
        temp = poolName.Replace("Pool", "");
        if(!temp.Equals(poolName))
        {
            poolName = temp;
        }

        if (ContainsKey(poolName))
            return false;
        return true;
    }

    internal void Add(SpawnPool spawnPool)
    {
        if(ContainsKey(spawnPool.PoolName))
        {
            return;
        }

        _pools.Add(spawnPool.PoolName, spawnPool);

        if (onCreatePools.ContainsKey(spawnPool.PoolName))
            onCreatePools[spawnPool.PoolName](spawnPool);
    }

    internal bool Remove(SpawnPool spawnPool)
    {
        if (!ContainsKey(spawnPool.PoolName))
            return false;

        _pools.Remove(spawnPool.PoolName);
        return true;
    }

    private void CopyTo(KeyValuePair<string, SpawnPool>[] array, int arrayIndex)
    {
        throw new System.NotImplementedException();
    }

    void ICollection<KeyValuePair<string, SpawnPool>>.CopyTo(KeyValuePair<string, SpawnPool>[] array, int arrayIndex)
    {
        throw new System.NotImplementedException();
    }

    #endregion

}
