﻿using UnityEngine;
using System.Collections;

public static class InstanceHandler
{

    #region Public Field

    #region Delegate
    public delegate GameObject OnInstantiateDelegate(GameObject prfab, Vector3 pos, Quaternion rot);
    public delegate void OnDestroyInstanceDelegate(GameObject instance);
    #endregion

    #region Event

    public static OnInstantiateDelegate OnInstantiate;
    public static OnDestroyInstanceDelegate OnDestroyInstance;

    #endregion

    #endregion

    #region Private Method

    internal static GameObject InstantiatePrefab(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        if (OnInstantiate != null)
            return OnInstantiate(prefab, pos, rot);
        else
            return Object.Instantiate(prefab, pos, rot) as GameObject;
    }

    internal static void DestroyInstance(GameObject instance)
    {
        if (OnDestroyInstance != null)
            OnDestroyInstance(instance);
        else
            Object.Destroy(instance);
    }

    #endregion

}
