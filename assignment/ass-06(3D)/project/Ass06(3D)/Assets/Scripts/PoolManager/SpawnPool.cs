﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class SpawnPool : MonoBehaviour, IList<Transform>
{

    #region Public Field

    internal List<Transform> _spawned = new List<Transform>();
    public List<PrefabPool> PrefabPoolOption = new List<PrefabPool>();

    public Dictionary<object, bool> PrefabsFoldOutStates = new Dictionary<object, bool>();
    public Dictionary<object, bool> EditorListItemStates = new Dictionary<object, bool>();

    public string PoolName = "";

    public float maxParticleDespawnTime = 300;

    public PrefabsDict Prefabs = new PrefabsDict();

    public bool MatchPoolScale = false;
    public bool MatchPoolLayer = false;
    public bool DontReparent = false;
    public bool LogMessage = false;

    #region Get/Set

    public Transform Group { get; private set; }

    public Transform this[int index]
    {
        get { return _spawned[index]; }
        set { throw new System.NotImplementedException("Read-only."); }
    }

    private List<PrefabPool> _prefabPools = new List<PrefabPool>();
    public Dictionary<string, PrefabPool> PrefabPools
    {
        get
        {
            Dictionary<string, PrefabPool> dict = new Dictionary<string, PrefabPool>();

            for (int i = 0; i < _prefabPools.Count; i++)
                dict[_prefabPools[i]._prefabPool.name] = _prefabPools[i];

            return dict;
        }
    }

    public bool IsReadOnly { get { throw new System.NotImplementedException(); } }

    public bool _dontDestroyOnLoad = false;
    public bool dontDestroyOnLoad
    {
        get
        {
            return _dontDestroyOnLoad;
        }
        set
        {
            _dontDestroyOnLoad = value;

            if (Group != null)
                DontDestroyOnLoad(Group.gameObject);
        }
    }

    public int Count
    {
        get { return this._spawned.Count; }
    }

    #endregion

    #region Delegate

    public delegate GameObject OnInstantiateDelegate(GameObject prefab, Vector3 pos, Quaternion rot);
    public delegate void OnDestroyInstanceDelegate(GameObject instance);

    #endregion

    #region Event

    public OnInstantiateDelegate OnInstantiate;
    public OnDestroyInstanceDelegate OnDestroyInstance;

    #endregion

    #endregion

    #region Public Method

    public void CreatePrefabPool(PrefabPool prefabPool)
    {
        bool isAldreadyPool = GetPrefabPool(prefabPool.PrefabT) == null ? false : true;
        if (isAldreadyPool)
            throw new System.Exception(string.Format("Prefab '{0}' is already in  SpawnPool '{1}'." +
                                                     "Prefabs can be in more than 1 SpawnPool but " +
                                                     "cannot be in the same SpawnPool twice.",
                                                     prefabPool.PrefabT,
                                                     PoolName
                                      ));

        prefabPool.spawnPool = this;

        _prefabPools.Add(prefabPool);

        Prefabs._Add(prefabPool.PrefabT.name, prefabPool.PrefabT);

        if (!prefabPool.Preloaded)
        {
            if (LogMessage)
                Debug.Log(string.Format("SpawnPool {0}: Preloading {1} {2}",
                        PoolName,
                        prefabPool.PreLoadAmount,
                        prefabPool.PrefabT.name
                    ));
            prefabPool.PreLoadInstance();
        }
    }

    public void Add(Transform instance, string prefabName, bool despawn, bool parent)
    {
        for (int i = 0; i < _prefabPools.Count; i++)
        {
            if (_prefabPools[i]._prefabPool == null)
            {
                Debug.LogError("Unexpected Error: PrefabPool._prefabPool is null");
                return;
            }

            if (_prefabPools[i]._prefabPool.name.Equals(prefabName))
            {
                _prefabPools[i].AddUnpooled(instance, despawn);

                if (LogMessage)
                    Debug.Log(string.Format("SpawnPool {0}: Adding previously unpooled instance {1}",
                                            PoolName,
                                            instance.name));

                if (parent)
                {
                    bool worldPositionStays = !(instance as RectTransform);
                    instance.SetParent(Group.transform, worldPositionStays);
                }

                if (!despawn)
                    _spawned.Add(instance);

                return;
            }

            Debug.LogError(string.Format("SpawnPool {0}: PrefabPool {1} not found.",
                                         PoolName,
                                         prefabName));
        }
    }

    public void Add(Transform item)
    {
        throw new System.NotImplementedException("Use SpawnPool.Spawn() to properly add items to the pool.");
    }

    public void Remove(Transform item)
    {
        throw new System.NotImplementedException("Use Despawn() to properly manage items that should " +
                                                 "remain in the pool but be deactivated.");
    }

    public PrefabPool GetPrefabPool(Transform prefab)
    {
        for (int i = 0; i < _prefabPools.Count; i++)
        {
            if (_prefabPools[i]._prefabPool == null)
                Debug.LogError(string.Format("SpawnPool {0}: PrefabPool._prefabPool is null",
                                             PoolName));
            if (_prefabPools[i]._prefabPool == prefab.gameObject)
                return _prefabPools[i];
        }

        return null;
    }

    public PrefabPool GetPrefabPool(GameObject prefab)
    {
        for (int i = 0; i < _prefabPools.Count; i++)
        {
            if (_prefabPools[i]._prefabPool == null)
                Debug.LogError(string.Format("SpawnPool {0}: PrefabPool._prefabPool is null",
                                             PoolName));
            if (_prefabPools[i]._prefabPool == prefab)
                return _prefabPools[i];
        }

        return null;
    }

    public Transform GetPrefab(Transform instance)
    {
        for (int i = 0; i < Prefabs.Count; i++)
            if (_prefabPools[i].Contains(instance))
                return _prefabPools[i].PrefabT;

        return null;
    }

    public Transform GetPrefab(GameObject instance)
    {
        for (int i = 0; i < Prefabs.Count; i++)
            if (_prefabPools[i].Contains(instance.transform))
                return _prefabPools[i].PrefabT;

        return null;
    }

    public IEnumerator<Transform> GetEnumerator()
    {
        for (int i = 0; i < _spawned.Count; i++)
            yield return _spawned[i];
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        for (int i = 0; i < _spawned.Count; i++)
            yield return _spawned[i];
    }

    public void CopyTo(Transform[] array, int arrayIndex)
    {
        _spawned.CopyTo(array, arrayIndex);
    }

    public bool IsSpawned(Transform instance)
    {
        return _spawned.Contains(instance);
    }

    public Transform Spawn(string prefabName)
    {
        Transform prefab = Prefabs[prefabName];
        return Spawn(prefab);
    }

    public Transform Spawn(string prefabName, Transform parent)
    {
        Transform prefab = Prefabs[prefabName];
        return this.Spawn(prefab, parent);
    }

    public Transform Spawn(string prefabName, Vector3 pos, Quaternion rot)
    {
        Transform prefab = Prefabs[prefabName];
        return this.Spawn(prefab, pos, rot);
    }

    public Transform Spawn(string prefabName, Vector3 pos, Quaternion rot, Transform parent)
    {
        Transform prefab = Prefabs[prefabName];
        return this.Spawn(prefab, pos, rot, parent);
    }

    public Transform Spawn(Transform prefab)
    {
        return Spawn(prefab, Vector3.zero, Quaternion.identity);
    }

    public Transform Spawn(Transform prefab, Transform parent)
    {
        return Spawn(prefab, Vector3.zero, Quaternion.identity, parent);
    }

    public Transform Spawn(Transform prefab, Vector3 pos, Quaternion rot)
    {
        Transform inst = Spawn(prefab, pos, rot, null);

        if (inst == null) return null;

        return inst;
    }

    public Transform Spawn(GameObject prefab)
    {
        return Spawn(prefab.transform);
    }

    public Transform Spawn(GameObject prefab, Transform parent)
    {
        return Spawn(prefab.transform, parent);
    }

    public Transform Spawn(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        return Spawn(prefab.transform, pos, rot);
    }

    public Transform Spawn(GameObject prefab, Vector3 pos, Quaternion rot, Transform parent)
    {
        return Spawn(prefab.transform, pos, rot, parent);
    }

    public Transform Spawn(Transform prefab, Vector3 pos, Quaternion rot, Transform parent)
    {
        Transform instance;

        for (int i = 0; i < _prefabPools.Count; i++)
        {
            if (_prefabPools[i]._prefabPool == prefab.gameObject)
            {
                instance = _prefabPools[i].SpawnInstance(pos, rot);

                if (instance == null)
                    return null;

                if (parent != null)
                    instance.parent = parent;
                else if (!DontReparent && instance.parent != Group)
                    instance.parent = Group;

                _spawned.Add(instance);

                instance.gameObject.BroadcastMessage("OnSpawned", this, SendMessageOptions.DontRequireReceiver);

                return instance;
            }
        }

        PrefabPool newPrefabPool = new PrefabPool(prefab);
        CreatePrefabPool(newPrefabPool);

        instance = newPrefabPool.SpawnInstance(pos, rot);

        if (parent != null)
            instance.parent = parent;
        else
            instance.parent = Group;

        _spawned.Add(instance);

        instance.gameObject.BroadcastMessage("OnSpawned", this, SendMessageOptions.DontRequireReceiver);

        return instance;
    }

    public AudioSource Spawn(AudioSource prefab)
    {
        return Spawn(prefab, Vector3.zero, Quaternion.identity, null);
    }

    public AudioSource Spawn(AudioSource prefab, Transform parent)
    {
        return Spawn(prefab, Vector3.zero, Quaternion.identity, parent);
    }

    public AudioSource Spawn(AudioSource prefab, Vector3 pos, Quaternion rot)
    {
        return Spawn(prefab, pos, rot, null);
    }

    public AudioSource Spawn(AudioSource prefab, Vector3 pos, Quaternion rot, Transform parent)
    {
        Transform instance = Spawn(prefab.transform, pos, rot, parent);

        if (instance == null)
            return null;

        AudioSource audioSource = instance.GetComponent<AudioSource>();
        audioSource.Play();

        StartCoroutine(IEListForAudioStop(audioSource));

        return audioSource;
    }

    public ParticleSystem Spawn(ParticleSystem prefab,
                                   Vector3 pos, Quaternion rot)
    {
        return Spawn(prefab, pos, rot, null);
    }

    public ParticleSystem Spawn(ParticleSystem prefab,
                                    Vector3 pos, Quaternion rot,
                                    Transform parent)
    {
        Transform inst = Spawn(prefab.transform, pos, rot, parent);

        if (inst == null) return null;

        ParticleSystem emitter = inst.GetComponent<ParticleSystem>();

        StartCoroutine(ListenForEmitDespawn(emitter));

        return emitter;
    }

    public ParticleEmitter Spawn(ParticleEmitter prefab,
                                     Vector3 pos, Quaternion rot)
    {
        Transform inst = Spawn(prefab.transform, pos, rot);

        if (inst == null) return null;

        ParticleAnimator animator = inst.GetComponent<ParticleAnimator>();
        if (animator != null) animator.autodestruct = false;

        ParticleEmitter emitter = inst.GetComponent<ParticleEmitter>();
        emitter.emit = true;

        StartCoroutine(ListenForEmitDespawn(emitter));

        return emitter;
    }

    public ParticleEmitter Spawn(ParticleEmitter prefab,
                                     Vector3 pos, Quaternion rot,
                                     string colorPropertyName, Color color)
    {
        Transform inst = Spawn(prefab.transform, pos, rot);

        if (inst == null) return null;
        var animator = inst.GetComponent<ParticleAnimator>();
        if (animator != null) animator.autodestruct = false;

        var emitter = inst.GetComponent<ParticleEmitter>();

        emitter.GetComponent<Renderer>().material.SetColor(colorPropertyName, color);
        emitter.emit = true;

        StartCoroutine(ListenForEmitDespawn(emitter));

        return emitter;
    }

    public void Despawn(Transform instance)
    {
        bool despawned = false;

        for (int i = 0; i < _prefabPools.Count; i++)
        {
            if (_prefabPools[i]._spawned.Contains(instance))
            {
                despawned = _prefabPools[i].DespawnInstance(instance);
            }
            else if (_prefabPools[i]._despawned.Contains(instance))
            {
                return;
            }
        }

        if (!despawned)
            return;

        _spawned.Remove(instance);
    }

    public void Despawn(Transform instance, Transform parent)
    {
        instance.parent = parent;
        Despawn(instance);
    }

    public void Despawn(Transform instance, float second)
    {
        StartCoroutine(IEDoDespawnAfterSeconds(instance, second, false, null));
    }

    public void Despawn(Transform instance, float second, Transform parent)
    {
        StartCoroutine(IEDoDespawnAfterSeconds(instance, second, true, parent));
    }

    public void DespawnAll()
    {
        List<Transform> spawned = new List<Transform>(_spawned);
        for (int i = 0; i < spawned.Count; i++)
            Despawn(spawned[i]);
    }

    public bool Contains(Transform item)
    {
        throw new System.NotImplementedException();
    }

    public int IndexOf(Transform item) { 
        throw new System.NotImplementedException(); 
    }
    public void Insert(int index, Transform item) { 
        throw new System.NotImplementedException(); 
    }
    public void RemoveAt(int index) { 
        throw new System.NotImplementedException(); 
    }
    public void Clear() { 
        throw new System.NotImplementedException(); 
    }

    bool ICollection<Transform>.Remove(Transform item) { 
        throw new System.NotImplementedException(); 
    }

    public override string ToString()
    {
        List<string> name_list = new List<string>();
        foreach (Transform item in _spawned)
            name_list.Add(item.name);

        return string.Join(", ", name_list.ToArray());
    }

    #endregion

    #region Private Method

    private void Awake()
    {
        if (_dontDestroyOnLoad)
            DontDestroyOnLoad(gameObject);

        Group = transform;

        if (string.IsNullOrEmpty(PoolName))
        {
            PoolName = Group.name.Replace("Pool", "");
            PoolName = PoolName.Replace("(Clone)", "");
        }

        if (LogMessage)
            Debug.Log(string.Format("SpawnPool {0}: Initializing..", PoolName));

        for (int i = 0; i < PrefabPoolOption.Count; i++)
        {
            if (PrefabPoolOption[i].PrefabT == null)
            {
                continue;
            }

            PrefabPoolOption[i].InspectorInstanceConstructor();
            CreatePrefabPool(PrefabPoolOption[i]);
        }

        PoolManager2.Pools.Add(this);
    }

    internal GameObject InstantiatePrefab(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        if (OnInstantiate != null)
            return OnInstantiate(prefab, pos, rot);
        else
            return InstanceHandler.InstantiatePrefab(prefab, pos, rot);
    }

    internal void DestroyInstance(GameObject instance)
    {
        if (OnDestroyInstance != null)
            OnDestroyInstance(instance);
        else
            InstanceHandler.DestroyInstance(instance);
    }

    private void OnDestroy()
    {
        if (LogMessage)
            Debug.Log(string.Format("SpawnPool {0}: Destroying...", PoolName));

        PoolManager2.Pools.Remove(this);

        StopAllCoroutines();

        _spawned.Clear();

        foreach (PrefabPool pool in _prefabPools)
            pool.SeldDestruct();

        _prefabPools.Clear();
        Prefabs._Clear();
    }

    private IEnumerator IEListForAudioStop(AudioSource src)
    {
        yield return null;

        GameObject srcObj = src.gameObject;

        while (src.isPlaying)
            yield return null;

        if (!srcObj.activeInHierarchy)
        {
            src.Stop();
            yield break;
        }

        Despawn(src.transform);
    }

    private IEnumerator IEDoDespawnAfterSeconds(Transform instance, float second, bool useParent, Transform parent)
    {
        GameObject obj = instance.gameObject;
        while (second > 0)
        {
            yield return null;

            if (!obj.activeInHierarchy)
                yield break;

            second -= Time.deltaTime;
        }

        if (useParent)
            Despawn(instance, parent);
        else
            Despawn(instance);

    }

    private IEnumerator ListenForEmitDespawn(ParticleEmitter emitter)
    {
        yield return null;
        yield return new WaitForEndOfFrame();

        float safetimer = 0;   
        GameObject emitterGO = emitter.gameObject;
        while (emitter.particleCount > 0 && emitterGO.activeInHierarchy)
        {
            safetimer += Time.deltaTime;
            yield return null;
        }

        emitter.emit = false;
        if (emitterGO.activeInHierarchy)
            Despawn(emitter.transform);
    }

    private IEnumerator ListenForEmitDespawn(ParticleSystem emitter)
    {
        yield return new WaitForSeconds(emitter.startDelay + 0.25f);

        float safetimer = 0;   
        GameObject emitterGO = emitter.gameObject;
        while (emitter.IsAlive(true) && emitterGO.activeInHierarchy)
        {
            safetimer += Time.deltaTime;
            yield return null;
        }

        if (emitterGO.activeInHierarchy)
        {
            Despawn(emitter.transform);
            emitter.Clear(true);
        }
    }


    #endregion
}