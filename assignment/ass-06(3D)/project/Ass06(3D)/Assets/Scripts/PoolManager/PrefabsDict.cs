﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabsDict : IDictionary<string, Transform>
{

    #region Public Field

    private Dictionary<string, Transform> _prefabs = new Dictionary<string, Transform>();

    #region Get/Set

    public int Count
    {
        get
        {
            return _prefabs.Count;
        }
    }

    public Transform this[string key]
    {
        get
        {
            return _prefabs[key];
        }
        set
        {
            throw new System.NotImplementedException("Read-only.");
        }
    }

    public ICollection<string> Keys
    {
        get
        {
            return _prefabs.Keys;
        }
    }

    public ICollection<Transform> Values
    {
        get
        {
            return _prefabs.Values;
        }
    }

    bool ICollection<KeyValuePair<string, Transform>>.IsReadOnly { get { return true; } }

    private bool IsReadOnly { get { return true; } }

    #endregion

    #endregion

    #region Public Method

    public override string ToString()
    {
        string[] keysArray = new string[_prefabs.Count];
        _prefabs.Keys.CopyTo(keysArray, 0);
        
        return string.Format("[{0}]", System.String.Join(", ", keysArray));
    }

    public bool ContainsKey(string prefabName)
    {
        return _prefabs.ContainsKey(prefabName);
    }

    public bool Contains(KeyValuePair<string, Transform> item)
    {
        throw new System.NotImplementedException("Use Contains(string prefabName) instead.");
    }

    public bool TryGetValue(string prefabName, out Transform prfabT)
    {
        return _prefabs.TryGetValue(prefabName, out prfabT);
    }

    public void Add(string key, Transform value)
    {
        throw new System.NotImplementedException("Read-Only");
    }

    public void Add(KeyValuePair<string, Transform> item)
    {
        throw new System.NotImplementedException("Read-only");
    }

    public bool Remove(string prefabName)
    {
        throw new System.NotImplementedException("Read-Only");
    }

    public bool Remove(KeyValuePair<string, Transform> item)
    {
        throw new System.NotImplementedException("Read-only");
    }

    public void Clear() { 
        throw new System.NotImplementedException(); 
    }

    public IEnumerator<KeyValuePair<string, Transform>> GetEnumerator()
    {
        return _prefabs.GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        return this._prefabs.GetEnumerator();
    }

    #endregion

    #region Private Method

    internal void _Add(string prefabName, Transform prefabT)
    {
        _prefabs.Add(prefabName, prefabT);
    }

    internal void _Remove(string prefabName)
    {
        _prefabs.Remove(prefabName);
    }

    internal void _Clear()
    {
        _prefabs.Clear();
    }

    private void CopyTo(KeyValuePair<string, Transform>[] array, int arrayIndex)
    {
        throw new System.NotImplementedException();
    }

    void ICollection<KeyValuePair<string, Transform>>.CopyTo(KeyValuePair<string, Transform>[] array, int arrayIndex)
    {
        throw new System.NotImplementedException();
    }

    #endregion
}
