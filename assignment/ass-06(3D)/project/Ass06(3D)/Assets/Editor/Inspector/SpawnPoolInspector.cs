﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SpawnPool))]
public class SpawnPoolInspector2 : Editor
{

    #region Public Field

    public bool expandPrefabs = true;

    #endregion

    #region Public Method

    public override void OnInspectorGUI()
    {
        SpawnPool script = (SpawnPool)target;

        EditorGUI.indentLevel = 0;
        PoolEditorHelper.LookLikeControls();

        script.PoolName = EditorGUILayout.TextField("Pool Name", script.PoolName);

        script.MatchPoolScale = EditorGUILayout.Toggle("Match Pool Scale", script.MatchPoolScale);
        script.MatchPoolLayer = EditorGUILayout.Toggle("Match Pool Layer", script.MatchPoolLayer);

        script.DontReparent = EditorGUILayout.Toggle("Don't Reparent", script.DontReparent);

        script._dontDestroyOnLoad = EditorGUILayout.Toggle("Don't Destroy On Load", script._dontDestroyOnLoad);

        script.LogMessage = EditorGUILayout.Toggle("Log Messages", script.LogMessage);

        expandPrefabs = PoolEditorHelper.SerializedObjFoldOutList<PrefabPool>
                            (
                                "Per-Prefab Pool Options",
                                script.PrefabPoolOption,
                                this.expandPrefabs,
                                ref script.EditorListItemStates,
                                true
                            );

        if (GUI.changed)
            EditorUtility.SetDirty(target);

    }

    #endregion
}
