﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(WayPointManager))]
public class WayPointManagerInspector : Editor
{

    #region Protected Field

    protected List<GameObject> m_WayPointsList = new List<GameObject>();
    protected GameObject m_Path;

    protected WayPointManager m_Script;
    protected PathManager m_PathManager;

    protected string m_PathName;

    protected bool m_IsPlacing = false;

    #endregion

    #region Public Method

    public void OnSceneGUI()
    {
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && Event.current.alt && m_IsPlacing)
        {
            Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(worldRay, out hit))
            {
                Event.current.Use();

                PlaceWayPoint(hit.point);
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        m_Script = (WayPointManager)target;

        EditorGUIUtility.LookLikeControls();

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Enter Path Name: ", EditorStyles.boldLabel, GUILayout.Height(15));

        m_PathName = EditorGUILayout.TextField(m_PathName, GUILayout.Height(15));

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Start Path", GUILayout.Height(40)))
        {
            if(string.IsNullOrEmpty(m_PathName))
            {
                Debug.LogError("No Path Name");
                return;
            }

            if(m_Script.transform.FindChild(m_PathName) != null)
            {
                Debug.LogError("Path Name Aldready Exist");
                return;
            }

            if (m_IsPlacing)
            {
                Debug.LogError("Path already started, use alt + left mouse button to place new waypoints within scene view");
                return;
            }

            m_IsPlacing = true;

            m_Path = new GameObject(m_PathName);
            m_PathManager = m_Path.AddComponent<PathManager>();
            m_PathManager.WayPoints = new Transform[0];
            m_Path.transform.position = m_Script.gameObject.transform.position;
            m_Path.transform.parent = m_Script.gameObject.transform;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        if (GUILayout.Button("Finish Editing", GUILayout.Height(40)))
        {
            if (m_WayPointsList.Count < 2)
            {
                Debug.LogError("Not Enought Way Point");
                if (m_Path)
                    DestroyImmediate(m_Path);
            }
            else
            {
                m_WayPointsList[m_WayPointsList.Count - 1].name = "WayPointEnd";
                m_WayPointsList[m_WayPointsList.Count - 1].gameObject.tag = UnityConstants.Tags.Untagged;

                m_WayPointsList[0].name = "WayPointStart";
                m_WayPointsList[0].gameObject.tag = UnityConstants.Tags.Untagged;
            }

            m_IsPlacing = false;
            m_WayPointsList.Clear();
            m_PathName = string.Empty;
        }

        EditorGUILayout.Space();

        GUILayout.Label("Hint:\nPress 'Start Path' to begin a new path," +
                        "\nALT + Left Click lets you place waypoints\nonto objects." +
                        "\nPress 'Finish Editing' to end your path.");
    }

    #endregion

    #region Private Method

    private void PlaceWayPoint(Vector3 WayPoint)
    {
        GameObject obj = new GameObject("WayPoint");
        obj.tag = UnityConstants.Tags.WayPoint;

        Transform[] WayPointsCache = new Transform[m_PathManager.WayPoints.Length];
        System.Array.Copy(m_PathManager.WayPoints, WayPointsCache, m_PathManager.WayPoints.Length);

        m_PathManager.WayPoints = new Transform[m_PathManager.WayPoints.Length + 1];
        System.Array.Copy(WayPointsCache, m_PathManager.WayPoints, WayPointsCache.Length);
        m_PathManager.WayPoints[m_PathManager.WayPoints.Length - 1] = obj.transform;

        if (m_WayPointsList.Count == 0)
            m_PathManager.transform.position = WayPoint;

        obj.transform.position = WayPoint;
        obj.transform.parent = m_PathManager.transform;
        m_WayPointsList.Add(obj);
    }

    #endregion

}
