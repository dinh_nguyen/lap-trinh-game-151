﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraControl))]
public class CameraControlInspector : Editor {

    private static CameraControl _instance;

    private GUIContent cont;

    private float width;

    private static bool _showDefaultFlag;

    void Awake()
    {
        _instance = (CameraControl)target;

        EditorUtility.SetDirty(_instance);
    }

    public override void OnInspectorGUI()
    {
        GUI.changed = false;

        EditorGUILayout.Space();

        cont = new GUIContent("Pan Speed:", "The speed at which the camera pans on the horizontal axis");
        _instance.PanSpeed = EditorGUILayout.FloatField(cont, _instance.PanSpeed);

        cont = new GUIContent("Zoom Speed:", "The speed at witch the camera zooms");
        _instance.ZoomSpeed = EditorGUILayout.FloatField(cont, _instance.ZoomSpeed);

        EditorGUILayout.Space();

        width = 150f;

    #if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
			
    #else

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("EnableKeyPanning:", "Check to enable camera panning when the mouse cursor is moved to the edge of the screen");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.EnableKeyPanning = EditorGUILayout.Toggle(_instance.EnableKeyPanning);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("EnableMousePanning:", "Check to enable camera panning using 'wasd' key");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.EnableMousePanning = EditorGUILayout.Toggle(_instance.EnableMousePanning);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("EnableMouseRotate:", "Check to enable right-mouse-click drag to rotate the camera angle");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.EnableMouseRotate = EditorGUILayout.Toggle(_instance.EnableMouseRotate);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("EnableMouseZoom:", "Check to enable using mouse wheel to zoom the camera");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.EnableMouseZoom = EditorGUILayout.Toggle(_instance.EnableMouseZoom);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("MousePanningZoneWidth:", "The clearing from the edge of the screen where the mouse panning will start");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.MousePanningZoneWidth = EditorGUILayout.IntField(_instance.MousePanningZoneWidth);
        EditorGUILayout.EndHorizontal();

    #endif

        width = 116f;

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("X-Axis Limit:", "The min/max X-axis position limit of the camera pivot");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.MinPosX = EditorGUILayout.FloatField(_instance.MinPosX);
            _instance.MaxPosX = EditorGUILayout.FloatField(_instance.MaxPosX);
            EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("Z-Axis Limit:", "The min/max Z-axis position limit of the camera pivot");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.MinPosZ = EditorGUILayout.FloatField(_instance.MinPosZ);
            _instance.MaxPosZ = EditorGUILayout.FloatField(_instance.MaxPosZ);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("Zoom Limit:", "The limit of the camera zoom. This is effectively the local Z-axis position limit of the camera transform as a child of the camera pivot"); 
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.MinZoomDistance = EditorGUILayout.FloatField(_instance.MinZoomDistance);
            _instance.MaxZoomDistance = EditorGUILayout.FloatField(_instance.MaxZoomDistance);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            cont = new GUIContent("Elevation Limit:", "The limit of the elevation of the camera pivot, effectively the X-axis rotation. Recommend to keep the value between 10 to 89");
            EditorGUILayout.LabelField(cont, GUILayout.Width(width));
            _instance.MinRotateAngle = EditorGUILayout.FloatField(_instance.MinRotateAngle);
            _instance.MaxRotateAngle = EditorGUILayout.FloatField(_instance.MaxRotateAngle);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        if (GUI.changed)
            EditorUtility.SetDirty(_instance);
    }

}
