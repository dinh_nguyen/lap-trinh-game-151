﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Friends_Tower
{
    [CustomEditor(typeof(TowerManager))]
    public class TowerManagerInspector : Editor
    {
        public TowerManager Script;
        public string NewName = "";

        public override void OnInspectorGUI()
        {
            Script = (TowerManager)target;

            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("New Tower Name:");

            NewName = EditorGUILayout.TextField(NewName);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Add Tower!"))
            {
                Undo.RecordObject(Script, "AddTower");

                if (string.IsNullOrEmpty(NewName))
                    return;

                Script.TowerNames.Add(NewName);
                Script.TowerPrefabs.Add(null);
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Sell Loss %");
            Script.SellLoss = EditorGUILayout.IntField(Script.SellLoss);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            if (Script.TowerNames.Count == 0)
                return;

            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Name:");
            GUILayout.Label("Prefabs:");

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < Script.TowerNames.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                Script.TowerNames[i] = EditorGUILayout.TextField(Script.TowerNames[i]);
                Script.TowerPrefabs[i] = (GameObject)EditorGUILayout.ObjectField(Script.TowerPrefabs[i], typeof(GameObject), false);

                if (GUILayout.Button("X"))
                {
                    Undo.RecordObject(Script, "DeleteTower");

                    Script.TowerNames.RemoveAt(i);
                    Script.TowerPrefabs.RemoveAt(i);
                }

                EditorGUILayout.EndHorizontal();
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(Script);
                Repaint();
            }
        }
    }
}
