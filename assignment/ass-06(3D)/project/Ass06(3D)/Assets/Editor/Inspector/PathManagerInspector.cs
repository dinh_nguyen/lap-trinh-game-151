﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(PathManager))]
public class PathManagerInspector : Editor
{

    #region Protected Field

    protected SerializedObject m_Object;

    protected SerializedProperty m_WayPoints;
    protected SerializedProperty m_WayPointsCount;

    protected SerializedProperty m_Check1;
    protected SerializedProperty m_Check2;

    protected SerializedProperty m_Color1;
    protected SerializedProperty m_Color2;

    #endregion

    #region Private Field

    private const string _wayPointsArraySize = "WayPoints.Array.size";

    private const string _wayPointArrayData = "WayPoints.Array.data[{0}]";

    #endregion

    #region Public Method

    public void OnEnable()
    {
        m_Object = new SerializedObject(target);

        m_Check1 = m_Object.FindProperty("IsDrawStraight");
        m_Check2 = m_Object.FindProperty("IsDrawCurved");
        m_Color1 = m_Object.FindProperty("Color1");
        m_Color2 = m_Object.FindProperty("Color2");

        m_WayPointsCount = m_Object.FindProperty(_wayPointsArraySize);
    }

    public override void OnInspectorGUI()
    {
        m_Object.Update();

        m_Check1.boolValue = EditorGUILayout.Toggle("Draw straight Lines", m_Check1.boolValue);
        m_Check2.boolValue = EditorGUILayout.Toggle("Draw curved Lines", m_Check2.boolValue);

        EditorGUILayout.PropertyField(m_Color1);
        EditorGUILayout.PropertyField(m_Color2);

        Transform[] WayPoints = GetWayPointsArray();

        float pathLength = 0f;
        for (int i = 0; i < WayPoints.Length - 1; i++)
            pathLength += Vector3.Distance(WayPoints[i].position, WayPoints[i + 1].position);

        GUILayout.Label("Path Length: " + pathLength);

        GUILayout.Label("Waypoints: ", EditorStyles.boldLabel);

        for(int i=0; i<WayPoints.Length; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label((i + 1) + ".", GUILayout.Width(20));

            Transform result = EditorGUILayout.ObjectField(WayPoints[i], typeof(Transform), true) as Transform;

            if(GUI.changed)
            {
                SetWayPoint(i, result);
            }

            if(i < WayPoints.Length - 1 &&  GUILayout.Button("+", GUILayout.Width(30f)))
            {
                AddWayPointAtIndex(i);
                break;
            }

            if(i > 0 && i < WayPoints.Length - 1 &&  GUILayout.Button("-", GUILayout.Width(30f)))
            {
                RemoveWayPointAtIndex(i);
                break;
            }
            
            GUILayout.EndHorizontal();
        }
        
        EditorGUILayout.Space();


        if (GUILayout.Button("Place to Ground"))
        {
            foreach(Transform WayPoint in WayPoints)
            {
                Undo.RecordObject(WayPoint, "PlaceGround");

                Ray ray = new Ray(WayPoint.position + new Vector3(0, 2, 0), -WayPoint.up);
                RaycastHit hit;
                if(Physics.Raycast(ray, out hit, 100))
                {
                    WayPoint.position = new Vector3(WayPoint.position.x, hit.point.y, WayPoint.position.z);
                }
            }
        }
        
        EditorGUILayout.Space();

        if (GUILayout.Button("Invert Direction"))
        {
            Undo.RecordObjects(WayPoints, "InvertDirection");

            Vector3[] WayPointCopy = new Vector3[WayPoints.Length];
            for(int i=0; i<WayPoints.Length; i++)
                WayPointCopy[i] = WayPoints[i].position;

            for(int i=0; i<WayPoints.Length; i++)
                WayPoints[i].position = WayPointCopy[WayPointCopy.Length - 1 - i];
        }
        
        m_Object.ApplyModifiedProperties();
    }

    public void OnSceneGUI()
    {
        Transform[] WayPoints = GetWayPointsArray();

        if (WayPoints.Length == 0)
            return;

        Handles.BeginGUI();
        for(int i=0; i<WayPoints.Length; i++)
        {
            if (WayPoints[i] == null)
                continue;

            var guiPoint = HandleUtility.WorldToGUIPoint(WayPoints[i].position);

            var rect = new Rect(guiPoint.x - 50.0f, guiPoint.y - 40, 100, 20);

            GUI.Box(rect, "WayPoint: " + (i + 1));
        }
        Handles.EndGUI(); 
    }

    #endregion

    #region Private Method

    private Transform[] GetWayPointsArray()
    {
        int arrayCount = m_Object.FindProperty(_wayPointsArraySize).intValue;

        Transform[] transfromArray = new Transform[arrayCount];

        for (int i = 0; i < arrayCount; i++)
        {
            transfromArray[i] = m_Object.FindProperty(string.Format(_wayPointArrayData, i)).objectReferenceValue as Transform;
        }

        return transfromArray;
    }

    private void SetWayPoint(int index, Transform WayPoint)
    {
        m_Object.FindProperty(string.Format(_wayPointArrayData, index)).objectReferenceValue = WayPoint;
    }

    private Transform GetWayPointAtIndex(int index)
    {
        return m_Object.FindProperty(string.Format(_wayPointArrayData, index)).objectReferenceValue as Transform;
    }

    private void RemoveWayPointAtIndex(int index)
    {
        Undo.RegisterFullObjectHierarchyUndo(GetWayPointAtIndex(index).parent, "RemoveWayPoint");
        Undo.DestroyObjectImmediate(GetWayPointAtIndex(index).gameObject);

        for (int i = 0; i < m_WayPointsCount.intValue - 1; i++)
            SetWayPoint(i, GetWayPointAtIndex(i + 1));

        m_WayPointsCount.intValue--;
    }

    private void AddWayPointAtIndex(int index)
    {
        m_WayPointsCount.intValue++;

        for(int i=m_WayPointsCount.intValue-1; i>index; i--)
        {
            SetWayPoint(i, GetWayPointAtIndex(i - 1));
        }

        GameObject obj = new GameObject("WayPoint");
        Undo.RegisterCreatedObjectUndo(obj, "CreatedWayPoint");

        obj.transform.position = GetWayPointAtIndex(index).position;
        obj.transform.parent = GetWayPointAtIndex(index).parent;

        SetWayPoint(index + 1, obj.transform);

        Selection.activeGameObject = obj;
    }

    #endregion

}
