﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GridManager))]
public class GridManagerInspector : Editor
{
    public GridManager Script;

    public int mask = (1 << LayerMask.NameToLayer("WorldLimit"));

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Script = (GridManager)target;

        EditorGUILayout.Space();

        if (GUILayout.Button("Generate Grid"))
        {
            int numberGrid = 0;
            if(Script.transform.childCount > 0)
            {
                string[] gridName = Script.transform.GetChild(Script.transform.childCount - 1).name.Split(' ');
                numberGrid = int.Parse(gridName[1]) + 1;
            }

            for(int i=0; i<Script.Height; i++)
            {
                for(int j=0; j<Script.Width; j++)
                {
                    GameObject obj = PrefabUtility.InstantiatePrefab(Script.GridPrefab) as GameObject;

                    Undo.RegisterCreatedObjectUndo(obj, "GridGeneration");

                    obj.transform.position = new Vector3(Script.transform.position.x + Script.GridSize / 2,
                                                         Script.transform.position.y,
                                                         Script.transform.position.z - Script.GridSize / 2)
                                           - new Vector3(j * -Script.GridSize - j * Script.offsetX, 0, i * Script.GridSize + i * Script.offsetY);
                    obj.transform.localScale = new Vector3(Script.GridSize, 0.01f, Script.GridSize);
                    obj.transform.parent = Script.transform;
                    obj.name = ("Grid " + numberGrid);
                    numberGrid++;
                }
            }

            foreach (Transform child in Script.transform)
            {
                child.parent = child.parent;
            }
        }

        if (GUILayout.Button("Place to Ground"))
        {
            foreach (Transform trans in Script.transform)
            {
                Undo.RecordObject(trans, "PlaceGround");

                Ray ray = new Ray(trans.position, -Script.transform.up);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100, mask))
                {
                    trans.position = new Vector3(trans.position.x, hit.point.y, trans.position.z);
                }
            }
        }

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Check Height"))
        {
            if (Script.GridHeight <= 0)
            {
                Debug.Log("gridHeight has no value to check");
                return;
            }

            Undo.RecordObject(Script, "CheckHeight");

            foreach (Transform trans in Script.transform)
            {
                Undo.RecordObject(trans.GetComponent<Renderer>(), "CheckHeight");

                Ray ray = new Ray(trans.position + new Vector3(0, 0.1f, 0), -trans.up);
                RaycastHit hit;

                if (!Physics.Raycast(ray, out hit, Script.GridHeight, this.mask))
                {
                    trans.GetComponent<Renderer>().material = Script.GridFullMat;

                    if (!Script.GridList.Contains(trans.name))
                        Script.GridList.Add(trans.name);
                }
                else
                {
                    trans.GetComponent<Renderer>().material = Script.GridFreeMat;
                    if (Script.GridList.Contains(trans.name))
                        Script.GridList.Remove(trans.name);
                }
            }
        }

        if (GUILayout.Button("Clear Height"))
        {
            Undo.RecordObject(Script, "ClearHeight");

            Script.GridList.Clear();

            foreach (Transform trans in Script.transform)
            {
                Undo.RecordObject(trans.GetComponent<Renderer>(), "ClearHeight");
                trans.GetComponent<Renderer>().material = Script.GridFreeMat;
            }
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        if (GUILayout.Button("Delete all Grids"))
        {
            Undo.RecordObject(Script, "DeleteGrids");

            if (EditorUtility.DisplayDialog("Delete all Grids?",
                "Are you sure you want to delete all grids placed\nin your scene?", "Delete", "Cancel"))
            {
                Script.GridList.Clear();

                while (Script.transform.childCount > 0)
                {
                    foreach (Transform trans in Script.transform)
                    {
                        //destroy grid in editor mode
                        Undo.DestroyObjectImmediate(trans.gameObject);
                    }
                }
            }
        }

        if (Script.GridHeight <= 0) 
            return;

        foreach (Transform trans in Script.transform)
        {
            Ray ray = new Ray(trans.position + new Vector3(0, 0.1f, 0), -trans.up);
            RaycastHit hit; 

            if (Physics.Raycast(ray, out hit, Script.GridHeight, mask))
            {
                Debug.DrawLine(ray.origin, hit.point, Color.yellow);
            }
            else
            {
                Debug.DrawRay(ray.origin, Script.GridHeight * -trans.up, Color.red);
            }
        }
    }

}
