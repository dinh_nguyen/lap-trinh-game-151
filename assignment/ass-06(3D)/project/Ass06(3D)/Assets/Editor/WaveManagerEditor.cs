﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Friends_Tower
{
    public class WaveManagerEditor : EditorWindow
    {

        #region Public Field

        public WaveManager WaveScript;

        public Vector2 ScrollPos;

        public static WaveManagerEditor waveEditor;

        public bool AutoRepaintOnSceneChange;

        #endregion

        [MenuItem("Chida/Tower Defend/Wave Setting")]
        public static void Init()
        {
            waveEditor = (WaveManagerEditor)EditorWindow.GetWindowWithRect(typeof(WaveManagerEditor),
                                                                           new Rect(0, 0, 800, 400),
                                                                           false,
                                                                           "Wave Setting");
            waveEditor.AutoRepaintOnSceneChange = false;
        }

        public void OnGUI()
        {
            GameObject waveObj = GameObject.FindGameObjectWithTag(UnityConstants.Tags.WaveManager);

            if (waveObj == null)
            {
                Debug.LogError("No Wave Manager");
                waveEditor.Close();
                return;
            }

            WaveScript = waveObj.GetComponent<WaveManager>();

            if (WaveScript == null)
            {
                Debug.LogError("No Wave Manager Script");
                waveEditor.Close();
                return;
            }

            bool waveChanged = false;

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Wave"))
            {
                Undo.RecordObject(WaveScript, "AddWave");

                WaveOption newWave = new WaveOption();
                newWave.EnemyPrefabs.Add(null);
                newWave.EnemyHP.Add(0);
                newWave.EnemyShield.Add(0);
                newWave.EnemyCount.Add(1);
                newWave.StartDelayMin.Add(0);
                newWave.StartDelayMax.Add(0);
                newWave.DelayBetweenMin.Add(0);
                newWave.DelayBetweenMax.Add(0);
                newWave.Path.Add(null);

                WaveScript.Options.Add(newWave);

                waveChanged = true;
            }

            if (GUILayout.Button("Delete All Wave"))
            {
                Undo.RecordObject(WaveScript, "DeleteWaves");
                WaveScript.Options.Clear();
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos, GUILayout.Height(370));

            EditorGUILayout.BeginHorizontal(); GUILayout.Label("No. |", EditorStyles.boldLabel, GUILayout.Width(40));
            GUILayout.Label("      Prefab      ", EditorStyles.boldLabel, GUILayout.Width(100));
            GUILayout.Label("| Overwrite | Overwrite", EditorStyles.boldLabel, GUILayout.Width(155));
            GUILayout.Label("|  Count  |", EditorStyles.boldLabel, GUILayout.Width(70));
            GUILayout.Label(" Spawn-Delay  |", EditorStyles.boldLabel, GUILayout.Width(110));
            GUILayout.Label("Delay-Between |", EditorStyles.boldLabel, GUILayout.Width(115));
            GUILayout.Label("      Path      ", EditorStyles.boldLabel, GUILayout.Width(90));
            GUILayout.Label("| DELETE", EditorStyles.boldLabel, GUILayout.Width(60));

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(170);
            GUILayout.Label("Health", EditorStyles.boldLabel, GUILayout.Width(75));
            GUILayout.Label("Shield", EditorStyles.boldLabel, GUILayout.Width(80));
            GUILayout.Space(70);
            GUILayout.Label("From-To", EditorStyles.boldLabel, GUILayout.Width(110));
            GUILayout.Label("From-To", EditorStyles.boldLabel, GUILayout.Width(125));

            EditorGUILayout.EndHorizontal();

            if (WaveScript.Options.Count == 0)
            {
                EditorGUILayout.EndScrollView();
                return;
            }

            EditorGUILayout.Space();

            for (int i = 0; i < WaveScript.Options.Count; i++)
            {
                WaveOption option = WaveScript.Options[i];

                EditorGUILayout.BeginHorizontal();

                GUILayout.Label((i + 1) + "", GUILayout.Width(90));
                GUILayout.Space(580);

                if (GUILayout.Button("X"))
                {
                    Undo.RecordObject(WaveScript, "DeleteWave");

                    WaveScript.Options.RemoveAt(i);

                    TrackChange(true);

                    return;
                }

                EditorGUILayout.EndHorizontal();

                for (int j = 0; j < option.EnemyPrefabs.Count; j++)
                {
                    if (WaveScript.StartOption == WaveStartOption.Interval)
                    {
                        float finalDelay = option.StartDelayMax[j] + (option.EnemyCount[j] - 1) * option.DelayBetweenMax[j];
                        float allowedDelay = WaveScript.SecondBetweenWaves + i * WaveScript.SecondIncrement;

                        if (finalDelay > allowedDelay && option.EnemyPrefabs[i] != null)
                        {
                            Debug.LogError("Delay of Enemy " + option.EnemyPrefabs[j].name + ", Wave " +
                                             (i + 1) + " exceeds Delay between Waves");
                        }
                    }

                    for (int h = option.EnemyHP.Count; h < option.EnemyPrefabs.Count; h++)
                        WaveScript.Options[i].EnemyHP.Add(0);
                    for (int h = option.EnemyShield.Count; h < option.EnemyPrefabs.Count; h++)
                        WaveScript.Options[i].EnemyShield.Add(0);

                    EditorGUILayout.BeginHorizontal();

                    GUILayout.Space(40);

                    WaveScript.Options[i].EnemyPrefabs[j] = (GameObject)EditorGUILayout.ObjectField(WaveScript.Options[i].EnemyPrefabs[j], typeof(GameObject), false, GUILayout.Width(115));
                    GUILayout.Space(15);

                    WaveScript.Options[i].EnemyHP[j] = EditorGUILayout.FloatField(option.EnemyHP[j], GUILayout.Width(50));
                    GUILayout.Space(20);

                    WaveScript.Options[i].EnemyShield[j] = EditorGUILayout.FloatField(option.EnemyShield[j], GUILayout.Width(50));
                    GUILayout.Space(25);

                    WaveScript.Options[i].EnemyCount[j] = EditorGUILayout.IntField(option.EnemyCount[j], GUILayout.Width(40));
                    GUILayout.Space(35);

                    WaveScript.Options[i].StartDelayMin[j] = EditorGUILayout.FloatField(option.StartDelayMin[j], GUILayout.Width(25));
                    WaveScript.Options[i].StartDelayMax[j] = EditorGUILayout.FloatField(option.StartDelayMax[j], GUILayout.Width(25));
                    GUILayout.Space(55);

                    WaveScript.Options[i].DelayBetweenMin[j] = EditorGUILayout.FloatField(option.DelayBetweenMin[j], GUILayout.Width(25));
                    WaveScript.Options[i].DelayBetweenMax[j] = EditorGUILayout.FloatField(option.DelayBetweenMax[j], GUILayout.Width(25));
                    GUILayout.Space(35);

                    WaveScript.Options[i].Path[j] = (PathManager)EditorGUILayout.ObjectField(option.Path[j], typeof(PathManager), true, GUILayout.Width(105));

                    if (GUILayout.Button("X"))
                    {
                        Undo.RecordObject(WaveScript, "DeleteRow");

                        WaveScript.Options[i].EnemyPrefabs.RemoveAt(j);
                        WaveScript.Options[i].EnemyHP.RemoveAt(j);
                        WaveScript.Options[i].EnemyShield.RemoveAt(j);
                        WaveScript.Options[i].EnemyCount.RemoveAt(j);
                        WaveScript.Options[i].StartDelayMin.RemoveAt(j);
                        WaveScript.Options[i].StartDelayMax.RemoveAt(j);
                        WaveScript.Options[i].DelayBetweenMin.RemoveAt(j);
                        WaveScript.Options[i].DelayBetweenMax.RemoveAt(j);
                        WaveScript.Options[i].Path.RemoveAt(j);

                        if (WaveScript.Options[i].EnemyPrefabs.Count == 0)
                            WaveScript.Options.RemoveAt(i);

                        TrackChange(true);

                        return;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.Space();

                if (GUILayout.Button("Add Enemy"))
                {
                    Undo.RecordObject(WaveScript, "AddRow");

                    WaveScript.Options[i].EnemyPrefabs.Add(null);
                    WaveScript.Options[i].EnemyHP.Add(0);
                    WaveScript.Options[i].EnemyShield.Add(0);
                    WaveScript.Options[i].EnemyCount.Add(1);
                    WaveScript.Options[i].StartDelayMin.Add(0);
                    WaveScript.Options[i].StartDelayMax.Add(0);
                    WaveScript.Options[i].DelayBetweenMin.Add(0);
                    WaveScript.Options[i].DelayBetweenMax.Add(0);
                    WaveScript.Options[i].Path.Add(null);
                    //set wave manipulation variable to true
                    waveChanged = true;
                }

                if (i < (WaveScript.Options.Count - 1) && GUILayout.Button("Insert Wave"))
                {
                    Undo.RecordObject(WaveScript, "AddWave");

                    WaveOption newWave = new WaveOption();
                    //initialize first row / creep of the new wave
                    newWave.EnemyPrefabs.Add(null);
                    newWave.EnemyHP.Add(0);
                    newWave.EnemyShield.Add(0);
                    newWave.EnemyCount.Add(1);
                    newWave.StartDelayMin.Add(0);
                    newWave.StartDelayMax.Add(0);
                    newWave.DelayBetweenMin.Add(0);
                    newWave.DelayBetweenMax.Add(0);
                    newWave.Path.Add(null);
                    //insert new wave option to wave list
                    //waveScript.options.Add(newWave);
                    WaveScript.Options.Insert(i + 1, newWave);
                    //set wave manipulation variable to true
                    waveChanged = true;
                }

                EditorGUILayout.Space();

                EditorGUILayout.EndHorizontal();
            }


            EditorGUILayout.EndScrollView();

            TrackChange(waveChanged);

        }

        public void TrackChange(bool waveChange)
        {
            if (GUI.changed || waveChange)
            {
                EditorUtility.SetDirty(WaveScript);
                Repaint();
            }
        }
    }
}