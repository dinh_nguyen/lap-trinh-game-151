Tên game: DeZombie
Thể loại: Tower Defense
Môi trường: Unity (phiên bản 5.3.f1 Personal)

Đối với project:
- Trong màn hình unity (sau khi đã import project) nếu không thấy các object trong Hierachy thì chọn 
Assert -> Scenes, mở file Game.unitys

Hướng dẫn:
-  Bấm các nút mũi tên lên, xuống, trái, phải để di chuyển camera (hoặc W, S, A, D).
-  Chuột phải để thay đổi góc nhìn 3D.
-  Ấn nút Esc trên bàn phím để tạm dừng (pause) (Chỉ sử dụng được trong Window Mode - Fullscreen tạm thời bị bug).